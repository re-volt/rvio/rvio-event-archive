import markdown

class EventPage(): 

    def __init__(self, yaml_data):
        self.yaml = yaml_data

    def render(self):

        title = self.yaml["title"] if "title" in self.yaml else "[no data]"
        date = self.yaml["date"] if "date" in self.yaml else "[no data]"
        category = self.yaml["event"]["category"] if "category" in self.yaml["event"] else "[no data]"
        host = self.yaml["event"]["host"] if "host" in self.yaml["event"] else "[no data]"
        host2 = self.yaml["event"]["host2"] if "host2" in self.yaml["event"] else "[no data]"
        mode = self.yaml["event"]["mode"] if "mode" in self.yaml["event"] else "[no data]"
        stream = self.yaml["event"]["stream"] if "stream" in self.yaml["event"] else "[no data]"
        vod = self.yaml["event"]["vod"] if "vod" in self.yaml["event"] else "[no data]"
        results = self.yaml["event"]["results"] if "results" in self.yaml["event"] else "[no data]"
        packages = self.yaml["event"]["packages"] if "packages" in self.yaml["event"] else "[no data]"
        class_ = self.yaml["event"]["class"] if "class" in self.yaml["event"] else "[no data]"
        otherclass = self.yaml["event"]["otherclass"] if "otherclass" in self.yaml["event"] else "[no data]"
        classlink = self.yaml["event"]["classlink"] if "classlink" in self.yaml["event"] else "[no data]"
        tracklist = "<br>".join(self.yaml["event"]["tracklist"].split("\n")) if "tracklist" in self.yaml["event"] else "[no data]"


        html = f"""
        <header>
            <h1>Re-Volt I/O Event Archive - {title}</h1>
            <nav>
                    <a href="../../index.html">Back</a>
            </nav>
        </header>
        <div class="event">
            <h2>{title}</h2>
            <p>Date: {date}</p>
            <p>Host: {host}</p>
            <p>Host 2: {host2}</p>
            <p>Category: {category}</p>
            <p>Mode: {mode}</p>
            <p>Class: {class_}</p>
            <p>Other Class: {otherclass}</p>
            <p>Class Link: {classlink}</p>
            <p>Stream: {stream}</p>
            <p>vod: {vod}</p>
            <p>Results: {results}</p>
            <p>Required Packs: {packages}</p>
            <p>Tracklist:<br>{tracklist}</p>
        </div>
        <hr>
        <div class="content">
        {markdown.markdown(self.yaml["md"])}
        </div>

        """

        return html