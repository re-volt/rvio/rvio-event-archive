import os
import yaml
from event_page import EventPage

events_path = "../events/"

event_folders = os.listdir(events_path)

os.system("cp -r ../events ../public/events/")

events = []

def generate_html_page(title, body):
    return f"""
    <!DOCTYPE html>
    <html>
        <head>
            <title>{title}</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="https://re-volt.gitlab.io/rvio/rvio-event-archive/new.min.css">
        </head>
        <body>
            {body}
        </body>
    </html>
    """

event_folders.sort()

for event in event_folders:
    print(f"  /events/{event}: https://re-volt.gitlab.io/rvio/rvio-event-archive/events/{event}")

for event in event_folders:
    with open(os.path.join(events_path, event, "event.md"), "r") as f:

        file_data = f.readlines()

        dashes = 0

        yml = []
        md = []

        for line in file_data:
            if line == "---\n":
                dashes += 1
                continue
            
            if dashes == 1:
                yml.append(line)
            elif dashes == 2:
                md.append(line)


        yml = "\n".join(yml)
        md = "\n".join(md)

        event_data = yaml.safe_load(yml)
        event_data["slug"] = event
        event_data["md"] = md

        events.append(event_data)

link_list = []

link_list_years = {}

# events.sort(key=lambda x: x["slug"], reverse=True)

for event in events:
    event_page = EventPage(event)
    body = event_page.render()
    slug = event["slug"]
    title = event["title"] if "title" in event else "No title"
    date = event["date"] if "date" in event else "No date"
    host = event["event"]["host"] if "host" in event["event"] else "No host"
    class_ = event["event"]["class"] if "class" in event["event"] else "No class"
    # os.mkdir(f"../public/events/{slug}")
    with open(f"../public/events/{slug}/index.html", "w") as f:
        f.write(generate_html_page(title, body))


    entry = f"""
<tr>
    <td>
        {date}
    </td>
    <td>
        <a href="events/{slug}/index.html">{title}</a>
    </td>
    <td>
        {host}
    </td>
    <td>
        {class_}
    </td>
</tr>
"""


    link_list.append(entry)

    year = event["slug"][0:4]

    if not year in link_list_years:
        link_list_years[year] = []

    link_list_years[year].append(entry)


link_list = "\n".join(link_list)


def generate_index_body(link_list):
    year_links = " | ".join([f"""<a href="{year}.html">{year}</a>""" for year in link_list_years])
    body = f"""
<header>
    <h1>Re-Volt I/O Event Archive</h1>
    <br>
    <nav>
            <a href="https://gitlab.com/re-volt/rvio/rvio-event-archive/">GitLab Repository</a> | 
            <a href="https://re-volt.io">Re-Volt I/O</a>
            <br>
            <br>
            <a href="index.html">All</a> | {year_links}
    </nav>
</header>
<p>For performance reasons, all events are archived here.</p>
<table>
    <tr>
        <th>Date</th>
        <th>Title</th>
        <th>Host</th>
        <th>Class</th>
    </tr>
    {link_list}
</table>
    """
    return body


with open(f"../public/index.html", "w") as f:
    f.write(generate_html_page("RVIO Event Archive", generate_index_body(link_list)))


for year in link_list_years:
    with open(f"../public/{year}.html", "w") as f:
        f.write(generate_html_page("RVIO Event Archive", generate_index_body("\n".join(link_list_years[year]))))

