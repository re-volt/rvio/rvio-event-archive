---
title: 'Random Semi-Pros'
titleoverride: false
date: '27-03-2019 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: 'Random Tracks'
published: true
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
article:
    datePublished: '19-03-2019 20:55'
    dateModified: '19-03-2019 20:55'
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Visit **[RVGL Ladder](https://ladder.rv.gl/)** for more information!

**Links**:
* [Track Selection](https://ladder.rv.gl/info/#events-general-tracks)
* [Car Selection](https://ladder.rv.gl/info/#events-general-cars)
* [Ranking](https://ladder.rv.gl/info/#events-general-ranking)