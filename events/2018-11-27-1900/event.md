---
title: 'Pro Slugs'
titleoverride: false
date: '27-11-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: prugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Botanical Garden\r\nPenny Racers - Harbour\r\nGhost Town 2\r\nToy World 1 (R)\r\nToy World Mayhem\r\nIndustry\r\nHelios \r\nSpa-Volt 1\r\nRooftop Chase\r\nCliffside\r\nFool's Mate 2\r\nGrisville (R)\r\nIllusion\r\nVenice (R)\r\nRooftops"
    vod: 'https://www.youtube.com/watch?v=PCdTDhAr1Yo'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-27_20-03-25.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

