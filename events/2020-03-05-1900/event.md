---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '05-03-2020 19:00'
event:
    host: 'OhNej & Skarma'
    ip: 'ohnej.rv.gl OR sk.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Penny Racers - Harbour — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nVenice (R) — 3 laps\r\nToys in the Hood 2 — 3 laps\r\nToytanic 1 — 3 laps\r\nSpa-Volt 2 — 3 laps\r\nRooftops (R) — 3 laps\r\nToy World 2 — 4 laps\r\nRoute-77 — 3 laps\r\nSnowy River — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nHelios — 3 laps\r\nBiohazard Factory (R) — 2 laps\r\nRadioactive Garden (M) — 5 laps"
    vod: 'https://www.youtube.com/watch?v=OZHWldezdIk'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

