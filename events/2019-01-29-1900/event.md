---
title: 'Semi-Pro Races'
titleoverride: false
date: '29-01-2019 19:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 2\r\nSkating Toys\r\nSpa-Volt 1\r\nFool's Mate 2\r\nHelios\r\nToytanic 2 (R)\r\nKadish Sprint\r\nToySoldierz\r\nRooftops\r\nToy World Mayhem (R)\r\nCactus-Volt\r\nMetro-Volt\r\nPenny Racers - Harbour (R)\r\nToy World 2\r\nCliffside"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

