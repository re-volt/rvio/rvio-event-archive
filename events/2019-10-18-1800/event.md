---
title: 'Circuit Races (Elimination)'
titleoverride: false
date: '18-10-2019 18:00'
event:
    host: Saffron
    ip: saff.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Real Cars (Elimination)'
    classlink: 'https://files.re-volt.io/packs/rlcars.zip'
    tracklist: "Touge Mountain\r\nDaybreak Raceway R\r\nLS Tama Valley Route C\r\nRed Rock Valley\r\nAMCO Bitume\r\nBerm Lake Raceway\r\nClubman Stage Route 5 R\r\nAMCO Driftume\r\nSideways Sanctuary A\r\nTVGP Bitume\r\nLS Kart Space I\r\nStagnaro\r\nRV-Simo On-Road Track\r\nEmerald Pastures Raceway\r\nTouge Mountain R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

### Requirements
* [Circuit Tracks](https://files.re-volt.io/packs/circuit_tracks.zip)
* [Real Cars](https://files.re-volt.io/packs/rlcars.zip)

### Car Selection
* Alfa Romeo GT
* Audi TT Quattro
* BMW M3 E30
* BMW Z4 E85
* Buick Regal GNX
* Chevy Camaro SS
* Chevy Cobalt SS
* Dodge Challenger SRT-8
* Dodge Charger R/T
* Dodge SRT-4 ACR
* Ford Focus RS
* Ford Mustang Fastback
* Honda Integra Type-R
* Honda S2000
* Lotus Elise
* Mazda RX-7
* Mazda RX-8
* Mercedes 190E Evo II
* Mitsubishi GTO
* Nissan 350Z
* Nissan GT-R R32
* Peugeot RCZ R
* Porsche Boxster S
* Subaru Impreza
* Toyota MR2
* Toyota Supra
* Toyota Trueno AE86
* VW Golf R32