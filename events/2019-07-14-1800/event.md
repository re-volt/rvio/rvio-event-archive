---
title: 'Battle Tag'
titleoverride: false
date: '14-07-2019 18:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: battle-tag
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Supermarket Battle\r\nParty in the Toy World\r\nBlock Fort\r\nMuseum Battle\r\nGarden Battle\r\nToy World Battle\r\nNeighbourhood Battle"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

We will be using [Pro Cars](https://re-volt.io/online/car-classes).