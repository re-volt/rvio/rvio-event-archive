---
title: 'Super Pro Races'
titleoverride: true
date: '16-08-2020 18:00'
event:
    host: Frosttbitten
    ip: frost.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Sakura — 3 laps\r\nHelios — 3 laps\r\nWildland — 3 laps\r\nToy World Mayhem (R) — 4 laps\r\nVenice — 4 laps\r\nToy World 2 (R) — 4 laps\r\nMuseum 2 — 3 laps\r\nToytanic 1 — 3 laps\r\nWhite Rose Chapel — 3 laps\r\nRooftop Chase: Redux (R) — 3 laps\r\nSantorini — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

