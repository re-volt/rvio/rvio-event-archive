---
title: Hell-Volt
titleoverride: true
date: '27-07-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Secret cars'
    classlink: 'https://re-volt.io/online/cars'
    packages:
        - io_cars
        - io_tracks
    tracklist: 'Secret tracks'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: Hell-Volt
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Premise of the session is simple. The host will make use of the JOKER code to make everyone play the same car, and the track list and cars chosen are made in such a manner that it will make the races hell to drive.
Because of the nature of the session, the track list and the cars will be kept as a secret, and just so you know, we will be playing some mirrored and/or reversed tracks.

- Pickups will be **ON**
- Difficulty will be **Simulation**

You will be ashes and dust by the end of this session, with the only consolation being that you were suffering together.