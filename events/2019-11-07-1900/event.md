---
title: 'Rookie Races'
titleoverride: true
date: '07-11-2019 19:00'
event:
    host: Skarma
    ip: sk.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Industry\r\nVenice (R)\r\nMetro-Volt\r\nToys in the Hood 2\r\nToytanic 1 (R)\r\nLunar\r\nThe Bunker\r\nFool's Mate 2\r\nGhost Town 1\r\nToy World 2\r\nSpa-Volt 1\r\nDonut Plains 3 (R)\r\nAMCO TT (M)\r\nOverground"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

