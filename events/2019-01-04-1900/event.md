---
title: 'Drift Races'
titleoverride: false
date: '04-01-2019 19:00'
event:
    host: Saffron
    ip: 'announced on Discord'
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://files.re-volt.io/packs/drift_cars.7z'
    tracklist: "AMCO Bitume\r\nSideways Sanctuary C (R)\r\nDrivers School\r\nSideways Sanctuary B\r\nNamco Trains\r\nAMCO Driftume\r\nAMCO Bitume (R)\r\nNamco Racer\r\nSideways Sanctuary C\r\nRV-Simo On-Road Track\r\nAMCO Driftume R\r\nSideways Sanctuary B (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only **D1GP** or **D2GP** cars are allowed.

**Requirements**
* [Drift Cars](https://files.re-volt.io/packs/drift_cars.7z)
* [Drift Tracks](https://files.re-volt.io/packs/drift_tracks.7z)