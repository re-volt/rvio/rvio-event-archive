---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '26-03-2020 19:00'
event:
    host: 'Saffron & Kirioso'
    ip: 'saff.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Venice – 4 laps\r\nPetroVolt (R) – 2 laps\r\nBiohazard Factory – 3 laps\r\nGhost Town 2 – 5 laps\r\nRV Temple – 2 laps\r\nRooftops – 4 laps\r\nThe Felling Yard – 3 laps\r\nSakura – 3 laps\r\nSchool's Out – 2 laps\r\nRanch – 2 laps\r\nToys in the Hood 2 – 4 laps\r\nSwan Street (M) – 3 laps\r\nToytanic 1 (R) – 3 laps\r\nGrisville (R) – 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

