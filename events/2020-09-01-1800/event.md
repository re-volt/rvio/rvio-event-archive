---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '01-09-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Museum 2 — 4 laps\r\nToy World 2 — 5 laps\r\nSupermarket 1 — 4 laps\r\nSmashride Circuit — 3 laps\r\nSpa-Volt 1 — 3 laps\r\nMuseum 3 — 4 laps\r\nSnowy River (R) — 4 laps\r\nMedieval: Redux — 5 laps\r\nMuseum 1 (R) — 3 laps\r\nVenice (R) — 4 laps\r\nPetroVolt — 2 laps\r\nWhite Rose Chapel — 3 laps\r\nMeltdown — 5 laps\r\nMetro-Volt — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

