---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '21-12-2019 19:00'
event:
    host: 'Kirioso OR Delecto'
    ip: 'kiri.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "White Rose Chapel\r\nHelios (M)\r\nPenny Racers - Harbour\r\nRoute-77\r\nSupermarket 1\r\nMoon Dawn\r\nSpa-Volt 2 (R)\r\nHull Breach 3000\r\nToytanic 2 (R)\r\nDonut Plains 3 (R)\r\nBlood on the Rooftops\r\nSpa-Volt 1\r\nMuseum 1\r\nRV Temple\r\nSupermarket 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

