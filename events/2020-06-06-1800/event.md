---
title: 'Super Pro Races'
titleoverride: true
date: '06-06-2020 18:00'
event:
    host: Etneus
    ip: etn.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World 2 — 5 laps\r\nSwan Street — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nRoute-77 — 4 laps\r\nPetroVolt (R) — 2 laps\r\nGhost Town 1 (R) — 7 laps\r\nGhost Town 2 — 5 laps\r\nImages of Giza: Redux — 5 laps\r\nHoliday Camp: California Edition — 2 laps\r\nMoon Dawn (R) — 2 laps\r\nSantorini — 4 laps\r\nToy World 1 — 6 laps\r\nMetro-Volt — 3 laps\r\nHull Breach 3000 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

