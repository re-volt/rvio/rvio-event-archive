---
title: 'Amateur Races <small>(Triple Lobby)</small>'
titleoverride: true
date: '16-04-2020 18:00'
event:
    host: 'OhNej & Narusori & Delecto'
    ip: 'ohnej.rv.gl OR naru.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Route-77 —  3 laps\r\nSupermarket 2 —  7 laps\r\nHoliday Camp: California Edition —  2 laps\r\nToytanic 1 —  3 laps\r\nSwan Street — 3 laps\r\nVenice — 3 laps\r\nCliffside —  5 laps\r\nOverground —  2 laps\r\nSantorini (R) —  4 laps\r\nSakura —  2 laps\r\nQuake! —  2 laps\r\nToys in the Hood 1 (M) —  3 laps\r\nCake —  4 laps\r\nToy World 2 (R) —  4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

