---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '24-03-2020 19:00'
event:
    host: 'Saffron & URV'
    ip: 'saff.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Fool's Mate 2 — 4 laps\r\nToy World 2 — 5 laps\r\nSantorini — 4 laps\r\nMetro-Volt — 3 laps\r\nSakura (R) — 3 laps\r\nCliffside — 6 laps\r\nToySoldierz (M) — 4 laps\r\nGhost Town 1 — 6 laps\r\nPenny Racers - Caves — 5 laps\r\nSpa-Volt 2 — 3 laps\r\nToy World Mayhem — 4 laps\r\nSupermarket 2 (R) — 8 laps\r\nMolten Caverns (R) — 2 laps\r\nToytanic 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

