---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '09-04-2020 18:00'
event:
    host: 'Narusori & OhNej'
    ip: 'sas.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Toy World 2 — 4 laps\r\nRooftop Chase: Redux — 3 laps\r\nToytanic 1 — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nBotanical Garden (R) — 6 laps\r\nBlood on the Rooftops — 3 laps\r\nSchool's Out — 2 laps\r\nSantorini — 4 laps\r\nToy World Mayhem (R) (M) — 4 laps\r\nSakura — 2 laps\r\nSkating Toys — 2 laps\r\nMuseum 2 — 4 laps\r\nPenny Racers - Harbour (R) — 3 laps\r\nOverground — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

