---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '15-08-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "AMCO TT — 3 laps\r\nQuake! — 2 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nGhost Town 2 (R) — 4 laps\r\nMolten Caverns (R) — 2 laps\r\nSantorini — 4 laps\r\nToy World 1 — 5 laps\r\nThe Bunker — 4 laps\r\nRooftops — 3 laps\r\nMuseum 1 — 2 laps\r\nSpa-Volt 1 — 3 laps\r\nRooftop Chase: Redux — 3 laps\r\nWildland — 3 laps\r\nPetroVolt — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

