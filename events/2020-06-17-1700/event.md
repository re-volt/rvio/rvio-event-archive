---
title: 'Long Journey 4: Day 5'
titleoverride: true
date: '17-06-2020 17:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_cars_bonus
        - io_skins
        - io_tracks
        - io_tracks_bonus
    tracklist: "The Keep\r\nDonut Plains 3\r\nVenice\r\nRooftops 2\r\nCliffside\r\nTerminus\r\nMedieval Mayhem\r\nRooftops\r\nToySoldierz\r\nIndustry\r\nToy World 2\r\nFukushima\r\nHull Breach 3000\r\nMid-sea Island\r\nAMCO TT\r\nChristmas Special Stage\r\nSuperMarket 1\r\nCactus Volt\r\nQuake!\r\nRanch\r\nFreestoyle 2"
media_order: LongJourney4.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Long Journey 4: Day 5'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](https://re-volt.io/user/pages/events/2020-06-17-1700/LongJourney4.png)