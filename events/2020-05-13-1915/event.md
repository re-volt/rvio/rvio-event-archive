---
title: 'Normal Amateur Session'
titleoverride: true
date: '13-05-2020 19:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Normal Amateur'
    classlink: 'https://re-volt.io/events/2020-05-13-1915'
    tracklist: "Botanical Garden (5 laps)\r\nToys in the Hood 1 RM (4 laps)\r\nFloating World (4 laps)\r\nHarbor Lights M (4 laps)\r\nToys in the Hood 2 M (4 laps)\r\nBiohazard Factory R (3 laps)\r\nSchools Out (2 laps)\r\nHallows Eve M (4 laps)\r\nSkating Toys M (3 laps)\r\nToy World 2 R (4 laps)\r\nPenny Racers Caves R (4 laps)\r\nDonut Plains 3 (4 laps)\r\nToy World 2 (4 laps)\r\nRoute 77 R (4 laps)\r\nGhost Town 2 R (4 laps)\r\nSpa Volt 1 R (4 laps)\r\nToys in the Hood 2 R (4 laps)\r\nRanch RM (3 laps)\r\nMuseum 2 RM (4 laps)\r\nMeltdown (4 laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Normal Amateur Session'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous avarage prestige results be sure to [check this](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).

### Requirements
* [Re-Volt Online track pack version 20.1](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1) (needed)
* [Re-Volt Online car pack version 20.1](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (advised)
* Seperate game installation required if you got I/O content on the game you want to use!

### Car Selection
* Any car that has been rated for amateur or lower on [this car list](https://www.dropbox.com/s/u71xh54lw48noxu/rvon%20car%20list%20%2820.0%29.txt?dl=0)