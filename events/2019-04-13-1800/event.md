---
title: 'Amateur Races'
titleoverride: false
date: '13-04-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 2 (R)\r\nSwan Street\r\nSpa-Volt 2\r\nThe Bunker\r\nToy World Mayhem\r\nAMCO TT\r\nToy World 2\r\nCliffside\r\nToys in the Hood 1\r\nJailhouse Rock\r\nDonut Plains 3 (R)\r\nSupermarket 2\r\nPenny Racers - Caves\r\nVenice (R)\r\nSantorini\r\nRV Temple"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

