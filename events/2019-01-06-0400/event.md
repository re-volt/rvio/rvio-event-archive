---
title: 'Amateur Races'
titleoverride: false
date: '06-01-2019 04:00'
event:
    host: L!LMexican
    ip: mex.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World Mayhem\r\nFool's Mate 2\r\nSakura\r\nRanch (R)\r\nSuperMarket 2\r\nSwan Street\r\nToy World 1\r\nPetroVolt\r\nGhost Town 1 (R)\r\nRooftop Chase\r\nGhost Town 2\r\nAMCO TT (R)\r\nYABBA DABBA DOO!\r\nOverground"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

