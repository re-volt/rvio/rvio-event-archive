---
title: 'Semi-Pro Races'
titleoverride: false
date: '15-06-2019 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "RV Temple\r\nRadioactive Garden\r\nThe Felling Yard\r\nRanch R\r\nMetro-Volt\r\nToy World 2\r\nSpa-Volt 2\r\nSantorini R\r\nBotanical Garden\r\nCliffside\r\nMolten Caverns\r\nSuperMarket 1\r\nIllusion\r\nVenice\r\nToytanic 2 M"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

