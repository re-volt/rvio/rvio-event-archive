---
title: 'Gold Cup'
titleoverride: false
date: '26-09-2018 18:00'
event:
    host: Whitedoom
    category: unofficial
    mode: race
    class: other
    otherclass: 'Gold Cup'
    classlink: 'https://www.dropbox.com/s/ab9t5cc7wkwm8je/Online%20cup%20data.txt?dl=0'
    tracklist: "Toys in the Hood 2\r\nToy World 1 (M)\r\nToytanic 1\r\nMuseum 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only 9 spots are available. To reserve a spot, contact Whitedoom on [Discord](https://re-volt.io/discord). The IP will be shared there in case any free spots are available when the event starts.

**Requirements:**
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip)

**Car Selection:**
* Crankenwagon
* Pest Control
* Hydrox
* Nice Black
* Nimiarc
* Riptor
* Varflame

[More information here](https://www.dropbox.com/s/ab9t5cc7wkwm8je/Online%20cup%20data.txt?dl=0).