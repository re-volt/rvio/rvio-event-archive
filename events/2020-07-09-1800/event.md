---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '09-07-2020 18:00'
event:
    host: 'URV & Ashen Forest'
    ip: 'u.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Snowy River — 3 laps\r\n    Metro-Volt — 3 laps\r\n    Hull Breach 3000 — 3 laps\r\n    Toytanic 1 — 3 laps\r\n    Museum 2 — 3 laps\r\n    Holiday Camp: California Edition — 2 laps\r\n    Toy World 2 — 4 laps\r\n    Toy World Aquatica: Redux — 3 laps\r\n    Images of Giza: Redux (R) — 4 laps\r\n    Cliffside — 5 laps\r\n    Toy World 1 (R) — 5 laps\r\n    The Bunker — 4 laps\r\n    Museum 3 — 3 laps\r\n    Quake! (R) — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

