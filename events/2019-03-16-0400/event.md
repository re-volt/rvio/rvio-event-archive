---
title: 'Pro Races'
titleoverride: false
date: '16-03-2019 04:00'
event:
    host: '[AR]Santi™'
    ip: santi.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the hood 1 (R)\r\nSakura\r\nToytanic 2\r\nPetrovolt (R)\r\nFreestoyle 2\r\nQuake!\r\nPalm Marsh\r\nSantorini\r\nMuseum 1\r\nThe Felling Yard\r\nThe Gorge\r\nRooftop Chase (R)\r\nSupermarket 2\r\nToy World Mayhem\r\nSwan Street\r\nSnowy River"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

