---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '27-08-2020 18:00'
event:
    host: 'Etneus & Ashen Forest'
    ip: 'etn.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Blood on the Rooftops — 3 laps\r\nMuseum 1 — 3 laps\r\nMuseum 3 — 3 laps\r\nMedieval: Redux — 4 laps\r\nToytanic 1 — 3 laps\r\nMetro-Volt — 3 laps\r\nRadioactive Garden (R) — 5 laps\r\nSupermarket 2 — 8 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nMeltdown — 4 laps\r\nToys in the Hood 2 (R) — 4 laps\r\nSakura — 2 laps\r\nAMCO TT — 3 laps\r\nHoliday Camp: California Edition — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

