---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '16-01-2020 19:00'
event:
    host: 'Kirioso & Skarma'
    ip: 'kiri.rv.gl OR sk.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Spa-Volt 1 — 3 laps\r\nToy World Mayhem (R) — 4 laps\r\nKadish Sprint — 2 laps\r\nThe Felling Yard — 3 laps\r\nHelios — 3 laps\r\nCliffside — 6 laps\r\nToytanic 2 — 3 laps\r\nSantorini (M) — 4 laps\r\nRadioactive Garden (R) — 5 laps\r\nToy World 2 — 5 laps\r\nToy World 1 (R) — 6 laps\r\nBlood on the Rooftops — 3 laps\r\nSupermarket 2 — 8 laps\r\nMetro-Volt — 3 laps"
    stream: 'https://www.twitch.tv/ohnej6'
    vod: 'https://www.youtube.com/watch?v=IyKpPdHtUdc'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

