---
title: 'Advanced Races'
titleoverride: false
date: '14-09-2019 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Penny Racers - Harbour\r\nRadioactive Garden\r\nToytanic 1 R\r\nSwan Street\r\nSpa-Volt 2\r\nToy World 2\r\nToy World Aquatica: Redux\r\nPetroVolt\r\nAMCO TT\r\nToys in the Hood 1\r\nQuake!\r\nWhite Rose Chapel M\r\nRanch R\r\nSuperMarket 1\r\nHull Breach 3000"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

