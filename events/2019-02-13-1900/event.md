---
title: 'Circuit Races'
titleoverride: false
date: '13-02-2019 19:00'
event:
    host: Saffron
    ip: 'announced on Discord'
    category: special
    mode: race
    class: other
    otherclass: 'Circuit Tracks with Pemto'
    classlink: 'https://re-volt.io/events/2019-13-02-1900'
    tracklist: "AMCO Bitume\r\nNamco Racer\r\nGP1\r\nDaybreak Raceway\r\nDrivers School\r\nRed Rock Valley\r\nTouge Drift\r\nHockenheimring\r\nGPX\r\nSpecial Stage Route 5"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Requirements**
* [Circuit Tracks](https://files.re-volt.io/packs/circuit_tracks.zip)
* [Pemto](http://revoltzone.net/cars/28297/Pemto) (also part of the [I/O car pack](https://distribute.re-volt.io/packs/io_cars.zip))
* [Pemto Pack Project](https://kiwis.rv.gl/files/ppp-181010.zip) (optional skins)