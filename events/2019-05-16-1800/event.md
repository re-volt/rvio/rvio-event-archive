---
title: 'Rookie Races'
titleoverride: false
date: '16-05-2019 18:00'
event:
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Botanical Garden (R)\r\nThe Bunker\r\nBiohazard Factory (R)\r\nRooftops DC\r\nSuperMarket 2\r\nGhost Town 2\r\nIllusion\r\nSpa-Volt 2\r\nSpa-Volt 1\r\nSkating Toys\r\nVenice (R)\r\nJailhouse Rock\r\nDonut Plains 3\r\nRadioactive Garden"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

