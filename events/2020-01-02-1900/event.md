---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '02-01-2020 19:00'
event:
    host: 'URV & OhNej'
    ip: 'u.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Toy World Mayhem — 4 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nToy World Aquatica: Redux (R) — 3 laps\r\nGrisville — 3 laps\r\nAMCO TT - 3 laps\r\nBlood on the Rooftops — 3 laps\r\nToySoldierz — 4 laps\r\nToys in the Hood 1 — 3 laps\r\nGhost Town 1 — 4 laps\r\nGhost Town 2 (R) — 4 laps\r\nSnowy River — 4 laps\r\nMetro-Volt (M) — 3 laps\r\nLunar — 3 laps\r\nSnowland 1 — 3 laps\r\nToy World 1 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

