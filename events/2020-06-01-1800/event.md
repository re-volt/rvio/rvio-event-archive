---
title: 'Challenging I/O Supers'
titleoverride: true
date: '01-06-2020 18:00'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/events/2020-06-01-1800'
    tracklist: "1. Toy World 2\r\n2. Spa-Volt 2\r\n3. Re-Ville\r\n4. ToySoldierz\r\n5. Museum 1\r\n6. Spa-Volt 1\r\n7. Overground\r\n8. Rooftop Chase: Redux (R)\r\n9. Biohazard Factory\r\n10. Toy World Mayhem"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Challenging I/O Supers'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

i will be hosting a Challenging I/O Supers on Monday, 1st June at 18 UTC.
we will be racing with Super Pro cars that have a real danger of instability, from I/O Main and I/O Bonus.
none of these cars are easy-stable-consistent-railers, these cars will react unexpectedly if left unattended.
cars used in this session will require high amounts of concentration, to keep their reactions in check.
we will be racing on difficult I/O Main tracks, for 4 laps, in Arcade mode, with pickups enabled.

----
cars:
- AU-8
- King Kaiju
- King Moloko
- La Rossa
- Napalm
- Rinne
- Saeger
- Selsia Turbo
- Taurus
- Tesseract
- Cherry Top
- Cougar Spec-S
- Formula Z
- Infernal
- Screamer
- Toyeca Evo-R MK2

I/O Main cars     : [https://distribute.re-volt.io/packs/io_cars.zip](https://distribute.re-volt.io/packs/io_cars.zip)

I/O Bonus cars   : [https://distribute.re-volt.io/packs/io_cars_bonus.zip](https://distribute.re-volt.io/packs/io_cars_bonus.zip)

I/O Main tracks : [https://distribute.re-volt.io/packs/io_tracks.zip](https://distribute.re-volt.io/packs/io_tracks.zip)