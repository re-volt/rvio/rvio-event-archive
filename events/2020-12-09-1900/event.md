---
title: 'Dynamic Dash'
titleoverride: true
date: '09-12-2020 19:00'
event:
    host: 'URV & OhNej'
    ip: 'u.rv.gl OR ohnej.rv.gl'
    category: tourney
    mode: race
    class: other
    otherclass: 'Dynamic Dash'
    classlink: 'https://re-volt.io/events/2020-12-09-1900'
    tracklist: "Warehouse Showdown — 8 laps\r\nAspiria — 3 laps\r\nQWorld — 3 laps\r\nSS Route Re-Volt — 3 laps\r\nPOD: Plant XXI — 2 laps\r\nToys in the Hood 1 — 3 laps\r\nWhite Rose Chapel — 3 laps\r\nCasino RV — 3 laps\r\nKadish Basic — 2 laps\r\nCryo-Volt 3 — 3 laps\r\nQuake! — 2 laps\r\nSuperMarket 1 — 4 laps\r\nIceatopia    — 4 laps\r\nMoon Dawn    — 2 laps"
media_order: rvcc20_poster1.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Dynamic Dash'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](rvcc20_poster1.png)

### Choose between the following cars
* Comet
* Golden Bell
* Speed Gnome
* Zerostar

### Requirements
* [CC20 Cars](https://files.re-volt.io/packs/christmas/2020/rvcc20_cars.7z)
* [CC20 Tracks](https://files.re-volt.io/packs/christmas/2020/rvcc20_tracks.7z)