---
title: 'Last Man Standing'
titleoverride: false
date: '28-07-2019 17:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: last-man-standing
    class: other
    otherclass: 'Modern Clockworks'
    classlink: 'https://re-volt.io/events/2019-07-28-1800'
    tracklist: "Rooftops\r\nTemple 2\r\nRoad of Simplicity\r\nToshinden Arena\r\nToshinden Mona\r\nChinatown"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

### Requirements
* [LMS & Tag Collection](https://distribute.re-volt.io/packs/io_lmstag.zip)
* [Modern Clockworks](https://distribute.re-volt.io/packs/io_clockworks_modern.zip)

The contents of those packs are listed [here](https://re-volt.io/online/other-content).