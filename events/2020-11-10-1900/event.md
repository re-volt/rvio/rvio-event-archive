---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '10-11-2020 19:00'
event:
    host: 'Ashen Forest & URV'
    ip: 'ash.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World Mayhem (R) — 3 laps\r\nRooftops (R) — 3 laps\r\nThe Felling Yard — 2 laps\r\nStadVolt — 3 laps\r\nToytanic 2 — 3 laps\r\nHelios — 3 laps\r\nToy World 1 — 5 laps\r\nMolten Caverns — 2 laps\r\nPetroVolt — 2 laps\r\nSwan Street — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nLibrary — 2 laps\r\nJailhouse Rock — 2 laps\r\nSpooky-Volt (R) — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

