---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '06-10-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Museum 2 — 4 laps\r\nToytanic 2 — 3 laps\r\nQuake! — 2 laps\r\nCasino RV — 3 laps\r\nMoon Dawn — 2 laps\r\nGhost Town 2 (R) — 5 laps\r\nSmashride Circuit — 3 laps\r\nSnowy River (R) — 4 laps\r\nSantorini — 4 laps\r\nSchools Out! — 2 laps\r\nSkating Toys: Redux — 2 laps\r\nJailhouse Rock (R) — 3 laps\r\nBotanical Garden — 6 laps\r\nSakura — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

