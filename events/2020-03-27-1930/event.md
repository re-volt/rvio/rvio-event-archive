---
title: 'Custom Pro Races'
titleoverride: false
date: '27-03-2020 19:30'
event:
    host: Delecto
    ip: dele.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Custom Pro Cars'
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Museum 1 (M) — 3 laps\r\nHelios — 3 laps\r\nMoon Dawn — 2 laps\r\nToy World Mayham — 4 laps\r\nVenice (R) — 3 laps\r\nRooftop Chase Redux — 3 laps\r\nSpa-Volt 2 (R) — 4 laps\r\nSchool's Out — 2 laps\r\nSakura — 3 laps\r\nOverground — 3 laps\r\nMuseum 2 (R) — 4 laps\r\nSkating Toys (M) — 2 laps\r\nAMCO TT — 3 laps\r\nToy World Aquatica: Redux — 4 laps\r\nRV Temple — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only custom cars from the [Pro class](https://re-volt.io/online/cars/pro) will be allowed.