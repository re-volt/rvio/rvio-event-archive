---
title: 'Amateur Races'
titleoverride: false
date: '05-10-2019 18:00'
event:
    host: Skarma
    ip: u.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Blood on the Rooftops\r\nRooftops (R)\r\nIndustry\r\nGhost Town 2\r\nMuseum 2\r\nHull Breach 3000\r\nSakura\r\nVenice\r\nDonut Plains 3 (M)(R)\r\nKadish Sprint\r\nYABBA DABBA DOO!\r\nRanch (R)\r\nHoliday Camp: California Edition\r\nGhost Town 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

The IP is not a mistake, `u.rv.gl` will correctly redirect to Skarma's IP.