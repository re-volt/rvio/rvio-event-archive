---
title: Pros
titleoverride: false
date: '16-10-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Venice\r\nToy World 1 (R)\r\nMuseum 2\r\nGround N Smash 2\r\nGhost Town 1\r\nToy World Mayhem (R)\r\nRooftop Chase\r\nHoliday Camp: California Edition\r\nFool's Mate 2\r\nDonut Plains 3 (R)\r\nSkating Toys\r\nToySoldierz\r\nRanch\r\nRe-Ville\r\nSuperMarket 2"
    vod: 'https://www.youtube.com/watch?v=xxLYR-dRAsM'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-16_21-05-46.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

