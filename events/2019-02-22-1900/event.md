---
title: 'Circuit Races'
titleoverride: false
date: '22-02-2019 19:00'
event:
    host: Saffron
    ip: 'announced on Discord'
    category: special
    mode: race
    class: other
    otherclass: 'Circuit Tracks with Pemto'
    classlink: 'https://re-volt.io/events/2019-22-02-1900'
    tracklist: "Red Rock Valley R\r\nSS Route Revolt\r\nClubman Stage Route 5\r\nElkhart\r\nSideways Sanctuary C\r\nNamco Trains\r\nAMCO Bitume R\r\nFAT-TRACK\r\nRV-Simo On-Road Track\r\nMidnight GP\r\nSideways Sanctuary B\r\nSpecial Stage Route 5 R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Requirements**
* [Circuit Tracks](https://files.re-volt.io/packs/circuit_tracks.zip)
* [Pemto](http://revoltzone.net/cars/28297/Pemto) (also part of the [I/O car pack](https://distribute.re-volt.io/packs/io_cars.zip))
* [Pemto Pack Project](https://kiwis.rv.gl/files/ppp-181010.zip) (optional skins)

The races will have 4 laps.