---
title: 'Pro Races'
titleoverride: true
date: '28-01-2020 19:00'
event:
    host: 'Skarma & Saffron'
    ip: 'sk.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Molten Caverns — 2 laps\r\nBotanical Garden — 6 laps\r\nToys in the Hood 1 (R) —  3 laps\r\nBiohazard Factory — 2 laps\r\nHoliday Camp: California Edition (M) — 2 laps\r\nVenice — 3 laps\r\nOverground —  3 laps\r\nToy World Mayhem (R) — 4 laps\r\nIndustry — 3 laps\r\nRoute-77 (R) — 4 laps\r\nFool's Mate 2 — 4 laps\r\nToys in the Hood 2 — 4 laps\r\nGhost Town 1 — 6 laps\r\nHelios — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
---

