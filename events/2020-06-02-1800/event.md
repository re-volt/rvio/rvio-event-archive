---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '02-06-2020 18:00'
event:
    host: 'Etneus & Delecto'
    ip: 'etn.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Jailhouse Rock (R) — 2 laps\r\nIndustry — 2 laps\r\nToys in the Hood 2 — 3 laps\r\nBotanical Garden (R) — 5 laps\r\nVenice — 3 laps\r\nGrisville — 3 laps\r\nRV Temple — 2 laps\r\nSakura — 2 laps\r\nMeltdown — 4 laps\r\nFiddlers on the Roof: Redux (R) — 2 laps\r\nSupermarket 1 — 4 laps\r\nRooftops — 3 laps\r\nHelios — 3 laps\r\nThe Felling Yard — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

