---
title: 'Amateur Races'
titleoverride: false
date: '25-04-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Supermarket 2\r\nToySoldierz (R)\r\nRooftop Chase: Redux\r\nHelios\r\nMuseum 2 (R)\r\nSwan Street\r\nSantorini\r\nVenice (R)\r\nBotanical Garden\r\nRadioactive Garden\r\nMetro-Volt\r\nToy World 1\r\nRe-Ville\r\nSpa-Volt 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

