---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '21-05-2020 18:00'
event:
    host: 'Dvark & OhNej'
    ip: 'dv.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Holiday Camp: California Edition — 2 laps\r\nSakura — 2 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nMoon Dawn — 2 laps\r\nMuseum 2 — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nRoute-77 (R) — 3 laps\r\nBiohazard Factory — 2 laps\r\nToys in the Hood 2 — 3 laps\r\nRooftop Chase: Redux — 3 laps\r\nLunar — 3 laps\r\nSupermarket 2 (R) — 7 laps\r\nSnowland 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

