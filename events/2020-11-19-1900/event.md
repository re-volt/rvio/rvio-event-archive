---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '19-11-2020 19:00'
event:
    host: 'Frosttbitten & Bismarck'
    ip: 'frost.rv.gl or bis.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Lunar — 3 laps\r\nQuake! — 2 laps\r\nCasino RV (R) — 3 laps\r\nSpooky-Volt — 6 laps\r\nGrisville — 3 laps\r\nToy World Mayhem — 4 laps\r\nToy World 2 — 5 laps\r\nSupermarket 2 (R) — 8 laps\r\nBotanical Garden — 6 laps\r\nWhite Rose Chapel — 3 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nThe Bunker — 4 laps\r\nStadVolt — 4 laps\r\nToytanic 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

