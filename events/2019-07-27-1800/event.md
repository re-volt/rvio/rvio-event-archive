---
title: 'Rookie Races'
titleoverride: false
date: '27-07-2019 18:00'
event:
    host: TBA
    ip: TBA
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "1. Ghost Town 2 (R)\r\n2. Toy World 1\r\n3. Venice\r\n4. Donut Plains 3 (M)\r\n5. ToySoldierz (R)\r\n6. Fool's Mate 2\r\n7. Cliffside\r\n8. Swan Street \r\n9. Rooftops \r\n10. Lunar\r\n11. AMCO TT \r\n12. Spa-Volt 2 (R)\r\n13. Botanical Garden\r\n14. Toy World Mayhem"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

