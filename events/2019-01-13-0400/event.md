---
title: 'Semi-Pro Races'
titleoverride: false
date: '13-01-2019 04:00'
event:
    ip: 'announced on Discord'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Cliffside\r\nToy World 2\r\nHoliday Camp: California Edition\r\nKadish Sprint\r\nThe Felling Yard\r\nGround N Smash 2\r\nToytanic 2 (R)\r\nBlood on the Rooftops\r\nRooftop Chase\r\nRadioactive Garden\r\nGhost Town 2\r\nBotanical Garden\r\nRanch (R)\r\nPalm Marsh (R)\r\nLunar\r\nVenice"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

