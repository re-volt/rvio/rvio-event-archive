---
title: 'Toyeca Exclusive'
titleoverride: true
date: '06-12-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: Toyeca
    classlink: 'https://revolt.wikia.com/wiki/Toyeca'
    tracklist: "Toys in the Hood 1\r\nHoliday Camp: California Edition\r\nSuperMarket 1\r\nGhost Town 2\r\nToy World 1\r\nSakura\r\nPetroVolt\r\nQuake!\r\nBotanical Garden\r\nToytanic 1\r\nRooftops\r\nMuseum 2\r\nToys in the Hood 2\r\nMuseum 1\r\nJailhouse Rock\r\nHelios\r\nMetro-Volt\r\nVenice\r\nSuperMarket 2\r\nGhost Town 1\r\nToy World 2\r\nToy World Mayhem\r\nHull Breach 3000\r\nSpa-Volt 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

You may only race with **Toyeca**.