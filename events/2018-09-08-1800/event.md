---
title: Slugs
titleoverride: false
date: '2018-09-08 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: slugs
    tracklist: "Toy World 2\r\nToys in the Hood 2\r\nQuake!\r\nIndustry\r\nHelios\r\nSwan Street\r\nMK64 Wario Stadium\r\nRadioactive Garden\r\nToy World 1 (R)\r\nToys in the Hood 1\r\nIllusion (R)\r\nSkating Toys (R)\r\nPetroVolt\r\nGrisville"
    results: 'http://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-08_20-02-27.csv'
taxonomy:
    category:
        - events
visible: true
sidebarlayout: global
sidebarpath: /sidebar/events
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

