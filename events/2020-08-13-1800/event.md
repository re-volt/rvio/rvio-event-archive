---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '13-08-2020 18:00'
event:
    host: 'OhNej & Ashen Forest'
    ip: 'ohnej.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Medieval: Redux — 5 laps\r\nLunar — 3 laps\r\nGhost Town 1 — 6 laps\r\nToys in the Hood 1 (R) — 3 laps\r\nOverground — 3 laps\r\nSupermarket 1 — 4 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nIndustry — 3 laps\r\nGrisville — 3 laps\r\nToy World 1 — 6 laps\r\nSmashride Circuit — 3 laps\r\nToy World Mayhem — 4 laps\r\nSnowland 1 (R) — 4 laps\r\nMetro-Volt — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

