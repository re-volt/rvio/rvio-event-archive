---
title: 'Cat & Mouse <small>(Dual Lobby)</small>'
titleoverride: true
date: '13-12-2020 18:00'
event:
    host: 'URV & OhNej'
    ip: 'u.rv.gl OR ohnej.rv.gl'
    category: tourney
    mode: race
    class: other
    otherclass: 'Cat & Mouse'
    classlink: 'https://re-volt.io/events/2020-12-13-1800'
    tracklist: "Toys in the Hood 2 — 3 laps\r\nBiohazard Factory — 2 laps\r\nCake — 2 laps\r\nMuseum 1 — 2 laps\r\nAutumn Castle — sprint\r\nLuncheon Tour — 5 laps\r\nFairground 2 — 2 laps\r\nToy World Mayhem — 4 laps\r\nChristmas Crib 2011 — 3 laps\r\nA Snowy Night Out West — 4 laps\r\nIllusion — 2 laps\r\nRoute-77 — 3 laps\r\nWinter Madness — 9 laps\r\nWinter Doomsday — 3 laps"
media_order: rvcc20_poster2.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Cat & Mouse'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](rvcc20_poster2.png)

This will be a team-based event where you can either choose between the "cats" or the "mice" (see list below). Separate team results will be posted in Discord, and points will be granted based on the winner of each race. No sign-ups required, but the teams should be balanced before the races begin, and changing sides should be avoided.

### Choose between the following cars
|  Team Cats  |  Team Mice  |
|  :-----          |  :-----          |
|  Arctic Dozer  |  Snowball  |
|  Monster Knot  |  Snowflake  |

### Requirements
* [CC20 Cars](https://files.re-volt.io/packs/christmas/2020/rvcc20_cars.7z)
* [CC20 Tracks](https://files.re-volt.io/packs/christmas/2020/rvcc20_tracks.7z)