---
title: 'Semi-Pro Races'
titleoverride: false
date: '22-10-2019 18:00'
event:
    host: N/A
    ip: N/A
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 2 (R)\r\nJailhouse Rock (R)\r\nWhite Rose Chapel\r\nMuseum 1\r\nToytanic 1\r\nSkating Toys\r\nPenny Racers - Caves (R)\r\nGrisville (M)\r\nThe Bunker\r\nRadioactive Garden\r\nHoliday Camp: California Edition\r\nSpa-Volt 1\r\nSwan Street\r\nToy World 1\r\nPetroVolt"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

