---
title: 'Harvester Exclusive'
titleoverride: false
date: '08-07-2019 18:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Harvester Exclusive'
    classlink: 'https://revolt.fandom.com/wiki/Harvester'
    tracklist: "Jailhouse Rock R\r\nDonut Plains 3\r\nSupermarket 2\r\nThe Bunker\r\nPenny Racers - Harbour\r\nSpa-Volt 1\r\nToy World 2 R\r\nToy World Mayhem R\r\nBotanical Garden\r\nToy World Aquatica: Redux\r\nRadioactive Garden\r\nGhost Town 1\r\nLunar"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

