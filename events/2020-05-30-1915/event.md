---
title: 'Online Diamond Cup'
titleoverride: true
date: '30-05-2020 19:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Online Diamond Cup'
    classlink: 'https://re-volt.io/events/2020-05-30-1915'
    tracklist: "Supermarket 1 M\r\nGhost Town 2 M\r\nToy World 2 RM\r\nMuseum 1 RM\r\nToytanic 2 M"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Online Diamond Cup'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 4 cup standings and individual player progress, be sure to [check this](https://www.dropbox.com/s/qfzn2vwewz114d7/season%204%20cup%20session%20info.txt?dl=0).

### Requirements
* [Re-Volt Online car pack version 20.1](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (advised)
* [super tier car pack](https://www.dropbox.com/sh/uv3dvm1081w04uj/AADekFTAfRxCGicVmYsAPUEOa?dl=1) (advised)
* Seperate game installation required if you got I/O content on the game you want to use!

### Car Selection
* Humma
* Kazuki
* King Kaiju
* King Moloko
* Komet
* P4 Super
* Toyeca