---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '12-09-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Museum 1 — 3 laps\r\nKadish Sprint — 2 laps\r\nCliffside — 6 laps\r\nToys in the Hood 1 — 3 laps\r\nRadioactive Garden — 5 laps\r\nSpa-Volt 1 — 3 laps\r\nSupermarket 1 — 4 laps\r\nToy World Mayhem — 4 laps\r\nJailhouse Rock (R) — 3 laps\r\nPetroVolt — 2 laps\r\nSantorini (R) — 4 laps\r\nWhite Rose Chapel — 3 laps\r\nGhost Town 2 (R) — 5 laps\r\nIndustry — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

