---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '21-11-2020 19:00'
event:
    host: 'Ashen Forest & URV'
    ip: 'ash.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Medieval: Redux — 4 laps\r\nSakura — 2 laps\r\nToys in the Hood 2 — 4 laps\r\nHull Breach 3000 — 3 laps\r\nSchools Out! 2 — 2 laps\r\nSantorini (R) — 4 laps\r\nMuseum 2 (R) — 4 laps\r\nToySoldierz — 4 laps\r\nMysterious Toy-Volt Factory 2 — 2 laps\r\nToy World 1 — 6 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nGhost Town 2 — 4 laps\r\nKadish Sprint — 2 laps\r\nSnowland 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

