---
title: 'Rookie Races'
titleoverride: false
date: '03-10-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Fiddlers on the Roof\r\nMetro-Volt\r\nSuperMarket 1\r\nThe Felling Yard\r\nSpa-Volt 1\r\nGrisville (R)\r\nSantorini\r\nToySoldierz\r\nJailhouse Rock\r\nMuseum 1\r\nQuake! (M)(R)\r\nToytanic 2\r\nBotanical Garden (R)\r\nPenny Racers - Harbour"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

