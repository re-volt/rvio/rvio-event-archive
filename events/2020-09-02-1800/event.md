---
title: Tomatoes
titleoverride: true
date: '02-09-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'a tomato'
    classlink: 'http://files.re-volt.io/misc/fresh.zip'
    tracklist: "SuperMarket 2\r\nMetro-Volt\r\nBotanical Garden\r\nSmashride Circuit (M)\r\nRadioactive Garden\r\nSupermarket 1 (M)(R)\r\nCliffside\r\nToys In The Hood 2 (R)\r\nWhite Rose Chapel\r\nAMCO TT (R)\r\nSurprise Track"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

We're doing 5 laps on each track. Join us in the voice channel on [Discord](https://re-volt.io/discord) to hear a special guest talk about growing tomatoes (if they didn't flee to the Maldives).

**Remember to [download your own tomato](http://files.re-volt.io/misc/fresh.zip)!**