---
title: 'Rookie Knockout Session'
titleoverride: true
date: '08-05-2020 19:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Rookie Knockout'
    classlink: 'https://re-volt.io/events/2020-05-08-1915'
    tracklist: Random
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Knockout Session'
googledesc: 'https://re-volt.io/events/2020-05-08-1915'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information, be sure to check the [Knockout Session Info](<https://www.dropbox.com/s/evna7pslcw0rgvz/re-volt%20knockout%20session%20info.txt?dl=0>).

### Requirements
* [RVON community track pack](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1) (needed)
* [RVON community car pack](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (advised)

### Car Selection
* Every car rated rookie or lower on [this car list](<https://www.dropbox.com/s/qeni7m2lyfvb9m1/rvon%20car%20list%20%2820.1%29.txt?dl=0>)