---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '25-02-2020 19:00'
event:
    host: 'Saffron & Floxit'
    ip: 'saff.rv.gl OR flo.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Toy World 2 — 5 laps\r\nMuseum 1 — 3 laps\r\nRadioactive Garden (R) — 5 laps\r\nQuake! — 2 laps\r\nPenny Racers - Caves — 5 laps\r\nHelios — 3 laps\r\nToy World 1 (R) — 5 laps\r\nAMCO TT — 3 laps\r\nIndustry — 3 laps\r\nFool's Mate 2 — 4 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nSupermarket 1 (M) — 4 laps\r\nFiddlers on the Roof: Redux — 3 laps\r\nHoliday Camp: California Edition — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

