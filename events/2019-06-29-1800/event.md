---
title: 'Pro Races (Dual Lobby)'
titleoverride: false
date: '29-06-2019 18:00'
event:
    host: 'Geromu & URV'
    ip: 'ger.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Spa-Volt 2\r\nJailhouse Rock\r\nGhost Town 1 RM\r\nGrisville R\r\nRe-Ville\r\nDonut Plains 3\r\nVenice R\r\nToy World Aquatica: Redux\r\nRadioactive Garden\r\nToys in the Hood 1\r\nCliffside\r\nToy World 1\r\nPenny Racers - Harbour\r\nSuperMarket 2\r\nYABBA DABBA DOO!\r\nHelios"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

