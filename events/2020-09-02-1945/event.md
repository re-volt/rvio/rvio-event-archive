---
title: 'Super Pro Races'
titleoverride: true
date: '02-09-2020 19:45'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Underrated Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Nain — Toy World 1 — 6 laps\r\nAnaconda GT — Museum 2 — 4 laps\r\nSkull Crusher — Toys in the Hood 2 — 3 laps\r\nUltra RV — Petrovolt — 3 laps\r\nDragheat — Toy World 2 — 5 laps\r\nArion — Metro-Volt — 3 laps\r\nEndo — Lunar — 3 laps\r\nDaemmon — Toytanic 1 — 3 laps\r\nDaemmon — Sakura — 3 laps\r\nTesseract — Petrovolt — 2 laps\r\nTesseract — Sakura — 3 laps\r\nSelcia Turbo — White Rose Chapel — 4 laps\r\nSelcia Turbo — Supermarket 1 — 4 laps\r\nCommandine — Toy World Mayhem — 4 laps\r\nCommandine — Medieval: Redux — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

