---
title: Slugs
titleoverride: false
date: '06-12-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1 (R)\r\nPetroVolt\r\nSuperMarket 1\r\nQuake! (R)\r\nToy World Mayhem\r\nSwan Street (R)\r\nFool's Mate 2\r\nRadioactive Garden\r\nThe Bunker\r\nMuseum 1\r\nThe Gorge\r\nLunar\r\nGrisville\r\nBotanical Garden"
    vod: 'https://www.youtube.com/watch?v=_j66NbbN5sg'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-06_20-01-32.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

