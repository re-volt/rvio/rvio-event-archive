---
title: 'Pro Races'
titleoverride: false
date: '20-06-2019 18:00'
event:
    host: Flyboy
    ip: fly.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 1\r\nQuake!\r\nFiddlers on the Roof\r\nCliffside\r\nPetroVolt\r\nSuperMarket 2 (R)\r\nMolten Caverns\r\nHoliday Camp: California Edition (R)\r\nPenny Racers - Harbour (R)\r\nGhost Town 1\r\nToytanic 2\r\nGrisville\r\nThe Felling Yard (M)\r\nRV Temple\r\nThe Bunker\r\nVenice"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

