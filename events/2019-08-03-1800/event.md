---
title: 'Pro Races'
titleoverride: false
date: '03-08-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Helios\r\nToySoldierz (M)\r\nMoon Dawn\r\nGhost Town 1\r\nJailhouse Rock (R)\r\nThe Felling Yard\r\nRV Temple\r\nToy World Aquatica: Redux (R)\r\nSpa-Volt 1\r\nToytanic 2 (R)\r\nMolten Caverns\r\nMuseum 1\r\nRe-Ville\r\nHoliday Camp: California Edition\r\nFiddlers on the Roof\r\nToy World 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

