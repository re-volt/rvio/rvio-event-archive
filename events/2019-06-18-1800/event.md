---
title: 'Rookie Races'
titleoverride: false
date: '18-06-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "YABBA DABBA DOO!\r\nBotanical Garden (M)(R)\r\nRanch\r\nJailhouse Rock\r\nSpa-Volt 1 (R)\r\nRooftop Chase: Redux\r\nToy World Mayhem\r\nToySoldierz\r\nOverground\r\nToy World Aquatica: Redux (R)\r\nGhost Town 2\r\nToytanic 1\r\nToys in the Hood 1\r\nBlood on the Rooftops"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

