---
title: 'RC San Exclusive'
titleoverride: false
date: '07-04-2019 23:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'RC San Exclusive'
    tracklist: "Cliffside\r\nLunar\r\nSakura (R)\r\nToys in the Hood 2\r\nKadish Sprint\r\nSnowland 1\r\nRadioactive Garden\r\nBotanical Garden (R)\r\nToy World 2 (R)\r\nWhite Rose Chapel\r\nSnowy River\r\nFool's Mate 2\r\nRe-Ville\r\nRV Temple"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

