---
title: 'Cinnamon Cup <small>(Dual Lobby)</small>'
titleoverride: true
date: '16-12-2020 19:00'
event:
    host: 'URV & Frosttbitten'
    ip: 'u.rv.gl OR frost.rv.gl'
    category: tourney
    mode: race
    class: other
    otherclass: 'Cinnamon Cup'
    classlink: 'https://re-volt.io/events/2020-12-16-1900'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Endgame — 3 laps\r\nSwan Street — 3 laps\r\nSakura — 3 laps\r\nBotanical Garden — 6 laps\r\nMysterious Toy-Volt Factory — 4 laps\r\nToy World Winter — 3 laps\r\nThe Glaciers — 7 laps\r\nFrostbite — 2 laps\r\nHome for Christmas — 3 laps\r\nCliffs Maze — 2 laps\r\nSuperMarket 2 — 7 laps\r\nCandy Cane Land — 3 laps\r\nSkiing Paradise — 2 laps\r\nWonderful Skylands 1 — 5 laps"
media_order: rvcc20_poster3.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Cinnamon Cup'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](rvcc20_poster3.png)
This will be a regular racing session with three wacky cars.

### Choose between the following cars
* Candy Cane
* Ski X
* Starlight

### Requirements
* [CC20 Cars](https://files.re-volt.io/packs/christmas/2020/rvcc20_cars.7z)
* [CC20 Tracks](https://files.re-volt.io/packs/christmas/2020/rvcc20_tracks.7z)