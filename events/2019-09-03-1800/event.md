---
title: 'Advanced Races'
titleoverride: false
date: '03-09-2019 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Quake!\r\nSantorini\r\nLunar (M)\r\nToy World 1\r\nBlood on the Rooftops\r\nVenice (R)\r\nMuseum 2 (R)\r\nGhost Town 2\r\nIndustry\r\nMoon Dawn\r\nSwan Street\r\nSuperMarket 2\r\nMetro-Volt\r\nRooftop Chase: Redux (R)\r\nWhite Rose Chapel"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

