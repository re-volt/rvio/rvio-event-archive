---
title: 'Amateur Races'
titleoverride: false
date: '13-07-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Overground\r\nSakura\r\nMuseum 2\r\nPenny Racers - Harbour\r\nToys in the Hood 1\r\nIndustry\r\nPenny Racers - Caves (R)\r\nPetroVolt\r\nToy World Mayhem (M)\r\nSpa-Volt 1 (R)\r\nToy World 1\r\nFool's Mate 2\r\nBotanical Garden (R)\r\nMoon Dawn\r\nSnowland 1 (Bonus Race, Unranked)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

