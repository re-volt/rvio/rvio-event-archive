---
title: 'Advanced Races'
titleoverride: false
date: '06-07-2019 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Spa-Volt 2\r\nMolten Caverns R\r\nToys in the Hood 2\r\nCliffside\r\nMetro-Volt\r\nSuperMarket 2\r\nWhite Rose Chapel\r\nToytanic 2\r\nThe Felling Yard\r\nBlood on the Rooftops\r\nJailhouse Rock R\r\nHelios\r\nRooftop Chase: Redux\r\nRV Temple\r\nGhost Town 1 RM"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

