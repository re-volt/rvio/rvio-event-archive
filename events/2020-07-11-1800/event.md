---
title: 'Semi-Pro Races'
titleoverride: true
date: '11-07-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "    Museum 1 — 3 laps\r\n    ToySoldierz — 4 laps\r\n    Jailhouse Rock (R) — 3 laps\r\n    Snowland 1 — 3 laps\r\n    RV Temple — 2 laps\r\n    Ghost Town 1 (R) — 6 laps\r\n    Moon Dawn — 2 laps\r\n    Lunar — 3 laps\r\n    Overground — 2 laps\r\n    PetroVolt — 2 laps\r\n    Sakura — 3 laps\r\n    Toys in the Hood 1 — 3 laps\r\n    Santorini (R) — 4 laps\r\n    Ghost Town 2 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

