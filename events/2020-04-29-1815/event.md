---
title: 'Rookie Normal Session'
titleoverride: true
date: '29-04-2020 18:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Rookie Normal Session'
    classlink: 'https://re-volt.io/events/2020-04-29-1815'
    tracklist: "Playa+  (4 laps)\r\nHoliday Camp California Edition R (3 laps)\r\nToy World Ex RM (4 laps)\r\nFiddles on the Roof M (4 laps)\r\nHelios (4 laps)\r\nVenice R (4 laps)\r\nMikes Medieval Mahem M (4 laps)\r\nGrisville (4 laps)\r\nSynthwave M (4 laps)\r\nSchools Out R (2 laps)\r\nSkating Toys R (3 laps)\r\nWipeout (4 laps)\r\nCliffside M (5 laps)\r\nSpa-Volt 2 (4 laps)\r\nToysoldierz (4 laps)\r\nGhost Town 2 R (4 laps)\r\nLunar M (4 laps)\r\nOverground (4 laps)\r\nWhite Rose Chapel M (4 laps)\r\nToytanic 2 (4 laps)\r\nHarbor Lights (4 laps)\r\nDonut Plains 3 R (4 laps)\r\nSupermarket 2 R (5 laps)\r\nSantorini RM (4 laps)\r\nPenny Racers Harbour M (4 laps)\r\nToy World Aqua: Redux R (4 laps)\r\nRe-Ville (4 laps)\r\nPenny Racers Caves R (4 laps)\r\nRoute-77 M (4 laps)\r\nAmco Tt R (4 laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Normal Session'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous avarage prestige results be sure to [check this](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).

### Requirements
* [Re-Volt Online track pack version 20.0](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1) (needed)
* [Re-Volt Online car pack version 20.0](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (advised)
* Seperate game installation required if you got I/O content on the game you want to use!

### Car Selection
* Any car that has been rated for rookie on [this car list](https://www.dropbox.com/s/u71xh54lw48noxu/rvon%20car%20list%20%2820.0%29.txt?dl=0)