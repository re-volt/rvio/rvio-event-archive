---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '18-08-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Rooftops 1 — 3 laps\r\nSantorini — 4 laps\r\nGhost Town 2 — 4 laps\r\nGhost Town 1 (R) — 6 laps\r\nRooftop Chase: Redux (R) — 3 laps\r\nHelios — 3 laps\r\nRadioactive Garden — 5 laps\r\nToy World Mayhem — 4 laps\r\nThe Bunker — 4 laps\r\nSpa-Volt 1 — 3 laps\r\nToy World 2 — 5 laps\r\nToys in the Hood 2 — 4 laps\r\nJailhouse Rock (R) — 3 laps\r\nToy World Aquatica: Redux — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

