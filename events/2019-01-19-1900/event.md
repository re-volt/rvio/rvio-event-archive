---
title: 'Rookie Races'
titleoverride: false
date: '19-01-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Radioactive Garden\r\nSupermarket 2 (R)\r\nToy World Mayhem (R)\r\nSantorini\r\nThe Bunker\r\nMuseum 2\r\nYABBA DABBA DOO!\r\nLunar\r\nDonut Plains 3 (R)\r\nGhost Town 1\r\nSpa-Volt 1\r\nPenny Racers - Harbour\r\nVenice\r\nBotanical Garden"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

