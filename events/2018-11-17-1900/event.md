---
title: Rookies
titleoverride: false
date: '17-11-2018 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: rookies
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 2\r\nToySoldierz\r\nCliffside\r\nToys in the Hood 2\r\nLunar\r\nSuperMarket 1\r\nToy World Mayhem (R)\r\nSakura\r\nAMCO TT (R)\r\nFool's Mate 2\r\nMeltdown\r\nToy World 1 (R)\r\nSpa-Volt 1\r\nKadish Sprint"
    vod: 'https://www.youtube.com/watch?v=pmjszsTAHDU'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-17_20-02-46.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

