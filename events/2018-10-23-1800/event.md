---
title: 'Pro Slugs'
titleoverride: false
date: '23-10-2018 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: prugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "The Bunker\r\nMuseum 1 (R)\r\nGhost Town 1\r\nIllusion\r\nHoliday Camp: California Edition\r\nSantorini\r\nToy World 1\r\nMetro-Volt\r\nAMCO TT (R)\r\nToy World Aquatica: Redux\r\nHelios\r\nToySoldierz\r\nToy World Mayhem (R)\r\nToytanic 1"
    vod: 'https://www.youtube.com/watch?v=iXYYRylpYtI'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-23_20-07-16.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

