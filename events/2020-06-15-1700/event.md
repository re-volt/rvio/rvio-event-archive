---
title: 'Long Journey 4: Day 4'
titleoverride: true
date: '15-06-2020 17:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_cars_bonus
        - io_skins
        - io_tracks
        - io_tracks_bonus
    tracklist: "MKDD - Mushroom City\r\nToytanic 2\r\nFrostbite\r\nBlood on the Rooftops\r\nCatfish Cove\r\nCliffside Court\r\nSakura\r\nToy World 1\r\nThe Catacombs\r\nToy World Mayhem\r\nMysterious Toy Factory\r\nHelios\r\nToy World EX\r\nMeltdown\r\nGround N Smash 2\r\nWhite Rose Chapel\r\nFiddlers on the Roof: Redux\r\nIllusion\r\nOverground\r\nMolten Caverns\r\nGhost Town 1\r\nSkating Toys"
    vod: 'https://www.youtube.com/watch?v=rtVxk8bCLmY'
    results: 'https://online.re-volt.io/sessions/results.php?file=main/session_2020-06-15_19-00-07.csv'
media_order: LongJourney4.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Long Journey 4: Day 4'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](https://re-volt.io/user/pages/events/2020-06-15-1700/LongJourney4.png)