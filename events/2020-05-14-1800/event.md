---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '14-05-2020 18:00'
event:
    host: 'Kirioso & Delecto'
    ip: 'kiri.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Supermarket 1 — 4 laps\r\nCliffside — 6 laps\r\nRV Temple — 2 laps\r\nOverground — 3 laps\r\nMoon Dawn (R) — 2 laps\r\nMeltdown — 5 laps\r\nToys in the Hood 2 — 4 laps\r\nBotanical Garden — 6 laps\r\nRooftops (R) — 4 laps\r\nVenice — 4 laps\r\nMetro-Volt — 3 laps\r\nFools Mate 2 — 4 laps\r\nLunar — 3 laps\r\nToy World Mayhem (R) — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

