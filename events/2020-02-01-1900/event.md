---
title: 'Semi-Pro Races'
titleoverride: true
date: '01-02-2020 19:00'
event:
    host: 'Delecto & Dvark'
    ip: 'dele.rv.gl OR dv.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Lunar (M) — 3 laps\r\nToySoldierz (R) — 4 laps\r\nAMCO TT — 3 laps\r\nMetro-Volt — 3 laps\r\nRooftops —  3 laps\r\nFiddlers on the Roof: Redux — 3 laps\r\nSupermarket 1 — 4 laps\r\nSkating Toys — 2 laps\r\nSakura — 3 laps\r\nSupermarket 2 (R) — 8 laps\r\nThe Bunker — 4 laps\r\nBlood on the Rooftops — 3 laps\r\nMuseum 1 — 3 laps\r\nPetroVolt (R) — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

