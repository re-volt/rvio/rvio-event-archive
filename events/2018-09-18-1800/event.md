---
title: Slugs
titleoverride: false
date: '2018-09-18 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 2\r\nRanch (R)\r\nGrisville\r\nSakura\r\nYABBA DABBA DOO!\r\nJailhouse Rock\r\nMK64 Wario Stadium\r\nDonut Plains 3\r\nMuseum 2\r\nCliffside\r\nRooftop Chase (R)\r\nBotanical Garden\r\nMuseum 1 (R)\r\nCactus-Volt"
    vod: 'https://www.youtube.com/watch?v=9jPXj0dfjVI'
    results: 'http://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-18_21-03-08.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**[Remember to update your custom content!](https://re-volt.io/blog/online-packs-update-september-2018)**