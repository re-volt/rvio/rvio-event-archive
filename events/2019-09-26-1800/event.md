---
title: 'Advanced Races'
titleoverride: false
date: '26-09-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1\r\nRanch (R)\r\nSantorini\r\nAMCO TT (R)\r\nSkating Toys\r\nSpa-Volt 2\r\nSakura\r\nMetro-Volt (M)\r\nLunar\r\nGhost Town 2\r\nMuseum 1\r\nMoon Dawn\r\nPetroVolt\r\nToytanic 1 (R)\r\nSnowy River"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

