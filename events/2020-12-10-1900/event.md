---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '10-12-2020 19:00'
event:
    host: 'Bismarck & Mighty'
    ip: 'bis.rv.gl OR mighty.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Sakura — 3 laps\r\nRooftop Chase: Redux — 3 laps\r\nRooftops (R) — 4 laps\r\nSkating Toys: Redux — 2 laps\r\nLibrary — 3 laps\r\nMysterious Toy-Volt Factory — 5 laps\r\nToys in the Hood 2 — 4 laps\r\nJailhouse Rock (R) — 3 laps\r\nMetro-Volt — 3 laps\r\nBiohazard Factory — 3 laps\r\nMuseum EX — 2 laps\r\nGhost Town 2 — 5 laps\r\nMysterious Toy-Volt Factory 2 (R) — 2 laps\r\nBotanical Garden — 6 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

