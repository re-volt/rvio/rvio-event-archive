---
title: 'Advanced Races'
titleoverride: false
date: '27-04-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Lunar\r\nMolten Caverns (R)\r\nIndustry\r\nThe Bunker\r\nGhost Town 1\r\nToy World Mayhem\r\nSakura \r\nToys in the Hood 1\r\nFiddler's on the Roof\r\nThe Felling Yard\r\nRooftops \r\nSpa-Volt 1 (R)\r\nCliffside\r\nOverground\r\nToy World 2 (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

