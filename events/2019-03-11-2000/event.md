---
title: 'Pemto Exclusive'
titleoverride: false
date: '11-03-2019 20:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: Pemto
    classlink: 'http://revoltzone.net/cars/28297/Pemto'
    tracklist: 'Random Tracks'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Visit **[RVGL Ladder](https://ladder.rv.gl/)** for more information!

**Links**:
* [Track Selection](https://ladder.rv.gl/info/#events-general-tracks)
* [Car Selection](https://ladder.rv.gl/info/#events-general-cars)
* [Ranking](https://ladder.rv.gl/info/#events-general-ranking)