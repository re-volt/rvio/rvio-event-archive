---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '22-10-2020 18:00'
event:
    host: 'Ashen Forest & Floxit'
    ip: 'ash.rv.gl OR flo.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Hull Breach 3000 — 4 laps\r\nGhost Town 2 (R) — 5 laps\r\nSchools Out! — 2 laps\r\nToytanic 1 — 3 laps\r\nMoon Dawn (R) — 2 laps\r\nJailhouse Rock — 3 laps\r\nRoute-77 — 4 laps\r\nBlood on the Rooftops — 3 laps\r\nSnowland 1 — 4 laps\r\nToy World Mayhem (R) — 4 laps\r\nMuseum 2 — 4 laps\r\nOverground — 3 laps\r\nToy World 2 — 5 laps\r\nToy World Aquatica: Redux — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

