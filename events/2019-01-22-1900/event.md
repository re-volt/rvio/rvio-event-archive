---
title: 'Pro Races'
titleoverride: false
date: '22-01-2019 19:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Blood on the Rooftops\r\nBiohazard Factory (R)\r\nSwan Street (R)\r\nAMCO TT\r\nToys in the Hood 1\r\nQuake!\r\nMeltdown\r\nToySoldierz\r\nToytanic 1 (R)\r\nJailhouse Rock\r\nGrisville\r\nLunar\r\nGhost Town 1\r\nKadish Sprint\r\nToy World 2\r\nFreestoyle 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

