---
title: 'Semi-Pro Races'
titleoverride: true
date: '05-11-2019 19:00'
event:
    host: Skarma
    ip: sk.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World Mayhem\r\nSnowy River (M)\r\nSuperMarket 1\r\nMolten Caverns\r\nToySoldierz\r\nBotanical Garden\r\nHoliday Camp: California Edition\r\nGrisville\r\nRadioactive Garden (R)\r\nToytanic 2 (R)\r\nSantorini\r\nPenny Racers - Harbour\r\nSuperMarket 2\r\nQuake!\r\nJailhouse Rock (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

