---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '09-01-2020 19:00'
event:
    host: 'Kirioso & Saffron'
    ip: 'kiri.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Rooftop Chase: Redux — 3 laps\r\nThe Felling Yard — 3 laps\r\nGhost Town 2 — 4 laps\r\nToy World Aquatica: Redux (R) — 3 laps\r\nVenice (M) — 3 laps\r\nSuperMarket 1 — 4 laps\r\nHull Breach 3000 — 3 laps\r\nSpa-Volt 2 — 3 laps\r\nRooftops — 3 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nRV Temple — 2 laps\r\nBlood on the Rooftops — 3 laps\r\nToytanic 2 (R) — 3 laps\r\nSantorini — 4 laps\r\nFool's Mate 2 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

