---
title: 'Pro Races'
titleoverride: false
date: '13-08-2019 18:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Biohazard Factory R\r\nMetro-Volt\r\nMoon Dawn\r\nToytanic 2\r\nRadioactive Garden \r\nSnowland 1\r\nRanch\r\nThe Felling Yard\r\nMuseum 1 M\r\nSakura\r\nWhite Rose Chapel\r\nYABBA DABBA DOO!\r\nToys in the Hood 2\r\nSupermarket 1 R\r\nKadish Sprint\r\nHoliday Camp: California Edition R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

