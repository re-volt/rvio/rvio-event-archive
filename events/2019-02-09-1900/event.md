---
title: 'Semi-Pro Races'
titleoverride: false
date: '09-02-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "MK64 Wario Stadium\r\nToys in the Hood 1\r\nFreestoyle 2\r\nPenny Racers - Caves\r\nGhost Town 2 (R)\r\nPetroVolt\r\nRooftop Chase\r\nToytanic 2\r\nRanch (R)\r\nGround N Smash 2\r\nToy World 2\r\nCactus-Volt\r\nJailhouse Rock\r\nIllusion (R)\r\nHoliday Camp - California Edition"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

