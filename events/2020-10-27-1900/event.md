---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '27-10-2020 19:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Radioactive Garden — 4 laps\r\nMuseum 1 (R) — 2 laps\r\nSchools Out! 2 — 2 laps\r\nHelios — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nPetroVolt (R) — 2 laps\r\nIndustry — 2 laps\r\nSupermarket 2 — 7 laps\r\nThe Bunker — 3 laps\r\nSakura — 2 laps\r\nBotanical Garden — 5 laps\r\nToytanic 2 — 3 laps\r\nMolten Caverns — 2 laps\r\nSpooky-Volt (R) — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

