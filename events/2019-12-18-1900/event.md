---
title: 'Battle Tag'
titleoverride: true
date: '18-12-2019 19:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: special
    mode: battle-tag
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Neighbourhood Battle\r\nSupermarket Battle\r\nGarden Battle\r\nToy World Battle\r\nBlock Fort Battle"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

