---
title: 'Drift Unleashed'
titleoverride: true
date: '27-05-2020 18:00'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://re-volt.io/events/2020-05-25-1800'
    tracklist: "1. Toy World 2\r\n2. Supermarket 2\r\n3. Smashride Circuit\r\n4. Radioactive Garden\r\n5. MKDD - Mushroom City\r\n6. L.A. Smash\r\n7. Donut Plains 3\r\n8. Penny Racers - Harbor\r\n9. Christmas Snow Globe\r\n10. Sunset Hills\r\n11. The Bunker\r\n12. Mysterious Toy-Volt Factory 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Drift Unleashed'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

i will be hosting a Drift Unleashed session on Monday, 25 May at 18 UTC.
we will be racing with all of I/O drift cars, with an addition of few select RVZ drift cars.
we will be racing on stock, I/O main and I/O bonus tracks, for 3 laps, in Arcade mode, with pickups disabled.

due to the non-standard nature of this session, intentional wallbangs are tolerated.
however still, please try to avoid wallriding as much as possible.

---
cars:
- Easy:    Drift Harvester
- Easy:    Night Rider Drift      (from 'oldcars' folder, taken from newest Night Rider from RVZ)
- Easy:    Drift_Volken E36
- Med:    Monster Drift
- Med:    Drift D15
- Med:    Destroyer
- Med:    Cy Drift
- Hard:    Most Wanted Drift
- Hard:    Netheria Drifter
- Quirk:   Sai Drift
- I/O   :    all of I/O drift cars is allowed, D1GP, D2GP and DXGP (https://re-volt.io/online/cars/drift)

---
RVZ drift cars    : [https://www.dropbox.com/s/bijzxvkyfr5mrcv/underdogdriftingzoo.zip?dl=1](https://www.dropbox.com/s/bijzxvkyfr5mrcv/underdogdriftingzoo.zip?dl=1)

I/O drift cars     : [https://files.re-volt.io/packs/drift_cars.7z](https://files.re-volt.io/packs/drift_cars.7z)

I/O main track  : [https://distribute.re-volt.io/packs/io_tracks.zip](https://distribute.re-volt.io/packs/io_tracks.zip)

I/O bonus track: [https://distribute.re-volt.io/packs/io_tracks_bonus.zip](https://distribute.re-volt.io/packs/io_tracks_bonus.zip)