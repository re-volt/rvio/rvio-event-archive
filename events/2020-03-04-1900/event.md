---
title: 'PRV Rookie Cars'
titleoverride: true
date: '04-03-2020 19:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Rookie Cars'
    classlink: 'https://re-volt.io/events/2020-03-04-1900'
    tracklist: "Toys in the Hood 1\r\nSuperMarket 2\r\nMuseum 2\r\nBotanical Garden\r\nRooftops\r\nToy World 1\r\nGhost Town 1\r\nToy World 2\r\nToys in the Hood 2\r\nToytanic 1\r\nMuseum 1\r\nSuperMarket 1\r\nGhost Town 2\r\nToytanic 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Note: The tracklist will be played twice, if you want to join more sessions like these, suggest visiting our server [Discord](https://discord.gg/cgHb4Wr)

### Car Selection
* Dust Mite
* Phat Slug
* Col. Moss
* Harvester
