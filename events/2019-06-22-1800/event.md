---
title: 'Amateur Races'
titleoverride: false
date: '22-06-2019 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Snowy River\r\nToy World 1\r\nToys in the Hood 2 (R)\r\nRadioactive Garden (M)\r\nSakura\r\nPenny Racers - Caves\r\nLunar\r\nIndustry\r\nDonut Plains 3\r\nRooftops (R)\r\nSantorini\r\nMetro-Volt\r\nSpa-Volt 2 (R)\r\nToy World 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

