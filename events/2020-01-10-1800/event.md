---
title: '<small>Long Journey: Awakening (Day 2)</small>'
titleoverride: true
date: '10-01-2020 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "The Bunker\r\nSantorini\r\nToys in the Hood 1 (M)\r\nRe-Ville\r\nBotanical Garden\r\nHoliday Camp - California Edition\r\nPenny Racers - Harbour\r\nSpa-Volt 1 (M)\r\nCliffside\r\nBiohazard Factory\r\nToy World 2 (RM)\r\nToy World Mayhem\r\nKadish Sprint\r\nHelios\r\nRadioactive Garden\r\nToytanic 2\r\nSwan Street\r\nDonut Plains 3\r\nPenny Racers - Caves\r\nSuperMarket 2 (M)\r\nSkating Toys (RM)\r\nQuake!\r\nFool's Mate 2\r\nRanch (R)\r\nMuseum 1\r\nGhost Town 2\r\nBlood on the Rooftops\r\nSakura (M)\r\nMoon Dawn (RM)"
    vod: 'https://www.youtube.com/watch?v=ch4FAPHZqfg'
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2020-01-10_19-00-04.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Long Journey: Awakening (Day 2)'
twitterenable: true
twittercardoptions: summary
articleenabled: false
article:
    headline: 'Long Journey: Awakening (Day 2)'
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](https://re-volt.io/user/pages/events/2020-01-08-1800/Awekaning.png)