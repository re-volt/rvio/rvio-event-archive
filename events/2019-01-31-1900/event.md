---
title: 'Pro Races'
titleoverride: false
date: '31-01-2019 19:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Holiday Camp - California Edition (R)\r\nGhost Town 2\r\nSakura\r\nIllusion\r\nRadioactive Garden\r\nMolten Caverns (R)\r\nRanch\r\nYABBA DABBA DOO!\r\nThe Felling Yard\r\nBiohazard Factory\r\nToys in the Hood 2 (R)\r\nDonut Plains 3\r\nPetroVolt\r\nSupermarket 1\r\nThe Gorge\r\nMuseum 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

