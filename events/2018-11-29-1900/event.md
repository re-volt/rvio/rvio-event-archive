---
title: Pros
titleoverride: false
date: '29-11-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 1 (R)\r\nHoliday Camp: California Edition\r\nToytanic 2\r\nQuake! (R)\r\nSakura\r\nMetro-Volt\r\nRe-Ville\r\nThe Bunker\r\nBlood on the Rooftops\r\nCactus-Volt\r\nThe Felling Yard\r\nThe Gorge\r\nSuperMarket 2\r\nToy World Aquatica: Redux (R)\r\nSkating Toys\r\nMuseum 2"
    vod: 'https://www.youtube.com/watch?v=O5gU21p2Dfw'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-29_20-02-30.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

