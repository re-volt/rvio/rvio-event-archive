---
title: 'CC20 Premiere <small>Dual Lobby</small>'
titleoverride: true
date: '06-12-2020 18:00'
event:
    host: 'URV & Floxit'
    ip: 'u.rv.gl OR flo.rv.gl'
    host2: O
    category: tourney
    mode: race
    class: other
    otherclass: 'CC20 Cars'
    classlink: 'https://re-volt.io/events/2020-12-06-1800'
    tracklist: "Christmas Snow Globe — 5 laps\r\nSnowy River — 4 laps\r\nArctic Air — 2 laps\r\nMK64: Sherbet Land — 4 laps\r\nWonderful Skylands 2 — 3 laps\r\nZero Degrees — 2 laps\r\nCandyland — 3 laps\r\nGlacier Cliffs 3 — 7 laps\r\nPR: Caverns — 6 laps\r\nPOD: Spin — 2 laps\r\nFrozen Caverns — 2 laps\r\nSnowland 1 — 5 laps\r\nR2049 Tundra — 5 laps\r\nChilled to the Bone — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'CC20 Premiere'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

[![](https://files.re-volt.io/projects/rvcc/rvcc20.png)](https://re-volt.io/blog/christmas-championship-2020)

We will be racing each car in order, culminating in a final race where everyone can choose their favourite!

### Requirements
* [CC20 Cars](https://files.re-volt.io/packs/christmas/2020/rvcc20_cars.7z)
* [CC20 Premiere Tracks](https://files.re-volt.io/packs/christmas/2020/rvcc20_tracks_premiere.7z)