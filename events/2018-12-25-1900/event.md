---
title: 'Pro Slugs'
titleoverride: false
date: '25-12-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: prugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Rooftops\r\nSnowy River\r\nPetroVolt\r\nFreestoyle 2\r\nFool's Mate 2\r\nMuseum 1\r\nPenny Racers - Caves\r\nSpa-Volt 1\r\nRanch (R)\r\nLunar\r\nThe Felling Yard\r\nBiohazard Factory\r\nToytanic 2\r\nIllusion (R)\r\nSuperMarket 1 (R)"
    vod: 'https://www.youtube.com/watch?v=3UdglY-3M9A'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-25_21-03-26.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

