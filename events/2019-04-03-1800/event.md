---
title: 'Super Pro Races'
titleoverride: false
date: '03-04-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "RV Temple\r\nHoliday Camp: California Edition\r\nMolten Caverns\r\nFiddlers on the Roof\r\nMoon Dawn (R)\r\nRanch (R)\r\nKadish Sprint\r\nSpa-Volt 2\r\nHelios\r\nJailhouse Rock\r\nVenice\r\nIllusion\r\nQuake!\r\nMuseum 1 (R)"
    results: 'https://online.re-volt.io/sessions/results.php?file=competitive/session_2019-04-03_20-59-54.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

[**Super Pros**](https://distribute.re-volt.io/packs/io_superpros.zip) are required.