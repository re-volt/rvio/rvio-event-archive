---
title: 'Rookie Races'
titleoverride: false
date: '18-07-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 2\r\nToytanic 2 (R)\r\nSuperMarket 2\r\nSuperMarket 1\r\nLunar\r\nVenice (R)\r\nJailhouse Rock\r\nQuake!\r\nDonut Plains 3\r\nThe Bunker\r\nPenny Racers - Harbour\r\nRe-Ville\r\nWhite Rose Chapel (M)\r\nToySoldierz (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

