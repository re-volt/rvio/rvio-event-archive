---
title: 'Normal Rookie Races'
titleoverride: true
date: '23-12-2019 19:00'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Normal Rookie Cars'
    classlink: 'https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0'
    tracklist: "Belgium\r\nGhost Town 2 RM\r\nToys in the Hood Ex R\r\nSwan Street R\r\nSnowy River M\r\nSupermarket 1\r\nSakura R\r\nVenice R\r\nTerminus M\r\nToys in the Hood 2 R\r\nPenny Racers Caves R\r\nToy World 2 RM\r\nMysterious Toy-Factory R\r\nIllusion R\r\nPalm Marsh R\r\nDaydreaming\r\nSpa-Volt 2 R\r\nDonut Plains 3 RM\r\nKadish Sprint  M\r\nFiddlers on the Roof M\r\nBlood on the Rooftops M\r\nMetro-Volt\r\nSupermarket 2 R\r\nToytanic 2 RM\r\nAvalon\r\nTerminus R\r\nMysterious Toy-Factory M\r\nComputer Virus 2 M\r\nMuseum 1 R\r\nRadioactive Garden R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous average prestige results, make sure to [check this](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Old IO track pack](https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1) (needed)

### Car Selection
* All Rookie rated cars and [I/O Rookie] cars from stock, DC, normal and bonus car pack (except Nano Rascal)