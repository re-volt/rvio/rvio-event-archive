---
title: 'Cougar Exclusive (Ladder)'
titleoverride: false
date: '10-04-2019 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Cougar Exclusive (Ladder)'
    classlink: 'https://re-volt.io/events/2019-04-10-1800'
    tracklist: "Toys in the Hood 1\r\nMuseum 2 (RM)\r\nBotanical Garden\r\nGhost Town 1 (R)\r\nToy World 2\r\nToytanic 1\r\nToys in the Hood 2\r\nMuseum 1 (M)\r\nSupermarket 1\r\nGhost Town 2"
published: true
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
article:
    datePublished: '19-03-2019 20:55'
    dateModified: '19-03-2019 20:55'
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event will count for the **RVGL Ladder**.

* [Current Ladder standings](https://ladder.rv.gl)
* [Information & Rules](https://ladder.rv.gl/info)

You need the **RVGL Plus Pack** to join the races!

* [Download](https://kiwis.rv.gl/plus)
* [RVGL Plus Car Classes](https://kiwis.rv.gl/plus/cars)
* [RVGL Plus Track Categories](https://kiwis.rv.gl/plus/tracks)