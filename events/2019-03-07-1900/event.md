---
title: 'Amateur Races'
titleoverride: false
date: '07-03-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Blood on the Rooftops\r\nToys in the Hood 2\r\nToy World 2 (R)\r\nJailhouse Rock\r\nSwan Street (R)\r\nHelios\r\nMuseum 2\r\nGrisville\r\nPenny Racers - Caves\r\nToy World Mayhem\r\nFreestoyle 2\r\nGhost Town 1\r\nHoliday Camp: California Edition (R)\r\nThe Bunker"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

