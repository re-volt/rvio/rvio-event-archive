---
title: 'Rare Cars'
titleoverride: false
date: '05-08-2019 18:00'
event:
    host: hajducsekb
    ip: hb.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Rare Cars'
    classlink: 'https://re-volt.io/events/2019-08-05-1800'
    tracklist: "Fool's Mate 2\r\nMuseum 1 (R)\r\nSpa-Volt 1\r\nToy World 1\r\nSantorini\r\nPetro-Volt\r\nSkating Toys\r\nRe-Ville\r\nKadish Sprint\r\nIndustry (M)\r\nMuseum 2\r\nThe Bunker\r\nHoliday Camp: California Edition (R)\r\nVenice\r\nToytanic 2\r\nToySoldierz (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

A special Pro event that only allows the least used cars. Based on [this forum thread](https://forum.re-volt.io/viewtopic.php?f=20&t=1024).

#### Car Selection
* Acclaim F1
* Eaglet
* G3X
* Gear GT
* Panga
* S13 Alltune
* Sater XL
* Shining Fairy
* Sir Gleam
* TT Raider
* Zoondiac