---
title: 'Semi-Pro Races'
titleoverride: true
date: '16-11-2019 19:00'
event:
    host: 'URV & Kirioso'
    ip: 'u.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Overground\r\nIllusion (R)\r\nMuseum 2 (R)\r\nMuseum 1\r\nRooftop Chase: Redux\r\nJailhouse Rock (R)\r\nBotanical Garden\r\nYABBA DABBA DOO!\r\nSupermarket 2\r\nToy World Mayhem\r\nThe Bunker\r\nThe Felling Yard (M)\r\nLunar\r\nGrisville\r\nHoliday Camp: California Edition"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

