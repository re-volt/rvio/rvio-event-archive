---
title: '20th Anniversary Special'
titleoverride: false
date: '31-07-2019 17:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: race
    class: other
    otherclass: '20th Anniversary Special'
    classlink: 'https://re-volt.io/events/2019-07-31-1700'
    tracklist: "Museum 1 (R)\r\nToytanic 2 \r\nRooftops (M)\r\nGhost Town 2 (RM)\r\nToytanic 1 (M)\r\nToys in the Hood 1\r\nMuseum 2 (M)\r\nSuperMarket 2 (R)\r\nGhost Town 1\r\nSuperMarket 1 (M)\r\nToy World 1 (RM)\r\nToys in the Hood (R)\r\nToy World 1 (R)\r\nBotanical Garden (M)"
    vod: 'https://www.youtube.com/watch?v=DyLXJmhMTNU'
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2019-07-31_20-00-07.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

A special event organised by Geromu in celebration of the 20th Re-Volt Anniversary. We will be racing 5 laps on each track, and there will be two different lobbies.

### Rules
- **Only stock content will be used this session.** You may choose any car as long as it is a [stock car](https://revolt.fandom.com/wiki/Category:Standard_Cars#List_of_cars), including [DC content](https://revolt.fandom.com/wiki/Category:Console_Cars#List_of_cars). Clockworks, Probe UFO, Trolley, Mystery or any custom cars are **not** allowed.
- **Every player has to race with a unique car**, meaning that you may not choose a car that has been already chosen by a player before you.
- **It is mandatory to use the default skins.**