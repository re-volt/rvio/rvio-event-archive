---
title: 'Rookie Races'
titleoverride: true
date: '16-06-2020 20:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Hull Breach 3000 — 3 laps\r\nToy World 1 — 5 laps\r\nSupermarket 1 — 4 laps\r\nCliffside — 5 laps\r\nToytanic 2 (R) — 3 laps\r\nMoon Dawn — 2 laps\r\nIndustry — 2 laps\r\nSakura (R) — 2 laps\r\nPetroVolt — 2 laps\r\nVenice — 3 laps\r\nAMCO TT — 3 laps\r\nSwan Street (R) — 3 laps\r\nRooftop Chase: Redux — 3 laps\r\nGhost Town 2 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

