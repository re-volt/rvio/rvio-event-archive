---
title: 'Last Man Standing'
titleoverride: true
date: '04-07-2020 19:15'
event:
    host: Santi
    ip: santi.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'see event'
    classlink: 'https://re-volt.io/events/2020-07-04-1915'
    packages:
        - io_lmstag
        - io_cars
    tracklist: "LMS InfernHell Haze\r\nLMS (To)Shooting Stars\r\nLMS Road of Simplicity\r\nLMS Transformers Arena\r\nLMS Chinatown\r\nLMS Planet\r\nLMS Ouija\r\nLMS Skyblock\r\nLMS Valley\r\nLMS Toshinden Mona\r\nLMS Lego 4\r\nLMS Arena\r\nLMS Rooftops\r\nLMS Battle Platform 1\r\nLMS Temple 2\r\nLMS Soul Tosh\r\nLMS Toshinden Arena\r\nLMS Toytanic"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Last Man Standing'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

The following cars will be allowed: Bumblebee, LR 64, Kanaberra Kruiser, Sprinter XL, R6, Mite & AMW.

---
- The limit of a round is 5 minutes. If multiple people survive, it's a tie.
- Don't camp! (Penalty: kicking)
- Please don't abuse lag. If you teleport too much, you may be kicked.
- On any LMS map, reposition should not be used until you are eliminated.
- LMS Planet exclusive rule: because there are multiple levels, round time is 10 minutes. If you are the last one on a platform, you have to go down. (Next platform>Stage)