---
title: 'Amateur Races'
titleoverride: true
date: '15-07-2020 20:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Rooftops — 3 laps\r\nWhite Rose Chapel — 2 laps\r\nSakura (R) — 2 laps\r\nRooftop Chase: Redux — 3 laps\r\nSpa-Volt 1 — 3 laps\r\nMetro-Volt — 3 laps\r\nRoute-77 — 3 laps\r\nMuseum 1 — 2 laps\r\nHull Breach 3000 — 3 laps\r\nAMCO TT — 3 laps\r\nToy World Aquatica: Redux (R) — 3 laps\r\nToytanic 2 — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nToys in the Hood 2 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

