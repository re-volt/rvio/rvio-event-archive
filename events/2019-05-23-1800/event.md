---
title: 'Advanced Races'
titleoverride: false
date: '23-05-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Fool's Mate 2\r\nDonut Plains 3 (R)\r\nToySoldierz\r\nPetroVolt\r\nSpa-Volt 2 (M)\r\nRV Temple\r\nGhost Town 2\r\nRooftops\r\nSpa-Volt 1 (R)\r\nRe-Ville\r\nJailhouse Rock\r\nSuperMarket 2\r\nThe Bunker\r\nMuseum 1 (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

