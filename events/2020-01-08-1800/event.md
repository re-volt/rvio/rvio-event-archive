---
title: '<small>Long Journey: Awakening (Day 1)</small>'
titleoverride: true
date: '08-01-2020 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "White Rose Chapel\r\nToy World Aquatica: Redux\r\nToytanic 1 (R)\r\nSnowy River\r\nSpa-Volt 2 \r\nMetro-Volt (M)\r\nOverground\r\nPetroVolt\r\nToys in the Hood 2 (RM)\r\nRV Temple\r\nVenice (RM)\r\nMolten Caverns\r\nToy World 1\r\nJailhouse Rock\r\nThe Felling Yard\r\nRooftop Chase: Redux\r\nSuperMarket 1\r\nAMCO TT (M)\r\nFiddlers on the Roof\r\nToySoldierz\r\nMuseum 2\r\nIndustry\r\nHull Breach 3000\r\nRooftops\r\nGrisville (R)\r\nGhost Town 1\r\nLunar\r\nRoute-77 (M)\r\nSnowland 1"
media_order: Awekaning.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Long Journey: Awakening (Day 1)'
twitterenable: true
twittercardoptions: summary
articleenabled: false
article:
    headline: 'Long Journey: Awakening (Day 1)'
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](Awekaning.png)