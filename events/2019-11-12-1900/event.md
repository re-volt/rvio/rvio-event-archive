---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '12-11-2019 19:00'
event:
    host: 'URV & Narusori'
    ip: 'u.rv.gl OR sas.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Metro-Volt\r\nBiohazard Factory\r\nPenny Racers - Harbour\r\nGhost Town 2\r\nSpa-Volt 1 (R)\r\nSnowy River (R)\r\nSupermarket 1\r\nToytanic 2\r\nRadioactive Garden\r\nFool's Mate 2\r\nHelios\r\nToys in the Hood 2 (R)\r\nRanch\r\nSakura (M)\r\nToy World Aquatica: Redux"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event will be split into two different lobbies. The player limit will be set to 12 and will only be raised as needed if both lobbies reach the limit.

**Lobby #1 IP:** `u.rv.gl`<br>
**Lobby #2 IP:** `sas.rv.gl`