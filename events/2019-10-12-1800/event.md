---
title: 'Semi-Pro Races'
titleoverride: false
date: '12-10-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 1\r\nPetroVolt\r\nRooftop Chase: Redux\r\nMolten Caverns\r\nVenice\r\nOverground\r\nToy World 2\r\nMuseum 2 (M)(R)\r\nAMCO TT\r\nRe-Ville\r\nSantorini (R)\r\nRanch\r\nToys in the Hood 2\r\nToy World Mayhem (R)\r\nRadioactive Garden"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

