---
title: 'Amateur Races'
titleoverride: false
date: '07-02-2019 19:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Quake!\r\nRooftops\r\nGrisville\r\nSakura (R)\r\nToys in the Hood 2\r\nLunar\r\nToytanic 1 (R)\r\nDonut Plains 3\r\nSwan Street (R)\r\nSpa-Volt 1\r\nToy World Aquatica: Redux\r\nGhost Town 1\r\nSnowy River\r\nMeltdown"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

