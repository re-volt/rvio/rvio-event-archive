---
title: Pros
titleoverride: false
date: '20-11-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "AMCO TT\r\nToy World 1\r\nToys in the Hood 2\r\nPetroVolt\r\nPenny Racers - Harbour\r\nCactus-Volt\r\nMK64 Wario Stadium\r\nMolten Caverns\r\nHelios\r\nBiohazard Factory (R)\r\nCliffside\r\nRanch\r\nGrisville\r\nQuake!\r\nToytanic 2 (R)\r\nToys in the Hood 1"
    vod: 'https://www.youtube.com/watch?v=pr1YIJdS0Y8'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-20_21-03-43.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

