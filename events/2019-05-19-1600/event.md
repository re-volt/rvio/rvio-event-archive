---
title: 'Toyeca vs. Humma (Ladder)'
titleoverride: false
date: '16:00 19-05-2019'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Toyeca vs. Humma (Ladder)'
    classlink: 'https://re-volt.io/events/2019-05-19-1600'
    tracklist: 'Random Tracks'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event will count for the **RVGL Ladder**.

* [Current Ladder ranking](https://ladder.rv.gl/ranking)
* [Information & Rules](https://ladder.rv.gl/info)

You need the **I/O Track- and Carpacks** to join the races!

* [Ladder's Car Pool](https://ladder.rv.gl/info/cars)
* [Ladder's Track Pool](https://ladder.rv.gl/info/tracks)