---
title: Pros
titleoverride: false
date: '15-11-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Botanical Garden\r\nHelios \r\nMuseum 1 (R)\r\nRadioactive Garden\r\nJailhouse Rock\r\nHoliday Camp: California Edition\r\nPenny Racers - Harbour\r\nPenny Racers - Caves\r\nSwan Street\r\nSnowy River (R)\r\nToy World Aquatica: Redux\r\nThe Felling Yard\r\nToytanic 1\r\nRanch (R)\r\nToys in the Hood 1\r\nIllusion"
    vod: 'https://www.youtube.com/watch?v=_amlea92Rr8'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-15_20-03-15.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

