---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '25-01-2020 19:00'
event:
    host: 'Skarma & Delecto'
    ip: 'sk.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Blood on the Rooftops — 3 laps\r\nPenny Racers - Harbour — 3 laps\r\nPenny Racers - Caves — 4 laps\r\nPetroVolt — 2 laps\r\nToytanic 2 (R) — 3 laps\r\nToys in the Hood 2 — 3 laps\r\nSnowland 1 — 3 laps\r\nRoute-77 — 3 laps\r\nDonut Plains 3 (R) — 4 laps\r\nBotanical Garden — 5 laps\r\nToy World Mayhem (R) — 3 laps\r\nHull Breach 3000 — 3 laps\r\nJailhouse Rock (M) — 2 laps\r\nGhost Town 2 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

