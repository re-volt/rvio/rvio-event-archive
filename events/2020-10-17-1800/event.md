---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '17-10-2020 18:00'
event:
    host: 'URV & Floxit'
    ip: 'u.rv.gl OR flo.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Quake! — 2 laps\r\nMoon Dawn — 2 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nWhite Rose Chapel — 2 laps\r\nToys in the Hood 1 — 3 laps\r\nSpa-Volt 1 — 3 laps\r\nPetroVolt — 2 laps\r\nToySoldierz — 3 laps\r\nThe Felling Yard — 2 laps\r\nToys in the Hood 2 — 3 laps\r\nSupermarket 2 (R) — 7 laps\r\nMuseum 3 — 3 laps\r\nToy World 2 — 4 laps\r\nRooftop Chase: Redux (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

