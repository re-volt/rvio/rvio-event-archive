---
title: 'Online Gold Cup'
titleoverride: true
date: '06-03-2020 19:00'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Online Gold Cup'
    classlink: 'https://re-volt.io/events/2020-03-04-1900'
    tracklist: "Toys in the Hood 2\r\nToy World 1 M\r\nToytanic 1\r\nMuseum 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Online Gold Cup'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 4 cup standings and individual player progress, be sure to [check this](https://www.dropbox.com/s/qfzn2vwewz114d7/season%204%20cup%20session%20info.txt?dl=0).

### Requirements
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Extra Old I/O Cars](https://www.dropbox.com/sh/qqkplevr9df6l3a/AACsyvafB1QkhnU_GvtULqFJa?dl=1) (advised)

### Car Selection
* Dozzan
* Lv 54
* Misterio
* Mr. Bob
* Rg1
* Rotor
* Siri