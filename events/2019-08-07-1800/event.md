---
title: 'Best 4 Laps'
titleoverride: false
date: '07-08-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Month Tracks'
    classlink: 'https://re-volt.io/events/2019-08-07-1800'
    tracklist: "8 on Ice\r\nHello Kitty\r\nHotel Retro\r\nHull Breach 3000\r\nRosedale\r\nRV Temple\r\nSpa-Volt 1\r\nStagnaro"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Only stock Pro cars are allowed, excluding DC cars.** Results will be posted on [Re-Volt Race](https://www.revoltrace.net/) if you have an account there. Each track will be raced twice in a row.

#### Requirements:
* [Tracks of the Month](https://files.re-volt.io/packs/month_tracks.zip)