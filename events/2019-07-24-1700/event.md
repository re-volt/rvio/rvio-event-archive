---
title: 'Drift Races'
titleoverride: false
date: '24-07-2019 17:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars (D1GP)'
    classlink: 'https://re-volt.io/events/2019-07-24-1800'
    tracklist: "Night City\r\nFoxglen\r\nEggland\r\nLS Kart Space I\r\nAMCO Bitume\r\nTouge Drift\r\nAMCO Driftume\r\nRV-Simo On-Road Track\r\nDrivers School\r\nWarehouse Showdown\r\nMelville\r\nTVGP Bitume"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only cars with the **D1GP** prefix are allowed.

### Requirements
* [Drift Cars](https://files.re-volt.io/packs/drift_cars.7z)
* [Circuit Tracks](https://files.re-volt.io/packs/circuit_tracks.zip)

The contents of those packs are listed [here](https://re-volt.io/online/other-content).