---
title: 'R.I.P. Muscle Memory'
titleoverride: true
date: '09-06-2020 17:00'
event:
    host: Shara
    ip: 139.192.218.169
    category: casual
    mode: race
    class: other
    otherclass: 'RVZ Pro Cars'
    classlink: 'https://re-volt.io/events/2020-06-09-1700'
    tracklist: "i will be hosting a **R.I.P. Muscle Memory** session on **Tuesday, 9 June** at **17 UTC**\r\nwe will be racing with different incarnations of familiar cars, in Pro rating.\r\nwe will be racing on stock and I/O main tracks, some of them mirrorred, some of them are not.\r\nraces are for 3 laps, in Console mode (for added rip muscle memory), with pickups enabled.\r\n\r\n---\r\ncars:\r\n- Airsonic\r\n- Baja Grudge\r\n- Bi-Infection\r\n- Centaur\r\n- LP440\r\n- RG0\r\n- R.I.P.\r\n- Ronin\r\n- Seibu Keisatsu\r\n- Skyedge S\r\n- The Mudman\r\n- Yager\r\n\r\n---\r\n**Content Download**: [https://www.dropbox.com/s/obnidazitcq0wkn/altfamiliarcars.zip?dl=1][https://www.dropbox.com/s/obnidazitcq0wkn/altfamiliarcars.zip?dl=1]\r\nI/O Main Tracks     : [https://distribute.re-volt.io/packs/io_tracks.zip][https://distribute.re-volt.io/packs/io_tracks.zip]"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'R.I.P. Muscle Memory'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

1. Toys in the Hood 1 (R)
2. Toy World 1 (M)
3. Ghost Town 1 (M)
4. Sakura
5. Toy World 1 (R)
6. Jailhouse Rock (M)
7. ToySoldierz (R)
8. Images of Giza: Redux (R)(M)
9. Meltdown
10. Toys in the Hood 1 (M)