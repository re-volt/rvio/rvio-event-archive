---
title: 'Drift Races'
titleoverride: false
date: '21-10-2019 18:00'
event:
    host: Saffron
    ip: saff.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://re-volt.io/events/2019-10-21-1800'
    tracklist: "Touge Mountain - Stage 1\r\nBerm Lake Raceway\r\nStagnaro\r\nSideways Sanctuary B\r\nClubman Stage Route 5 (R)\r\nLS Tama Valley Route B\r\nRV-Simo On-Road Track\r\nSpecial Stage Route 5\r\nFoxglen\r\nAMCO Driftume\r\nLS Kart Space I\r\nTouge Mountain - Stage 2 (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

### Requirements
* [Drift Cars](https://files.re-volt.io/packs/drift_cars.7z)
* [Circuit Tracks](https://files.re-volt.io/packs/circuit_tracks.zip)