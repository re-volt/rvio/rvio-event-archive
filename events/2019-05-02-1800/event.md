---
title: 'Semi-Pro Races'
titleoverride: false
date: '02-05-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Quake!(R)\r\nMoon Dawn\r\nRadioactive Garden\r\nBiohazard Factory\r\nToys in the Hood 2 (R)\r\nRanch \r\nThe Felling Yard\r\nToytanic 1\r\nOverground\r\nSpa-Volt 1\r\nSupermarket 1\r\nFool's Mate 2\r\nPetroVolt\r\nIllusion (R)\r\nToy World 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

