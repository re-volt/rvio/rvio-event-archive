---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '04-07-2020 18:00'
event:
    host: 'Etneus & Ashen Forest'
    ip: 'etn.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Overground — 2 laps\r\n    Toytanic 2 (R) — 3 laps\r\n    Lunar — 3 laps\r\n    Swan Street — 3 laps\r\n    Cliffside — 5 laps\r\n    Holiday Camp: California Edition — 2 laps\r\n    Ghost Town 2 — 4 laps\r\n    Santorini (R) — 4 laps\r\n    Industry — 2 laps\r\n    Toys in the Hood 1 — 3 laps\r\n    Rooftops — 3 laps\r\n    Toy World Mayhem (R) — 3 laps\r\n    Sakura — 2 laps\r\n    Snowland 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

