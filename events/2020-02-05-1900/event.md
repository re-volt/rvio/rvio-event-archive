---
title: 'Online Platinum Cup'
titleoverride: true
date: '05-02-2020 19:00'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Online Platinum Cup'
    classlink: 'https://re-volt.io/events/2020-02-05-1900'
    tracklist: "Supermarket 1\r\nGhost Town 2\r\nToy Worls 1 RM\r\nMuseum 1 M\r\nToytanic 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Online Platinum Cup'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 3 cup standings and individual player progress, be sure to [check this](https://www.dropbox.com/s/x8ffi2o516r8xju/season%203%20cup%20session%20info.txt?dl=0).

### Requirements
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Extra Old Io Cars](https://www.dropbox.com/sh/qqkplevr9df6l3a/AACsyvafB1QkhnU_GvtULqFJa?dl=1) (advised)

### Car Selection
* Acclaim F1
* Aeroflash
* Amw
* Gear Gt
* Oval Runner
* Panga
* Tribute