---
title: 'Rookie Races'
titleoverride: false
date: '28-04-2020 18:00'
event:
    host: Narusori
    ip: naru.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Supermarket 1 — 4 laps\r\nHelios — 3 laps\r\nRanch — 2 laps\r\nBotanical Garden R — 5 laps\r\nMuseum 2 — 3 laps\r\nSmashride Circuit — 2 laps\r\nJailhouse Rock R — 2 laps\r\nRooftop Chase: Redux — 3 laps\r\nAMCO TT R — 3 laps\r\nSakura — 2 laps\r\nHull Breach 3000 — 3 laps\r\nFools Mate 2 — 3 laps\r\nToytanic 2 — 3 laps\r\nRadioactive Garden — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

