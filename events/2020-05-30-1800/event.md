---
title: 'Super Pro Races'
titleoverride: true
date: '30-05-2020 18:00'
event:
    host: 'Delecto & Etneus'
    ip: 'dele.rv.gl OR etn.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Smashride Circuit — 3 laps\r\nPetroVolt — 2 laps\r\nMetro-Volt — 3 laps\r\nToys in the Hood 2 — 4 laps\r\nToytanic 2 (R) — 3 laps\r\nRooftops — 4 laps\r\nRooftop Chase: Redux — 3 laps\r\nLunar — 3 laps\r\nCliffside — 6 laps\r\nRoute-77 — 4 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nBiohazard Factory (R) — 3 laps\r\nMuseum 1 — 3 laps\r\nToySoldierz — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

