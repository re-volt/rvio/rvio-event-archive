---
title: 'RC Bandit Exclusive'
titleoverride: false
date: '19-12-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'RC Bandit Exclusive'
    tracklist: "Toys In the Hood 1\r\nRanch (R)\r\nMuseum 2\r\nLunar (M)\r\nBotanical Garden\r\nVenice\r\nToy World 1 (R)\r\nJailhouse Rock (M)\r\nToys In the Hood 2 (R)\r\nPetroVolt\r\nMuseum 1 (M)\r\nSpa-Volt 1 (R)\r\nSuperMarket 1\r\nHoliday Camp: California Edition (RM)\r\nToytanic 2 (RM)"
    vod: 'https://www.youtube.com/watch?v=_pqQ8hYiDmU'
    results: 'https://online.re-volt.io/sessions/results.php?file=competitive/session_2018-12-19_20-04-40.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

