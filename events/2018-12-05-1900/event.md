---
title: 'Toyeca Exclusive'
titleoverride: false
date: '05-12-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Toyeca Exclusive'
    classlink: 'https://revolt.wikia.com/wiki/Toyeca'
    tracklist: "Toys in the Hood 1\r\nSuperMarket 2\r\nMuseum 2\r\nBotanical Garden\r\nRooftops\r\nToy World 1\r\nGhost Town 1\r\nToy World 2\r\nToys in the Hood 2\r\nToytanic 2\r\nMuseum 1\r\nSuperMarket 1\r\nGhost Town 2\r\nQuake!\r\nVenice\r\nHoliday Camp: California Edition\r\nPetroVolt\r\nMetro-Volt\r\nLunar\r\nSakura\r\nJailhouse Rock\r\nToy World Mayhem\r\nRanch\r\nSpa-Volt 1"
    vod: 'https://www.youtube.com/watch?v=5pdW6Tyr1zQ'
    results: 'https://online.re-volt.io/sessions/results.php?file=competitive/session_2018-12-05_21-02-24.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

You may only race with **Toyeca**.