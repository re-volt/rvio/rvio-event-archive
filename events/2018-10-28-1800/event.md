---
title: 'Halloween Carnival 2018'
titleoverride: false
date: '28-10-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Halloween Carnival'
    classlink: 'https://re-volt.io/blog/halloween-carnival-2018'
    tracklist: "Halloween 1\r\nTenebrosity\r\nLunar\r\nThe Felling Yard EXTREME\r\nMeltdown\r\nCake\r\nSpooky-Volt\r\nMikes Medievel Mayhem\r\nQuake!\r\nGhost Town 3\r\nFukushima\r\nRanch (R)\r\nKadish Sprint\r\nIllusion (M)(R)"
    vod: 'https://www.twitch.tv/videos/328610170'
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2018-10-28_20-05-05.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event requires the [Halloween Carnival](https://files.re-volt.io/packs/halloween/2018/full.7z) custom content pack.