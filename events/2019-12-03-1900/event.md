---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '03-12-2019 19:00'
event:
    host: 'Saffron & Narusori'
    ip: 'saff.rv.gl OR sas.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Grisville\r\nMoon Dawn\r\nToy World 1\r\nMolten Caverns (R)\r\nRooftop Chase: Redux (M)\r\nHull Breach 3000\r\nMuseum 2\r\nRooftops\r\nToySoldierz\r\nJailhouse Rock (R)\r\nToytanic 1 (R)\r\nMetro-Volt\r\nSwan Street\r\nSnowy River\r\nSpa-Volt 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

