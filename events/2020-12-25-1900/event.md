---
title: '<small>Christmas Special (Dual Lobby)</small>'
titleoverride: true
date: '25-12-2020 19:00'
event:
    host: 'URV & OhNej'
    ip: 'u.rv.gl OR ohnej.rv.gl'
    category: tourney
    mode: race
    class: other
    otherclass: 'Christmas Special'
    classlink: 'https://re-volt.io/events/2020-12-25-1900'
    tracklist: "Toy World EX — 3 laps (Fairylight)\r\nToytanic 1 — 3 laps (Fireplace)\r\nToy World 1 — 6 laps (Revenge Sled)\r\nMisty Valley — 2 laps (Rodolph Sleigh)\r\nSynthwave — 4 laps (Winterise)\r\nMysterious Toy-Volt Factory 2 — 2 laps (XMAS 2020)\r\nWinter Night — 4 laps\r\nWinter Fest — 3 laps\r\nFrostyZ Rally — 4 laps\r\nSnow Pass — 2 laps\r\nGlacier Cliffs 4 — 2 laps\r\nSBX Alpine — 4 laps\r\nAspenside — 6 laps\r\nTetris Festival — 4 laps"
media_order: rvcc20_poster7.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Christmas Special'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](rvcc20_poster7.png)
A special event where we'll be racing 5 brand new cars, with XMAS 2020 making a guest appearance! We'll be racing each car in order for the first 6 races, but everyone will be free to choose their car for the remaining 8 tracks.

### Requirements
* [CC20 Cars](https://files.re-volt.io/packs/christmas/2020/rvcc20_cars.7z)
* [CC20 Tracks](https://files.re-volt.io/packs/christmas/2020/rvcc20_tracks.7z)

!! **Important:** The packs have been updated! If you downloaded them before, you can just get the [Update Package](https://files.re-volt.io/packs/christmas/2020/rvcc20_update.7z) instead for the new content.

### Choose your car
![](carselect.png)