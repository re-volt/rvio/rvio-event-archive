---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '11-01-2020 19:00'
event:
    host: 'Dvark & URV'
    ip: 'dv.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Metro-Volt — 3 laps\r\nSnowy River — 4 laps\r\nPenny Racers - Harbour (M) — 3 laps\r\nMuseum 2 (R) — 4 laps\r\nPetroVolt (R) — 2 laps\r\nToy World Mayhem — 4 laps\r\nRadioactive Garden (R) — 5 laps\r\nGhost Town 1 — 6 laps\r\nSakura — 3 laps\r\nToy World 2 — 5 laps\r\nMuseum 1 — 3 laps\r\nMolten Caverns — 2 laps\r\nMoon Dawn — 2 laps\r\nDonut Plains 3 — 5 laps\r\nFiddlers on the Roof — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

