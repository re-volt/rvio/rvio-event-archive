---
title: 'Super Pro Races'
titleoverride: true
date: '21-06-2020 18:00'
event:
    host: Kirioso
    ip: kiri.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Swan Street — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nRoute-77 — 4 laps\r\nPetroVolt (R) — 2 laps\r\nSpa-Volt 1 — 3 laps\r\nMoon Dawn (R) — 2 laps\r\nSantorini — 4 laps\r\nToy World 1 — 6 laps\r\nMetro-Volt — 3 laps\r\nToy World 2 — 5 laps\r\nImages of Giza: Redux — 5 laps\r\nGhost Town 2 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

