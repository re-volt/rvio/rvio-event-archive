---
title: 'Santa''s Team Battles'
titleoverride: true
date: '28-12-2020 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: battle-tag
    class: other
    otherclass: 'Rodolph Sleigh'
    classlink: 'https://re-volt.io/events/2020-12-28-1900'
    tracklist: "Christmas Globe Battle\r\nFrozen Pit\r\nThe Arena\r\nR2049 Tundra Battle\r\nR2049 Atomic Battle\r\nR2049 Melee Battle\r\nR2049 Stadium Battle\r\nBlock Fort\r\nGBA Battle Course 3\r\nSNES Battle Course 4\r\nMuseum Battle (in case of a tie)"
media_order: rvcc20_poster8.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Santa''s Team Battles'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](rvcc20_poster8.png)
A team-based event where only the Santas need to win! With one Santa on each team, they must rely on their team members' help to win against the rival Santa. May the best team win!

### Rules
* There can only be one Santa car per team, with the other players having the role of Elves.
* The Elves must use **Rodolph Sleigh** with the correct color for their team.
* Each Santa's main goal is to win the battle against the rival Santa!
* Each Elf's main goal is to help their Santa win the battle!
* As an Elf, if you get the star, pass it on to your Santa as soon as you can. Take care not to run out of time!
* Once one of the Santas wins the battle, the winning team must allow the rival Santa to finish as well (this is for the results).
* You are not allowed to switch sides, unless the Host states it's needed for balancing purposes.
* At the end of each race, both Santas must assign the next Santa from someone on their team. It cannot be themselves or the previous Santa.
* Make sure to type `BLUE` or `RED` as a prefix in your name, depending on which team you join.

### Sign up
To reserve a spot for the race, join us on [Discord](https://discord.gg/NMT4Xdb) and obtain the Tourneys role from the `#roles` channel. Afterwards, go to `#cc20-chat` and type the following message:
```
@URV#9830 I'm joining Team Red/Blue
```
Make sure to only choose one team. Depending on how many players sign up for one team, you might not be able to join the team you want, so make sure to reserve a spot as soon as possible!

### Requirements
* [CC20 Battle Pack](https://files.re-volt.io/packs/christmas/2020/rvcc20_battles.7z) **(New!)**