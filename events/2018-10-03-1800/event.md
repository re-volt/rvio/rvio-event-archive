---
title: 'Random Cars'
titleoverride: false
date: '03-10-2018 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Cars'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "AMCO TT\r\nOverground\r\nToy World Mayhem\r\nBotanical Garden\r\nHelios\r\nToys in the Hood 2\r\nGhost Town 1\r\nGhost Town 2\r\nRanch\r\nSakura\r\nMuseum 1\r\nSuperMarket 1\r\nSpa-Volt 1\r\nPetroVolt"
    vod: 'https://www.youtube.com/watch?v=SFrv4XwwPzs'
    results: 'https://online.re-volt.io/sessions/results.php?file=competitive/session_2018-10-03_20-04-51.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Requirements:**
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip)
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)