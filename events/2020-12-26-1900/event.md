---
title: 'Rookie Races'
titleoverride: true
date: '26-12-2020 19:00'
event:
    host: 'Ashen Forest & URV'
    ip: 'ash.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World 2 — 4 laps\r\nQuake! (R) — 2 laps\r\nLunar — 3 laps\r\nRooftop Chase: Redux — 3 laps\r\nFools Mate 2 — 3 laps\r\nOverground — 2 laps\r\nCliffside — 5 laps\r\nSupermarket 1 (R) — 4 laps\r\nToytanic 2 — 3 laps\r\nMetro-Volt — 3 laps\r\nMeltdown — 4 laps\r\nGhost Town 2 — 4 laps\r\nSnowy River (R) — 3 laps\r\nSnowland 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

