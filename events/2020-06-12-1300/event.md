---
title: 'Amateur Races'
titleoverride: true
date: '12-06-2020 13:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Cliffside — 5 laps\r\nToytanic 2 — 3 laps\r\nMuseum 1 (R) — 2 laps\r\nMeltdown — 4 laps\r\nSupermarket 1 — 4 laps\r\nLunar — 3 laps\r\nSnowland 1 — 3 laps\r\nSwan Street — 3 laps\r\nJailhouse Rock (R) — 2 laps\r\nRV Temple — 2 laps\r\nGhost Town 2 — 4 laps\r\nMetro-Volt — 3 laps\r\nHull Breach 3000 — 3 laps\r\nRooftop Chase: Redux (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

