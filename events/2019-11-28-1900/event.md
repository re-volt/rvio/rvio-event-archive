---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '28-11-2019 19:00'
event:
    host: 'Skarma & Saffron'
    ip: 'sk.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Snowy River\r\nToy World 2 (R) (M)\r\nQuake! (R)\r\nSpa-Volt 1\r\nMetro-Volt\r\nToy World Mayhem\r\nDonut Plains 3 (R)\r\nToys in the Hood 2\r\nBlood on the Rooftops\r\nBiohazard Factory\r\nGhost Town 2\r\nMoon Dawn\r\nPenny Racers - Harbour\r\nToy World Aquatica: Redux\r\nToys in the Hood 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

