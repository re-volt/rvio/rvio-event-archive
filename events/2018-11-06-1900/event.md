---
title: Semi-Pros
titleoverride: false
date: '06-11-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1\r\nHelios \r\nRadioactive Garden\r\nSnowy River\r\nToytanic 1 (R)\r\nToy World 2\r\nHoliday Camp: California Edition\r\nSakura (R)\r\nVenice\r\nRooftop Chase\r\nDonut Plains 3\r\nCliffside\r\nAMCO TT (R)\r\nKadish Sprint\r\nRooftops"
    vod: 'https://www.youtube.com/watch?v=1dseKgsrH08'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-06_21-04-22.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

