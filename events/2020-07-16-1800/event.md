---
title: 'Rookie Races'
titleoverride: true
date: '16-07-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    The Felling Yard — 2 laps\r\n    Museum 1 (R) — 2 laps\r\n    Toy World Mayhem — 3 laps\r\n    Rooftops — 3 laps\r\n    Snowland 1 (R) — 3 laps\r\n    Biohazard Factory — 2 laps\r\n    Rooftops 1 — 2 laps\r\n    Lunar — 3 laps\r\n    Venice — 3 laps\r\n    Quake! (R) — 2 laps\r\n    Santorini — 4 laps\r\n    AMCO TT — 3 laps\r\n    Ghost Town 1 — 5 laps\r\n    Museum 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

