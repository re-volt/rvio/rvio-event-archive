---
title: 'Semi-Pro Races'
titleoverride: true
date: '13-05-2020 17:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_tracks
        - io_cars
    tracklist: "Toys in the Hood 2 — 4 laps\r\nSakura — 3 laps\r\nMuseum 2 — 4 laps\r\nFiddlers on the Roof: Redux — 3 laps\r\nSpa-Volt 1 — 3 laps\r\nSnowy River — 4 laps\r\nToy World 1 R — 5 laps\r\nRoute-77 — 4 laps\r\nJailhouse Rock — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nVenice R — 3 laps\r\nOverground — 2 laps\r\nRooftops — 3 laps\r\nGhost Town 2 R — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

