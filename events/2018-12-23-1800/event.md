---
title: 'Christmas Carnival (Battle Tag)'
titleoverride: false
date: '23-12-2018 18:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: unofficial
    mode: battle-tag
    class: other
    otherclass: 'Christmas Carnival Cars'
    classlink: 'https://files.re-volt.io/packs/christmas/2018/cars.7z'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Requirements:**
* [Christmas Carnival Car Pack](https://files.re-volt.io/packs/christmas/2018/cars.7z)
* [Track Pack](https://www.dropbox.com/s/hzfg4pgwevcmb6k/ChristmasTag.rar?dl=0)