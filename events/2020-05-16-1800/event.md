---
title: 'Pro Races'
titleoverride: true
date: '16-05-2020 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Route-77 — 4 laps\r\nSakura — 3 laps\r\nSupermarket 2 — 8 laps\r\nSmashride Circuit — 3 laps\r\nToytanic 2 — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nMuseum 2 — 4 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nHull Breach 3000 — 4 laps\r\nKadish Sprint — 2 laps\r\nIndustry — 3 laps\r\nGhost Town 2 (R) — 5 laps\r\nQuake! — 2 laps\r\nRadioactive Garden (R) — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

