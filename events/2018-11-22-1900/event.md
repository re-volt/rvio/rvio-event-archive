---
title: Semi-Pros
titleoverride: false
date: '22-11-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: semi-pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 1\r\nVenice (R)\r\nToy World 2 (R)\r\nMuseum 1\r\nHoliday Camp: California Edition\r\nFreestoyle 2\r\nSkating Toys (R)\r\nSpa-Volt 1 (R)\r\nSantorini\r\nThe Felling Yard\r\nSakura\r\nDonut Plains 3\r\nRadioactive Garden\r\nJailhouse Rock\r\nKadish Sprint"
    vod: 'https://www.youtube.com/watch?v=_n2oTAv1SGE'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-22_20-03-01.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

