---
title: 'Halloween Carnival <small>(Day 1)</small>'
titleoverride: true
date: '31-10-2020 18:00'
event:
    host: duc
    ip: hb.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Halloween Carnival'
    classlink: 'https://re-volt.io/blog/halloween-carnival-2020#downloads'
    tracklist: "Cycle Road — 4 laps\r\nhalloween — 3 laps\r\nBedlam — 4 laps\r\nRanch (R) — 3 laps\r\nFairground 2 (R) — 3 laps\r\nGBA Bowser Castle 3 — 4 laps\r\nMuseum EX — 3 laps\r\nSchool's Out 1 (R) — 4 laps\r\nMiner's End — 3 laps\r\nLantern Hollow remix — 3 laps\r\nRC Jungle — 3 laps\r\nRainy Hood — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Halloween Carnival'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**For more information, visit**: [ https://re-volt.io/blog/halloween-carnival-2020]( https://re-volt.io/blog/halloween-carnival-2020)