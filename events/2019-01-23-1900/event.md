---
title: 'Last Man Standing'
titleoverride: false
date: '23-01-2019 19:00'
event:
    host: Sarorii
    ip: sas.rv.gl
    category: special
    mode: last-man-standing
    class: other
    otherclass: 'Classic Clockworks'
    classlink: 'https://distribute.re-volt.io/packs/io_clockworks.zip'
    tracklist: "LMS Chinatown\r\nLMS Temple 2\r\nLMS Arena Toshinden\r\nLMS Road of Simplicity \r\nLMS Toshinden Mona\r\nLMS Rooftops"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Requirements:
* [LMS Arena Collection](https://distribute.re-volt.io/packs/io_lmstag.zip)
* [Classic Clockworks](https://distribute.re-volt.io/packs/io_clockworks.zip)