---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '04-06-2020 18:00'
event:
    host: 'Etneus & OhNej'
    ip: 'etn.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Museum 2 — 4 laps\r\nQuake! — 2 laps\r\nToytanic 1 — 3 laps\r\nSpa-Volt 2 — 3 laps\r\nMuseum 1 — 3 laps\r\nOverground — 3 laps\r\nSnowland 1 — 4 laps\r\nToy World Mayhem (R) — 4 laps\r\nLunar — 3 laps\r\nSupermarket 2 (R) — 8 laps\r\nFools Mate 2 — 4 laps\r\nWhite Rose Chapel — 3 laps\r\nRadioactive Garden (R) — 5 laps\r\nSnowy River — 4 lap"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

