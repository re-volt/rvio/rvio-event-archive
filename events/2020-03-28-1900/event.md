---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '28-03-2020 19:00'
event:
    host: 'Kirioso & Delecto'
    ip: 'kiri.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Quake! – 2 laps\r\nToys in the Hood 1 (R) – 3 laps\r\nSpa-Volt 1 (R) – 3 laps\r\nJailhouse Rock – 3 laps\r\nSkating Toys (M) – 2 laps\r\nLunar – 3 laps\r\nMuseum 1 – 3 laps\r\nBlood on the Rooftops – 3 laps\r\nMuseum 2 – 4 laps\r\nSnowland 1 (R) – 3 laps\r\nBotanical Garden – 6 laps\r\nIndustry – 3 laps\r\nRoute-77 – 3 laps\r\nMoon Dawn – 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

