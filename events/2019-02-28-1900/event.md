---
title: 'Semi-Pro Races'
titleoverride: false
date: '28-02-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "The Felling Yard\r\nLunar\r\nDonut Plains 3\r\nSpa-Volt 1\r\nBiohazard Factory (R)\r\nMuseum 1\r\nBotanical Garden (R)\r\nVenice (R)\r\nToytanic 1\r\nQuake!\r\nKadish Sprint\r\nToys in the Hood 2\r\nGround N Smash 2\r\nFreestoyle 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

