---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '05-11-2020 19:00'
event:
    host: 'Floxit & Ashen Forest'
    ip: 'flo.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World Aquatica: Redux — 3 laps\r\nRoute-77 — 3 laps\r\nBiohazard Factory — 2 laps\r\nPetroVolt — 2 laps\r\nMuseum 2 — 4 laps\r\nToys in the Hood 1 — 3 laps\r\nSkating Toys: Redux — 2 laps\r\nToy World 1 (R) — 6 laps\r\nRooftops — 3 laps\r\nQuake! (R) — 2 laps\r\nCasino RV (R) — 3 laps\r\nSchools Out! 2 — 2 laps\r\nHull Breach 3000 — 3 laps\r\nBlood on the Rooftops — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

