---
title: 'Zipper Exclusive'
titleoverride: false
date: '25-03-2019 20:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: Zipper
    classlink: 'https://revolt.fandom.com/wiki/Zipper'
    tracklist: 'Random Tracks'
published: true
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Visit **[RVGL Ladder](https://ladder.rv.gl/)** for more information!

**Links**:
* [Track Selection](https://ladder.rv.gl/info/#events-general-tracks)
* [Car Selection](https://ladder.rv.gl/info/#events-general-cars)
* [Ranking](https://ladder.rv.gl/info/#events-general-ranking)