---
title: Pros
titleoverride: false
date: '03-11-2018 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1 (R)\r\nToy World 1\r\nSpa-Volt 1\r\nMK64 Wario Stadium\r\nMuseum 1\r\nThe Felling Yard\r\nSkating Toys\r\nPetroVolt\r\nHoliday Camp: California Edition\r\nGrisville\r\nMolten Caverns\r\nCactus-Volt\r\nToytanic 1\r\nRooftop Chase (R)\r\nRanch (R)\r\nKadish Sprint"
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-03_20-05-27.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

