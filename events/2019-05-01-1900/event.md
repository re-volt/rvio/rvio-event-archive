---
title: 'Random Rookies (Ladder)'
titleoverride: false
date: '19:00 01-05-2019'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Rookies (Ladder)'
    classlink: 'https://re-volt.io/events/2019-05-01-1900'
    tracklist: 'Random Category I Tracks'
published: true
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event will count for the **RVGL Ladder**.

* [Current Ladder standings](https://ladder.rv.gl/#ladder)
* [Information & Rules](https://ladder.rv.gl/info)

You need the **RVGL Plus Pack** to join the races!

* [Download](https://kiwis.rv.gl/plus)
* [RVGL Plus Car Classes](https://kiwis.rv.gl/plus/cars)
* [RVGL Plus Track Categories](https://kiwis.rv.gl/plus/tracks)