---
title: 'Semi-Pro Races'
titleoverride: false
date: '30-03-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Jailhouse Rock\r\nGhost Town 2 \r\nIllusion\r\nVenice \r\nPenny Racers - Caves\r\nRanch (R)\r\nMuseum 1\r\nMoon Dawn (R)\r\nYABBA DABBA DOO!\r\nDonut Plains 3\r\nToytanic 1 (R)\r\nSpa-Volt 1\r\nSupermarket 1\r\nSkating Toys\r\nThe Bunker"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

