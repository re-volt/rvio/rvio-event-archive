---
title: 'Rookie Races'
titleoverride: false
date: '30-04-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Sakura \r\nDonut Plains 3 (R)\r\nPenny Racers-Harbour\r\nAMCO TT\r\nSupermarket 2\r\nToy World Aquatica:Redux\r\nLunar\r\nBotanical Garden\r\nSnowy River\r\nGhost Town 1 (R)\r\nToySoldierz\r\nSpa-Volt 2 (R)\r\nVenice\r\nToy World 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

