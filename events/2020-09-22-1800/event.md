---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '22-09-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Moon Dawn — 2 laps\r\nToy World 1 — 6 laps\r\nBotanical Garden EX — 3 laps\r\nQuake! — 3 laps\r\nSpooky-Volt — 6 laps\r\nIndustry — 3 laps\r\nMolten Caverns (R) — 2 laps\r\nJailhouse Rock (R) — 3 laps\r\nToy World Mayhem — 4 laps\r\nSnowland 1 — 4 laps\r\nMuseum 1 (R) — 3 laps\r\nSakura — 3 laps\r\nRooftops — 4 laps\r\nBotanical Garden — 6 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

