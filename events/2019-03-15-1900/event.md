---
title: 'Advanced Races'
titleoverride: false
date: '15-03-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "The Felling Yard\r\nToy World Mayhem(R)\r\nGrisville\r\nRe-Ville\r\nLunar\r\nFool's Mate 2\r\nToy World 2\r\nBotanical Garden\r\nToytanic 1 (R)\r\nVenice\r\nRooftops\r\nMetro-Volt\r\nMolten Caverns\r\nToy World Aquatica: Redux (R)\r\nKadish Sprint"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

