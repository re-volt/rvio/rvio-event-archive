---
title: 'Advanced Races'
titleoverride: false
date: '31-03-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Spa-Volt 1 (R)\r\nToys in the Hood 1 \r\nRooftop Chase: Redux\r\nHelios\r\nRe-Ville\r\nKadish Sprint\r\nBotanical Garden\r\nSnowland 1 (R)\r\nMetro-Volt\r\nFiddlers on the Roof\r\nToys in the Hood 2 (R)\r\nPenny Racers - Harbour\r\nQuake!\r\nGrisville\r\nRV Temple"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

