---
title: 'Advanced Races'
titleoverride: true
date: '24-05-2020 20:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Ghost Town 1 — 6 laps\r\nLunar — 3 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nFools Mate 2 — 4 laps\r\nAMCO TT (R) — 3 laps\r\nWhite Rose Chapel — 3 laps\r\nRooftops — 3 laps\r\nMuseum 1 (R) — 3 laps\r\nToy World Mayhem — 4 laps\r\nToys in the Hood 1 — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nCliffside — 6 laps\r\nHull Breach 3000 — 3 laps\r\nImages of Giza: Redux — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

