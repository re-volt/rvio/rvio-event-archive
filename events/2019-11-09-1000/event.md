---
title: 'Korean Re-Volt Races'
titleoverride: true
date: '09-11-2019 10:00'
event:
    host: DarkHunt
    ip: swcode.ddns.net
    category: unofficial
    mode: race
    class: other
    otherclass: 'Rookie Cars (Re-Volt Korea)'
    classlink: 'https://drive.google.com/drive/folders/1ZsuXpO2cIRSZ_hQGgbSXcyrUaxyjSI9W?usp=sharing'
    tracklist: Random
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Some important information is available on the [Re-Volt Mania thread](https://cafe.naver.com/revoltmania/5711).

#### Car Selection
DarkHuntToyeca *(for beginners 1)*, Adeon *(for beginners 2)*, RC San *(for beginners 3)*, Reddlum *(for beginners 4)*, Abracadabra, Angus 400, Dr. Grudge, Bed Bug, Bendor, BigVolt, Bulkhead, Bumblebee, Fast Traxx, Rouge, SchoolVolt, Trirraut, Alien, Panorama, Perfect Shot, LR 64, Road Pirate, Micro, El Gekko, Heatroad, Hurricane, Dump Truck, Kanberra Kruiser, Miss Kuma, Tesla, Dust Mite, Col. Moss, Harvester, Phat Slug, Phat Trucker, Phenom, RC Bandit, High-Rod, Scorcher 6x6, Swish KX2, Sprinter XL, Volken Turbo, Wind Up

**[Download the content pack!](https://drive.google.com/drive/folders/1ZsuXpO2cIRSZ_hQGgbSXcyrUaxyjSI9W?usp=sharing)**