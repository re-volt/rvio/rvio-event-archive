---
title: 'Super Pro Races'
titleoverride: true
date: '31-12-2020 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Medieval: Redux — 5 laps\r\nMuseum 3 — 4 laps\r\nSchools Out! 1 (R) — 4 laps\r\nRadioactive Garden — 5 laps\r\nSupermarket 1 (R) — 4 laps\r\nCasino RV — 3 laps\r\nRooftop Chase: Redux — 3 laps\r\nRoute-77 (R) — 4 laps\r\nSpooky-Volt — 7 laps\r\nSupermarket 2 — 9 laps\r\nHoliday Camp: California Edition — 2 laps\r\nBotanical Garden — 6 laps\r\nSpa-Volt 2 — 4 laps\r\nToytanic 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

