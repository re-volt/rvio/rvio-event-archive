---
title: 'Random Cars'
titleoverride: false
date: '17-10-2018 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Cars'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Rooftops\r\nPetroVolt\r\nSakura\r\nHelios\r\nJailhouse Rock\r\nToys in the Hood 2\r\nMuseum 1\r\nVenice\r\nSuperMarket 1\r\nBotanical Garden\r\nGhost Town 1\r\nGhost Town 2\r\nRanch\r\nAMCO TT"
    results: 'https://online.re-volt.io/sessions/results.php?file=competitive/session_2018-10-17_20-10-00.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

