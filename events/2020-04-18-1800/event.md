---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '18-04-2020 18:00'
event:
    host: 'Delecto & Narusori'
    ip: 'dele.rv.gl OR naru.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Spa-Volt 2 (R) —  3 laps\r\nGrisville —  3 laps\r\nAMCO TT (M) —  3 laps\r\nMeltdown —  4 laps\r\nSupermarket 1 —  4 laps\r\nPetro-Volt (R) —  2 laps\r\nMuseum 2 —  3 laps\r\nBotanical Garden (R) —  5 laps\r\nJailhouse Rock —  2 laps\r\nRadioactive Garden —  4 laps\r\nGhost Town 2 —  4 laps\r\nFool's Mate 2 —  3 laps\r\nRooftop Chase: Redux (R) —  2 laps\r\nMetro-Volt —  3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

