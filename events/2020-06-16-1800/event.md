---
title: 'Pro Races'
titleoverride: true
date: '16-06-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "ToySoldierz — 4 laps\r\nWhite Rose Chapel — 3 laps\r\nBiohazard Factory — 2 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nToy World Mayhem — 4 laps\r\nRooftop Chase: Redux — 3 laps\r\nToy World 2 (R) — 5 laps\r\nSpa-Volt 2 — 3 laps\r\nRooftops — 3 laps\r\nMuseum 1 — 3 laps\r\nGhost Town 2 — 5 laps\r\nMetro-Volt — 3 laps\r\nIndustry — 3 laps\r\nToy World Aquatica: Redux (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

