---
title: 'Semi-Pro Races'
titleoverride: true
date: '13-06-2020 18:00'
event:
    host: Etneus
    ip: etn.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Toys in the Hood 2 — 4 laps\r\nVenice — 3 laps\r\nFiddlers on the Roof: Redux — 3 laps\r\nMeltdown — 4 laps\r\nMetro-Volt — 3 laps\r\nSupermarket 2 (R) — 8 laps\r\nThe Felling Yard — 3 laps\r\nSnowland 1 (R) — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nIndustry — 3 laps\r\nKadish Sprint — 2 laps\r\nToy World 1 — 6 laps\r\nLunar — 3 laps "
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

