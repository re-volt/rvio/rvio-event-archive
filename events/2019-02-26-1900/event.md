---
title: 'Advanced Races'
titleoverride: false
date: '26-02-2019 19:00'
event:
    host: Floxit
    ip: flox.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Swan Street\r\nCliffside\r\nPetroVolt (R)\r\nToy World 1\r\nRooftop Chase (R)\r\nRanch\r\nHelios\r\nGhost Town 1\r\nToy World Aquatica: Redux\r\nThe Gorge\r\nMuseum 2\r\nMeltdown\r\nToy World Mayhem\r\nToytanic 2 (R)\r\nSnowy River"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

