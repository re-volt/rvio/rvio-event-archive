---
title: 'Platinum Cup'
titleoverride: false
date: '20-07-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Platinum Cup'
    classlink: 'https://re-volt.io/blog/online-championship-mode-tournament'
    tracklist: "Supermarket 1 (6 laps)\r\nGhost Town 2 (6 laps)\r\nToy World 1 RM (10 laps)\r\nMuseum 1 M (6 laps)\r\nToytanic 2 (6 laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Signing up is required for participation in this event.** [More information here.](https://re-volt.io/blog/online-championship-mode-tournament)