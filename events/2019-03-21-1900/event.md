---
title: 'Semi-Pro Races'
titleoverride: false
date: '21-03-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Rooftops\r\nSpa-Volt 2\r\nSwan Street (R)\r\nToys in the Hood 2 \r\nRV Temple\r\nVenice (R)\r\nThe Felling Yard\r\nRooftop Chase: Redux\r\nMuseum 1 (R)\r\nWhite Rose Chapel\r\nSakura\r\nBlood on the Rooftops\r\nToytanic 2\r\nToySoldierz\r\nHelios"
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2019-03-21_21-00-06.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

