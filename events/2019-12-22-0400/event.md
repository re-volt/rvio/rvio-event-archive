---
title: 'Pro Races'
titleoverride: true
date: '22-12-2019 04:00'
event:
    host: Santi
    ip: santi.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Metro-Volt\r\nHoliday Camp: California Edition (R)\r\nLunar\r\nRV Temple\r\nToy World 1\r\nBiohazard Factory\r\nSnowy River\r\nToy World Aquatica: Redux (R)\r\nHull Breach 3000\r\nThe Felling Yard\r\nToytanic 1 (R)\r\nSkating Toys\r\nMuseum 1 (M)\r\nToySoldierz\r\nRooftops\r\nOverground"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

