---
title: 'Rookie Races'
titleoverride: false
date: '02-04-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Spa-Volt 1\r\nToy World Aquatica:Redux\r\nToys in the Hood 2 (R)\r\nDonut Plains 3\r\nSakura\r\nPenny Racers - Caves \r\nGhost Town 2\r\nSnowland 1 (R)\r\nLunar\r\nOverground\r\nMuseum 2\r\nVenice (R)\r\nToySoldierz\r\nRooftops"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

