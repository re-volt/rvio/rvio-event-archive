---
title: 'Pro Races'
titleoverride: false
date: '23-02-2019 04:00'
event:
    host: '[AR]Santi™'
    ip: santi.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Donut Plains 3 \r\nToytanic 2 (R) \r\nSakura \r\nBiohazard Factory (R) \r\nSupermarket 1 \r\nJailhouse Rock \r\nMolten Caverns \r\nSnowy River \r\nMuseum 2 \r\nVenice (R) \r\nCliffside \r\nRadioactive Garden \r\nToys in the Hood 1 \r\nKadish Sprint \r\nHoliday Camp: California Edition \r\nToySoldierz"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

