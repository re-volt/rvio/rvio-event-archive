---
title: 'Rookie Races'
titleoverride: false
date: '15-10-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Fiddlers on the Roof\r\nIllusion\r\nToy World 1\r\nDonut Plains 3\r\nPetroVolt (R)\r\nLunar\r\nThe Bunker\r\nSakura\r\nToys in the Hood 2 (R)\r\nRadioactive Garden (R)\r\nFool's Mate 2 (M)\r\nMuseum 1\r\nSuperMarket 2\r\nToy World Mayhem"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

