---
title: 'Normal Semi-Pro'
titleoverride: true
date: '19-04-2020 18:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: Semi-Pro
    classlink: 'https://www.dropbox.com/s/u71xh54lw48noxu/rvon%20car%20list%20%2820.0%29.txt?dl=0'
    tracklist: "Toys in the hood Ex M (4 laps)\r\nMetro-Volt (4 laps)\r\nSakura RM (4 laps)\r\nRed Rock Valley R (4 laps)\r\nRooftops R (4 laps)\r\nToy World Mayhem M (4 laps) \r\nThe Gorge M (4 laps)\r\nLunar (4 laps)\r\nToysoldierz M (4 laps)\r\nToy World 2 RM (4 laps)\r\nThe Felling Yard RM (4 laps)\r\nSwan Street RM (4 laps)\r\nToys in the Hood 1 (4 laps)\r\nSnowy River RM (4 laps)\r\nRv Temple M (3 laps)\r\nRooftop Chase: Redux (4 laps)\r\nGreen Night R (4 laps)\r\nGrisville R (4 laps)\r\nMolten Caverns R (3 laps)\r\nToys in the Hood Ex R (4 laps)\r\nFiddlers on the Roof Redux M (4 laps)\r\nHoliday Camp California Edition R (3 laps)\r\nMkdd – Mushroom Kingdom (4 laps)\r\nSupermarket 2 RM (5 laps)\r\nSpa-Volt 2 RM (4 laps)\r\nMuseum 1 R (4 laps)\r\nToy World Ex (4 laps)\r\nRooftop Chase: Redux RM (4 laps)\r\nPetrovolt R (3 laps)\r\nToy World Ex RM (4 laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Normal Semi-Pro'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous avarage prestige results be sure to [check this](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).

### Requirements
* [Full Re-Volt Online version 20.0 game installation needed, this can be downloaded currently from the Re-Volt Online discord server](https://discord.gg/CzJjxWu) (needed)
* Seperate game installation required if you got I/O content on the game you want to use!

### Car Selection
* Any car that has been rated for semi-pro or lower on [this car list](https://www.dropbox.com/s/u71xh54lw48noxu/rvon%20car%20list%20%2820.0%29.txt?dl=0)