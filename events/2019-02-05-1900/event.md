---
title: 'Rookie Races'
titleoverride: false
date: '05-02-2019 19:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Santorini\r\nHelios\r\nAMCO TT (R)\r\nToy World 1 (R)\r\nPenny Racers - Harbour\r\nVenice\r\nToy World Mayhem (R)\r\nBotanical Garden\r\nRadioactive Garden\r\nMetro-Volt\r\nSupermarket 2\r\nThe Bunker\r\nToySoldierz\r\nMuseum 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

