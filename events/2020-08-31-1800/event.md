---
title: 'Ciągnik exclusive'
titleoverride: true
date: '31-08-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: Ciągnik
    classlink: 'http://revoltzone.net/cars/53448/Ciagnik'
    packages:
        - io_tracks
    tracklist: "Toys in the Hood 1 - 5 laps\r\nToy World Mayhem - 8 Laps\r\nLunar - 7 Laps\r\nGhost Town 1 (M) - 10 Laps\r\nMoon Dawn - 4 Laps\r\nRoute-77 - 6 Laps\r\nToy World 1 - 12 Laps\r\nSmashride Circuit - 5 Laps\r\nToy Soldiers (R) - 6 Laps\r\nFool's Mate 2 - 6 Laps\r\nSpa-Volt 2 - 5 Laps\r\nMetro-Volt - 4 Laps\r\nSakura - 4 Laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Ciągnik exclusive'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

* To take part in session, you need to have [Ciągnik](http://revoltzone.net/cars/53448/Ciagnik)