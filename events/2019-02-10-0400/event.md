---
title: 'Rookie Races'
titleoverride: false
date: '10-02-2019 04:00'
event:
    host: L!LMexican
    ip: mex.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Botanical Garden\r\nSupermarket 1 (R)\r\nLunar (R)\r\nVenice\r\nMuseum 2\r\nToy World Aquatica: Redux (R)\r\nMetro-Volt\r\nSakura\r\nYABBA DABBA DOO!\r\nOverground\r\nThe Gorge\r\nBlood on the Rooftops\r\nPalm Marsh\r\nMuseum 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

