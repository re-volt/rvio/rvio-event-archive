---
title: 'Pro Races (Dual Lobby)'
titleoverride: false
date: '28-09-2019 18:00'
event:
    host: 'URV & Floxit'
    ip: 'u.rv.gl OR flo.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Fool's Mate 2\r\nQuake!\r\nDonut Plains 3\r\nToys in the Hood 2 (R)\r\nToy World Aquatica: Redux (R)\r\nBiohazard Factory\r\nMuseum 1\r\nBlood on the Rooftops\r\nHelios\r\nJailhouse Rock (R)\r\nRooftop Chase: Redux (M)\r\nOverground\r\nToySoldierz\r\nSpa-Volt 1\r\nToytanic 2\r\nBotanical Garden"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

