---
title: 'Super Pro Races'
titleoverride: true
date: '26-01-2020 19:00'
event:
    host: Skarma
    ip: sk.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Toy World Aquatica: Redux — 4 laps\r\nKadish Sprint — 3 laps\r\nToys in the Hood 2 — 4 laps\r\nSpa-Volt 1 — 3 laps\r\nMuseum 1 R — 3 laps\r\nToySoldierz — 4 laps\r\nMetro-Volt — 3 laps\r\nRooftops — 4 laps\r\nRadioactive Garden — 5 laps\r\nToytanic 2 RM — 3 laps\r\nSakura — 3 laps\r\nMoon Dawn R — 2 laps\r\nIndustry — 3 laps\r\nHull Breach 3000 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
---

**[Super Pros](https://distribute.re-volt.io/packs/io_superpros.zip)** are required.