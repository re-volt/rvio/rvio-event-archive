---
title: 'Rookie Races'
titleoverride: false
date: '02-07-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 1 RM\r\nSpa-Volt 1 \r\nRe-Ville\r\nOverground\r\nFiddlers on the Roof\r\nPetroVolt R\r\nGhost Town 2\r\nGrisville R\r\nLunar\r\nSantorini \r\nRanch\r\nMuseum 2\r\nRooftops\r\nHull Breach 3000"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

