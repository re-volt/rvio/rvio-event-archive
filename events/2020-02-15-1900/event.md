---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '15-02-2020 19:00'
event:
    host: 'Delecto & OhNej'
    ip: 'dele.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Supermarket 2 — 7 laps\r\nBlood on the Rooftops — 3 laps\r\nSakura — 2 laps\r\nMuseum 1 (R) — 2 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nSupermarket 1 — 4 laps\r\nToy World Aquatica: Redux (R) — 3 laps\r\nSwan Street — 3 laps\r\nRoute-77 — 3 laps\r\nToy World Mayhem — 3 laps\r\nGrisville — 3 laps\r\nRadioactive Garden — 4 laps\r\nBotanical Garden — 5 laps\r\nMetro-Volt — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

