---
title: 'Long Journey 4: Day 3'
titleoverride: true
date: '12-06-2020 17:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_cars_bonus
        - io_skins
        - io_tracks
        - io_tracks_bonus
    tracklist: "Go Play Outside!\r\nSchool's Out!\r\nWipeout 2097\r\nSpa-Volt 2\r\nChristmas Snow Globe\r\nToys in the Hood 1\r\nChilled to the Bone\r\nRe-Ville\r\nBiohazard Factory\r\nPenny Racers - Caves\r\nMuseum 1\r\nSnowy River\r\nToys in the Hood EX\r\nKadish Sprint\r\nBroken Sunlight\r\nBotanical Garden\r\nPlaya+\r\nJailhouse Rock\r\nHallow's Eve\r\nHull Breach 3\r\nRoute-77\r\nTemple of the Burning Darkness"
    vod: 'https://www.youtube.com/watch?v=CBw0p3hw62I'
    results: 'https://online.re-volt.io/sessions/results.php?file=main/session_2020-06-12_19-07-42.csv'
media_order: LongJourney4.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Long Journey 4: Day 3'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](https://re-volt.io/user/pages/events/2020-06-12-1700/LongJourney4.png)