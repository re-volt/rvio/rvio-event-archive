---
title: 'Amateur Races'
titleoverride: true
date: '30-01-2020 19:00'
event:
    host: 'Floxit & OhNej'
    ip: 'flo.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Toy World 1 — 5 laps\r\nGrisville (R) — 3 laps\r\nHull Breach 3000 — 3 laps\r\nToytanic 1 (R) — 3 laps\r\nQuake! (R) — 2 laps\r\nSnowland 1 — 3 laps\r\nMuseum 2 — 3 laps\r\nWhite Rose Chapel — 2 laps\r\nPenny Racers - Harbour — 3 laps\r\nSantorini (M) — 4 laps\r\nSpa-Volt 2 — 3 laps\r\nJailhouse Rock — 2 laps\r\nThe Felling Yard — 2 laps\r\nToy World 2 — 4 laps"
    stream: 'https://www.twitch.tv/ohnej6'
    vod: 'https://www.youtube.com/watch?v=adVBINI8vhE'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

