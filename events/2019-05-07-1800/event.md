---
title: 'Advanced Races'
titleoverride: false
date: '07-05-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Spa-Volt 2\r\nBotanical Garden\r\nMuseum 2\r\nMolten Caverns\r\nWhite Rose Chapel\r\nSantorini (M)(R)\r\nMuseum 1\r\nPenny Racers - Harbour\r\nPetroVolt\r\nHoliday Camp: California Edition (R)\r\nOverground\r\nRe-Ville\r\nGhost Town 2 (R)\r\nSakura"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

