---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '22-12-2020 19:00'
event:
    host: 'Frostbitten & Mighty'
    ip: 'frost.rv.gl OR mighty.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Rooftops 1 (R) — 2 laps\r\nSpa-Volt 2 — 3 laps\r\nGrisville — 3 laps\r\nToys in the Hood 2 (R) — 4 laps\r\nMuseum 1 — 3 laps\r\nMolten Caverns — 2 laps\r\nSupermarket 2 — 8 laps\r\nJailhouse Rock — 3 laps\r\nStadVolt (R) — 4 laps\r\nBotanical Garden EX — 2 laps\r\nToySoldierz — 4 laps\r\nHoliday Camp: California Edition — 2 laps\r\nGhost Town 1 — 6 laps\r\nWhite Rose Chapel — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

