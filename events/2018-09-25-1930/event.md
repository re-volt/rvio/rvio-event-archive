---
title: 'Month Tracks'
titleoverride: false
date: '25-09-2018 19:30'
event:
    host: Whitedoom
    category: unofficial
    mode: race
    class: other
    otherclass: 'Month Tracks'
    classlink: 'https://www.revoltrace.net/month_tracks.php'
    tracklist: "AMCO TT\r\nBiohazard Factory\r\nN10c\r\nPOD: Alderon\r\nRooftop Chase (original)\r\nThe Mines of Alderon\r\nToy World 3\r\nToytanic Arcade"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only the original stock Pros are allowed:
* AMW
* Cougar
* Humma
* Panga
* Toyeca

The [month tracks pack](https://distribute.re-volt.io/packs/month_tracks.zip) is required. Please note that this pack contains a different version of Rooftop Chase than the version in the I/O pack, which may cause conflict in the future.

The current results can be seen [here](https://www.revoltrace.net/month_tracks.php).