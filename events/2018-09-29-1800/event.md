---
title: Slugs
titleoverride: false
date: '29-09-2018 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 1\r\nSwan Street (R)\r\nBlood on the Rooftops\r\nThe Gorge\r\nLunar\r\nGrisville\r\nMuseum 1\r\nToy World Mayhem\r\nSakura\r\nQuake! (R)\r\nRe-Ville\r\nSuperMarket 2\r\nToytanic 2 (R)\r\nToy World Aquatica: Redux"
    vod: 'https://www.youtube.com/watch?v=HcH6ZxkDg9g'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-29_20-04-24.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

