---
title: Rookies
titleoverride: false
date: '06-10-2018 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: rookies
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 1\r\nGhost Town 1\r\nRadioactive Garden\r\nVenice (R)\r\nFreestoyle 2\r\nMetro-Volt\r\nGrisville\r\nBiohazard Factory (R)\r\nToy World 1 (R)\r\nQuake!\r\nAMCO TT\r\nRe-Ville\r\nSnowy River\r\nSuperMarket 2"
    vod: 'https://www.youtube.com/watch?v=IayF_gZzRt0'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-06_20-03-52.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

