---
title: 'Line Elimination'
titleoverride: true
date: '01-11-2019 19:00'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'Recent Semi-Pros'
    classlink: 'https://discordapp.com/channels/85338251597983744/375395272085209098/638200114896306185'
    tracklist: "Lunar\r\nVenice\r\nPR Caves\r\nSantorini\r\nRooftops R\r\nBotanical Garden R\r\nMuseum 2 ⚡\r\nToy World Aquatica Redux ⚡\r\nKadish Sprint ⚡\r\nPR Harbor ⚡\r\nAMCO TT ⚡\r\nToytanic 2 ⚡"
media_order: linelminton.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](linelminton.png)

### Car Selection
- AMW M425
- Battaglia
- Blaze V8
- Cube RS
- DOD-63
- Elelibra
- Fire & Ice
- PAV-Guardian
- Sidewall
- Subi Civi
- Team AMA
- Wulff Bolter

The content pack (cars only) will be available sometime before the session starts.
The first six tracks will be raced with pick-ups **disabled**.