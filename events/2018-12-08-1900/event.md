---
title: 'Pro Slugs'
titleoverride: false
date: '08-12-2018 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: prugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 2\r\nSuperMarket 2\r\nJailhouse Rock\r\nMeltdown\r\nRanch\r\nMetro-Volt\r\nSantorini\r\nToy World 2 (R)\r\nPalm Marsh\r\nSnowy River (R)\r\nHoliday Camp: California Edition (R)\r\nGround N Smash 2\r\nOverground\r\nMolten Caverns\r\nToys in the Hood 2"
    vod: 'https://www.youtube.com/watch?v=Vc7MpGDTNMw'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-08_20-05-22.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

