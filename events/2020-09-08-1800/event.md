---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '08-09-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Rooftops — 3 laps\r\nHull Breach 3000 — 3 laps\r\nSchool's Out! — 2 laps\r\nSupermarket 2 — 7 laps\r\nSakura — 2 laps\r\nSmashride Circuit — 2 laps\r\nToytanic 2 — 3 laps\r\nToy World 1 (R) — 5 laps\r\nAMCO TT — 3 laps\r\nRooftop Chase: Redux — 3 laps\r\nSnowland 1 — 3 laps\r\nMuseum 3 (R) — 3 laps\r\nFools Mate 2 — 3 laps\r\nVenice (R) — 3 lap"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

