---
title: 'Advanced Races'
titleoverride: false
date: '06-04-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1 (R)\r\nRanch\r\nPalm Marsh\r\nBotanical Garden\r\nPenny Racers - Harbour\r\nQuake! (R)\r\nSupermarket 1\r\nSwan Street (R)\r\nToy World Mayhem\r\nIndustry\r\nBiohazard Factory\r\nMetro-Volt\r\nGrisville\r\nToy World 2\r\nThe Felling Yard"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

