---
title: 'Videogame Night at PizzaLab'
titleoverride: true
date: '31-01-2019 18:00'
event:
    host: 'Pizza Lab'
    category: casual
    mode: other
    class: other
    otherclass: 'any cars'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Come to the PizzaLab in Leipzig for some casual 1v1 races and battle tags! :)

More information on Facebook:  
[https://www.facebook.com/events/242272490046187/](https://www.facebook.com/events/242272490046187/)