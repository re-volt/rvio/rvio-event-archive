---
title: 'Semi-Pro Races'
titleoverride: false
date: '03-01-2019 19:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 2\r\nJailhouse Rock\r\nGhost Town 1 (R)\r\nSnowy River\r\nGround N Smash 2\r\nToySoldierz\r\nMeltdown\r\nToy World Mayhem (R)\r\nPenny Racers - Caves (R)\r\nSuperMarket 1\r\nSakura\r\nBlood on the Rooftops\r\nHoliday Camp: California Edition\r\nRadioactive Garden\r\nToy World 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

