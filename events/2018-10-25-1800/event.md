---
title: Semi-Pros
titleoverride: false
date: '25-10-2018 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: semi-pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1\r\nToytanic 2 (R)\r\nKadish Sprint\r\nPenny Racers - Caves\r\nJailhouse Rock\r\nThe Gorge\r\nMuseum 2\r\nBiohazard Factory\r\nMK64 Wario Stadium\r\nDonut Plains 3 (R)\r\nFool's Mate 2\r\nSpa-Volt 1 (R)\r\nRanch\r\nPetroVolt\r\nToy World 2"
    vod: 'https://www.youtube.com/watch?v=DH6jYRoOF38'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-25_20-02-10.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

