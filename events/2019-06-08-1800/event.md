---
title: 'Rookie Races (Dual Lobby)'
titleoverride: false
date: '08-06-2019 18:00'
event:
    host: 'Floxit & Wichilie'
    ip: 'flo.rv.gl OR w.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Rookie Cars (Dual Lobby)'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 1\r\nToys in the Hood 2\r\nGrisville (R)\r\nPetroVolt (R)\r\nCliffside\r\nToySoldierz\r\nToytanic 2 (R)\r\nHelios\r\nSpa-Volt 1\r\nSpa-Volt 2\r\nFool's Mate 2\r\nFiddlers on the Roof (M)\r\nIllusion\r\nToy World 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

