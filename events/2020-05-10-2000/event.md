---
title: 'Rookie Races'
titleoverride: true
date: '10-05-2020 20:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Fools Mate 2 — 3 laps\r\nPetroVolt — 2 laps\r\nGhost Town 2 (R) — 4 laps\r\nQuake! — 2 laps\r\nRooftop Chase: Redux — 3 laps\r\nRooftops — 3 laps\r\nGhost Town 1 — 5 laps\r\nSnowy River — 3 laps\r\nGrisville — 3 laps\r\nMoon Dawn (R) — 2 laps\r\nJailhouse Rock — 2 laps\r\nToytanic 1 — 3 laps\r\nSakura (R) — 2 laps\r\nToySoldierz — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

