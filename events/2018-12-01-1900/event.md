---
title: Semi-Pros
titleoverride: false
date: '01-12-2018 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: semi-pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 1 (R)\r\nKadish Sprint\r\nToys in the Hood 2\r\nSuperMarket 1\r\nJailhouse Rock\r\nLunar\r\nToySoldierz\r\nYABBA DABBA DOO!\r\nMeltdown\r\nBiohazard Factory\r\nDonut Plains 3 (R)\r\nPetroVolt (R)\r\nMK64 Wario Stadium\r\nRanch\r\nToy World 2"
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-01_20-05-01.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

