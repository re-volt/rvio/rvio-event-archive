---
title: 'Adeon Exclusive'
titleoverride: false
date: '08-04-2019 18:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Adeon Exclusive'
    classlink: 'https://revolt.fandom.com/wiki/Adeon'
    tracklist: "Industry\r\nFool's Mate 2\r\nWhite Rose Chapel\r\nRooftops\r\nMetro-Volt\r\nToytanic 2 (R)\r\nOverground\r\nCliffside\r\nMuseum 1 (R)\r\nGrisville\r\nQuake!\r\nSuperMarket 2\r\nSpa-Volt 2 (R)\r\nRV Temple"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

