---
title: Tomatoes
titleoverride: false
date: '03-09-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'a tomato'
    classlink: 'http://files.re-volt.io/misc/fresh.zip'
    tracklist: "Supermarket 2\r\nMetro-Volt \r\nBotanical Garden\r\nPenny Racers - Harbour (M)\r\nRadioactive Garden\r\nSupermarket 1 (M)(R)\r\nPenny Racers - Caves\r\nToys In The Hood 2 (R)\r\nDonut Plains 3\r\nAMCO TT (R)"
    stream: 'https://twitch.tv/pongcat'
    vod: 'https://www.youtube.com/watch?v=XwMOe9rzYmM'
    results: 'https://online.re-volt.io/sessions/results.php?file=session_2018-09-03_21-04-07.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

We're doing 5 laps on each track. Join us in the voice channel on [Discord](https://re-volt.io/discord) to hear Andor talk about growing tomatoes (if he didn't flee to the Maldives).