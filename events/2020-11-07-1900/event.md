---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '07-11-2020 19:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Holiday Camp: California Edition (R) — 2 laps\r\nSupermarket 2 (R) — 8 laps\r\nMetro-Volt — 3 laps\r\nLunar — 3 laps\r\nSnowland 1 — 4 laps\r\nSpa-Volt 2 — 3 laps\r\nSmashride Circuit — 3 laps\r\nThe Felling Yard — 3 laps\r\nWildland — 3 laps\r\nSupermarket 1 — 4 laps\r\nBotanical Garden EX (R) — 3 laps\r\nToytanic 2 — 3 laps\r\nToys in the Hood 2 — 4 laps\r\nRooftops 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

