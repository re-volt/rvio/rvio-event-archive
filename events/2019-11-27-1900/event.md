---
title: 'Random Cars <small>(Dual Lobby)</small>'
titleoverride: true
date: '27-11-2019 19:00'
event:
    host: 'URV & Kirioso'
    ip: 'u.rv.gl OR kiri.rv.gl'
    category: special
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "Penny Racers - Caves\r\nRe-Ville\r\nSwan Street\r\nHelios\r\nToy World 2\r\nHull Breach 3000\r\nMoon Dawn (R)\r\nSantorini (M)\r\nMuseum 2\r\nSpa-Volt 1 (R)\r\nRV Temple\r\nThe Felling Yard\r\nSupermarket 2\r\nIndustry\r\nToytanic 1 (R)\r\nVenice"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Everyone will use the same car every race. The car will change randomly every race from the [Super Pro](https://re-volt.io/online/other-content) car pool. Pick-ups will be enabled.

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [Super Pros](https://distribute.re-volt.io/packs/io_superpros.zip)