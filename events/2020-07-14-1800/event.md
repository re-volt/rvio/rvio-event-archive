---
title: 'Advanced Races'
titleoverride: true
date: '14-07-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Rooftop Chase: Redux — 3 laps\r\n    Wildland — 3 laps\r\n    Metro-Volt — 3 laps\r\n    Sakura — 2 laps\r\n    Swan Street (R) — 3 laps\r\n    Hull Breach 3000 — 3 laps\r\n    Botanical Garden — 6 laps\r\n    Supermarket 2 (R) — 8 laps\r\n    Toy World 1 — 6 laps\r\n    Grisville — 3 laps\r\n    Ghost Town 2 — 4 laps\r\n    Toy World Aquatica: Redux — 3 laps\r\n    Spa-Volt 2 (R) — 3 laps\r\n    ToySoldierz — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

