---
title: 'Online Platinum Cup'
titleoverride: false
date: '03-04-2020 18:00'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Online Platinum Cup'
    classlink: 'https://re-volt.io/events/2020-04-03-1800'
    tracklist: "Rooftop Chase Redux R\r\nGrisville M\r\nToys in the Hood 1\r\nMuseum 2 R\r\nToy World 1 RM"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 4 cup standings and individual player progress, be sure to [check this](https://www.dropbox.com/s/qfzn2vwewz114d7/season%204%20cup%20session%20info.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Online Car Pack](https://www.dropbox.com/sh/eq0zk2ivb0smsfi/AAD4esNGAJ65nSnOdP8kE2PWa?dl=1) (advised)

### Car Selection
* -Amw
* -Gear Gt
* -Night Drifter
* -Panga
* -Prophet
* -Sir Gleam
* -Vitesse