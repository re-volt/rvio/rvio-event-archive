---
title: 'Semi-Pro Races'
titleoverride: false
date: '17-01-2019 19:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 2\r\nMolten Caverns\r\nSakura\r\nHoliday Camp: California Edition (R)\r\nBiohazard Factory\r\nToytanic 2 (R)\r\nPetroVolt\r\nCactus-Volt\r\nKadish Sprint\r\nMK64 Wario Stadium\r\nSupermarket 1\r\nJailhouse Rock\r\nRooftops\r\nSkating Toys (R)\r\nIndustry"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

