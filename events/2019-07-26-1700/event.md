---
title: 'Clockwork Races'
titleoverride: false
date: '26-07-2019 17:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Classic Clockworks'
    classlink: 'https://re-volt.io/events/2019-07-26-1800'
    tracklist: "Ghost Town 2\r\nChilled to the Bone\r\nToy World 2 R\r\nPalm Marsh M\r\nToy World Mayhem\r\nIsola Verde\r\nHallow's Eve\r\nEver After\r\nBotanical Garden R\r\nRoad in the Sky\r\nLunar\r\nHoliday Camp: California Edition R\r\nToys in the Hood 2\r\nPetroVolt"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

### Requirements
* [Classic Clockworks](https://distribute.re-volt.io/packs/io_clockworks.zip)
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip)