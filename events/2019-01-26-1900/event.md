---
title: 'Amateur Races'
titleoverride: false
date: '26-01-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Overground\r\nSakura (R)\r\nRooftops \r\nToy World Mayhem\r\nSantorini (R)\r\nThe Bunker\r\nRooftop Chase \r\nPetroVolt\r\nToy World 1\r\nToys in the Hood 2\r\nHelios\r\nSnowy River\r\nSupermarket 1 (R)\r\nRe-Ville"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

