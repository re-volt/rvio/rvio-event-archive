---
title: 'Rookie Races'
titleoverride: true
date: '03-12-2020 19:00'
event:
    host: 'Frosttbitten & Mighty'
    ip: 'frost.rv.gl OR mighty.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Metro-Volt — 3 laps\r\nLunar — 3 laps\r\nSchools Out! 2 (R) — 2 laps\r\nToytanic 1 — 3 laps\r\nWhite Rose Chapel — 2 laps\r\nRV Temple — 2 laps\r\nBiohazard Factory — 2 laps\r\nRooftops (R) — 3 laps\r\nSupermarket 1 — 4 laps\r\nGhost Town 1 — 5 laps\r\nSantorini — 4 laps\r\nWildland (R) — 3 laps\r\nCliffside — 5 laps\r\nStadVolt — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
---

