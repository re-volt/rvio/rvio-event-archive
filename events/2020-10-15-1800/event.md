---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '15-10-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World Mayhem (R) — 4 laps\r\nMetro-Volt — 3 laps\r\nBotanical Garden — 6 laps\r\nSnowland 1 (R) — 3 laps\r\nSkating Toys: Redux — 2 laps\r\nHull Breach 3000 — 4 laps\r\nJailhouse Rock — 3 laps\r\nRooftops — 3 laps\r\nGhost Town 1 — 6 laps\r\nImages of Giza: Redux — 5 laps\r\nGrisville — 3 laps\r\nRV Temple — 2 laps\r\nToy World 1 (R) — 6 laps\r\nMeltdown — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

