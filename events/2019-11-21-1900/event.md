---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '21-11-2019 19:00'
event:
    host: 'Skarma & OhNej'
    ip: 'sk.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 2\r\nPenny Racers - Caves\r\nSwan Street\r\nToy World 2\r\nToySoldierz\r\nMuseum 1\r\nQuake! (R)\r\nMolten Caverns\r\nAMCO TT (R)\r\nSantorini\r\nToy World Mayhem\r\nSpa-Volt 1\r\nToytanic 2 (R)\r\nVenice (M)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

