---
title: 'Last Man Standing'
titleoverride: false
date: '08-10-2018 18:00'
event:
    host: Instant
    ip: i.rv.gl
    category: special
    mode: last-man-standing
    class: other
    otherclass: 'Classic Clockworks'
    classlink: 'https://distribute.re-volt.io/packs/io_clockworks.zip'
    tracklist: "LMS Chinatown\r\nLMS Rooftops\r\nLMS Temple 2\r\nLMS Toshinden Arena\r\nLMS Toshinden Mona"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Requirements:
* [LMS Arena Collection](https://distribute.re-volt.io/packs/io_lmstag.zip)
* [Classic Clockworks](https://distribute.re-volt.io/packs/io_clockworks.zip)