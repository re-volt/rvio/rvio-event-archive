---
title: 'Pro Slugs'
titleoverride: false
date: '10-11-2018 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: prugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 1\r\nJailhouse Rock\r\nToys in the Hood 2\r\nSuperMarket 2 (R)\r\nRanch\r\nGrisville\r\nPalm Marsh (R)\r\nOverground\r\nMeltdown\r\nYABBA DABBA DOO!\r\nBlood on the Rooftops\r\nCactus-Volt\r\nQuake!\r\nPetroVolt (R)\r\nToytanic 2"
    vod: 'https://www.youtube.com/watch?v=zPyE0Qx7dhg'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-10_20-04-08.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

