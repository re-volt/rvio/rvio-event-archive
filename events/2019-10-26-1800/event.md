---
title: 'Halloween Carnival: Day 1'
titleoverride: false
date: '26-10-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Halloween Carnival: Day 1'
    classlink: 'https://re-volt.io/blog/halloween-carnival-2019'
    tracklist: "Hallows Eve\r\nCreepy Canyon\r\nReturn to Goblin Wood\r\nKhadish Sprint\r\nVampira\r\nZombie\r\nForest of Horror\r\nSCP: Containment Breach\r\nRooftops\r\nWaterfall\r\nMiner's End\r\nThe Bunker\r\nThe Citadel\r\nCastle Keepers"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

