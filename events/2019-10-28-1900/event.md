---
title: 'Super Pro Races'
titleoverride: true
date: '28-10-2019 19:00'
event:
    host: Saffron
    ip: saff.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "Rooftops\r\nRanch\r\nSakura\r\nPetroVolt\r\nThe Felling Yard\r\nMoon Dawn (R)\r\nSupermarket 2\r\nDonut Plains 3\r\nToys in the Hood 1 (R)\r\nToy World Mayhem (R)\r\nIllusion\r\nThe Bunker\r\nGhost Town 1\r\nToySoldierz\r\nRV Temple\r\nQuake!"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**[Super Pros](https://distribute.re-volt.io/packs/io_superpros.zip)** are required.