---
title: 'Get Bulli''d Elimination'
titleoverride: false
date: '06-04-2020 18:00'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'Get Bulli''d Elimination'
    classlink: 'https://re-volt.io/events/2020-04-06-1800'
    tracklist: 'Random Tracks'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

i will be hosting a **Get Bulli'd Elimination** session on **Monday, 6 April** at **18 UTC**.
this session is similar to a traditional per-race elimination mode, with a twist.
it is like Elimination mode combined with Cops&Robbers.

——

everyone starts by using **Ceyx**. 
__any Ceyx player who gets eliminated must change to **Battaglia**.__

Battaglia players no longer "count" in the race. Ceyx players will race as usual, but,
__the job of Battaglia players is to cause as much chaos as possible to disrupt Ceyx racers.__

to disrupt Ceyx players, Battaglia players can block others, drive in opposite direction, push racers, slowing down to use pickups, and so on. to put in into Cops&Robbers analogy, Battaglia is the 'cops' whose jobs is to disrupt Ceyx being the 'robbers'. both sides can do whatever they please to win and survive ☢️ 

——

here's how the cycle works:
- at the start, everyone^ will be using Ceyx car. everyone will race as usual.
- anyone who places last on the race, must change their car to Battaglia.
- Ceyx players will race as usual, but Battaglia players will try to cause as much chaos as possible.
- any Ceyx player who places last on the race (among other Ceyx players), must change to Battaglia.
- the cycle repeats, until there is only one Ceyx remaining.


^ the host, Shara, will start with Battaglia. the host will immediately cause chaos from the start

in short, all start as Robber*(Ceyx)*, anyone eliminated becomes Cop*(Battaglia)*
——

we will race on stock and I/O main tracks, 
in randomized selection with no track repeats, 
for 4 laps in Arcade mode with pickups on.

due to the nature of elimination mode, late joiners are not allowed to partake in Ceyx races.
late joiners can still join as Battaglia to cause havoc.



Ceyx: <http://revoltzone.net/sitescripts/dload.php?id=47724>

Battaglia: <http://revoltzone.net/sitescripts/dload.php?id=37078>

I/O main tracks: <https://distribute.re-volt.io/packs/io_tracks.zip>