---
title: 'Bronze Cup'
titleoverride: false
date: '29-06-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Bronze Cup'
    classlink: 'https://re-volt.io/blog/online-championship-mode-tournament'
    tracklist: "Toys in the Hood 1 (3 laps)\r\nSuperMarket 2 (4 laps)\r\nMuseum 2 (3 laps)\r\nBotanical Garden (5 laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Signing up is required for participation in this event.** [More information here.](https://re-volt.io/blog/online-championship-mode-tournament)

### Participants
|  Player  |  Car  |
|  :-----          |  :-----          |
|  Dovahkin |  Dr. Grudge |
|  Rider450 |  Col. Moss |
|  Daniel Carlos |  Phat Slug |
|  NightZin |  Dust Mite |
|  Jsn |  RC Bandit |
|  Daniel |  Harvester |
|  Atlas9983 |  Volken Turbo |
|  hajducsekb |  Sprinter XL |