---
title: 'Amateur Races'
titleoverride: false
date: '18-05-2019 18:00'
event:
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 1 (R)\r\nRooftop Chase: Redux (R)\r\nMuseum 2\r\nSwan Street\r\nRanch\r\nPetroVolt (R)\r\nMetro-Volt\r\nToySoldierz\r\nWhite Rose Chapel\r\nFool's Mate 2\r\nToy World 1\r\nYABBA DABBA DOO!\r\nGhost Town 1\r\nSakura"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

