---
title: 'Semi-Pro Races'
titleoverride: false
date: '25-06-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Santorini\r\nRooftop Chase: Redux RM\r\nFiddlers on the Roof\r\nToytanic 1\r\nAMCO TT\r\nRV Temple\r\nThe Felling Yard\r\nSakura\r\nSuperMarket 1\r\nRooftops R\r\nRanch\r\nSpa-Volt 1 R\r\nHoliday Camp: California Edition\r\nSnowy River\r\nToys in the Hood 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

