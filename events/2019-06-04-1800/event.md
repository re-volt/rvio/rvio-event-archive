---
title: 'Semi-Pro Races'
titleoverride: false
date: '04-06-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 1\r\nSnowy River\r\nSkating Toys\r\nSakura\r\nRanch\r\nToy World Mayhem\r\nYABBA DABBA DOO!\r\nRooftop Chase: Redux\r\nRV Temple\r\nAMCO TT (R)\r\nVenice (R)\r\nToytanic 1 (M)\r\nGhost Town 1\r\nOverground\r\nSupermarket 2 (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

