---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '30-11-2019 19:00'
event:
    host: 'Skarma & Saffron'
    ip: 'sk.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "RV Temple\r\nHull Breach 3000\r\nToy World 1\r\nWhite Rose Chapel\r\nKadish Sprint\r\nLunar\r\nSpa-Volt 2 (R) (M)\r\nSnowland 1\r\nSkating Toys\r\nBotanical Garden\r\nFool's Mate 2\r\nRooftops\r\nPetroVolt\r\nToytanic 2 (R)\r\nRooftop Chase: Redux\r\nMolten Caverns (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

