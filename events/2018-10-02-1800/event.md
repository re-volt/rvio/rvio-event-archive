---
title: 'Pro Slugs'
titleoverride: false
date: '02-10-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Pro Slugs'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Botanical Garden\r\nToys in the Hood 1\r\nPenny Racers - Caves\r\nIllusion\r\nToy World Mayhem\r\nFool's Mate 2\r\nRanch (R)\r\nLunar\r\nRooftops (R)\r\nSwan Street\r\nToytanic 1\r\nRooftop Chase (R)\r\nCactus-Volt\r\nOverground\r\nJailhouse Rock"
    vod: 'https://www.youtube.com/watch?v=rNR3e85IaM4'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-02_21-04-08.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

