---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '04-02-2020 19:00'
event:
    host: 'Skarma & OhNej'
    ip: 'sk.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Snowy River — 4 laps\r\nToySoldierz — 4 laps\r\nToy World 2 — 4 laps\r\nHelios — 3 laps\r\nSchool's Out — 2 laps\r\nPetroVolt — 2 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nSnowland 1 (M) — 3 laps \r\nSupermarket 2 — 8 laps\r\nGhost Town 2 (R) — 4 laps\r\nToytanic 2 — 3 laps\r\nFiddlers on the Roof: Redux — 2 laps\r\nMoon Dawn (R) — 2 laps\r\nHull Breach 3000 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

