---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '14-11-2020 19:00'
event:
    host: 'Bismarck & Frostbitten'
    ip: 'bis.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Spa-Volt 2 — 4 laps\r\nSnowland 1 — 4 laps\r\nIndustry — 3 laps\r\nMedieval: Redux — 5 laps\r\nHull Breach 3000 — 4 laps\r\nThe Bunker — 4 laps\r\nSchools Out! 2 — 2 laps\r\nGhost Town 2 — 5 laps\r\nSupermarket 2 — 9 laps\r\nMuseum 1 (R) — 3 laps\r\nMoon Dawn (R) — 2 laps\r\nWhite Rose Chapel — 3 laps\r\nToys in the Hood 2 — 4 laps\r\nSpa-Volt 1 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

