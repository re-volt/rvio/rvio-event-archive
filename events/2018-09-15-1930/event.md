---
title: Knockout
titleoverride: false
date: '2018-09-15 19:30'
event:
    host: Whitedoom
    category: unofficial
    mode: race
    class: other
    otherclass: Knockout
    classlink: 'https://www.dropbox.com/s/evna7pslcw0rgvz/re-volt%20knockout%20session%20info.txt?dl=0'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Players are allowed to drive any of the cars provided in the packs below. You can also submit a car by sending it to Whitedoom via a DM on [Discord](https://re-volt.io/discord). The only restriction is that it may not have a top speed faster than 40 MPH. Submissions must be sent before [17:00 UTC](time.unitarium.com/utc/17)!

**Requirements:**
* [I/O Car Collection](https://distribute.re-volt.io/packs/io_cars.zip)
* [I/O Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [I/O Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip)
* [I/O Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip)
* [I/O Real Cars](https://files.re-volt.io/packs/rlcars.zip)

**Optional:**
* [Super Tier Pack](https://www.dropbox.com/sh/5zchczny8ms2wpf/AADYbF6XdAdYer8Lk64jDEtEa?dl=1)
* User-submitted content (to be announced on [Discord](https://re-volt.io/discord))

For more information about Knockout races, [click here](https://www.dropbox.com/s/evna7pslcw0rgvz/re-volt%20knockout%20session%20info.txt?dl=0).