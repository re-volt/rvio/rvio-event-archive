---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '12-11-2020 19:00'
event:
    host: 'Ashen Forest & OhNej'
    ip: 'ash.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Lunar — 3 laps\r\nMysterious Toy-Volt Factory 2 — 2 laps\r\nCasino RV (R) — 3 laps\r\nBotanical Garden — 6 laps\r\nToy World 2 — 5 laps\r\nVenice (R) — 3 laps\r\nGhost Town 1 — 6 laps\r\nMuseum 2 (R) — 4 laps\r\nQuake! — 2 laps\r\nFools Mate 2 — 4 laps\r\nSakura — 3 laps\r\nKadish Sprint — 2 laps\r\nBlood on the Rooftops — 3 laps\r\nToy World Aquatica: Redux — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

