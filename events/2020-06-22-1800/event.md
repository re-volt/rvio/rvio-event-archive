---
title: 'Rookie Races'
titleoverride: true
date: '22-06-2020 18:00'
event:
    host: Geromu
    ip: 213.167.22.211
    category: other
    mode: race
    class: other
    otherclass: Rookie
    classlink: 'https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0'
    tracklist: "Ghost Town 3\r\nRe-Ville - 4 laps\r\nThe Felling Yard - 4 laps\r\nSuperMarket 2 - 4 laps\r\nMuseum 2 - 4 laps\r\nPenny Racers - Harbour - 4 laps\r\nCake - 4 laps\r\nRanch - 4 laps\r\nHoliday Camp: California Edition - 4 laps\r\nRadioactive Garden - 4 laps\r\nSpa-Volt 1 - 4 laps\r\nPenny Racers - Caves - 4 laps\r\nFiddlers on the Roof - 4 laps\r\nMikes Medieval Mayhem - 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirement:
* [RVON community track pack 20.2](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1) (needed)
* [RVON community car pack 20.2](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (needed)
* [RVON car list](https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0)