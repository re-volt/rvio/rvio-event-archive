---
title: Semi-Pros
titleoverride: false
date: '13-10-2018 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: semi-pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ground N Smash 2\r\nDonut Plains 3 (R)\r\nToytanic 1\r\nKadish Sprint\r\nMuseum 2\r\nBotanical Garden\r\nThe Felling Yard\r\nMK64 Wario Stadium\r\nToy World 2 (R)\r\nVenice\r\nToys in the Hood 2\r\nRanch (R)\r\nMolten Caverns\r\nHelios\r\nSpa-Volt 1"
    vod: 'https://www.youtube.com/watch?v=FW1ZralZ7AI'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-13_20-06-02.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

