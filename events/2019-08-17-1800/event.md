---
title: 'Amateur Races'
titleoverride: false
date: '17-08-2019 18:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 1 R\r\nSantorini M\r\nIllusion\r\nJailhouse Rock\r\nSwan Street\r\nRooftops\r\nSpa-Volt 2 R\r\nCliffside\r\nSupermarket 2\r\nAMCO TT R\r\nVenice \r\nToy World 2\r\nToy World Mayhem \r\nPenny Racers - Caves"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

