---
title: 'Semi-Pro Races'
titleoverride: true
date: '25-06-2020 19:30'
event:
    host: Geromu
    ip: 213.167.22.211
    category: unofficial
    mode: race
    class: other
    otherclass: Semi-Pro
    classlink: 'https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0'
    tracklist: "White Rose Chapel - 4 laps\r\nPenny Racers - Caves - 4 laps\r\nRoad in the Sky - 4 laps\r\nSwan Street - 4 laps\r\nToys in the Hood 1 - 4 laps\r\nMetro-Volt - 4 laps\r\nBotanical Garden - 4 laps\r\nMuseum 1 - 4 laps\r\nToy World Mayhem - 4 laps\r\nSakura - 4 laps\r\nRooftop Chase Redux - 4 laps\r\nHallows Eve - 4 laps\r\nSuperMarket 2 - 4 laps\r\nSpa-Volt 2 - 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirement:
* [RVON community track pack 20.2](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1) (needed)
* [RVON community car pack 20.2](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (needed)
* [RVON car list](https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0)