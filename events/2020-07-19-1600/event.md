---
title: 'Semi-Pro Races'
titleoverride: true
date: '19-07-2020 16:00'
event:
    host: Frosttbitten
    ip: frost.rv.gl
    category: competitive
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_skins
        - io_tracks
    tracklist: "Toy World Mayhem - 4 laps\r\nGhost Town 2 - 4 laps\r\nCake - 3 laps\r\nWhite Rose Chapel - 3 laps\r\nJailhouse Rock - 3 laps\r\nBiohazard Factory - 2 laps\r\nMuseum 3 - 3 laps\r\nQuake! - 2 laps\r\nVenice - 3 laps\r\nSupermarket 1 - 4 laps\r\nSakura - 3 laps\r\nMetro-Volt - 3 laps\r\nRadioactive Garden - 5 laps\r\nRoute-77 - 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

