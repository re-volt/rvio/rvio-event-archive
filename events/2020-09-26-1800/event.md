---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '26-09-2020 18:00'
event:
    host: 'Ashen Forest & Etneus'
    ip: 'ash.rv.gl OR etn.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Radioactive Garden — 5 laps\r\nSpa-Volt 1 — 3 laps\r\nStadVolt — 4 laps\r\nToys in the Hood 2 — 4 laps\r\nRV Temple — 2 laps\r\nSantorini — 4 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nLunar — 3 laps\r\nToytanic 2 (R) — 3 laps\r\nToy World 2 — 5 laps\r\nCasino RV — 3 laps\r\nSwan Street (R) — 3 laps\r\nOverground — 3 laps\r\nGhost Town 1 — 6 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

