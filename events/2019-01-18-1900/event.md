---
title: 'Drift Races'
titleoverride: false
date: '18-01-2019 19:00'
event:
    host: Saffron
    ip: 'announced on Discord'
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://files.re-volt.io/packs/drift_cars.7z'
    tracklist: "Eggland\r\nSideways Sanctuary D\r\nRV-Simo On-Road Track\r\nLS Kart Space I\r\nSideways Sanctuary B\r\nAMCO Driftume\r\nFoxglen\r\nSideways Sanctuary C\r\nMelville\r\nWarehouse Showdown\r\nSideways Sanctuary A\r\nStagnaro\r\nTVGP Bitume"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only **D1GP** or **D2GP** cars are allowed.

**Requirements**
* [Drift Cars](https://files.re-volt.io/packs/drift_cars.7z)
* [Drift Tracks](https://files.re-volt.io/packs/drift_tracks.7z)

The races will have 4 laps.