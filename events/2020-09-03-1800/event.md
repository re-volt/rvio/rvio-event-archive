---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '03-09-2020 18:00'
event:
    host: 'Etneus & Ashen Forest'
    ip: 'etn.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Supermarket 2 (R) — 8 laps\r\nGhost Town 1 — 6 laps\r\nRooftops 1 — 2 laps\r\nSpa-Volt 2 — 3 laps\r\nKadish Sprint — 2 laps\r\nIndustry — 3 laps\r\nGhost Town 2 — 4 laps\r\nMoon Dawn — 2 laps\r\nSwan Street (R) — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nSakura (R) — 2 laps\r\nSantorini — 4 laps\r\nHoliday Camp: California Edition — 2 laps\r\nToy World Mayhem — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

