---
title: 'Amateur Knockout'
titleoverride: false
date: '30-09-2018 19:30'
event:
    host: Whitedoom
    category: unofficial
    mode: race
    class: other
    otherclass: 'Amateur Knockout'
    classlink: 'https://www.dropbox.com/s/evna7pslcw0rgvz/re-volt%20knockout%20session%20info.txt?dl=0'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

You may only select cars that are part of the I/O [Slugs](https://re-volt.io/online/car-classes) class or have the **Amateur** rating in-game. Lower ratings are also allowed.

**Requirements:**
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip)
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip)