---
title: 'Amateur Races'
titleoverride: false
date: '24-09-2019 18:00'
event:
    host: flyboy
    ip: fly.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 2\r\nPenny Racers - Harbour (R)\r\nRadioactive Garden\r\nPenny Racers - Caves\r\nMuseum 1 (M)\r\nPetroVolt (R)\r\nHoliday Camp: California Edition\r\nToy World Mayhem\r\nIllusion\r\nRooftops (R)\r\nGrisville\r\nGhost Town 2\r\nVenice\r\nRadioactive Garden"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

