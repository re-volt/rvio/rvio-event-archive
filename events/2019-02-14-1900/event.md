---
title: 'Advanced Races'
titleoverride: false
date: '14-02-2019 19:00'
event:
    host: Sarorii
    ip: sas.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "AMCO TT (R)\r\nSupermarket 2\r\nToy World Mayhem\r\nSwan Street\r\nSantorini\r\nLunar\r\nToytanic 2 (R)\r\nPenny Racers - Harbour\r\nGhost Town 2\r\nPetroVolt (R)\r\nRadioactive Garden\r\nJailhouse Rock\r\nDonut Plains 3\r\nToy World 1\r\nMolten Caverns"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

