---
title: 'Random Rookies'
titleoverride: false
date: '2018-09-12 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Rookies'
    tracklist: "Sakura\r\nVenice\r\nJailhouse Rock\r\nHelios\r\nMuseum 1\r\nGhost Town 1\r\nAMCO TT\r\nToy World Mayhem\r\nRooftops\r\nToys in the Hood 2\r\nSuperMarket 1\r\nSantorini\r\nOverground\r\nGhost Town 2\r\nBotanical Garden\r\nPetroVolt"
    vod: 'https://www.youtube.com/watch?v=duvWM9Arz-o'
    results: 'http://online.re-volt.io/sessions/results.php?file=competitive/session_2018-09-12_21-03-06.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

