---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '23-01-2020 19:00'
event:
    host: 'Delecto & Kirioso'
    ip: 'dele.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Rooftop Chase: Redux (R) — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nVenice — 3 laps\r\nSupermarket 2 — 8 laps\r\nToy World 2 (R) (M) — 4 laps\r\nToytanic 1 — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nFiddlers on the Roof: Redux — 3 laps\r\nMetro-Volt — 3 laps\r\nThe Felling Yard — 3 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nAMCO TT — 3 laps\r\nSantorini — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

