---
title: 'Amateur Races'
titleoverride: false
date: '29-03-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 2\r\nRanch (R)\r\nToySoldierz\r\nCliffside\r\nGhost Town 2 (R)\r\nOverground\r\nSpa-Volt 2\r\nRooftops\r\nWhite Rose Chapel\r\nJailhouse Rock\r\nBlood on the Rooftops\r\nSupermarket 2\r\nPalm Marsh (R)\r\nRV Temple"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

