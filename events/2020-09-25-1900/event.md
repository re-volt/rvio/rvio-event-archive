---
title: 'Classic Races <small> Dual Lobby </small>'
titleoverride: true
date: '25-09-2020 19:00'
event:
    host: 'mmud & duc'
    ip: 'mmud.rv.gl OR hb.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Stock Pro Cars'
    classlink: 'https://revolt.fandom.com/wiki/List_of_Re-Volt_cars'
    tracklist: "Toys in the Hood 1 — 3 laps\r\nSuperMarket 2 — 8 laps\r\nMuseum 2 — 4 laps\r\nBotanical Garden — 6 laps\r\nRooftops — 3 laps\r\nToy World 1 — 6 laps\r\nGhost Town 1 — 6 laps\r\nToy World 2 — 5 laps\r\nToys in the Hood 2 — 4 laps\r\nToytanic 1 — 3 laps\r\nMuseum 1 — 3 laps\r\nSuperMarket 1 — 4 laps\r\nGhost Town 2 — 5 laps\r\nToytanic 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Classic Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Only stock content!** DC cars are **not** allowed.

### Car Selection:
* AMW
* Cougar
* Humma
* Panga
* Toyeca