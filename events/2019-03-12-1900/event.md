---
title: 'Semi-Pro Races'
titleoverride: false
date: '12-03-2019 19:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Skating Toys\r\nHoliday Camp: California Edition\r\nToytanic 2\r\nQuake! (R)\r\nCactus-Volt\r\nAMCO TT (R)\r\nMeltdown\r\nOverground\r\nRanch\r\nMuseum 2 (R)\r\nYABBA DABBA DOO!\r\nToys in the Hood 1\r\nSuperMarket 2\r\nLunar\r\nThe Gorge"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

