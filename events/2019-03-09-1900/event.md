---
title: 'Pro Races'
titleoverride: false
date: '09-03-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "PetroVolt\r\nDonut Plains 3 (R)\r\nLunar\r\nYABBA DABBA DOO!\r\nSkating Toys\r\nMuseum 1\r\nToys in the Hood 1\r\nCactus-Volt\r\nPenny Racers - Harbour\r\nBotanical Garden\r\nToytanic 2 (R)\r\nSakura\r\nKadish Sprint\r\nToySoldierz\r\nSnowy River\r\nRanch (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

