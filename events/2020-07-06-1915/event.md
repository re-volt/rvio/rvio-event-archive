---
title: 'Saeger Vs Insane'
titleoverride: true
date: '06-07-2020 19:15'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Saeger / Insane'
    classlink: 'https://re-volt.io/events/2020-07-06-1800'
    packages:
        - io_cars
        - io_tracks
        - io_tracks_circuit
    tracklist: "Smashride Circuit (R) - 3 laps\r\nPetroVolt - 2 laps\r\nSpecial Stage Route 5 - 5 laps\r\nMetro-Volt - 3 laps\r\nMoon Dawn - 2 laps\r\nAutumn Ring - 5 laps\r\nToy World Mayhem - 4 lap\r\nSnowy River - 4 laps\r\nToys in the Hood 1 (R) - 4 laps\r\nHull Breach 3000 - 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Saeger Vs Insane'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirements:
* [I/O Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [I/O Circuit Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [Insane](http://revoltzone.net/cars/46498/Insane)
* [Saeger](https://gitlab.com/re-volt/rvio/cars/-/archive/master/cars-master.zip?path=cars/saeger) (I/O version)