---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '20-08-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Rookies\r\n\r\n    Meltdown — 4 laps\r\n    Wildland — 3 laps\r\n    Rooftops — 3 laps\r\n    Toy World 1 — 5 laps\r\n    Metro-Volt — 3 laps\r\n    Museum 3 — 3 laps\r\n    PetroVolt (R) — 2 laps\r\n    Toys in the Hood 1 (R) — 3 laps\r\n    Spa-Volt 2 — 3 laps\r\n    Toytanic 1 — 3 laps\r\n    Sakura — 2 laps\r\n    Molten Caverns (R) — 2 laps\r\n    Moon Dawn — 2 laps\r\n    Kadish Sprint — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

