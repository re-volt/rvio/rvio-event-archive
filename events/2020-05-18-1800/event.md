---
title: 'Island SportCar'
titleoverride: true
date: '18-05-2020 18:00'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: Zettai
    classlink: 'http://revoltzone.net/cars/49393/Zettai'
    tracklist: "1. GPX\r\n2. Toys in the Hood 1\r\n3. MKDD - Mushroom City\r\n4. Far-off Island\r\n5. GP1\r\n6. Snake Pliskin\r\n7. Another Generic Raceway\r\n8. Re-VoLectriX\r\n9. SS Highway Re-Volt\r\n10. FAT-TRACK\r\n11. Museum 2\r\n12. F1 RC GP"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Island SportCar'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

i will be hosting an Island SportCar session on Monday, 18th May at 18 UTC.
we will be racing with only Zettai car, a recreation of Island SportCar from TrackMania series.
we will be racing on wide open fast-paced tracks, for 3 laps, in Arcade mode, with pickups disabled.

---
despite the reputation of fun fast-paced drifty slides that Island SportCar has,
this session isnt as fun as might be initially thought.
be sure to use the custom camera view, for a more authentic TrackMania experience.

car: Zettai

---
Zettai island sportcar    : [http://revoltzone.net/cars/49393/Zettai](http://revoltzone.net/cars/49393/Zettai)

track content download: [https://www.dropbox.com/s/l2ybp0xltycmwio/islandsportcarsession.zip?dl=1](https://www.dropbox.com/s/l2ybp0xltycmwio/islandsportcarsession.zip?dl=1)