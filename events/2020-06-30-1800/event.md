---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '30-06-2020 18:00'
event:
    host: 'Ashen Forest & Etneus'
    ip: 'ash.rv.gl OR etn.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Toys in the Hood 2 — 3 laps\r\n    Jailhouse Rock — 2 laps\r\n    Toy World 1 (R) — 5 laps\r\n    Museum 3 — 3 laps\r\n    Wildland — 3 laps\r\n    Spa-Volt 2 (R) — 3 laps\r\n    Metro-Volt — 3 laps\r\n    Spa-Volt 1 — 3 laps\r\n    Ghost Town 1 — 6 laps\r\n    Molten Caverns (R) — 2 laps\r\n    Radioactive Garden — 5 laps\r\n    Rooftop Chase: Redux — 3 laps\r\n    Rooftops 1 — 2 laps\r\n    Supermarket 2 — 7 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

