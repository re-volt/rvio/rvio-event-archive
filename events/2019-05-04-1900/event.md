---
title: 'Pro Races'
titleoverride: false
date: '04-05-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1 (R)\r\nHelios\r\nSantorini\r\nRooftop Chase: Redux (R)\r\nCliffside\r\nGrisville\r\nKadish Sprint\r\nToy World Mayhem (R)\r\nMuseum 1\r\nYABBA DABBA DOO!\r\nHoliday Camp: California Edition\r\nMuseum 2\r\nWhite Rose Chapel\r\nMetro-Volt\r\nRooftops \r\nBlood on the Rooftops"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

