---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '09-06-2020 18:00'
event:
    host: 'Dvark & Etneus'
    ip: 'dv.rv.gl OR etn.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Rooftop Chase: Redux — 3 laps\r\nToy World 2 — 4 laps\r\nOverground — 2 laps\r\nSantorini (R) — 4 laps\r\nSakura — 2 laps\r\nGrisville — 3 laps\r\nMuseum 1 — 3 laps\r\nGhost Town 1 — 6 laps\r\nPetroVolt (R) — 2 laps\r\nAMCO TT — 3 laps\r\nBiohazard Factory — 2 laps\r\nSupermarket 1 (R) — 4 laps\r\nQuake! — 2 laps\r\nHelios — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

