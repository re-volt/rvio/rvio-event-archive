---
title: 'Rookie Races'
titleoverride: false
date: '23-03-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Jailhouse Rock\r\nDonut Plains 3\r\nSupermarket 2\r\nThe Bunker\r\nPenny Racers - Harbour\r\nSpa-Volt 1\r\nToy World 2 (R)\r\nToy World Mayhem (R)\r\nBotanical Garden\r\nToy World Aquatica: Redux\r\nRadioactive Garden\r\nGhost Town 1\r\nLunar"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

