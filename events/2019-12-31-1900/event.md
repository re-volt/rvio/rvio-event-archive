---
title: 'Random Cars <small>(Dual Lobby)</small>'
titleoverride: true
date: '31-12-2019 19:00'
event:
    host: 'Delecto & Saffron'
    ip: 'dele.rv.gl OR saff.rv.gl'
    category: special
    mode: race
    class: other
    otherclass: 'Random Cars'
    classlink: 'https://re-volt.io/online/cars'
    tracklist: "RV Temple\r\nRooftop Chase: Redux\r\nToytanic 2\r\nMolten Caverns\r\nMuseum 2 (R)\r\nHelios\r\nDonut Plains 3\r\nFiddlers on the Roof\r\nSantorini\r\nSkating Toys (R)\r\nSupermarket 2 (M)\r\nRooftops\r\nVenice (R)\r\nJailhouse Rock\r\nHull Breach 3000\r\nKadish Sprint"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

