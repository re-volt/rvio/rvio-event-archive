---
title: 'Advanced Races'
titleoverride: true
date: '29-06-2020 20:30'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Holiday Camp: California Edition (R) — 2 laps\r\nRooftops (R) — 3 laps\r\nMuseum 1 — 3 laps\r\nMedieval: Redux — 4 laps\r\nOverground — 2 laps\r\nSnowland 1 — 3 laps\r\nToys in the Hood 2 — 4 laps\r\nMuseum 3 — 3 laps\r\nMetro-Volt — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nHull Breach 3000 — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nRadioactive Garden (R) — 5 laps\r\nSpa-Volt 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

