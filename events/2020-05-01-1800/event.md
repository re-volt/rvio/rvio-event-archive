---
title: 'Lap Knockout'
titleoverride: false
date: '01-05-2020 18:00'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_tracks
        - io_cars
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

i will be hosting a Lap Knockout session on Friday, 1st May at 18 UTC.
we will be racing with Amateur class cars, from I/O Main Cars.
we will be racing on I/O main track cars in arcade mode with pickups disabled.
***
in this session, everyone will race as usual.
but, at the end of each lap, the player on last place will be eliminated.
this will continue for every lap, until there is 1 player remaining.
the number of laps are equal (decreased by one) to the number of players.
***
we will race on predetermined selection of I/O Main tracks, that is not publicly shown (because of below).
the number of tracks being raced depends on the number of players. more players means less races.
![Here's how it works.](https://i.imgur.com/cW0CKdG.png)
***
people are allowed to join late in the session. these people will play on the next race.

I/O Main Cars: [https://distribute.re-volt.io/packs/io_cars.zip](https://distribute.re-volt.io/packs/io_cars.zip)

I/O Main Tracks: [https://distribute.re-volt.io/packs/io_tracks.zip](https://distribute.re-volt.io/packs/io_tracks.zip)