---
title: 'Rookie Races'
titleoverride: false
date: '26-03-2019 19:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Supermarket 2\r\nSpa-Volt 2\r\nSnowy River\r\nBotanical Garden (R)\r\nWhite Rose Chapel\r\nToy World Mayhem\r\nGhost Town 1\r\nFool's Mate 2\r\nHelios\r\nSwan Street\r\nMetro-Volt\r\nRadioactive Garden\r\nToy World 1\r\nAMCO TT (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

