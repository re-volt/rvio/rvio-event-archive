---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '20-02-2020 19:00'
event:
    host: 'Delecto & Dvark'
    ip: 'dele.rv.gl OR dv.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Toytanic 1 — 3 laps\r\nMoon Dawn (R) (M) — 2 laps\r\nDonut Plains 3 — 5 laps\r\nGhost Town 1 — 6 laps\r\nBotanical Garden (R) — 6 laps\r\nBiohazard Factory — 2 laps\r\nFiddlers on the Roof: Redux — 3 laps\r\nRV Temple — 2 laps\r\nHull Breach 3000 — 4 laps\r\nPenny Racers - Harbour — 3 laps\r\nMuseum 2 — 4 laps\r\nJailhouse Rock — 3 laps\r\nToy World Mayhem (R) — 4 laps\r\nQuake! — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

