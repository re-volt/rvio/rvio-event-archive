---
title: 'Advanced Races'
titleoverride: false
date: '24-10-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 2 (R)\r\nSantorini (R)\r\nToy World Mayhem (R)\r\nGhost Town 1\r\nBotanical Garden\r\nToys in the Hood 1\r\nSnowy River (M)\r\nRe-Ville\r\nKadish Sprint\r\nDonut Plains 3\r\nQuake!\r\nHelios\r\nIllusion\r\nSakura\r\nFiddlers on the Roof"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

