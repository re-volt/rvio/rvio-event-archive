---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '22-01-2020 19:30'
event:
    host: 'Saffron & Kirioso'
    ip: 'saff.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Jailhouse Rock - 3 laps\r\nToys in the Hood 2 R - 4 laps\r\nRooftops M - 4 laps\r\nPetroVolt - 2 laps\r\nSnowland 1 R - 4 laps\r\nRooftop Chase: Redux - 3 laps\r\nMoon Dawn - 2 laps\r\nToy World Mayhem R - 4 laps\r\nBotanical Garden - 6 laps\r\nSupermarket 1 - 4 laps\r\nWhite Rose Chapel - 3 laps\r\nKadish Sprint - 2 laps\r\nSpa-Volt 1 - 3 laps\r\nFiddlers on the Roof: Redux - 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**[Super Pros](https://distribute.re-volt.io/packs/io_superpros.zip)** are required.