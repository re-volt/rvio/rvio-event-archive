---
title: 'Battle Tag'
titleoverride: false
date: '01-10-2018 18:00'
event:
    host: Instant
    ip: instant.rv.gl
    category: special
    mode: battle-tag
    class: other
    otherclass: 'all cars'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Neighborhood Battle\r\nGarden Battle\r\nToy World Battle\r\nMuseum Lobby Battle\r\nParty in the Toy World\r\nMuseum Battle\r\nSupermarket Battle"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Requirements:**
* [Battle Tag Arena Collection](https://distribute.re-volt.io/packs/io_lmstag.zip)