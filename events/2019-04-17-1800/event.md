---
title: 'Random Pro Cars (Ladder)'
titleoverride: false
date: '17-04-2019 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Pro Cars (Ladder)'
    classlink: 'https://re-volt.io/events/2019-04-17-1800'
    tracklist: "Cactus Volt\r\nCake\r\nChilled to the Bone\r\nGrisville\r\nHoliday Camp: California Edition\r\nHull Breach 3\r\nKadish Sprint\r\nMolten Caverns\r\nMoon Dawn\r\nMuseum 1\r\nPetroVolt\r\nQuake!\r\nRanch\r\nRe-Ville\r\nRooftop Chase: Redux\r\nRV Temple\r\nSakura\r\nSkating Toys\r\nSpa-Volt 1\r\nSpa-Volt 2"
published: true
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
article:
    datePublished: '19-03-2019 20:55'
    dateModified: '19-03-2019 20:55'
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event will count for the **RVGL Ladder**.

* [Current Ladder standings](https://ladder.rv.gl)
* [Information & Rules](https://ladder.rv.gl/info)

You need the **RVGL Plus Pack** to join the races!

* [Download](https://kiwis.rv.gl/plus)
* [RVGL Plus Car Classes](https://kiwis.rv.gl/plus/cars)
* [RVGL Plus Track Categories](https://kiwis.rv.gl/plus/tracks)

The event will have 10 Races with randomised **Category III** Plus Pack Tracks (4 laps):