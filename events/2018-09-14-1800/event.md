---
title: 'Rookies vs. Slugs'
titleoverride: false
date: '2018-09-14 18:00'
event:
    host: Guiga
    category: unofficial
    mode: race
    class: other
    otherclass: 'Rookies vs. Slugs'
    tracklist: "Museum 1\r\nThe Felling Yard\r\nMuseum 2\r\nPetro-Volt\r\nToys in the Hood 2\r\nVenice\r\nSuperMarket 1\r\nMetro-Volt\r\nSuperMarket 2\r\nIllusion\r\nRooftops\r\nCactus-Volt\r\nToysoldierz\r\nToy World Mayhem\r\nJailhouse Rock\r\nAMCO TT"
    results: 'http://online.re-volt.io/sessions/results.php?file=session_2018-09-14_19-07-50.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

*This is a testing session.*

**Rules:**
* You may select cars from both **Rookie** and **Slug** classes
* You are not allowed to choose the same car as another player
* You may change your car, but only from the same rating and no duplicate cars

The IP will be announced on [Discord](https://re-volt.io/discord).