---
title: 'Get Bulli''d Elimination'
titleoverride: true
date: '19-06-2020 18:00'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'see event'
    classlink: 'https://re-volt.io/events/2020-06-19-1800'
    tracklist: 'Random tracks'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Get Bulli''d Elimination'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![Get Bulli'd Elimination](https://i.imgur.com/kO4y5Kp.png)
i will be hosting a **Get Bulli'd Elimination** session on **Friday, 19 April** at **18 UTC**.
this session is similar to a traditional per-race elimination mode, with a twist.

---
everyone starts by using **Ceyx**. 
any Ceyx player who gets eliminated must change to **Battaglia**.
Battaglia players no longer "count" in the race. Ceyx players will race as usual, but,
the job of Battaglia players is to cause as much chaos as possible to disrupt Ceyx racers.
to disrupt Ceyx players, Battaglia players can block others, drive in opposite direction, push racers, slowing down to use pickups, and so on. to put in into Cops&Robbers analogy, Battaglia is the 'cops' whose jobs is to disrupt Ceyx being the 'robbers'. both sides can do whatever they please to win and survive ☢️

---
here's how the cycle works:
- at the start, everyone^ will be using Ceyx car. everyone will race as usual.
- anyone who places last on the race, must change their car to Battaglia.
- Ceyx players will race as usual, but Battaglia players will try to cause as much chaos as possible.
- any Ceyx player who places last on the race (among other Ceyx players), must change to Battaglia.
- the cycle repeats, until there is only one Ceyx remaining.

^ the host, Shara, will start with Battaglia. the host will immediately cause chaos from the start

---
we will race on stock and I/O main tracks, 
in randomized selection with no track repeats (first 3 tracks is predetermined),
for 3 laps in Arcade mode with pickups enabled.
due to the nature of elimination mode, late joiners are not allowed to partake in the races as Ceyx.
late joiners can still join the lobby as Battaglia, causing havoc among Ceyx racers.

Ceyx: [http://revoltzone.net/sitescripts/dload.php?id=47724](http://revoltzone.net/sitescripts/dload.php?id=47724)

Battaglia: [http://revoltzone.net/sitescripts/dload.php?id=37078](http://revoltzone.net/sitescripts/dload.php?id=37078)

I/O main tracks: [https://distribute.re-volt.io/packs/io_tracks.zip](https://distribute.re-volt.io/packs/io_tracks.zip)
