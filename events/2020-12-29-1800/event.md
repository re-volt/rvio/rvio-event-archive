---
title: 'Advanced Races'
titleoverride: true
date: '29-12-2020 18:00'
event:
    host: Instant
    ip: ins.rv.gl
    category: competitive
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Fools Mate 2 — 4 laps\r\nSpa-Volt 1 — 3 laps\r\nRooftops — 3 laps\r\nPetroVolt — 2 laps\r\nMedieval: Redux — 4 laps\r\nHMS Invincible: Redux — 3 laps\r\nToytanic 2 — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nToys in the Hood 2 — 4 laps\r\nToys in the Hood 1 — 3 laps\r\nKadish Sprint — 2 laps\r\nRooftops 1 — 2 laps\r\nSchools Out! 2 — 2 laps\r\nLunar — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

