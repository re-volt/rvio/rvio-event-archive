---
title: 'Amateur Races'
titleoverride: false
date: '06-08-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Snowy River\r\nRooftop Chase: Redux \r\nSakura \r\nGhost Town 1\r\nFool's Mate 2\r\nSpa-Volt 1 R\r\nOverground\r\nToy World 1\r\nVenice M\r\nToySoldierz R\r\nRe-Ville\r\nToys in the Hood 2\r\nLunar\r\nMuseum 2 R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

