---
title: 'Advanced Races'
titleoverride: false
date: '01-06-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Swan Street\r\nWhite Rose Chapel\r\nIndustry\r\nToy World 2 R\r\nGhost Town 1\r\nVenice R\r\nThe Bunker\r\nPenny Racers - Harbour\r\nMuseum 2 M\r\nKadish Sprint\r\nToytanic 1\r\nPetroVolt\r\nToySoldierz R\r\nLunar\r\nQuake!"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

