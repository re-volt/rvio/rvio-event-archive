---
title: 'Drift Races'
titleoverride: false
date: '25-01-2019 19:00'
event:
    host: Saffron
    ip: 'announced on Discord'
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://files.re-volt.io/packs/drift_cars.7z'
    tracklist: "TVGP Bitume\r\nDrivers School\r\nEggland\r\nSideways Sanctuary C (R)\r\nAMCO Bitume (R)\r\nWarehouse Showdown\r\nFoxglen\r\nNamco Racer\r\nAMCO Driftume\r\nSideways Sanctuary A (R)\r\nMelville\r\nNamco Trains\r\nLS Kart Space I\r\nSideways Sanctuary B (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only **DXGP** cars are allowed.

**Requirements**
* [Drift Cars](https://files.re-volt.io/packs/drift_cars.7z)
* [Drift Tracks](https://files.re-volt.io/packs/drift_tracks.7z)

The races will have 5 laps.