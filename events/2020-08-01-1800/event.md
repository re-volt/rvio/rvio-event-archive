---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '01-08-2020 18:00'
event:
    host: 'Kirioso & Ashen Forest'
    ip: 'kiri.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Meltdown — 4 laps\r\nMuseum 1 — 2 laps\r\nWhite Rose Chapel — 2 laps\r\nOverground — 2 laps\r\nVenice (R) — 3 laps\r\nQuake! — 2 laps\r\nToytanic 2 — 3 laps\r\nRadioactive Garden — 5 laps\r\nRoute-77 (R) — 3 laps\r\nHull Breach 3000 — 3 laps\r\nLunar — 3 laps\r\nMuseum 3 — 3 laps\r\nMuseum 2 — 3 laps\r\nGhost Town 1 (R) — 6 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

