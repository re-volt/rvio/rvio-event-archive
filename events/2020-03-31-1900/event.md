---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '31-03-2020 19:00'
event:
    host: 'Saffron & Dvark'
    ip: 'saff.rv.gl OR dv.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Spa-Volt 2 – 3 laps\r\nRadioactive Garden – 5 laps\r\nToySoldierz – 3 laps\r\nSupermarket 2 (R) – 7 laps\r\nFiddlers on the Roof: Redux – 2 laps\r\nHull Breach 3000 – 3 laps\r\nSupermarket 1 – 4 laps\r\nToys in the Hood 2 – 3 laps\r\nMuseum 1 – 2 laps\r\nThe Felling Yard – 2 laps\r\nPetroVolt (R) – 2 laps\r\nRoute-77 (M) – 3 laps\r\nAMCO TT (R) – 3 laps\r\nQuake! – 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

