---
title: 'Advanced Races'
titleoverride: true
date: '10-10-2020 18:00'
event:
    host: Kirioso
    ip: kiri.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toys in the Hood 1 — 3 laps\r\nThe Felling Yard — 2 laps\r\nSpa-Volt 1 — 3 laps\r\nHull Breach 3000 — 3 laps\r\nBotanical Garden EX — 2 laps\r\nPetroVolt (R) — 2 laps\r\nSupermarket 2 — 8 laps\r\nFools Mate 2 — 4 laps\r\nGrisville — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nSpa-Volt 2 — 3 laps\r\nToy World 2 (R) — 4 laps\r\nRooftops — 3 laps\r\nSwan Street (R) — 3 lap"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

