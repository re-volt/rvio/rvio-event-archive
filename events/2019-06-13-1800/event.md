---
title: 'Advanced Races'
titleoverride: false
date: '13-06-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 1\r\nSnowy River\r\nDonut Plains 3\r\nGhost Town 2\r\nSwan Street\r\nSpa-Volt 1 R\r\nMuseum 2 R\r\nBlood on the Rooftops\r\nHelios\r\nFool's Mate 2\r\nMoon Dawn M\r\nToys in the Hood 2\r\nPetroVolt\r\nJailhouse Rock\r\nKadish Sprint"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

