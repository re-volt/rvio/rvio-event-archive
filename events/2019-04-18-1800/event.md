---
title: 'Rookie Races'
titleoverride: false
date: '18:00 18-04-2019'
event:
    host: Narusori
    ip: sas.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "White Rose Chapel\r\nSupermarket 2 \r\nDonut Plains 3\r\nSpa-Volt 2 (R)\r\nOverground\r\nSakura \r\nPenny Racers - Caves\r\nToy World 2 \r\nRadioactive Garden\r\nToy World Mayhem\r\nBotanical Garden (R)\r\nPenny Racers - Harbour\r\nLunar\r\nMuseum 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

