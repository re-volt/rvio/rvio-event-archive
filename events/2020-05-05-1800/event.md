---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '05-05-2020 18:00'
event:
    host: 'Skarma & Narusori'
    ip: 'sk.rv.gl OR naru.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_tracks
        - io_cars
    tracklist: "Industry — 3 laps\r\nBiohazard Factory (R) — 2 laps\r\nQuake — 2 laps\r\nMoon Dawn — 2 laps\r\nToy World 2 (R) — 5 laps\r\nRooftops — 3 laps\r\nCliffside — 6 laps\r\nToy World 1 — 6 laps\r\nVenice — 3 laps\r\nRooftop Chase: Redux — 3 laps\r\nJailhouse Rock (R) — 3 laps\r\nGhost Town 1 — 6 laps\r\nSwan Street — 3 laps\r\nHelios — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

