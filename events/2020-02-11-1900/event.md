---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '11-02-2020 19:00'
event:
    host: 'Kirioso & Saffron'
    ip: 'kiri.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Donut Plains 3 — 5 laps\r\nKadish Sprint — 2 laps\r\nGhost Town 1 — 6 laps\r\nVenice — 3 laps\r\nSantorini — 4 laps\r\nHelios (M) — 3 laps\r\nPetroVolt — 2 laps\r\nMuseum 2 — 4 laps\r\nRooftops — 3 laps\r\nCliffside — 6 laps\r\nMoon Dawn — 2 laps\r\nAMCO TT (R) — 3 laps\r\nToytanic 2 (R) — 3 laps\r\nSpa-Volt 2 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

