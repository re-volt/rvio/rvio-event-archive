---
title: 'Saeger Vs Insane'
titleoverride: true
date: '21-07-2020 22:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Saeger / Insane'
    classlink: 'https://re-volt.io/events/2020-07-21-2200'
    packages:
        - io_cars
        - io_tracks
        - io_tracks_circuit
    tracklist: "Autumn Ring - 5 laps\r\nToy World Mayhem - 4 lap\r\nToys in the Hood 1 R - 4 laps\r\nSpecial Stage Route 5 - 5 laps\r\nMoon Dawn - 2 laps\r\nPetroVolt - 2 laps\r\nSmashride Circuit R - 3 laps\r\nMetro-Volt - 3 laps\r\nSnowy River - 4 laps\r\nHull Breach 3000 - 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Saeger Vs Insane'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![Saeger Vs Insane](https://cdn.discordapp.com/attachments/729062586494222367/733748115881852988/SvsI.png)
Content requirements:
* [I/O Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [I/O Circuit Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [Insane](http://revoltzone.net/cars/46498/Insane)
* [Saeger](https://gitlab.com/re-volt/rvio/cars/-/archive/master/cars-master.zip?path=cars/saeger) (I/O version)