---
title: 'Amateur Races'
titleoverride: false
date: '04-04-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 1\r\nPetroVolt\r\nRooftop Chase:Redux (R)\r\nSupermarket 2\r\nKadish Sprint\r\nBlood on the Rooftops\r\nMuseum 1 (R)\r\nIllusion\r\nSpa-Volt 2 (R)\r\nSantorini\r\nJailhouse Rock\r\nFiddlers on the Roof\r\nToy World 1\r\nHelios"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

