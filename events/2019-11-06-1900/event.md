---
title: 'Random Car Races'
titleoverride: true
date: '06-11-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Random Custom Cars'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Hull Breach 3000\r\nDonut Plains 3\r\nHelios\r\nToytanic 1 (R)\r\nToys in the Hood 1\r\nRV Temple\r\nToy World Aquatica: Redux\r\nToy World Mayhem\r\nToySoldierz\r\nGrisville (R)\r\nSkating Toys\r\nRadioactive Garden (M)\r\nMolten Caverns\r\nToys in the Hood 2\r\nSakura (R)\r\nToy World 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Everyone will use the same car every race. The car will change randomly every race from the [main I/O car pool](https://re-volt.io/online/car-classes), custom cars only (including [Super Pros](https://re-volt.io/online/other-content). Pick-ups will be enabled.