---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '07-03-2020 19:00'
event:
    host: 'Delecto & Floxit'
    ip: 'dele.rv.gl OR flo.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "White Rose Chapel — 3 laps\r\nSakura — 3 laps\r\nGhost Town 2 — 4 laps\r\nSchool's Out — 2 laps\r\nSnowland 1 (R) — 3 laps\r\nSupermarket 1 — 4 laps\r\nSkating Toys — 2 laps\r\nSantorini — 4 laps\r\nMuseum 2 (M) — 4 laps\r\nJailhouse Rock (R) — 3 laps\r\nMoon Dawn — 2 laps\r\nToytanic 2 (R) — 3 laps\r\nMetro-Volt — 3 laps\r\nHull Breach 3000 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

