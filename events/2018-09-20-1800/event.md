---
title: Prugs
titleoverride: false
date: '2018-09-20 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: prugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 1\r\nSpa-Volt 1\r\nToy World Mayhem (R)\r\nMeltdown\r\nRooftop Chase\r\nIndustry\r\nPetroVolt\r\nToys in the Hood 2 (R)\r\nHoliday Camp: California Edition (R)\r\nFool's Mate 2\r\nHelios\r\nSuperMarket 1\r\nThe Bunker\r\nRooftops\r\nSwan Street"
    results: 'http://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-20_20-02-56.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**[Remember to update your custom content!](https://re-volt.io/blog/online-packs-update-september-2018)**