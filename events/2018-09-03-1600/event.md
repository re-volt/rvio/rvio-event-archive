---
title: 'Last Man Standing'
titleoverride: false
date: '2018-09-03 16:00'
event:
    host: Instant
    ip: instant.rv.gl
    category: special
    mode: last-man-standing
    class: other
    otherclass: 'Classic Clockworks'
    classlink: 'https://distribute.re-volt.io/packs/io_clockworks.zip'
    tracklist: "LMS Chinatown\r\nLMS Rooftops\r\nLMS Temple 2\r\nLMS Toshinden Arena\r\nLMS Toshinden Mona"
    vod: 'https://www.youtube.com/watch?v=CDiy5haQfuc'
    description: "Requirements:\r\n* [LMS Arena Collection](https://distribute.re-volt.io/packs/io_lmstag.zip)\r\n* [Classic Clockworks](https://distribute.re-volt.io/packs/io_clockworks.zip)"
taxonomy:
    category:
        - events
sidebarlayout: global
sidebarpath: /sidebar/events
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Requirements:
* [LMS Arena Collection](https://distribute.re-volt.io/packs/io_lmstag.zip)
* [Classic Clockworks](https://distribute.re-volt.io/packs/io_clockworks.zip)