---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '16-02-2020 18:00'
event:
    host: 'Kirioso & Saffron'
    ip: 'kiri.rv.gl OR saff.rv.gl'
    category: competitive
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Rooftop Chase: Redux (R) — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nVenice — 3 laps\r\nSupermarket 2 — 8 laps\r\nToy World 2 (R) — 4 laps\r\nToytanic 1 — 3 laps\r\nMetro-Volt — 3 laps\r\nJailhouse Rock — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nSchool's Out — 2 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nToys in the Hood 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

