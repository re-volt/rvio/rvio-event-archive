---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '22-02-2020 19:00'
event:
    host: 'URV & Saffron'
    ip: 'u.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Molten Caverns — 2 laps\r\nToy World 2 — 4 laps\r\nSpa-Volt 1 (R) (M) — 3 laps\r\nToySoldierz — 3 laps\r\nRadioactive Garden (R) — 5 laps\r\nHelios — 3 laps\r\nGhost Town 2 — 4 laps\r\nThe Bunker — 4 laps\r\nSakura — 2 laps\r\nCliffside — 5 laps\r\nVenice — 3 laps\r\nSupermarket 2 — 7 laps\r\nSwan Street — 3 laps\r\nToytanic 2 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

