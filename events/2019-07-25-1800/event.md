---
title: 'Amateur Races'
titleoverride: false
date: '25-07-2019 18:00'
event:
    host: Flyboy
    ip: fly.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "1. Helios\r\n2. Rooftop Chase: Redux\r\n3. Museum 1 \r\n4. Penny Racers - Harbour\r\n5. Quake (R)\r\n6. Hull Breach 3000\r\n7. Toys in the Hood 1 \r\n8. Blood on the Rooftops\r\n9. Jailhouse Rock\r\n10. Santorini (R)\r\n11. Toy World 2\r\n12. Sakura (M)\r\n13. SuperMarket 2 (R)\r\n14. Overground"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

