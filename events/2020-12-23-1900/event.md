---
title: 'Santa''s Team Races'
titleoverride: true
date: '23-12-2020 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Santa''s Team Races'
    classlink: 'https://re-volt.io/events/2020-12-23-1900'
    tracklist: "Ghost Town 1 — 5 laps\r\nWildland — 3 laps\r\nFairground 2 — 3 laps\r\nToy World Mayhem — 4 laps\r\nEndgame — 3 laps\r\nIllusion — 3 laps\r\nPOD: Spin — 2 laps\r\nWarehouse Showdown — 8 laps\r\nToytanic 2 — 3 laps\r\nChristmas Crib 2011 — 4 laps\r\n8 on Ice — 4 laps\r\nR2049 Tundra — 4 laps\r\nSnowland — 4 laps\r\nWinter Madness — 8 laps\r\nSkiing Paradise — 1 lap (in case of a tie) "
media_order: 'rvcc20_poster6.png,redvsblu.png'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Santa''s Team Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](rvcc20_poster6.png)
A team-based event where only the Santas race! With one Santa on each team, they must rely on their team members' help to finish ahead of the rival Santa. May the best team win!

### Rules
* There can only be one Santa car per team, with the other players having the role of Elves (regular cars).
* Each Santa's main goal is to win the race!
* Each Elf's main goal is to attack the opposing team's Santa, so *your* Santa can win the race!
* As an Elf, you do not need to follow the raceline! You can go in reverse or even cut through the track when possible.
* The Elves can change cars after every race, but only to the cars available on their team.
* You are not allowed to switch sides, unless the Host states it's needed for balancing purposes.
* At the end of each race, both Santas must assign the next Santa from someone on their team. It cannot be themselves or the previous Santa.

### Choose your side
![](redvsblu.png)

### Sign up
To reserve a spot for the race, join us on [Discord](https://discord.gg/NMT4Xdb) and obtain the Tourneys role from the `#roles` channel. Afterwards, go to `#cc20-chat` and type the following message:
```
@URV#9830 I'm joining Team Red/Blue
```
Make sure to only choose one team, depending on the car selection you prefer (see image above). Depending on how many players sign up for one team, you might not be able to join the team you want, so make sure to reserve a spot as soon as possible!

### Requirements
* [CC20 Santas](https://files.re-volt.io/packs/christmas/2020/rvcc20_santas.7z) **(New!)**
* [CC20 Cars](https://files.re-volt.io/packs/christmas/2020/rvcc20_cars.7z)
* [CC20 Tracks](https://files.re-volt.io/packs/christmas/2020/rvcc20_tracks.7z)