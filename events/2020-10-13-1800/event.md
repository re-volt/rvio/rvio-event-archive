---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '13-10-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Spa-Volt 2 (R) — 3 laps\r\nVenice — 3 laps\r\nMuseum 1 — 2 laps\r\nSupermarket 1 — 4 laps\r\nRadioactive Garden — 5 laps\r\nSwan Street — 3 laps\r\nSakura — 2 laps\r\nSchools Out! — 2 laps\r\nGhost Town 2 (R) — 4 laps\r\nRoute-77 (R) — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nSnowy River — 3 laps\r\nKadish Sprint — 2 laps\r\nToytanic 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races '
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

