---
title: Slugs
titleoverride: false
date: '20-10-2018 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "The Gorge\r\nHelios\r\nSakura (R)\r\nRooftops\r\nGrisville\r\nJailhouse Rock\r\nToys in the Hood 1\r\nRadioactive Garden\r\nToy World 2 (R)\r\nSpa-Volt 1\r\nPenny Racers - Caves\r\nMuseum 1\r\nThe Bunker\r\nPenny Racers - Harbour (R)"
    vod: 'https://www.youtube.com/watch?v=nyQcVqTeO40'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-20_20-02-26.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

