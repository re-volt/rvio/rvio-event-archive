---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '01-10-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Overground — 2 laps\r\nCliffside — 6 laps\r\nSnowland 1 — 3 laps\r\nSchool's Out! — 2 laps\r\nBotanical Garden — 6 laps\r\nRoute-77 (R) — 4 laps\r\nQuake! — 2 laps\r\nToy World 1 — 6 laps\r\nToytanic 1 (R) — 3 laps\r\nSpa-Volt 1 — 3 laps\r\nRooftops — 3 laps\r\nSnowy River — 4 laps\r\nLibrary — 3 laps\r\nSwan Street — 3 laps\r\nJailhouse Rock (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

