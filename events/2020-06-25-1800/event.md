---
title: 'Amateur Races'
titleoverride: true
date: '25-06-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Botanical Garden — 5 laps\r\n    Museum 3 — 3 laps\r\n    Ghost Town 2 (R) — 4 laps\r\n    Spa-Volt 2 (R) — 3 laps\r\n    Quake! — 2 laps\r\n    Metro-Volt — 3 laps\r\n    The Bunker — 4 laps\r\n    Supermarket 2 — 7 laps\r\n    Rooftops 1 — 2 laps\r\n    White Rose Chapel — 2 laps\r\n    Spa-Volt 1 — 3 laps\r\n    Toy World 1 — 5 laps\r\n    Overground — 2 laps\r\n    Route-77 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

