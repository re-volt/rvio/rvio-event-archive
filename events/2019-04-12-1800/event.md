---
title: 'Below Rookies'
titleoverride: false
date: '12-04-2019 18:00'
event:
    host: Shara
    ip: 'on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'Below Rookies'
    classlink: 'https://garage.re-volt.io/apps/files_sharing/publicpreview/LNde4TKzaZNrQdT?x=1908&y=593&a=true&file=2019-04-12-1930.png&scalingup=0'
    tracklist: "Botanical Garden\r\nGhost Town 1\r\nSpa-Volt 2 (R)\r\nRadioactive Garden\r\nSnowland 1\r\nSuperMarket 2\r\nDonut Plains 3\r\nYABBA DABBA DOO!\r\nCliffside\r\nSnowy River"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Below-average performance cars with the Rookie ratings. Short tracks, 3 laps, and pick-ups enabled.