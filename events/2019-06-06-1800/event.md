---
title: 'Pro Races'
titleoverride: false
date: '06-06-2019 18:00'
event:
    host: Flyboy
    ip: fly.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Santorini\r\nHoliday Camp: California Edition (R)\r\nThe Felling Yard\r\nJailhouse Rock\r\nRadioactive Garden\r\nIndustry\r\nMoon Dawn\r\nMuseum 2\r\nSnowland 1\r\nQuake!\r\nWhite Rose Chapel (M)\r\nRooftops\r\nSupermarket 1 (R)\r\nGhost Town 2\r\nLunar\r\nPenny Racers - Caves (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

