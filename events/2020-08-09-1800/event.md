---
title: 'Super Pro Races'
titleoverride: true
date: '09-08-2020 18:00'
event:
    host: Kirioso
    ip: kiri.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Swan Street — 3 Laps\r\nWhite Rose Chapel — 3 Laps\r\nVenice — 4 Laps\r\nToy World Mayhem — 4 Laps\r\nRooftops 1 — 3 Laps\r\nToy World 1 (R) — 6 Laps\r\nGrisville — 3 Laps\r\nLunar — 3 Laps\r\nSpa-Volt 2 — 4 Laps\r\nWildland — 3 Laps\r\nMedieval: Redux — 5 Laps\r\nSupermarket 1 — 4 Laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

