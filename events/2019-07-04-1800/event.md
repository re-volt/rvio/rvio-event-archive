---
title: 'Amateur Races'
titleoverride: false
date: '04-07-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Botanical Garden M\r\nToys in the Hood 1\r\nBiohazard Factory\r\nVenice\r\nIllusion\r\nToy World Mayhem\r\nRadioactive Garden R\r\nSuperMarket 1\r\nPenny Racers - Harbour\r\nPenny Racers - Caves\r\nDonut Plains 3 R\r\nToy World 2\r\nQuake!\r\nSakura R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

