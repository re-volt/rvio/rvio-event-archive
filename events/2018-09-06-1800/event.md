---
title: Rookies
titleoverride: false
date: '2018-09-06 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: rookies
    tracklist: "Penny Racers - Caves\r\nDonut Plains 3\r\nLunar\r\nYABBA DABBA DOO!\r\nVenice\r\nMetro-Volt\r\nBlood on the Rooftops\r\nToy World Aquatica: Redux (R)\r\nRooftops\r\nAMCO TT (R)\r\nMuseum 2\r\nPenny Racers - Harbour\r\nBotanical Garden (R)\r\nGhost Town 1"
    results: 'http://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-06_20-02-17.csv'
taxonomy:
    category:
        - events
visible: true
sidebarlayout: global
sidebarpath: /sidebar/events
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

