---
title: 'Pro Races (Dual Lobby)'
titleoverride: false
date: '25-05-2019 18:00'
event:
    host: 'URV & Floxit'
    ip: 'u.rv.gl OR flo.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Pro Cars (Dual Lobby)'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Grisville\r\nRadioactive Garden\r\nMoon Dawn\r\nToytanic 2\r\nPenny Racers - Caves\r\nSuperMarket 1\r\nSakura\r\nHoliday Camp: California Edition\r\nFiddlers on the Roof\r\nBiohazard Factory\r\nRooftop Chase: Redux (R)\r\nAMCO TT\r\nToy World Mayhem (R)\r\nGhost Town 1 (M)\r\nToys in the Hood 1 (R)\r\nRanch"
    stream: 'https://www.twitch.tv/floxit'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**This session will be separated into two lobbies.** If you don't get a spot in one room, try joining the other.

URV's room will use a client-server connection (`-nop2p`). Floxit's room will use a hybrid P2P connection.