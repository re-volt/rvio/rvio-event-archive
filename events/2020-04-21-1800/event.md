---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '21-04-2020 18:00'
event:
    host: 'Narusori & Delecto'
    ip: 'naru.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Venice — 3 laps\r\nBotanical Garden — 6 laps\r\nToytanic 1 — 3 laps\r\nGhost Town 2 — 4 laps\r\nThe Felling Yard — 2 laps\r\nMoon Dawn (M) — 2 laps\r\nSmashride Circuit — 3 laps\r\nToySoldierz — 4 laps\r\nToy World 2 (R) — 4 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nPetroVolt (R) — 2 laps\r\nToy World Mayhem —4 laps\r\nSantorini — 4 laps\r\nIndustry — 3 laps\r\nRanch — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

