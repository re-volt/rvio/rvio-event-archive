---
title: 'Classic Clockworks'
titleoverride: false
date: '10-04-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Classic Clockworks'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "Toy World 1\r\nSpa-Volt 1\r\nToy World 2\r\nSpa-Volt 2\r\nToy World Aquatica: Redux\r\nMetro-Volt\r\nToy World Mayhem"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Requirements:**
* [Classic Clockworks](https://distribute.re-volt.io/packs/io_clockworks.zip)
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* RVGL **19.0405a** ([Win32](http://rvgl.re-volt.io/downloads/rvgl_19.0405a_test_win32.7z) | [Win64](http://rvgl.re-volt.io/downloads/rvgl_19.0405a_test_win64.7z) | [Linux](http://rvgl.re-volt.io/downloads/rvgl_19.0405a_test_linux.7z))

Please ensure to revert to RVGL [19.0330a](https://forum.re-volt.io/viewtopic.php?f=8&t=818) after the races. The version we are using for this event is an unofficial build intended for testing purposes.