---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '01-12-2020 19:00'
event:
    host: 'Frostbitten & Mighty'
    ip: 'frost.rv.gl OR mighty.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Museum 1 — 3 laps\r\nToy World 1 — 6 laps\r\nHoliday Camp: California Edition — 2 laps\r\nSupermarket 2 — 8 laps\r\nSchools Out! 1 (R) — 4 laps\r\nMedieval: Redux — 4 laps\r\nSpooky-Volt — 6 laps\r\nMoon Dawn — 2 laps\r\nPetroVolt — 2 laps\r\nBlood on the Rooftops — 3 laps\r\nMolten Caverns (R) — 2 laps\r\nToytanic 2 (R) — 3 laps\r\nRooftop Chase: Redux — 3 laps\r\nThe Bunker — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

