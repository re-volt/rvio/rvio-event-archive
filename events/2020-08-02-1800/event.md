---
title: 'Super Pro Races'
titleoverride: true
date: '02-08-2020 18:00'
event:
    host: Kirioso
    ip: kiri.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Helios — 3 Laps\r\nSakura — 3 Laps\r\nSantorini — 4 Laps\r\nMedieval: Redux — 5 Laps\r\nRooftop Chase: Redux — 3 Laps\r\nWhite Rose Chapel — 3 Laps\r\nToy World 1 (R) — 4 Laps\r\nSupermarket 2 — 9 Laps\r\nToy World 2 — 5 Laps\r\nVenice — 4 Laps\r\nBotanical Garden — 6 Laps\r\nRoute—77 — 4 Laps\r\nSchool's Out — 2 Laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

