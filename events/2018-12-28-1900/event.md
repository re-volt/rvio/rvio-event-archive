---
title: 'Drift Races'
titleoverride: false
date: '28-12-2018 19:00'
event:
    host: Saffron
    ip: 'announced on Discord'
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://files.re-volt.io/packs/drift_cars.7z'
    tracklist: "Foxglen\r\nSideways Sanctuary A\r\nEggland\r\nLS Kart Space I\r\nSideways Sanctuary C\r\nRV-Simo On-Road Track\r\nStagnaro\r\nSideways Sanctuary D\r\nWarehouse Showdown\r\nTVGP Bitume\r\nSideways Sanctuary B\r\nMelville"
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2018-12-28_19-04-01.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only **D1GP** or **D2GP** cars allowed.

**Download**
* [Car Pack](https://files.re-volt.io/packs/drift_cars.7z)
* [Track Pack](https://files.re-volt.io/packs/drift_tracks.7z)