---
title: Rookies
titleoverride: false
date: '30-10-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: rookies
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Lunar\r\nSuperMarket 2\r\nMeltdown\r\nToy World Mayhem (R)\r\nGhost Town 1\r\nToys in the Hood 2\r\nIndustry\r\nThe Gorge\r\nOverground\r\nSwan Street (R)\r\nToy World Aquatica: Redux\r\nJailhouse Rock\r\nSakura\r\nBotanical Garden (R)"
    vod: 'https://www.youtube.com/watch?v=_QFMYK9Wmok'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-30_21-03-06.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

