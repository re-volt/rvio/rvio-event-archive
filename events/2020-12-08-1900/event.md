---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '08-12-2020 19:00'
event:
    host: 'Frostbitten & OhNej'
    ip: 'frost.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Ghost Town 1 — 6 laps\r\nVenice (R) — 3 laps\r\nRooftops 1 — 3 laps\r\nWhite Rose Chapel — 3 laps\r\nSmashride Circuit — 3 laps\r\nSchools Out! 2 — 2 laps\r\nMoon Dawn — 2 laps\r\nSupermarket 1 — 4 laps\r\nToy World 2 (R) — 5 laps\r\nToy World 1 — 6 laps\r\nSnowland 1 — 4 laps\r\nSwan Street (R) — 3 laps\r\nQuake! — 2 laps\r\nToy World Mayhem — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

