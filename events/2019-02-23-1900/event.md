---
title: 'Rookie Races'
titleoverride: false
date: '23-02-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 2\r\nToys in the Hood 2\r\nToy World 1\r\nRooftops  (R)\r\nHelios\r\nToy World Mayhem\r\nJailhouse Rock\r\nSpa-Volt 1 (R)\r\nThe Gorge \r\nBlood on the Rooftops\r\nOverground\r\nPenny Racers - Harbour (R)\r\nMolten Caverns\r\nSnowy River"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

