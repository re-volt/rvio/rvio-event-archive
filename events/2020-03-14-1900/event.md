---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '14-03-2020 19:00'
event:
    host: 'Kirioso & Dvark'
    ip: 'kiri.rv.gl OR dv.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Ghost Town 2 — 4 laps\r\nSakura — 2 laps\r\nSnowland 1 — 3 laps\r\nJailhouse Rock — 3 laps\r\nAMCO TT — 3 laps\r\nRadioactive Garden — 5 laps\r\nThe Bunker (M) — 4 laps\r\nRooftops — 3 laps\r\nToytanic 1 (R) — 3 laps\r\nToy World 1 — 6 laps\r\nSantorini (R) — 4 laps\r\nQuake! (R) — 2 laps\r\nPetroVolt — 2 laps\r\nLunar — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

