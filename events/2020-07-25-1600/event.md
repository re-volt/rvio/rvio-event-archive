---
title: 'Splat GTE Champ. <small>(Day 3)</small>'
titleoverride: true
date: '25-07-2020 16:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Splat GTE'
    classlink: 'http://revoltzone.net/cars/50900/Splat%20GTE'
    tracklist: 'Tracks from the I/O Circuit Pack'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Splat GTE Championship'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Note**: Registrations are closed now

- You can sign up either as a single racer or as a team of 2 players.
- If you won't be able to join every day you can find yourself a substitute or just skip that day. (First option would be preferable though)
- Every racer/team has to prepare a skin with a race number (there's already a specific place for that in the original skin) for the car, racers of a team will use the same livery but with different race numbers.
- Teams have to choose a team name.
- Every racer fights for Driver Championship while teams fight for Constructor Championship.
- The points system will be based on the number of partecipants.
- The tournament will be held in arcade no-pick mode and every race will last approximately 10 minutes.
- Every race day is composed of 4 races for a total race time of about 40 minutes.
- We'll race on 2 different circuits every day, with Race 1 and Race 2 for every circuit. (Race 2 reversed for tracks that have it)
