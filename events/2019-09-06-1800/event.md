---
title: Tomatoes
titleoverride: false
date: '06-09-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'a tomato'
    classlink: 'http://files.re-volt.io/misc/fresh.zip'
    tracklist: "SuperMarket 2\r\nMetro-Volt \r\nBotanical Garden\r\nPenny Racers - Harbour (M)\r\nRadioactive Garden\r\nSupermarket 1 (M)(R)\r\nPenny Racers - Caves\r\nToys In The Hood 2 (R)\r\nDonut Plains 3\r\nAMCO TT (R)\r\nBLURSED LAND OF TOMATOES"
    vod: 'https://www.youtube.com/watch?v=2STCcv4U954'
    results: 'https://online.re-volt.io/sessions/results.php?file=main/session_2019-09-06_22-00-10.csv'
published: true
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

We're doing 5 laps on each track. Join us in the voice channel on [Discord](https://re-volt.io/discord) to hear Andor talk about growing tomatoes (if he didn't flee to the Maldives).

Remember to [download your own tomato](http://files.re-volt.io/misc/fresh.zip).