---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '24-09-2020 18:00'
event:
    host: 'Etneus & Ashen Forest'
    ip: 'etn.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toytanic 1 — 3 laps\r\nMuseum 2 (R) — 4 laps\r\nHull Breach 3000 — 3 laps\r\nImages of Giza: Redux — 5 laps\r\nMetro-Volt — 3 laps\r\nVenice (R) — 3 laps\r\nRooftops 1 — 2 laps\r\nMeltdown — 4 laps\r\nToy World Aquatica: Redux (R) — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nSkating Toys: Redux — 2 laps\r\nLibrary — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nSupermarket 1 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

