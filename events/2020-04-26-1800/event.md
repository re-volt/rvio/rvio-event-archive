---
title: 'Competitive Super Pro Knockout Races'
titleoverride: false
date: '26-04-2020 18:00'
event:
    host: Delecto
    ip: dele.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: 'Random tracks'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Tracks will be picked randomly from the main and the bonus track pack.

You will not be allowed to change cars during the session.

The races will be without pick-ups.

***
Main track pack: [https://distribute.re-volt.io/packs/io_tracks.zip](https://distribute.re-volt.io/packs/io_tracks.zip)

Bonus track pack: [https://distribute.re-volt.io/packs/io_tracks_bonus.zip](https://distribute.re-volt.io/packs/io_tracks_bonus.zip)