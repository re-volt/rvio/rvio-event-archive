---
title: 'Advanced Races'
titleoverride: false
date: '27-06-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Overground\r\nIndustry\r\nToy World 2\r\nToy World Mayhem R\r\nKadish Sprint\r\nQuake!\r\nMetro-Volt\r\nToytanic 2\r\nSkating Toys\r\nMuseum 2\r\nBotanical Garden M\r\nPetroVolt\r\nPenny Racers - Caves R\r\nToySoldierz\r\nMoon Dawn R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

