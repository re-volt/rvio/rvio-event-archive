---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '30-04-2020 18:00'
event:
    host: 'Kirioso & Narusori'
    ip: 'kiri.rv.gl OR naru.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Spa-Volt 2 — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nSupermarket 2 — 8 laps\r\nGhost Town 1 R — 6 laps\r\nQuake! — 2 laps\r\nToy World Mayhem R — 4 laps\r\nMoon Dawn — 2 laps\r\nBlood on the Rooftops — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nImages of Giza: Redux — 5 laps\r\nToy World 1 — 6 laps\r\nMetro-Volt — 3 laps\r\nMolten Caverns R — 2 laps\r\nSantorini — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

