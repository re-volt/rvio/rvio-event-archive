---
title: 'Rookie Races'
titleoverride: false
date: '22-08-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Supermarket 2\r\nDonut Plains 3\r\nRadioactive Garden (R)\r\nMetro-Volt\r\nSantorini\r\nToytanic 2 (R)\r\nCliffside\r\nLunar (M)\r\nSnowy River\r\nToys in the Hood 2\r\nSpa-Volt 1 (R)\r\nOverground\r\nGhost Town 1\r\nJailhouse Rock"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

