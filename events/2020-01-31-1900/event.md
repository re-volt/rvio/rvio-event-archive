---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: false
date: '31-01-2020 19:00'
event:
    host: 'Saffron & Delecto'
    ip: 'saff.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Toy World 2 RM - 5 laps\r\nVenice R - 4 laps\r\nMuseum 2 - 4 laps\r\nIndustry - 3 laps\r\nFiddlers on the Roof: Redux - 3 laps\r\nToy World Aquatica: Redux - 4 laps\r\nMoon Dawn - 2 laps\r\nRadioactive Garden - 5 laps\r\nSakura - 3 laps\r\nSupermarket 1 - 4 laps\r\nPetroVolt - 2 laps\r\nBotanical Garden - 6 laps\r\nRV Temple - 2 laps\r\nToy World Mayhem - 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**[Super Pros](https://distribute.re-volt.io/packs/io_superpros.zip)** are required.