---
title: 'Circuit Races'
titleoverride: true
date: '02-12-2020 18:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: special
    mode: race
    class: other
    otherclass: XM250
    classlink: 'http://revoltzone.net/cars/49365/XM250'
    tracklist: "Touge Mountain\r\nFoxglen — 10 laps\r\nAutumn Ring Mini — 10 laps\r\nRV-Simo On-Road Track — 9 laps\r\nSuper Speedway — 8 laps\r\nKeitune Speedway — 6 laps\r\nEmerald Pastures Raceway — 6 laps\r\nClubman Stage Route 5 — 6 laps\r\nAutumn Ring — 5 laps\r\nRed Rock Valley — 4 laps\r\nGreen Haze Valley — 6 laps\r\nStagnaro — 10 laps\r\nSpecial Stage Route 5 — 4 laps\r\nTouge Mountain (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Circuit Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

