---
title: 'Rookie Races'
titleoverride: false
date: '08-08-2019 18:00'
event:
    host: Flyboy
    ip: fly.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Jailhouse Rock R\r\nRooftops R\r\nPenny Racers - Caves\r\nDonut Plains 3\r\nSuperMarket 2\r\nToy World Mayhem\r\nCliffside\r\nMetro-Volt\r\nGhost Town 2\r\nSantorini R\r\nBlood on the Rooftops\r\nHelios\r\nBotanical Garden M\r\nThe Bunker"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

