---
title: 'Super Pro Races'
titleoverride: true
date: '12-08-2020 19:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Unpopular Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Identity X — Toy World 1 (R) — 6 laps\r\nNapalm — Toys in the Hood 1 — 4 laps\r\nKing Moloko — Wildland — 3 laps\r\nMudman — Toys in the hood 2 — 4 laps\r\nReiser — White Rose Chapel — 4 laps\r\nKazuki — Toy World 2 — 5 laps\r\nSterling 77 — Route 77 — 4 laps\r\nSterling 77 — Museum 1 — 4 laps\r\nSentaro XL — Museum 2 — 4 laps\r\nSentaro XL — Jailhouse Rock — 3 laps\r\nSideswipe — Museum 3 — 4 laps\r\nSideswipe — Supermarket 1 — 4 laps\r\nRinne — Smashride Circuit — 4 laps\r\nRinne — Toy World Mayhem — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

The JOKER cheat  code will be used to force everyone to play the same car. The game mode will be set to Arcade, and pick-ups will be turned off.
