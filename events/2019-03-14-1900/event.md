---
title: 'Pro Races'
titleoverride: false
date: '14-03-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Metro-Volt\r\nToy World 2\r\nSantorini\r\nPenny Racers - Caves\r\nToys in the Hood 2\r\nBlood on the Rooftops\r\nJailhouse Rock\r\nBotanical Garden\r\nPetroVolt\r\nGround N Smash 2\r\nIllusion (R)\r\nSakura (R)\r\nCliffside\r\nFreestoyle 2\r\nGhost Town 1 (R)\r\nThe Bunker"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

