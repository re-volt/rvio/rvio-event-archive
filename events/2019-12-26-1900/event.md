---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: false
date: '26-12-2019 19:00'
event:
    host: 'Saffron OR OhNej'
    ip: 'saff.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Donut Plains 3\r\nMetro-Volt\r\nSupermarket 1\r\nHull Breach 3000\r\nToys in the Hood 1 (R)\r\nPenny Racers - Harbour (R)\r\nVenice\r\nSpa-Volt 1 (R)\r\nBlood on the Rooftops (M)\r\nToys in the Hood 2\r\nThe Felling Yard\r\nFool's Mate 2\r\nGhost Town 2\r\nSwan Street"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

