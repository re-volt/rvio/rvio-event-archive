---
title: 'Volt Ball'
titleoverride: true
date: '28-07-2020 20:20'
event:
    host: Santi
    ip: santi.rv.gl
    category: special
    mode: volt-ball
    class: other
    otherclass: TBA
    packages:
        - io_lmstag
    tracklist: 'Volt Ball'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: Voltball
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirements:
* [Volt Ball](https://cdn.discordapp.com/attachments/364003785602760705/736647106831581324/Voltball.zip)

**Etiquette**:

Teams compete against each other on the Voltball Arenas. It will have two stages
The teams are divided into 2 basic colors: red and blue.
The teams facing each other will pick a side to play on. 
If there are 2 halves, each team alternatively begins, both the attack and defense team have to stay positioned as a row, in front of their goal after the attacking team begins to play, the game goes on until a goal is scored then, the team that suffers the goal, resumes the match by attacking, as started before.
Every match will have a referee overseeing that the players get to their spots before a kickoff, and tell them when kickoff happens. 
They will also keep track of time, and tell the players when the match is over. 
Time spent waiting for the ball to respawn does not count towards the match timer. 
People wait on a countdown from your positions when you first spawn in before moving towards the ball and then move again after a goal from your positions once the ball respawns.
The start of each match will be raffled by the google application (Flip Coin)