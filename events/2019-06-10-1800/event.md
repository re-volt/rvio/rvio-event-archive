---
title: 'Circuit Races'
titleoverride: false
date: '10-06-2019 18:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Real Cars'
    classlink: 'https://files.re-volt.io/packs/rlcars.zip'
    tracklist: "Clubman Stage Route 5 (R)\r\nNight City\r\nSS Route Revolt\r\nLS Kart Space I\r\nAMCO Bitume\r\nStagnaro\r\nRed Rock Valley\r\nSS Highway Revolt\r\nMelville\r\nSpecial Stage Route 5\r\nAMCO Driftume\r\nTouge Drift\r\nDaybreak Raceway\r\nSideways Sanctuary B"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

