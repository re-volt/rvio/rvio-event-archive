---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '14-01-2020 19:00'
event:
    host: 'URV & OhNej'
    ip: 'u.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Donut Plains 3 — 4 laps\r\nSupermarket 1 (R) — 4 laps\r\nPetroVolt — 2 laps\r\nGhost Town 1 — 5 laps\r\nFiddlers on the Roof — 2 laps\r\nSwan Street — 3 laps\r\nLunar — 3 laps\r\nQuake! — 2 laps\r\nVenice (R) — 3 laps\r\nIndustry — 2 laps\r\nToys in the Hood 1 (M) — 3 laps\r\nMuseum 2 — 3 laps\r\nSnowland 1 (R) — 3 laps\r\nPenny Racers - Harbour — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

