---
title: 'Super Pro Races'
titleoverride: true
date: '23-02-2020 18:00'
event:
    host: Delecto
    ip: dele.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Toys in the Hood 2 — 3 laps\r\nRooftop Chase: Redux (R) — 3 laps\r\nLunar (M) — 3 laps\r\nWhite Rose Chapel — 2 laps\r\nFools Mate 2 — 3 laps\r\nSantorini — 4 laps\r\nGrisville — 3 laps\r\nToy World Aquatica: Redux (M) — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nOverground — 2 laps\r\nKadish Sprint — 2 laps\r\nIndusty — 2 laps\r\nSkating Toys (R) — 2 laps\r\nThe Felling Yard — 2 laps\r\nToys in the Hood 1 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

