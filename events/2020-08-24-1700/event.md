---
title: 'RV_Pure Cars'
titleoverride: true
date: '24-08-2020 17:00'
event:
    host: '607'
    ip: 607.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Pure''s cars'
    classlink: 'https://drive.google.com/file/d/1haJG4mpaUQ7vFbK4-9T6GZ3z7BI0Aq90/view?usp=sharing'
    tracklist: "JungleVolt\r\nWargod Fortress\r\nChilled To The Bone\r\nSantorini\r\nLunar by human\r\nSynthwave\r\nNight Flights\r\nRosedale\r\nMineZcraft\r\nCastle Keepers by Manmountain"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'RV_Pure Cars'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

* [Tracks Download](http://folk.simpsite.nl/regular-track-pack)
* [Cars Download](https://drive.google.com/file/d/1haJG4mpaUQ7vFbK4-9T6GZ3z7BI0Aq90/view?usp=sharing)
* The cars will be played in order of release (From Naval Baron to Wing King)