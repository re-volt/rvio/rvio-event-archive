---
title: 'Advanced Races'
titleoverride: false
date: '24-01-2019 19:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Cliffside\r\nMetro-Volt\r\nGhost Town 2 (R)\r\nPenny Racers - Caves \r\nIllusion\r\nSupermarket 2\r\nThe Gorge\r\nThe Felling Yard\r\nSpa-Volt 1 (R)\r\nGround N Smash 2\r\nToytanic 2\r\nHoliday Camp - California Edition\r\nBotanical Garden\r\nVenice\r\nPalm Marsh (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

