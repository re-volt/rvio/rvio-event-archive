---
title: ' <small>Christmas All-Stars (Dual Lobby)</small>'
titleoverride: true
date: '20-12-2020 18:00'
event:
    host: 'URV & OhNej'
    ip: 'u.rv.gl OR ohnej.rv.gl'
    category: tourney
    mode: race
    class: other
    otherclass: 'CC20 Cars'
    tracklist: "Ghost Town 2 — 4 laps\r\nJailhouse Rock — 2 laps\r\nWarehouse Showdown — 8 laps\r\nRooftops — 3 laps\r\nToy World Xtreme — 3 laps\r\nChristmas Snow Globe — 5 laps\r\nPR: Caves — 5 laps\r\nMK64: Sherbet Land — 4 laps\r\nIcy Canyon — 3 laps\r\nChilled To The Bone — 2 laps\r\nWicked Winter — 5 laps\r\nAurora Borealis — 4 laps\r\nIceatopia — 4 laps\r\nWinter Park — 4 laps"
media_order: rvcc20_poster5.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Christmas All-Stars'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](rvcc20_poster5.png)
The first full-fledged event of RVCC20: a regular racing session where you can choose between any of the Christmas Championship cars!

### Choose between the following cars
* Arctic Dozer
* Candy Cane
* Comet
* Glacier
* Golden Bell
* Ice Spike
* Monster Knot
* Ski X
* Snowball
* Snowflake
* Speed Gnome
* Starlight
* Zerostar

### Requirements
* [CC20 Cars](https://files.re-volt.io/packs/christmas/2020/rvcc20_cars.7z)
* [CC20 Tracks](https://files.re-volt.io/packs/christmas/2020/rvcc20_tracks.7z)