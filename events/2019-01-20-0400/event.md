---
title: 'Advanced Races'
titleoverride: false
date: '20-01-2019 04:00'
event:
    host: '[AR]Santi™'
    ip: 152.168.255.87
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1\r\nAMCO TT\r\nGrisville\r\nPalm Marsh\r\nPenny Racers - Caves\r\nToy World 2 (R)\r\nRooftop Chase\r\nBlood on the Rooftops\r\nCliffside\r\nFreestoyle 2\r\nGround N Smash 2\r\nGhost Town 1 (M)\r\nMeltdown\r\nThe Felling Yard\r\nThe Gorge\r\nToySoldierz"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

