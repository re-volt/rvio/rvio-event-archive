---
title: 'Semi-Pro Races'
titleoverride: false
date: '30-07-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Hull Breach 3000\r\nMuseum 2\r\nMetro-Volt\r\nIllusion\r\nSpa-Volt 2\r\nGhost Town 2\r\nYABBA DABBA DOO!\r\nToy World Mayhem (R)\r\nSwan Street\r\nToytanic 1\r\nRadioactive Garden (R)\r\nKadish Sprint\r\nPenny Racers - Caves\r\nRanch (M)\r\nToys in the Hood 2 (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

