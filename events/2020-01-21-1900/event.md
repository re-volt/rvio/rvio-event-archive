---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '21-01-2020 19:00'
event:
    host: 'Saffron & Narusori'
    ip: 'saff.rv.gl OR sas.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Swan Street (R) — 3 laps\r\nGhost Town 1 — 5 laps\r\nWhite Rose Chapel — 2 laps\r\nIndustry — 2 laps\r\nThe Bunker — 3 laps\r\nGrisville — 3 laps\r\nLunar — 3 laps\r\nSupermarket 1 (R) — 4 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nSakura — 2 laps\r\nMuseum 1 — 2 laps\r\nSnowy River (M) — 3 laps\r\nToy World 1 — 5 laps\r\nQuake! — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro '
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

