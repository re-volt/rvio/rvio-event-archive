---
title: Semi-Pros
titleoverride: false
date: '27-12-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: semi-pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 1\r\nKadish Sprint\r\nMeltdown\r\nQuake!\r\nMuseum 2\r\nVenice\r\nThe Bunker\r\nAMCO TT\r\nGrisville\r\nToy World 2 (R)\r\nMetro-Volt\r\nSwan Street (R)\r\nGround N Smash 2\r\nHoliday Camp: California Edition (R)\r\nToys in the Hood 1"
    vod: 'https://www.youtube.com/watch?v=Noeztx5uULA'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-27_20-02-24.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

