---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '17-12-2020 19:00'
event:
    host: 'Mighty & URV'
    ip: 'mighty.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Santorini (R) — 4 laps\r\nMetro-Volt — 3 laps\r\nToytanic 1 — 3 laps\r\nToy World Mayhem — 3 laps\r\nQuake! (R) — 2 laps\r\nHMS Invincible: Redux — 3 laps\r\nGhost Town 2 — 4 laps\r\nGhost Town 1 — 6 laps\r\nThe Bunker — 4 laps\r\nBotanical Garden (R) — 5 laps\r\nLunar — 3 laps\r\nThe Felling Yard — 2 laps\r\nFools Mate 2 — 4 laps\r\nSakura — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

