---
title: 'Pro Slugs'
titleoverride: false
date: '13-12-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: prugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 2\r\nSpa-Volt 1\r\nKadish Sprint\r\nLunar\r\nGhost Town 2\r\nSantorini (R)\r\nAMCO TT\r\nThe Gorge\r\nMuseum 2\r\nOverground\r\nThe Felling Yard\r\nSakura (R)\r\nRooftop Chase\r\nQuake!\r\nBotanical Garden (R)"
    vod: 'https://www.youtube.com/watch?v=7JtHh_6Jyag'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-13_20-02-35.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

