---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '14-12-2019 19:00'
event:
    host: 'Narusori & Saffron'
    ip: 'sas.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Route-77\r\nSupermarket 2\r\nBlood on the Rooftops\r\nToytanic 2 (R)\r\nToy World Mayhem (M)\r\nDonut Plains 3 (R)\r\nJailhouse Rock\r\nCliffside\r\nGhost Town 2\r\nToy World 2\r\nVenice (R)\r\nPenny Racers - Caves\r\nPetroVolt\r\nSwan Street"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

