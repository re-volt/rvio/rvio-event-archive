---
title: 'Silver Cup'
titleoverride: false
date: '06-07-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Silver Cup'
    classlink: 'https://re-volt.io/blog/online-championship-mode-tournament'
    tracklist: "Toy World 1 (6 laps)\r\nToys in the Hood 1 M (4 laps)\r\nGhost Town 1 (4 laps)\r\nToy World 2 (4 laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Signing up is required for participation in this event.** [More information here.](https://re-volt.io/blog/online-championship-mode-tournament)

### Participants
|  Player  |  Car  |
|  :-----          |  :-----          |
|  URV |  *TBA* |
|  Trixed |  *TBA* |
|  Cosmin |  *TBA* |
|  Maci |  *TBA* |
|  THE MAN |  *TBA* |
|  Dovahkin |  *TBA* |
|  Jsn |  *TBA* |
|  hajduksekb |  *TBA* |