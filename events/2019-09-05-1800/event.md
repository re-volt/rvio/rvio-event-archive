---
title: 'Pro Races'
titleoverride: false
date: '05-09-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Penny Racers - Harbour\r\nSuperMarket 1\r\nKadish Sprint\r\nToySoldierz\r\nToys in the Hood 2\r\nDonut Plains 3\r\nGhost Town 1 (R)\r\nToy World Mayhem\r\nSakura\r\nJailhouse Rock (R)\r\nPetroVolt\r\nAMCO TT (M)\r\nThe Felling Yard\r\nMolten Caverns\r\nBiohazard Factory (R)\r\nRooftops"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

