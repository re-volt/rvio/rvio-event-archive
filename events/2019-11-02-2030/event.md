---
title: 'Line Elimination'
titleoverride: true
date: '02-11-2019 20:30'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'Recent Advanced Cars'
    classlink: 'https://www.dropbox.com/s/9af78dfyceqs73s/lineelimination.zip?dl=1'
    tracklist: "Helios\r\nPlaya+\r\nImages of Giza\r\nCake\r\nToy World EX\r\nMKDS - Airship Fortress\r\nMK64 - Wario Stadium ⚡\r\nChristmas Snow Globe ⚡\r\nThe Gorge ⚡\r\nFreestoyle 2 ⚡\r\nChilled To The Bone ⚡\r\nToytanic 1 ⚡"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](https://re-volt.io/user/pages/events/2019-11-01-1900/linelminton.png)

### Car Selection
- Ahead
- Akagi Attacker
- Drifter X
- Enforcer
- R6 Cargo
- Rascal
- Sooo Fast
- The Viper (TGO)
- Walkinshaw

The content pack (cars only) can be downloaded from [here](https://www.dropbox.com/s/9af78dfyceqs73s/lineelimination.zip?dl=1).
The first six tracks will be raced with pick-ups **disabled**.