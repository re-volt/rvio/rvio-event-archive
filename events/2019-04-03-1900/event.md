---
title: 'Candy Pebbles Exclusive'
titleoverride: false
date: '03-04-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Candy Pebbles Exclusive'
    tracklist: "Overground\r\nRooftops\r\nGhost Town 1\r\nToy World Aquatica: Redux (R)\r\nDonut Plains 3\r\nThe Felling Yard\r\nBotanical Garden\r\nSupermarket 2\r\nSpa-Volt 2 (R)\r\nVenice\r\nMolten Caverns\r\nIllusion\r\nRV Temple"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

