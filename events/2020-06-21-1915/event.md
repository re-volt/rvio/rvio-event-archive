---
title: 'Normal Rookie Session'
titleoverride: true
date: '21-06-2020 19:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: Rookie
    classlink: 'https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0'
    tracklist: "Toy World 2 RM (4 laps)\r\nWipeout (4 laps)\r\nSwan Street M (4 laps)\r\nRadioactive Garden (5 laps)\r\nLunar M (4 laps)\r\nRe-Ville M (4 laps)\r\nPetroVolt RM (3 laps)\r\nRooftops RM (4 laps)\r\nToy World 1 RM (5 laps)\r\nGhost Town 1 RM (5 laps)\r\nPalm Marsh R (4 laps)\r\nMoon Dawn M (2 laps)\r\nBiohazard Factory R (3 laps)\r\nRoute-77 R (4 laps)\r\nMuseum 2 R (4 laps)\r\nToy World Aqua Redux RM (4 laps)\r\nHallows Eve M (4 laps)\r\nDonut Plains 3 RM (4 laps)\r\nMuseum 1 RM (4 laps)\r\nPlaya+ M (4 laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Normal Rookie Session'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous avarage prestige results be sure to [check this](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).

### Requirements
* [Re-Volt Online track pack version 20.2](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1) (needed)
* [Re-Volt Online car pack version 20.2](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (advised)
* Seperate game installation required if you got I/O content on the game you want to use!

### Car Selection
* Any car that has been rated for rookie [this car list](https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0)