---
title: 'Semi-Pro Races'
titleoverride: true
date: '03-11-2020 19:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Swan Street — 3 laps\r\nSakura — 3 laps\r\nToy World Mayhem — 4 laps\r\nMedieval: Redux — 5 laps\r\nJailhouse Rock (R) — 3 laps\r\nSantorini (R) — 4 laps\r\nToySoldierz — 4 laps\r\nGhost Town 2 — 4 laps\r\nMeltdown — 4 laps\r\nBotanical Garden — 6 laps\r\nThe Bunker — 4 laps\r\nMuseum 1 — 3 laps\r\nGhost Town 1 (R) — 6 laps\r\nHelios — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

