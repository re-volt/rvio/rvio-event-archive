---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '07-12-2019 19:00'
event:
    host: 'URV & Saffron'
    ip: 'u.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Fiddlers on the Roof\r\nSantorini (R)\r\nToy World 2 (R)\r\nVenice (R)\r\nToys in the Hood 1\r\nLunar\r\nMuseum 1\r\nIndustry (M)\r\nSakura\r\nKadish Sprint\r\nHoliday Camp: California Edition\r\nCliffside\r\nToys in the Hood 2\r\nOverground"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

