---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '04-08-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World 1 — 6 laps\r\nOverground — 2 laps\r\nSupermarket 2 (R) — 8 laps\r\nToy World Mayhem — 4 laps\r\nHelios — 3 laps\r\nVenice — 3 laps\r\nToy World Aquatica: Redux (R) — 3 laps\r\nCliffside — 6 laps\r\nRV Temple — 2 laps\r\nJailhouse Rock (R) — 3 laps\r\nToys in the Hood 2 — 4 laps\r\nMolten Caverns — 2 laps\r\nMeltdown — 4 laps\r\nMuseum 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

