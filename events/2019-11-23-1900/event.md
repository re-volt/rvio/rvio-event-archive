---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '23-11-2019 19:00'
event:
    host: 'URV & Floxit'
    ip: 'u.rv.gl OR flo.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Rooftops (R)\r\nRV Temple\r\nFool's Mate 2\r\nToys in the Hood 1\r\nHull Breach 3000\r\nWhite Rose Chapel\r\nSpa-Volt 2 (R) (M)\r\nSupermarket 2\r\nJailhouse Rock\r\nSkating Toys\r\nOverground\r\nHelios\r\nBotanical Garden\r\nSnowland 1 (R)\r\nRooftop Chase: Redux"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

