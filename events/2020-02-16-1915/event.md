---
title: 'Online Diamond Cup'
titleoverride: true
date: '16-02-2020 19:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Online Diamond Cup'
    classlink: 'https://re-volt.io/events/2020-02-16-1915'
    tracklist: "Toys in the Hood 2 RM\r\nToytanic 1 R\r\nMid-Sea Island\r\nMysterious Toy-Factory R\r\nHoliday Camp Californication Edition R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Online Diamond Cup'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 3 cup standings and individual player progress, be sure to [check this](https://www.dropbox.com/s/x8ffi2o516r8xju/season%203%20cup%20session%20info.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Extra Old Io Tracks](https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Extra Old Io Cars](https://www.dropbox.com/sh/qqkplevr9df6l3a/AACsyvafB1QkhnU_GvtULqFJa?dl=1) (advised)
* [Super Tier Cars](https://www.dropbox.com/sh/5zchczny8ms2wpf/AADYbF6XdAdYer8Lk64jDEtEa?dl=1) (advised)

### Car Selection
* Dragheat
* Formula Z No. 112
* Humma
* King Kaiju
* Mudman
* Snw 35
* Toyeca