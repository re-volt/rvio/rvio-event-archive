---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '26-11-2020 19:00'
event:
    host: 'Kirioso & Mighty'
    ip: 'kiri.rv.gl OR mighty.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Rooftops 1 — 2 laps\r\nToy World 1 (R) — 4 laps\r\nSakura — 2 laps\r\nToySoldierz — 3 laps\r\nMeltdown — 4 laps\r\nSupermarket 2 — 7 laps\r\nToys in the Hood 2 — 3 laps\r\nJailhouse Rock (R) — 2 laps\r\nBiohazard Factory — 2 laps\r\nHoliday Camp: California Edition — 2 laps\r\nToys in the Hood 1 — 3 laps\r\nToy World Mayhem — 3 laps\r\nThe Felling Yard — 2 laps\r\nRoute-77 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

