---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '17-12-2019 19:00'
event:
    host: 'URV & Kirioso'
    ip: 'u.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Snowland 1 (M)\r\nJailhouse Rock (R)\r\nPenny Racers - Caves\r\nMuseum 2 (R)\r\nKadish Sprint\r\nToy World Mayhem\r\nMolten Caverns\r\nSakura\r\nToys in the Hood 2\r\nSwan Street (R)\r\nQuake!\r\nCliffside\r\nRooftops\r\nToy World 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

