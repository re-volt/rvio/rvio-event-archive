---
title: 'Amateur Races'
titleoverride: true
date: '14-11-2019 19:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "ToySoldierz\r\nSwan Street\r\nQuake! (M)\r\nToytanic 1 (R)\r\nVenice (R)\r\nAMCO TT\r\nToy World 1\r\nDonut Plains 3 (R)\r\nRe-Ville\r\nToys in the Hood 1\r\nHull Breach 3000\r\nSpa-Volt 2\r\nSantorini\r\nToy World 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

