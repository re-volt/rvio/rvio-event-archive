---
title: 'Pole Poz Exclusive'
titleoverride: false
date: '12-03-2019 21:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Pole Poz'
    classlink: 'https://revolt.fandom.com/wiki/Pole_Poz'
    tracklist: "The Felling Yard\r\nDonut Plains 3\r\nBotanical Garden (R)\r\nFreestoyle 2\r\nThe Bunker\r\nRooftops\r\nPenny Racers - Harbour (R)\r\nToys In The Hood 1\r\nJailhouse Rock\r\nMK64 Warrio Stadium\r\nBlood on the Rooftops\r\nCliffside\r\nToy World Mayhem (R)\r\nHoliday Camp: Califiornia Edition"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

