---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '10-03-2020 19:00'
event:
    host: 'Saffron & Narusori'
    ip: 'saff.rv.gl OR sas.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Toys in the Hood 1 — 4 laps\r\nMolten Caverns — 2 laps\r\nThe Felling Yard — 3 laps\r\nRV Temple — 2 laps\r\nToy World 2 — 5 laps\r\nSnowy River — 4 laps\r\nMuseum 2 (R) — 4 laps\r\nSpa-Volt 2 — 4 laps\r\nSchool's Out (R) — 2 laps\r\nToys in the Hood 2 — 4 laps\r\nVenice — 4 laps\r\nRooftop Chase: Redux (R) — 3 laps\r\nBlood on the Rooftops (M) — 3 laps\r\nHelios — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

