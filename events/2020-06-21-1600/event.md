---
title: 'Advanced Races'
titleoverride: true
date: '21-06-2020 16:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "The Felling Yard — 2 laps\r\nToy World Mayhem — 4 laps\r\nImages of Giza: Redux (R) — 5 laps\r\nToys in the Hood 2 — 4 laps\r\nQuake! (R) — 2 laps\r\nLunar — 3 laps\r\nMuseum 2 — 4 laps\r\nThe Bunker — 4 laps\r\nMuseum 1 (R) — 3 laps\r\nSantorini — 4 laps\r\nSnowy River — 4 laps\r\nMetro-Volt — 3 laps\r\nMeltdown — 4 laps\r\nToys in the Hood 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

