---
title: 'Pro Races'
titleoverride: false
date: '19-10-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 2\r\nBlood on the Rooftops\r\nRooftops (R)\r\nToySoldierz (R)\r\nGhost Town 2\r\nPenny Racers - Harbour\r\nIndustry (M)\r\nRanch (R)\r\nHull Breach 3000\r\nVenice\r\nSwan Street\r\nMuseum 2\r\nBiohazard Factory\r\nSpa-Volt 2\r\nJailhouse Rock\r\nRooftop Chase: Redux"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

