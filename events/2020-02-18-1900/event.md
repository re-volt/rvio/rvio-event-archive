---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '18-02-2020 19:00'
event:
    host: 'Saffron & Delecto'
    ip: 'saff.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Snowy River — 4 laps\r\nSpa-Volt 2 — 3 laps\r\nToy World 1 (R) — 6 laps\r\nRoute-77 — 3 laps\r\nPenny Racers - Caves (R) — 5 laps\r\nRooftops (M) — 3 laps\r\nPetroVolt (R) — 2 laps\r\nSupermarket 1 — 4 laps\r\nHoliday Camp: California Edition — 2 laps\r\nMetro-Volt — 3 laps\r\nSchool's Out — 2 laps\r\nAMCO TT — 3 laps\r\nMuseum 1 — 3 laps\r\nSnowland 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

