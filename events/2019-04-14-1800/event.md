---
title: 'Sadistic Supers'
titleoverride: false
date: '14-04-2019 18:00'
event:
    host: Shara
    ip: 'on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'Sadistic Supers'
    classlink: 'https://re-volt.io/events/2019-04-14-1800'
    tracklist: "Jailhouse Rock\r\nMetro-Volt\r\nSnowland 1\r\nWhite Rose Chapel\r\nSakura\r\nToys in the Hood 1\r\nRooftop Chase: Redux\r\nMuseum 1\r\nDonut Plains 3\r\nFreestoyle"
media_order: sadist.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

<div class="alert alert-success">
  <strong>This event uses RVGL 17.1009a.</strong> Everything you need is provided in the download below.
</div>

![](sadist.png)

### Rules & Information
* **Unique to this session, we will be specifically using `SADIST` cheat code.**<br>
Said cheat code enables players to change any pickup in their slot with Right Shift button.<br>
Players are allowed to wreak havoc and use any weapon they want midrace except for the star. 

* **Stars are not allowed, including those picked up from the track.**<br>
With track stars, there is no way to know who got the star and the possibility of changing weapons after the track star is picked up.<br>
One or more accidental stars can be tolerated, but suspicious frequency of star usage will result in the lobby being immediately closed.<br>
As a precaution, the `star` weapon comes after the `fake pickup` in the weapon cycle, so be careful when spamming fake pickups.

* **Make sure you don't have `Right Shift` assigned when you open the game.**<br>
The cheat code will not work if `Right Shift` is already assigned when the game is opened, and still won't work even if the input with is changed to something else. <br>
In order for the `SADIST` cheat to work, make sure `Right Shift` is not assigned to anything when you open the game.

* **Use DEV mode for the weapon cheat to work.**<br>
The `SADIST` cheat code will not work if DEV mode is not activated.<br>
DEV mode is activated by inputting `up, right, down, left, up, left, down, right` at the main menu with the speed of about 200 bpm.<br>
Alternatively, run the game with the included batch file (`rvgl_session.bat`).

 * * *

### Content Pack
* Re-Volt game with RVGL **17.1009a** already applied
* Unused stock assets removed (stock cars and stock tracks with their gfx files)
* Required content for the session included (Super-Pro cars +  all tracks in the list)
 * includes Saeger, P4 Super and Celica Pikes Peak
 * has updated Nimuace
* 100% profile with all unlockables is included
* does not include redbook music

<big><big>[**Download**](https://www.dropbox.com/s/xe9gpqe3g2wv3gt/sadisticsupers.zip?dl=1)</big> (96.4 MB)</big>