---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '27-06-2020 18:00'
event:
    host: 'Etneus & Kirioso'
    ip: 'etn.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Radioactive Garden — 5 laps\r\n    PetroVolt — 2 laps\r\n    Images of Giza: Redux — 5 laps\r\n    Toy World Mayhem (R) — 4 laps\r\n    Rooftops — 4 laps\r\n    Swan Street (R) — 3 laps\r\n    Lunar — 3 laps\r\n    Ghost Town 1 — 7 laps\r\n    Toytanic 2 — 3 laps\r\n    Jailhouse Rock — 3 laps\r\n    Rooftop Chase: Redux — 3 laps\r\n    Wildland — 3 laps\r\n    Toys in the Hood 1 (R) — 4 laps\r\n    Molten Caverns — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

