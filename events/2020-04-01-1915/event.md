---
title: 'Online Platinum Cup'
titleoverride: false
date: '01-04-2020 17:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Online Platinum Cup'
    classlink: 'https://re-volt.io/events/2020-04-03-1915'
    tracklist: "Supermarket 1\r\nGhost Town 2\r\nToy World 1 RM\r\nMuseum 1 M\r\nToytanic 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 4 cup standings and individual player progress, be sure to [check this](https://www.dropbox.com/s/qfzn2vwewz114d7/season%204%20cup%20session%20info.txt?dl=0).

### Requirements
* [Online Car Pack](https://www.dropbox.com/sh/eq0zk2ivb0smsfi/AAD4esNGAJ65nSnOdP8kE2PWa?dl=1) (advised)


### Car Selection
* -Amw
* -Gear Gt
* -Night Drifter
* -Panga
* -Prophet
* -Sir Gleam
* -Vitesse