---
title: 'Advanced Races'
titleoverride: false
date: '05-01-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 2 (R)\r\nHelios \r\nFool's Mate 2\r\nAMCO TT\r\nRe-Ville\r\nBotanical Garden\r\nQuake! (R)\r\nRanch\r\nMK64 Wario Stadium\r\nOverground\r\nToy World 2\r\nRooftop Chase (R)\r\nYABBA DABBA DOO!\r\nMetro-Volt\r\nMuseum 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

