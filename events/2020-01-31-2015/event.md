---
title: 'Normal Semi-Pro Session'
titleoverride: true
date: '31-01-2020 20:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: Semi-Pro
    classlink: 'https://re-volt.io/events/2020-01-31-2015'
    tracklist: "Swan Street R\r\nGhost Town 1 RM\r\nSantorini\r\nTemple of the Burning Darkness R\r\nSpa-Volt 2 RM\r\nSnowland 1 RM\r\nFreestoyle 2 RM\r\nGround N Smash 2 M\r\nRooftops2\r\nPenny Racers Caves RM\r\nIllusion RM\r\nToys in the Hood 1 RM\r\nEver After\r\nFiddlers on the Rooftops: Redux RM\r\nToy World Mayhem RM\r\nChristmas Snow Globe\r\nSpa-Volt 1 RM\r\nToysoldierz R\r\nMolten Carverns M\r\nToysoldierz M"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Normal Semi-Pro Session'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous avarage prestige results be sure to [check this](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Extra Old Io Tracks](https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Extra Old Io Cars](https://www.dropbox.com/sh/qqkplevr9df6l3a/AACsyvafB1QkhnU_GvtULqFJa?dl=1) (advised)

### Car Selection
* Any [stock](https://revolt.fandom.com/wiki/Category:Standard_Cars) or [DC cars](https://revolt.fandom.com/wiki/Category:Console_Cars) rated in-game as Semi-Pro or below
* Any [custom content included in the I/O packs](https://re-volt.io/downloads/packs) that are rated in-game as Semi-Pro or below
* Any [cars included in the I/O selection](https://re-volt.io/online/cars) that are classified on the website as Semi-Pro or below
* Any [cars included in the old I/O car pack](https://www.dropbox.com/s/qfy5bzs1uibqair/Old%20io%20car%20list.txt?dl=0) that are rated in-game as Semi-Pro or below