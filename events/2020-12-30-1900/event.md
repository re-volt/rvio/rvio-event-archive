---
title: 'CC20 Finale'
titleoverride: true
date: '30-12-2020 19:00'
event:
    host: 'URV & Frosttbitten'
    ip: 'u.rv.gl OR frost.rv.gl'
    category: tourney
    mode: race
    class: other
    otherclass: 'CC20 Cars'
    classlink: 'https://re-volt.io/events/2020-12-30-1900'
    tracklist: "Christmas Snow Globe — 5 laps\r\nPR: Caves — 4 laps\r\nMK64: Sherbet Land — 4 laps\r\nTetris Festival — 4 laps\r\nRoute-77 — 3 laps\r\nR2049 Tundra — 4 laps\r\nFrozen Caverns — 2 laps\r\nHome for Christmas — 4 laps\r\nSnowy River — 4 laps\r\nAurora Borealis — 4 laps\r\nWicked Winter — 5 laps\r\nArctic Air — 2 laps\r\nSnowland 1 — 3 laps\r\nWinter Doomsday — 2 laps\r\nSnow Pass — 2 laps\r\nSBX Alpine — 3 laps\r\nFrostbite — 2 laps\r\nGlacier Cliffs 3 — 7 laps\r\nZero Degrees — 2 laps\r\nChilled to the Bone — 3 laps"
media_order: 'rvcc20_finale.png,carselection.png'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'CC20 Finale'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

<img src="https://re-volt.io/user/pages/events/2020-12-30-1900/rvcc20_finale.png">

The final event of the Christmas Championship! A regular but slightly longer racing session with some of the best tracks we've raced this month, where you may choose any of the cars introduced for this series of events. Let's give them a nice send off until next time!

# Requirements
* [CC20 Cars](https://files.re-volt.io/packs/christmas/2020/rvcc20_cars.7z)
* [CC20 Tracks](https://files.re-volt.io/packs/christmas/2020/rvcc20_tracks.7z)

# Car Selection
![](carselection.png)