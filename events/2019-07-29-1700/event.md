---
title: 'Super Pro Races'
titleoverride: false
date: '29-07-2019 17:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "Venice\r\nAMCO TT (M)\r\nSnowland 1 (R)\r\nMoon Dawn\r\nRadioactive Garden\r\nSuperMarket 2 (R)\r\nToy Soldierz\r\nGhost Town 2\r\nPetroVolt \r\nSuperMarket 1\r\nKadish Sprint\r\nJailhouse Rock\r\nQuake!  (R)\r\nHull Breach 3000\r\nBotanical Garden\r\nGrisville"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

