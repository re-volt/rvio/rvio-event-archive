---
title: 'Semi-Pro Races'
titleoverride: false
date: '18:00 20-04-2019'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Jailhouse Rock\r\nToy World Aquatica: Redux\r\nToytanic 1 (R)\r\nFiddlers on the Roof\r\nRooftop Chase: Redux\r\nGhost Town 2\r\nIndustry\r\nIllusion\r\nRanch (R)\r\nToys in the Hood 1\r\nRV Temple\r\nPetro-Volt\r\nMuseum 2\r\nSnowy River (R)\r\nQuake!"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

