---
title: 'Amateur Races'
titleoverride: true
date: '29-12-2020 19:00'
event:
    host: Frostbitten
    ip: frost.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Toy World 1 — 5 laps\r\nQuake! — 2 laps\r\nMoon Dawn (R) — 2 laps\r\nMysterious Toy-Volt Factory 2 — 2 laps\r\nToy World 2 — 4 laps\r\nMuseum EX — 2 laps\r\nToySoldierz — 3 laps\r\nStadVolt — 3 laps\r\nToys in the Hood 2 (R) — 3 laps\r\nBiohazard Factory — 2 laps\r\nSchools Out! 2 — 2 laps\r\nWhite Rose Chapel — 2 laps\r\nRooftops — 3 laps\r\nPetroVolt (R) — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

