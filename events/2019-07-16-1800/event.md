---
title: 'Advanced Races'
titleoverride: false
date: '16-07-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 2 (R)\r\nRooftops\r\nToys in the Hood 1\r\nToy World 2\r\nToy World Mayhem (R)\r\nHoliday Camp: California Edition \r\nRanch\r\nPetroVolt\r\nRadioactive Garden\r\nCliffside\r\nFiddlers on the Roof\r\nRooftop Chase: Redux\r\nOverground\r\nKadish Sprint (M)\r\nSwan Street (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

