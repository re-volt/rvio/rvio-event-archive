---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '08-02-2020 19:00'
event:
    host: 'Delecto & Saffron'
    ip: 'dele.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Overground — 3 laps\r\nToytanic 1 (R) — 3 laps\r\nToy World 1 — 6 laps\r\nQuake! (M) — 2 laps\r\nFool's Mate 2 — 4 laps\r\nPenny Racers - Harbour — 3 laps\r\nVenice (R) — 3 laps\r\nIndustry — 3 laps\r\nRooftops — 3 laps\r\nJailhouse Rock — 3 laps\r\nGhost Town 1 — 6 laps\r\nRV Temple — 2 laps\r\nHoliday Camp: California Edition — 2 laps\r\nSwan Street (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

