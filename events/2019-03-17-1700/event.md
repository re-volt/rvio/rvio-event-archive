---
title: 'Random Cars'
titleoverride: false
date: '17-03-2019 17:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Cars'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: 'Random Tracks'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Visit **[RVGL Ladder](https://ladder.rv.gl/)** for more information!

**Tracks:**
Only stock tracks, and tracks from the [main I/O track pack](https://re-volt.io/online/track-list) will be chosen randomly.
R/M/RM versions will also be driven! (Maximum one R-/M-/RM-race per event.)