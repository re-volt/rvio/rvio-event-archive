---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '07-05-2020 18:00'
event:
    host: 'Nausori & URV'
    ip: 'naru.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_tracks
        - io_cars
    tracklist: "Snowy River — 3 laps\r\nSnowland 1 — 3 laps\r\nLunar — 3 laps\r\nGrisville — 3 laps\r\nSupermarket 2 — 7 laps\r\nSpa-Volt 2 — 3 laps\r\nBotanical Garden — 5 laps\r\nMuseum 2 — 3 laps\r\nSmashride Circuit (R) — 2 laps\r\nMeltdown — 4 laps\r\nMetro-Volt — 3 laps\r\nSakura R — 2 laps\r\nGhost Town 2 (R) — 4 laps\r\nRoute-77 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

