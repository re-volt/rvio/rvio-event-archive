---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '28-12-2019 19:00'
event:
    host: 'OhNej OR Delecto'
    ip: 'ohnej.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Museum 2\r\nFiddlers on the Roof\r\nMoon Dawn (R)\r\nSupermarket 2 (M)\r\nSantorini\r\nToy World Mayhem (R)\r\nGhost Town 1\r\nHoliday Camp: California Edition\r\nJailhouse Rock\r\nRV Temple\r\nOverground\r\nCliffside\r\nToy World 2 (R)\r\nThe Bunker\r\nLunar"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

