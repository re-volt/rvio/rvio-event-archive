---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '19-12-2019 19:00'
event:
    host: 'OhNej OR Dvark'
    ip: 'ohnej.rv.gl OR dv.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Ghost Town 2\r\nToy World 1\r\nToySoldierz\r\nSantorini (R)\r\nThe Felling Yard\r\nPetroVolt (R)\r\nThe Bunker\r\nVenice (M)\r\nOverground\r\nHoliday Camp: California Edition\r\nToytanic 1 (R)\r\nAMCO TT\r\nRooftop Chase: Redux\r\nMetro-Volt\r\nGhost Town 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

