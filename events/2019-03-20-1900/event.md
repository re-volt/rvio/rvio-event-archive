---
title: 'Random Super Pro Cars'
titleoverride: false
date: '20-03-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Random Super Pro Cars'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "Toys in the Hood 1\r\nPetroVolt\r\nSpa-Volt 1 (R)\r\nLunar\r\nSkating Toys\r\nBiohazard Factory\r\nGrisville\r\nWhite Rose Chapel\r\nBlood on the Rooftops\r\nThe Felling Yard\r\nSakura\r\nIndustry\r\nRooftop Chase: Redux (R)\r\nToytanic 1 (R)\r\nHoliday Camp: California Edition\r\nMolten Caverns"
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2019-03-20_21-00-06.csv'
taxonomy:
    category:
        - events
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

[**Super Pros**](https://distribute.re-volt.io/packs/io_superpros.zip) are required.