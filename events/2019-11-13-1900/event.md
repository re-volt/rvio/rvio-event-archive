---
title: 'Random Car Races'
titleoverride: true
date: '13-11-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Cars'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Santorini (R)\r\nMetro-Volt\r\nSpa-Volt 1 (R)\r\nToytanic 1 (R)\r\nCliffside\r\nBotanical Garden\r\nIllusion (M)\r\nOverground\r\nSnowland 1\r\nMoon Dawn\r\nToys in the Hood 1\r\nHull Breach 3000\r\nToySoldierz\r\nToy World 1\r\nLunar\r\nRanch"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Everyone will use the same car every race. The car will change randomly every race from the [main I/O car pool](https://re-volt.io/online/car-classes). Pick-ups will be disabled.