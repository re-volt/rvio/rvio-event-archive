---
title: 'Random Cars: Halloween Edition'
titleoverride: false
date: '31-10-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Cars: Halloween Edition'
    classlink: 'https://re-volt.io/events/2018-10-31-1900'
    tracklist: "Mikes Medievel Mayhem\r\nTenebrosity\r\nHalloween 1\r\nCake\r\nMKDS - Luigi's Mansion\r\nHalloween 2\r\nForest Run\r\nHallows Eve\r\nThe Keep\r\nSpooky-Volt\r\nFukushima\r\nThe Catacombs\r\nMysterious Toy-Volt Factory\r\nThe Felling Yard EXTREME"
    vod: 'https://www.youtube.com/watch?v=HXl24Grt0h0'
    results: 'https://online.re-volt.io/sessions/results.php?file=competitive/session_2018-10-31_20-04-08.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event requires the [Halloween Carnival custom track pack](https://files.re-volt.io/packs/halloween/2018/tracks.7z).