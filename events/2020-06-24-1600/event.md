---
title: 'Semi-Pro Races'
titleoverride: true
date: '24-06-2020 16:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "PetroVolt — 2 laps\r\nHull Breach 3000 — 4 laps\r\nSantorini — 4 laps\r\nToy World 1 (R) — 6 laps\r\nSmashride Circuit — 3 laps\r\nBotanical Garden — 6 laps\r\nRooftop Chase: Redux — 3 laps\r\nWhite Rose Chapel — 3 laps\r\nMuseum 2 — 4 laps\r\nHoliday Camp: California Edition — 2 laps\r\nSantorini — 4 laps\r\nToytanic 2 — 3 laps\r\nToy World Mayhem (R) — 4 laps\r\nRooftops 1 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

