---
title: 'Normal Advanced Races'
titleoverride: false
date: '15-11-2019 19:00'
event:
    host: Whitedoom
    ip: 'TBA on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'Normal Advanced Cars'
    classlink: 'https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0'
    tracklist: "Go Play Outside!\r\nGround n Smash 2 M\r\nSpa-Volt 2\r\nDonut Plains 3 RM\r\nMuseum 1 RM\r\nSupermarket 1 R\r\nOverground\r\nSplash Mountains\r\nToys in the Hood EX R\r\nMKDD Wario Coloseum M\r\nRooftops M\r\nRed Rock Valley RM\r\nToy World 2 R\r\nGhost Town 2 RM\r\nGrisville RM\r\nSpa-Volt 2 R\r\nTerminus\r\nSunset Hills\r\nMuseum 1 M\r\nGreen Night\r\nVenice R\r\nMoon Dawn R\r\nBroken Sunlight M\r\nSakura R\r\nPalm Marsh R\r\nSupermarket 2 RM\r\nGreen Night RM\r\nPenny Racers Caves M\r\nToytanic 2\r\nTemple of the Burning Darkness R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous average prestige results, [click here](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)

### Car Selection
* All cars with Advanced rating or lower, as well as the [I/O Advanced cars](https://re-volt.io/online/car-classes) or lower rated cars from Stock/DC content, main and bonus car pack (except Indy B).