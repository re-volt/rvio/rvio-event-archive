---
title: 'Amateur Races'
titleoverride: false
date: '11-06-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Lunar M\r\nPenny Racers - Harbour\r\nRooftops\r\nMuseum 1\r\nToy World Mayhem\r\nToySoldierz\r\nToys in the Hood 1\r\nRooftop Chase: Redux R\r\nQuake!\r\nFiddlers on the Roof\r\nOverground\r\nToy World 1 R\r\nIndustry\r\nHoliday Camp: California Edition"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

