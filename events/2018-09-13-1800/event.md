---
title: Pros
titleoverride: false
date: '2018-09-13 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: pros
    tracklist: "Kadish Sprint\r\nAMCO TT\r\nFreestoyle 2 (R)\r\nSuperMarket 2\r\nMeltdown\r\nGhost Town 2 (R)\r\nToy World 2 (R)\r\nToytanic 1 (R)\r\nToytanic 2\r\nSakura\r\nThe Gorge\r\nSnowy River\r\nFool's Mate 2\r\nSwan Street (R)\r\nSkating Toys\r\nToy World 1"
    vod: 'https://www.youtube.com/watch?v=eTTt3MQxejs'
    results: 'http://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-13_20-01-47.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

