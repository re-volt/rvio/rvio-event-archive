---
title: 'Pro Races'
titleoverride: false
date: '30-03-2019 04:00'
event:
    host: Santi
    ip: santi.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 1\r\nThe Felling Yard\r\nDonut Plains 3\r\nGhost Town 2 (R)\r\nSakura\r\nFool's Mate 2\r\nRooftops\r\nMoon Dawn (R)\r\nVenice\r\nBotanical Garden\r\nWhite Rose Chapel\r\nSnowy River\r\nBlood on the Rooftops\r\nSpa-Volt 1 (R)\r\nQuake!\r\nRooftop Chase: Redux"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

