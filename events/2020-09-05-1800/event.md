---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '05-09-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Grisville — 3 laps\r\nToy World 1 — 6 laps\r\nQuake! — 2 laps\r\nHull Breach 3000 — 4 laps\r\nJailhouse Rock (R) — 3 laps\r\nThe Bunker — 4 laps\r\nToySoldierz — 4 laps\r\nToytanic 2 (R) — 3 laps\r\nRooftops — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nFools Mate 2 — 4 laps\r\nToys in the Hood 2 — 4 laps\r\nLunar — 3 laps\r\nRadioactive Garden (R) — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: Semi-Pro
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

