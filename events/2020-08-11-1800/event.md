---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '11-08-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Quake! — 3 laps\r\nBiohazard Factory (R) — 3 laps\r\nToytanic 2 — 3 laps\r\nRoute-77 — 4 laps\r\nToySoldierz — 4 laps\r\nVenice — 4 laps\r\nBotanical Garden — 6 laps\r\nRV Temple — 2 laps\r\nMoon Dawn — 2 laps\r\nRadioactive Garden — 5 laps\r\nJailhouse Rock — 3 laps\r\nGhost Town 2 (R) — 5 laps\r\nRooftops — 4 laps\r\nSpa-Volt 1 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

