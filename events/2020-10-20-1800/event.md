---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '20-10-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "PetroVolt — 2 laps\r\nToys in the Hood 2 (R) — 4 laps\r\nSpooky-Volt (R) — 6 laps\r\nBotanical Garden — 6 laps\r\nQuake! — 2 laps\r\nGhost Town 1 — 6 laps\r\nSkating Toys: Redux — 2 laps\r\nMetro-Volt — 3 laps\r\nRadioactive Garden — 5 laps\r\nMuseum 1 — 3 laps\r\nCliffside — 6 laps\r\nCasino RV — 3 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nBiohazard Factory — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

