---
title: 'Semi-Pro Races'
titleoverride: false
date: '19-02-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 2\r\nMuseum 2\r\nGhost Town 1\r\nMuseum 1 (R)\r\nPetroVolt\r\nMetro-Volt\r\nLunar\r\nSakura (R)\r\nPenny Racers - Caves\r\nFreestoyle 2 (R)\r\nGround N Smash 2\r\nToySoldier\r\nRooftop Chase\r\nPalm Marsh\r\nMeltdown"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

