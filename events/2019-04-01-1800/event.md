---
title: 'Spring Final Special'
titleoverride: false
date: '01-04-2019 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Exotics'
    tracklist: 'Random Tracks (5 laps!)'
published: true
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
article:
    datePublished: '19-03-2019 20:55'
    dateModified: '19-03-2019 20:55'
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Cars in _Randomizer _ pool:** Dust Mite, Phat Slug, Angus 400, Perfect Shot, Genghis Kar, RV Loco, DGR 43, Stonemason, BossVolt, Fulon X, Pest Control, Pole Poz, Rotor, Alice, Naval Baron, RC X, Afterburner, Hydrox, KC-3, PRG 61, Panga, SNW 35, Acclaim F1, Black Widow, Eaglet, Patriot, Sater XL, Shining Fairy, Shinobi, Sir Gleam, Tribute

Visit **[RVGL Ladder](https://ladder.rv.gl/)** for more information!