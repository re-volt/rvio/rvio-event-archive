---
title: 'Clockwork Races'
titleoverride: true
date: '03-11-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Classic Clockworks'
    classlink: 'https://distribute.re-volt.io/packs/io_clockworks.zip'
    tracklist: "Toy World 2\r\nHull Breach 3000\r\nSkating Toys\r\nKadish Sprint\r\nSupermarket 2\r\nSakura (M)\r\nRe-Ville\r\nHelios\r\nMuseum 1 (R)\r\nToy World Mayhem (R)\r\nToys in the Hood 2\r\nSpa-Volt 2 (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

[Classic Clockworks](https://distribute.re-volt.io/packs/io_clockworks.zip) are required.