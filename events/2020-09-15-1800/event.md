---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '15-09-2020 18:00'
event:
    host: 'Etneus & Frostbitten'
    ip: 'etn.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World Mayhem — 4 laps\r\nWhite Rose Chapel — 3 laps\r\nQuake! — 2 laps\r\nToy World 2 (R) — 5 laps\r\nRV Temple — 2 laps\r\nToys in the Hood 2 — 4 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nPetroVolt — 2 laps\r\nRooftops — 3 laps\r\nSwan Street (R) — 3 laps\r\nToytanic 2 — 3 laps\r\nOverground — 2 laps\r\nBlood on the Rooftops — 3 laps\r\nBiohazard Factory — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

