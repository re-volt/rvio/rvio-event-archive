---
title: 'Amateur Races'
titleoverride: true
date: '06-05-2020 17:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World 1 — 5 laps\r\nHull Breach 3000 — 3 laps\r\nSupermarket 2 — 7 laps\r\nSmashride Circuit — 3 laps\r\nRanch (R) — 2 laps\r\nSwan Street — 3 laps\r\nOverground — 2 laps\r\nSantorini — 4 laps\r\nBlood on the Rooftops — 3 laps\r\nToys in the Hood 2 — 3 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nMuseum 2 (R) — 3 laps\r\nMetro-Volt — 3 laps\r\nHelios — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

