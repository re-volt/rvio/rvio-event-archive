---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '30-07-2020 18:00'
event:
    host: 'Frosttbitten & Kirioso'
    ip: 'frost.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Wildland — 3 laps\r\nToys in the Hood 2 — 4 laps\r\nAMCO TT (R) — 3 laps\r\nSnowland 1 — 3 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nPetroVolt — 2 laps\r\nMetro-Volt — 3 laps\r\nBotanical Garden — 6 laps\r\nSnowy River — 4 laps\r\nToys in the Hood 1 — 3 laps\r\nGrisville — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nSpa-Volt 2 — 3 laps\r\nToy World 1 (R) — 6 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

