---
title: 'Amateur Races'
titleoverride: true
date: '19-09-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Sakura — 2 laps\r\nSupermarket 2 — 7 laps\r\nMetro-Volt — 3 laps\r\nMedieval: Redux — 4 laps\r\nMeltdown — 4 laps\r\nAMCO TT (R) — 3 laps\r\nGrisville — 3 laps\r\nMuseum 2 — 3 laps\r\nThe Bunker — 4 laps\r\nMoon Dawn — 2 laps\r\nMuseum 1 — 2 laps\r\nJailhouse Rock — 2 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nToy World 1 (R) — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

