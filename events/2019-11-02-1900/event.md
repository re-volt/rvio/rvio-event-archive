---
title: 'Advanced Races'
titleoverride: true
date: '02-11-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World Mayhem (R)(M)\r\nWhite Rose Chapel\r\nToys in the Hood 1\r\nJailhouse Rock\r\nAMCO TT\r\nLunar\r\nToytanic 2 (R)\r\nBotanical Garden\r\nToys in the Hood 2\r\nSnowland 1 (R)\r\nIllusion\r\nMoon Dawn\r\nRe-Ville\r\nIndustry\r\nSpa-Volt 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

