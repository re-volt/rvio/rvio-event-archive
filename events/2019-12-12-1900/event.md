---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '12-12-2019 19:00'
event:
    host: 'Skarma & Kirioso'
    ip: 'sk.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "The Felling Yard\r\nAMCO TT\r\nMetro-Volt\r\nSpa-Volt 2 (R)\r\nGhost Town 1 (R)\r\nMuseum 1\r\nFiddlers on the Roof\r\nToys in the Hood 1\r\nMoon Dawn\r\nQuake!\r\nSupermarket 1 (M)\r\nToy World Aquatica: Redux (R)\r\nSnowy River\r\nLunar\r\nSnowland 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

