---
title: 'Random Super Pro Races'
titleoverride: false
date: '18:00 15-04-2019'
event:
    host: Saffron
    ip: 'on Discord'
    category: competitive
    mode: race
    class: other
    otherclass: 'Random Super Pro Cars'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "Toy World Aquatica: Redux R\r\nSpa-Volt 1 R\r\nRooftop Chase: Redux\r\nBiohazard Factory\r\nBotanical Garden R\r\nRV Temple\r\nToys in the Hood 2\r\nSantorini\r\nJailhouse Rock\r\nOverground\r\nFiddlers on the Roof\r\nRadioactive Garden\r\nPetroVolt\r\nToy World 2\r\nMuseum 1\r\nSakura"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

