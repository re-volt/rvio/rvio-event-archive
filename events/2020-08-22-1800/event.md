---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '22-08-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Route-77 — 3 laps\r\nLunar — 3 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nMuseum 2 — 4 laps\r\nMuseum 1 (R) — 3 laps\r\nIndustry — 3 laps\r\nBiohazard Factory — 2 laps\r\nRV Temple — 2 laps\r\nMedieval: Redux — 4 laps\r\nImages of Giza: Redux (R) — 5 laps\r\nSupermarket 2 — 8 laps\r\nHull Breach 3000 — 3 laps\r\nQuake! — 2 laps\r\nToytanic 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

