---
title: 'Toyeca Excluisve'
titleoverride: false
date: '05-04-2019 18:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Toyeca Exclusive'
    tracklist: "Quake!\r\nMuseum 1 (R)\r\nToy World Mayhem\r\nToytanic 2\r\nRanch\r\nBiohazard Factory\r\nSwan Street\r\nGhost Town 2 (R)\r\nPenny Racers - Caves\r\nSantoini\r\nJailhouse Rock\r\nBlood on the Rooftops\r\nPetroVolt (R)\r\nHoliday Camp: California Edition\r\nRV Temple"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

