---
title: 'Christmas Carnival (Day 1)'
titleoverride: true
date: '26-12-2020 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Christmas Carnival'
    classlink: 'https://re-volt.io/blog/christmas-carnival-2020'
    tracklist: "Christmas Special Stage — 4 laps\r\nSnowy River — 3 laps\r\nSnowland 1 — 3 laps\r\nAspenside — 5 laps\r\nCandyland — 2 laps\r\nGlacier Cliffs 3 — 6 laps\r\nFrostbite — 3 laps\r\nCryo-Volt 3 — 3 laps\r\n0 Degrees — 3 laps\r\nIceatopia — 4 laps\r\nWinter Park — 3 laps\r\nCumulonimbus Clouds — 4 laps\r\nSBX Alpine — 4 laps\r\nZero Degrees — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Christmas Carnival (Day 1)'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](https://re-volt.io/user/pages/blog/christmas-carnival-2020/carnival.jpg)
If you have not submitted a car, you are only allowed to race **XMAS 2020** unless told otherwise.

### Requirements
* [Christmas Carnival 2020 Pack](https://files.re-volt.io/packs/christmas/2020/carnival_full.7z)