---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '10-12-2019 19:00'
event:
    host: 'Saffron & Narusori'
    ip: 'saff.rv.gl OR sas.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Toy World 1\r\nSpa-Volt 1\r\nHull Breach 3000\r\nGrisville\r\nRooftops\r\nIndustry (M)\r\nBiohazard Factory\r\nPenny Racers - Harbour\r\nToySoldierz (R)\r\nToytanic 1 (R)\r\nToys in the Hood 2\r\nHoliday Camp: California Edition (R)\r\nSakura\r\nKadish Sprint\r\nRooftop Chase: Redux\r\nSkating Toys"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

