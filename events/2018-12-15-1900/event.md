---
title: Pros
titleoverride: false
date: '15-12-2018 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 2\r\nRanch (R)\r\nPenny Racers - Caves\r\nMuseum 1\r\nMetro-Volt\r\nMolten Caverns (R)\r\nToySoldierz\r\nToys in the Hood 1\r\nPalm Marsh\r\nRadioactive Garden\r\nIllusion\r\nSwan Street\r\nMK64 Wario Stadium\r\nJailhouse Rock\r\nHoliday Camp: California Edition\r\nSuperMarket 2 (R)"
    vod: 'https://www.youtube.com/watch?v=UVdjBXgCRbk'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-15_20-04-12.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

