---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '31-10-2020 19:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Rooftops 1 (R) — 2 laps\r\nToys in the Hood 1 — 3 laps\r\nThe Felling Yard — 2 laps\r\nToy World 2 — 4 laps\r\nVenice (R) — 3 laps\r\nQuake! — 2 laps\r\nSupermarket 1 — 4 laps\r\nHoliday Camp: California Edition — 2 laps\r\nKadish Sprint — 2 laps\r\nGhost Town 2 (R) — 4 laps\r\nLunar — 3 laps\r\nSnowy River — 3 laps\r\nWildland — 3 laps\r\nMysterious Toy-Volt Factory — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

