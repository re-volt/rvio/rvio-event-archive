---
title: 'Semi-Pro Races'
titleoverride: false
date: '01-10-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Illusion (R)\r\nToy World Aquatica: Redux\r\nSpa-Volt 2\r\nOverground\r\nPetroVolt (R)\r\nToy World Mayhem\r\nCliffside\r\nHelios\r\nToy World 2\r\nFool's Mate 2\r\nToys in the Hood 2\r\nThe Bunker\r\nToy World 1\r\nSnowland 1\r\nToys in the Hood 1 (M)(R)\r\nMoon Dawn (Bonus Race)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

