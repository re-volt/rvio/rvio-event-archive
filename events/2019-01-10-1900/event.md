---
title: 'Amateur Races'
titleoverride: false
date: '10-01-2019 19:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1\r\nToys in the Hood 2\r\nGrisville (R)\r\nToy World Mayhem\r\nPetroVolt\r\nSuperMarket 1\r\nPenny Racers - Caves\r\nBiohazard Factory\r\nMeltdown\r\nQuake!\r\nSkating Toys\r\nThe Gorge\r\nSpa-Volt 1 (R)\r\nMuseum 2 (R)"
    stream: 'http://twitch.tv/rvio'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

