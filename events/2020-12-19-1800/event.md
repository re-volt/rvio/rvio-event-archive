---
title: 'Frost Grand Prix'
titleoverride: true
date: '19-12-2020 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Frost Grand Prix'
    classlink: 'https://re-volt.io/events/2020-12-19-1800'
    tracklist: "Toy World 2 — 4 laps\r\nLS Kart Space I — 6 laps\r\nKadish Sprint — 2 laps\r\nTenebrosity — 3 laps\r\nCurves and Caves — 4 laps\r\nSlide in the Mounts — 3 laps\r\nTGO: Frigid Peaks — 4 laps\r\nCumulonimbus Clouds — 4 laps\r\nWipeout 2097 — 5 laps\r\nMuseum 2 — 4 laps\r\nChristmas Special Stage — 4 laps\r\nWinter Town — 5 laps\r\n0 Degrees — 2 laps\r\nAurora Borealis — 4 laps"
media_order: rvcc20_poster4.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](rvcc20_poster4.png)
A team-based event involving **Glacier** vs **Ice Spike**!

This time, the points will be scored based on the __overall performance__ of each team. It is preferable if the teams are balanced, but if not possible, then the team-based results will ignore points scored from the worst performing players on the bigger team.

### Choose between the following cars
* Glacier
* Ice Spike

### Requirements
* [CC20 Cars](https://files.re-volt.io/packs/christmas/2020/rvcc20_cars.7z)
* [CC20 Tracks](https://files.re-volt.io/packs/christmas/2020/rvcc20_tracks.7z)