---
title: 'Battle Tag'
titleoverride: false
date: '04-12-2019 19:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: special
    mode: battle-tag
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Garden Battle\r\nMuseum Battle\r\nNeighbourhood Battle\r\nSupermarket Battle\r\nToy World Battle"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

The **[Battle Arenas](https://distribute.re-volt.io/packs/io_lmstag.zip)** are required.