---
title: 'Amateur Races'
titleoverride: false
date: '09-05-2019 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 1 (M)\r\nIndustry\r\nHelios\r\nBiohazard Factory (R)\r\nThe Bunker\r\nJailhouse Rock\r\nDonut Plains 3\r\nSkating Toys\r\nQuake! (R)\r\nToySoldierz\r\nSpa-Volt 1\r\nToy World 1\r\nToys in the Hood 1 (R)\r\nSuperMarket 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

