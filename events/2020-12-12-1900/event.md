---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '12-12-2020 19:00'
event:
    host: 'Mighty & Bismarck'
    ip: 'mighty.rv.gl OR bis.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Botanical Garden EX (R) — 2 laps\r\nMeltdown — 4 laps\r\nLunar — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nSupermarket 2 — 7 laps\r\nToy World Aquatica: Redux — 3 laps\r\nToytanic 2 (R) — 3 laps\r\nThe Bunker — 4 laps\r\nSpa-Volt 1 — 3 laps\r\nCliffside — 5 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nHMS Invincible: Redux — 3 laps\r\nKadish Sprint — 2 laps\r\nMuseum 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

