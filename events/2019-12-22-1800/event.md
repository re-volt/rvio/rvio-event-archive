---
title: 'Christmas Carnival <small>(Day 2)</small>'
titleoverride: true
date: '22-12-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Christmas Carnival'
    classlink: 'https://re-volt.io/blog/christmas-carnival-2019'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Sign-ups are required.** [More information here.](https://re-volt.io/blog/christmas-carnival-2019)