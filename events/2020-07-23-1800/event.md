---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '23-07-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Lunar — 3 laps\r\n    White Rose Chapel — 3 laps\r\n    Museum 3 — 4 laps\r\n    Ghost Town 2 (R) — 5 laps\r\n    Toy World Mayhem — 4 laps\r\n    Biohazard Factory (R) — 3 laps\r\n    Spa-Volt 1 — 3 laps\r\n    Toys in the Hood 2 — 4 laps\r\n    Toys in the Hood 1 — 4 laps\r\n    PetroVolt (R) — 2 laps\r\n    Overground — 3 laps\r\n    Toytanic 1 — 3 laps\r\n    Radioactive Garden — 5 laps\r\n    Rooftop Chase: Redux — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

