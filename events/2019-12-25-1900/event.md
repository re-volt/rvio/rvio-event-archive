---
title: 'Christmas Bash 1'
titleoverride: true
date: '25-12-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Snowy River\r\nFrostbite\r\nChristmas Special Stage\r\nPenny Racers - Caves\r\nRally ZC SS 2\r\nGlacier Cliffs 3\r\nWinter Madness\r\nFrozen Caverns\r\nChristmas Snow Globe\r\nCandyland\r\nWinter Park\r\nChilled to the Bone\r\nArctic Air\r\nZero Degrees\r\nUranus\r\nSnowland 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

The **[Christmas Carnival Track Pack](https://files.re-volt.io/packs/christmas/2019/carnival_tracks.7z)** is required.