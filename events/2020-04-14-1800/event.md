---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '14-04-2020 18:00'
event:
    host: 'Narusori & Delecto'
    ip: 'naru.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Lunar —  3 laps\r\nToys in the Hood 2 (R) —  4 laps\r\nThe Felling Yard —  3 laps\r\nImages of Giza: Redux —  5 laps\r\nRooftops —  3 laps\r\nMuseum 1 —  3 laps\r\nBiohazard Factory (R) —  2 laps\r\nSchool's Out (R) —  2 laps\r\nToy World 1 —  6 laps\r\nToy World Mayhem —  4 laps\r\nSpa-Volt 1 —  3 laps\r\nRV Temple —  2 laps\r\nFiddlers on the Roof: Redux (M) —  3 laps\r\nSmashride Circuit —  3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

