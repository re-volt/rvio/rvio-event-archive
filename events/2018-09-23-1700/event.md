---
title: 'Cops vs. Tuners'
titleoverride: false
date: '23-09-2018 17:00'
event:
    host: FZG
    category: unofficial
    mode: race
    class: other
    otherclass: 'Cops vs. Tuners'
    classlink: 'https://re-volt.io/events/2018-09-20-1700'
published: false
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

A casual session with pickups on Simulation, 4 laps and 5 tracks. It is recommended to pick a different car and livery for each racer based on the lobby room list in chronological order. The amount of both sides should be equal, 50-50, so 8 tuners versus 8 cops and so on.

**Choose your side!**

Be the **TUNERS**: avoid the cops at all cost and win the race!
* [Evolution](http://revoltzone.net/cars/30818/Evolution)
* [Siri](http://revoltzone.net/cars/32908/Siri)
* [Skyedge](http://revoltzone.net/cars/30819/Skyedge)
* [Shino](http://revoltzone.net/cars/32907/Shino)

Be the **COPS**: tackle, shoot, and *serve* those illegal racers right!
* [RVPD](http://revoltzone.net/cars/29129/RVPD)
* [Crowd Control](http://revoltzone.net/cars/15075/Crowd%20Control)