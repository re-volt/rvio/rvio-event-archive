---
title: 'RC Bandit Exclusive'
titleoverride: false
date: '03-07-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'RC Bandit Exclusive'
    classlink: 'https://revolt.fandom.com/wiki/RC_Bandit'
    tracklist: "Spa-Volt 1 (R)\r\nGrisville\r\nToy World Mayhem\r\nToySoldierz\r\nWhite Rose Chapel\r\nBotanical Garden\r\nMuseum 2\r\nSpa-Volt 2\r\nToys in the Hood 2 (R)\r\nAMCO TT (R)\r\nRooftop Chase: Redux (M)\r\nSuperMarket 1\r\nJailhouse Rock\r\nRV Temple"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

