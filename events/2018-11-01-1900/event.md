---
title: Slugs
titleoverride: false
date: '01-11-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 1\r\nMuseum 2 (R)\r\nQuake! (R)\r\nCliffside\r\nToySoldierz\r\nThe Bunker\r\nGhost Town 2\r\nRooftops\r\nRadioactive Garden\r\nMetro-Volt\r\nAMCO TT\r\nSantorini (R)\r\nHelios\r\nVenice"
    vod: 'https://www.youtube.com/watch?v=b5OvuoMbkPU'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-01_20-06-13.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

