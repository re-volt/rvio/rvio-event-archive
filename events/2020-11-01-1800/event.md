---
title: 'Halloween Carnival <small>(Day 2)</small>'
titleoverride: true
date: '01-11-2020 18:00'
event:
    host: hajducsekb
    ip: hb.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Halloween Carnival'
    classlink: 'https://re-volt.io/blog/halloween-carnival-2020#downloads'
    tracklist: "HALLOWEEN 2 — 3 laps\r\nGhost Town 2 Halloween — 4 laps\r\nREAPERS_RELM — 4 laps\r\nHallows Eve — 3 laps\r\nSpooky-Volt — 4 laps\r\nSNES Ghost Valley 2 — 6 laps\r\nMuseum 6 — 3 laps\r\nStadVolt — 4 laps\r\nScary Caverns — 5 laps\r\nEpimysium — 5 laps\r\nSet The Controls... — 3 laps\r\nSuper Nova — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Halloween Carnival'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**For more information, visit**: [ https://re-volt.io/blog/halloween-carnival-2020]( https://re-volt.io/blog/halloween-carnival-2020)