---
title: 'Semi-Pro Races'
titleoverride: false
date: '20-07-2019 18:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Botanical Garden\r\nToy World 1\r\nToys in the Hood 2\r\nMuseum 1 (R)\r\nSpa-Volt 1\r\nSpa-Volt 2\r\nHelios (M)\r\nMetro-Volt\r\nGrisville\r\nThe Felling Yard\r\nAMCO TT\r\nIndustry (M)\r\nSantorini (R)\r\nYABBA DABBA DOO!\r\nSnowland 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

