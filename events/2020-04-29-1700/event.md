---
title: 'Amateur Races'
titleoverride: false
date: '29-04-2020 17:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Helios — 3 laps\r\nSuperMarket 1 — 4 laps\r\nPetroVolt — 2 laps\r\nQuake! R — 2 laps\r\nRooftops — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nVenice — 3 laps\r\nToy World 1 (R) — 4 laps\r\nSantorini — 4 laps\r\nMetro-Volt — 3 laps\r\nBotanical Garden — 5 laps\r\nIndustry — 2 laps\r\nRadioactive Garden (R) — 5 laps\r\nThe Bunker — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

