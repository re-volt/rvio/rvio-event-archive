---
title: 'Long Journey: 1st Day'
titleoverride: false
date: '15-07-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Long Journey: 1st Day'
    classlink: 'https://re-volt.io/events/2019-07-15-1800'
    tracklist: "Hull Breach 3000\r\nRV Temple\r\nSpa-Volt 1\r\nSpa-Volt 2 (R)\r\nGrisville (RM)\r\nThe Bunker\r\nFiddlers on the Roof\r\nDonut Plains 3 (M)\r\nPenny Racers - Caves\r\nRooftop Chase: Redux\r\nMetro-Volt\r\nCliffside\r\nIllusion\r\nToy World Mayhem\r\nSnowland 1"
media_order: GoingHome.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**A special 4-day event hosted by OhNej in celebration of Re-Volt's 20th Anniversary.**<br>
We will be racing with [Pro Cars](https://re-volt.io/online/car-classes) on Arcade mode, with pick-ups enabled and 3 laps on each track.

![](https://re-volt.io/user/pages/events/2019-07-15-1800/GoingHome.png)