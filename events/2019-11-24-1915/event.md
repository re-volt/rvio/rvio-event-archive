---
title: 'Rookie Knockout'
titleoverride: true
date: '24-11-2019 19:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: rookie
    classlink: 'https://www.dropbox.com/s/evna7pslcw0rgvz/re-volt%20knockout%20session%20info.txt?dl=0'
    tracklist: Random
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information, be sure to check the [Knockout Session Info](https://www.dropbox.com/s/evna7pslcw0rgvz/re-volt%20knockout%20session%20info.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)

### Car Selection
All cars that are listed as Rookie in-game or classified as Rookie in the [I/O car classes](https://re-volt.io/online/car-classes) may be selected.