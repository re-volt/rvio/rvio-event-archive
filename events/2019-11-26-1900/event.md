---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '26-11-2019 19:00'
event:
    host: 'OhNej & Saffron'
    ip: 'ohnej.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Jailhouse Rock (R)\r\nRoute-77\r\nHoliday Camp: California Edition\r\nPenny Racers - Caves\r\nGhost Town 1\r\nSakura (M)\r\nRadioactive Garden (R)\r\nToytanic 1 (R)\r\nHelios\r\nSupermarket 2\r\nCliffside\r\nSwan Street\r\nMuseum 2\r\nGrisville"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

