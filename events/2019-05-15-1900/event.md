---
title: 'Random RWD Cars (Ladder)'
titleoverride: false
date: '19:00 15-05-2019'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Random RWD Cars (Ladder)'
    classlink: 'https://re-volt.io/events/2019-05-15-1900'
    tracklist: 'Random Category III Tracks'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event will count for the **RVGL Ladder**.

* [Current Ladder standings](https://ladder.rv.gl/ranking)
* [Information & Rules](https://ladder.rv.gl/info)

You need the **I/O Track- and Carpacks** to join the races!

* [Ladder's Car Pool](https://ladder.rv.gl/info/cars)
* [Ladder's Track Pool](https://ladder.rv.gl/info/tracks)