---
title: 'Rookie Races'
titleoverride: false
date: '08-01-2019 19:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 2\r\nDonut Plains 3\r\nMuseum 1\r\nSakura (R)\r\nMetro-Volt\r\nJailhouse Rock\r\nAMCO TT\r\nRooftops\r\nIndustry\r\nToySoldierz\r\nSantorini (R)\r\nHelios \r\nMK64 Wario Stadium\r\nGhost Town 1 (R)"
    stream: 'http://twitch.tv/rvio'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

