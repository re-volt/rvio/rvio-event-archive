---
title: Pros
titleoverride: false
date: '04-10-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 2\r\nGhost Town 2\r\nSakura\r\nGround N Smash 2\r\nIndustry\r\nYABBA DABBA DOO!\r\nToy World Aquatica: Redux (R)\r\nMolten Caverns\r\nKadish Sprint\r\nSkating Toys\r\nMuseum 1\r\nToySoldierz\r\nHoliday Camp: California Edition\r\nSpa-Volt 1 (R)\r\nPetroVolt\r\nToytanic 2 (R)"
    vod: 'https://www.youtube.com/watch?v=fw-93SU_DMM'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-04_21-04-08.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

