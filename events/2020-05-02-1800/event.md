---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '02-05-2020 18:00'
event:
    host: 'Narusori & Kirioso'
    ip: 'naru.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "The Bunker — 4 laps\r\nLunar — 3 laps\r\nRoute-77 — 4 laps\r\nFiddlers on the Roof: Redux — 3 laps\r\nThe Felling Yard — 3 laps\r\nGrisville R — 3 laps\r\nCliffside — 6 laps\r\nPetroVolt — 2 laps\r\nGhost Town 2 — 5 laps\r\nToys in the Hood 2 — 4 laps\r\nSpa-Volt 1 — 3 laps\r\nToy World 2 — 5 laps\r\nRooftops R — 4 laps\r\nVenice R — 4 laps "
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

