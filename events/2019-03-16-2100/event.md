---
title: 'Battle Tag'
titleoverride: false
date: '16-03-2019 21:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: battle-tag
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Neighborhood Battle\r\nBlock Fort\r\nSupermarket Battle\r\nToy World Battle\r\nGarden Battle\r\nMuseum Battle\r\nParty in the Toy World"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

