---
title: 'Super Pro Races'
titleoverride: true
date: '23-05-2020 18:00'
event:
    host: 'Kirioso & URV'
    ip: 'kiri.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Radioactive Garden (R) — 5 laps\r\nToySoldierz — 4 laps\r\nMuseum 1 (R) — 3 laps\r\nMetro-Volt — 3 laps\r\nBotanical Garden — 6 laps\r\nBlood on the Rooftops — 3 laps\r\nOverground — 3 laps\r\nFools Mate 2 — 4 laps\r\nSpa-Volt 2 — 4 laps\r\nToy World 2 — 5 laps\r\nImages of Giza: Redux — 5 laps\r\nVenice (R) — 4 laps\r\nPetroVolt — 2 laps\r\nToy World 1 — 6 laps "
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

