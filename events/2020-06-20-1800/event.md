---
title: 'Super Pro Races'
titleoverride: true
date: '20-06-2020 18:00'
event:
    host: Kirioso
    ip: kiri.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "The Bunker — 4 laps\r\nToys in the Hood 2 — 4 laps\r\nLunar — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nToys in the Hood 1 (R) — 4 laps\r\nToytanic 2 — 3 laps\r\nQuake! (R) — 3 laps\r\nToy World 1 — 6 laps\r\nSakura — 3 laps\r\nJailhouse Rock — 3 laps\r\nAMCO TT — 3 laps\r\nSmashride Circuit — 3 laps\r\nRoute-77 (R) — 4 laps\r\nSantorini — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

