---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '13-02-2020 19:00'
event:
    host: 'Dvark & Kirioso'
    ip: 'dv.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Toy World 1 — 5 laps\r\nSnowy River — 3 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nPenny Racers - Harbour — 3 laps\r\nToytanic 1 (R) — 3 laps\r\nThe Felling Yard — 2 laps\r\nSchool's Out — 2 laps\r\nToySoldierz — 3 laps\r\nLunar — 3 laps\r\nToy World 2 — 4 laps\r\nBiohazard Factory (R) — 2 laps\r\nToys in the Hood 2 (M) — 3 laps\r\nQuake! — 2 laps\r\nJailhouse Rock — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Ameteur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

