---
title: 'Gungame Races'
titleoverride: true
date: '27-06-2020 19:15'
event:
    host: Saffron
    ip: saff.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Main Cars'
    classlink: 'https://re-volt.io/online/cars'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World Aquatica: Redux\r\nRooftop Chase: Redux\r\nImages of Giza: Redux\r\nBiohazard Factory\r\nMolten Caverns\r\nThe Felling Yard\r\nSupermarket 1\r\nMoon Dawn\r\nRooftops 1\r\nBlood on the Rooftops\r\nFool's Mate 2\r\nToy World 1\r\nRV Temple\r\nHull Breach 3000\r\nMetro-Volt\r\nJailhouse Rock\r\nThe Bunker\r\nHoliday Camp: California Edition\r\nToytanic 2\r\nSpa-Volt 2\r\nSakura\r\nMuseum 1\r\nRadioactive Garden\r\nSupermarket 2\r\nToys in the Hood 2\r\nRooftops\r\nKadish Sprint\r\nBotanical Garden\r\nOverground\r\nSantorini\r\nSwan Street\r\nMedieval: Redux\r\nQuake!\r\nMuseum 3\r\nAMCO TT\r\nWildland\r\nGrisville\r\nIndustry\r\nRoute-77\r\nToySoldierz\r\nSmashride Circuit\r\nMeltdown\r\nWhite Rose Chapel\r\nToy World 2\r\nVenice\r\nPetroVolt\r\nToys in the Hood 1\r\nSpa-Volt 1\r\nLunar\r\nSnowland 1\r\nMuseum 2\r\nToy World Mayhem\r\nGhost Town 1\r\nSnowy River\r\nGhost Town 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirements:
* [I/O Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [I/O Car Collection](https://distribute.re-volt.io/packs/io_cars.zip)

Everyone starts off by choosing the first car in the list, **Sterling F77.** Whoever gets a podium must switch to the next car in the list below. The session ends whenever someone finishes on the podium with the last car on the list, **Phat Slug.** If multiple people finish on the podium with **Phat Slug** in a race, the player with the higher finishing position is the winner.

* Sterling F77
* King Kaiju
* BanKing
* Humma
* Locust
* Power Station
* Rice Ball
* Panther
* Star Carbs
* Reddlum
* Dust Mite
* Phat Slug

Races will be a minute long, give-or-take. Tracklist will be played twice (if needed).