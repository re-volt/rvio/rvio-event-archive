---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '05-12-2019 19:00'
event:
    host: 'Skarma & OhNej'
    ip: 'sk.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Penny Racers - Harbour\r\nWhite Rose Chapel\r\nPetroVolt (R)\r\nDonut Plains 3 (R)\r\nToy World Mayhem\r\nGhost Town 2 (R)\r\nBotanical Garden\r\nSupermarket 2\r\nSpa-Volt 1\r\nAMCO TT (M)\r\nQuake!\r\nGhost Town 1\r\nBiohazard Factory\r\nToy World Aquatica: Redux"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

