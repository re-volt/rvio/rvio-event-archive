---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '06-02-2020 19:00'
event:
    host: 'Kirioso & Dvark'
    ip: 'kiri.rv.gl OR dv.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Blood on the Rooftops — 3 laps\r\nMuseum 2 (R) —  3 laps\r\nPenny Racers - Caves — 4 laps\r\nMetro-Volt — 3 laps\r\nThe Felling Yard — 2 laps\r\nToy World Mayhem — 3 laps\r\nLunar — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nCliffside — 5 laps\r\nBotanical Garden — 5 laps\r\nRoute-77 (R) — 3 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nToys in the Hood 1 (M) — 3 laps\r\nToys in the Hood 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

