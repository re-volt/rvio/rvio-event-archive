---
title: 'Triple Trouble'
titleoverride: true
date: '01-04-2020 19:00'
event:
    host: Narusori
    ip: sas.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Scania P94D OR Renault Master OR Toyota Coaster'
    classlink: 'https://re-volt.io/events/2020-04-01-1900'
    tracklist: "Swan Street — 3 laps\r\nSupermarket 1 — 4 laps\r\nMuseum 1 — 2 laps\r\nToy World Mayhem (R) — 3 laps\r\nDonut Plains 3 (R) — 4 laps\r\nCliffside — 5 laps\r\nToy World Aquatica: Redux — 3 laps\r\nBotanical Garden (M) — 5 laps\r\nAMCO TT — 3 laps\r\nSupermarket 2 (R) — 7 laps\r\nRoute-77 — 3 laps\r\nSpa-Volt 1 — 3 laps\r\nPenny Racers - Harbour — 3 laps\r\nOverground — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Simulation mode** will be used for the races.

Only the following cars will be allowed:
* Scania P94D
* Renault Master
* Toyota Coaster