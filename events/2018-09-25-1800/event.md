---
title: Rookies
titleoverride: false
date: '25-09-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: rookies
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Venice (R)\r\nToys in the Hood 2 (R)\r\nSuperMarket 1\r\nToy World 1\r\nRadioactive Garden\r\nRooftops\r\nMetro-Volt\r\nFreestoyle 2\r\nAMCO TT\r\nJailhouse Rock\r\nSnowy River (R)\r\nSpa-Volt 1\r\nToySoldierz\r\nBiohazard Factory"
    vod: 'https://www.youtube.com/watch?v=221N8n_JQcE'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-25_21-04-06.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

