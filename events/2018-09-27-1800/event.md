---
title: Semi-Pros
titleoverride: false
date: '27-09-2018 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: semi-pros
    otherclass: Semi-Pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Holiday Camp: California Edition\r\nToys in the Hood 1\r\nToy World 2\r\nOverground\r\nPetroVolt (R)\r\nMolten Caverns\r\nBotanical Garden\r\nHelios\r\nGhost Town 2 (R)\r\nPalm Marsh\r\nCliffside\r\nThe Bunker\r\nThe Felling Yard\r\nRanch\r\nIllusion (R)"
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-27_20-04-04.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

