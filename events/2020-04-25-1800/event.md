---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '25-04-2020 18:00'
event:
    host: 'Narusori & Delecto'
    ip: 'naru.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Museum 1 — 3 laps\r\nMeltdown — 4 laps\r\nSpa-Volt 2 (R) — 4 laps\r\nHelios — 3 laps\r\nKadish Sprint — 2 laps\r\nMetro-Volt — 3 laps\r\nThe Bunker — 4 laps\r\nCliffside — 6 laps\r\nSupermarket 2 — 8 laps\r\nSwan Street (M) — 3 laps\r\nAMCO TT (R) — 3 laps\r\nToytanic 2 (R) — 3 laps\r\nRadioactive Garden — 5 laps\r\nSchool's Out — 2 laps\r\nSupermarket 1 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

