---
title: 'Classic Slugs'
titleoverride: false
date: '03-11-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Classic Slugs'
    classlink: 'https://re-volt.io/events/2018-11-03-1800'
    tracklist: "Toys in the Hood 1\r\nSuperMarket 2\r\nMuseum 2\r\nBotanical Garden\r\nRooftops \r\nToy World 1\r\nGhost Town 1\r\nToy World 2\r\nToys in the Hood 2\r\nToytanic 1\r\nMuseum 1\r\nSuperMarket 1\r\nGhost Town 2\r\nToytanic 2"
    results: 'https://online.re-volt.io/sessions/results.php?file=session_2018-11-03_20-03-07.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

The only required content are the [stock assets](https://distribute.re-volt.io/packs/game_files.zip), the [DC content](https://distribute.re-volt.io/packs/rvgl_dcpack.zip), and the latest [RVGL](https://rvgl.re-volt.io/) version. The room will be limited to 12 players. **Each car may only be raced by one player at a time.**

### Car Selection:
* Aquasonic
* Candy Pebbles
* RC San
* Genghis Kar
* Mouse
* Panga TC
* Evil Weasel
* R6 Turbo
* NY 54
* Bertha Ballistics
* RC Phink
* LA 54
* Matra XL
* Groovster
* Splat
* Shocker