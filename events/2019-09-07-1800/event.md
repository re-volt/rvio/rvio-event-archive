---
title: 'Semi-Pro Races'
titleoverride: false
date: '07-09-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 1\r\nHoliday Camp: California Edition\r\nHull Breach 3000 (M)\r\nToy World 2\r\nRanch\r\nIllusion\r\nRadioactive Garden\r\nSpa-Volt 1 (R)\r\nRV Temple\r\nMuseum 1 (R)\r\nFool's Mate 2\r\nSkating Toys\r\nSpa-Volt 2\r\nRe-Ville\r\nToys in the Hood 1\r\nPenny Racers - Caves (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

