---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '15-12-2020 19:00'
event:
    host: 'Frostbitten & Mighty'
    ip: 'frost.rv.gl OR mighty.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Blood on the Rooftops — 3 laps\r\nMuseum 2 (R) — 4 laps\r\nMuseum 3 — 3 laps\r\nMetro-Volt — 3 laps\r\nOverground — 2 laps\r\nRooftops — 3 laps\r\nVenice — 3 laps\r\nLibrary — 3 laps\r\nRooftop Chase: Redux (R) — 3 laps\r\nSupermarket 2 — 8 laps\r\nToys in the Hood 2 — 4 laps\r\nPetroVolt — 2 laps\r\nCasino RV — 3 laps\r\nWildland (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

