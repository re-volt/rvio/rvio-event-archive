---
title: 'Long Journey: Final Day'
titleoverride: false
date: '21-07-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Long Journey: Day 4'
    classlink: 'https://re-volt.io/events/2019-07-21-1800'
    tracklist: "Toys in the Hood 1 (R)\r\nSuperMarket 2\r\nMuseum 2\r\nBotanical Garden (M)\r\nRooftops\r\nToy World 1\r\nGhost Town 1\r\nToy World 2\r\nToys in he Hood 2\r\nToytanic 1\r\nMuseum 1 (RM)\r\nSuperMarket 1\r\nGhost Town 2\r\nToytanic 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**A special 4-day event hosted by OhNej in celebration of Re-Volt's 20th Anniversary.**<br>
We will be racing with [Pro Cars](https://re-volt.io/online/car-classes) on Arcade mode, with pick-ups enabled and 3 laps on each track.

![](https://re-volt.io/user/pages/events/2019-07-15-1800/GoingHome.png)