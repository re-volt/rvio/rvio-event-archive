---
title: Prugs
titleoverride: false
date: '2018-09-11 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: prugs
    tracklist: "Toy World Aquatica: Redux (R)\r\nToys in the Hood 1\r\nPenny Racers - Caves\r\nIllusion\r\nGhost Town 1\r\nCliffside\r\nBotanical Garden\r\nRadioactive Garden\r\nHoliday Camp: California Edition\r\nMetro-Volt\r\nBlood on the Rooftops\r\nToys in the Hood 2 (R)\r\nRe-Ville\r\nToy World Mayhem\r\nPetroVolt (R)"
    vod: 'https://www.youtube.com/watch?v=_Buc6zWQ1UA'
    results: 'http://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-11_20-06-10.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

