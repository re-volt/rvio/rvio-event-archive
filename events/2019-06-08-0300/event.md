---
title: 'Pro Races'
titleoverride: false
date: '08-06-2019 03:00'
event:
    host: Santi
    ip: santi.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 1 (R)\r\nSupermarket 1\r\nMetro-Volt\r\nGrisville\r\nDonut Plains 3\r\nToy World Mayhem (M)\r\nSupermarket 2\r\nIllusion\r\nToy World 1\r\nThe Bunker\r\nRadioactive Garden\r\nAMCO TT (R)\r\nQuake!\r\nRe-Ville\r\nYABBA DABBA DOO!\r\nSpa-Volt 1 (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

