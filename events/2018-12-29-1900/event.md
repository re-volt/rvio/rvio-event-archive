---
title: Rookies
titleoverride: false
date: '29-12-2018 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: rookies
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 2\r\nToySoldierz\r\nJailhouse Rock\r\nSantorini (R)\r\nToy World Mayhem\r\nRe-Ville\r\nToy World Aquatica: Redux\r\nBotanical Garden\r\nSakura (R)\r\nToys in the Hood 2 (R)\r\nHelios\r\nPalm Marsh\r\nRadioactive Garden\r\nGhost Town 1"
    vod: 'https://www.youtube.com/watch?v=19jxgTTKPAk'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-29_20-03-08.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

