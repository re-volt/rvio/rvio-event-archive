---
title: 'Clockwork Races'
titleoverride: true
date: '10-11-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Modern Clockworks'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "Spa-Volt 1\r\nSnowy River (R)\r\nBotanical Garden\r\nMetro-Volt\r\nRooftop Chase: Redux\r\nHoliday Camp: California Edition\r\nFiddlers on the Roof\r\nKadish Sprint\r\nSnowland 1 (M)\r\nSupermarket 2\r\nMuseum 2\r\nVenice (R)\r\nThe Bunker\r\nMuseum 1 (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

[Modern Clockworks](https://distribute.re-volt.io/packs/io_clockworks_modern.zip) are required.