---
title: 'Amateur Races'
titleoverride: true
date: '31-10-2019 19:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Supermarket 2\r\nDonut Plains 3 (R)\r\nMetro-Volt (M)\r\nHelios\r\nCliffside\r\nHull Breach 3000\r\nOverground\r\nToytanic 1 (R)\r\nGhost Town 1\r\nMuseum 2\r\nSwan Street\r\nSpa-Volt 2 (R)\r\nRooftop Chase: Redux\r\nPenny Racers - Harbour"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

