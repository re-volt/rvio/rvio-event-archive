---
title: Semi-Pros
titleoverride: false
date: '2018-09-15 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pros
    tracklist: "Museum 1\r\nSantorini (R)\r\nQuake!\r\nPalm Marsh\r\nHelios\r\nPenny Racers - Harbour\r\nJailhouse Rock\r\nGrisville (R)\r\nRooftops\r\nGround N Smash 2\r\nBiohazard Factory\r\nSuperMarket 1 (R)\r\nYABBA DABBA DOO!\r\nMuseum 2\r\nVenice"
    vod: 'https://www.youtube.com/watch?v=jVfY4bo9wRQ'
    results: 'http://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-15_21-02-34.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

