---
title: 'Advanced Races'
titleoverride: true
date: '12-05-2020 18:00'
event:
    host: Delecto
    ip: dele.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World 2 (R) — 4 laps\r\nSantorini — 4 laps\r\nThe Felling Yard — 2 laps\r\nSpa-Volt 1 — 3 laps\r\nBiohazard Factory — 2 laps\r\nAMCO TT (R) — 3 laps\r\nJailhouse Rock (R) — 3 laps\r\nMuseum 1 — 3 laps\r\nWhite Rose Chapel — 3 laps\r\nPetroVolt — 2 laps\r\nHoliday Camp: California Edition — 2 laps\r\nMolten Caverns — 2 laps\r\nToys in the Hood 1 — 3 laps\r\nGhost Town 1 — 6 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

