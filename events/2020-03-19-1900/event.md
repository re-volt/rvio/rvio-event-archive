---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '19-03-2020 19:00'
event:
    host: 'Dvark & URV'
    ip: 'dv.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Molten Caverns (R) — 2 laps\r\nRooftops — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nToytanic 1 (R) — 3 laps\r\nThe Bunker — 4 laps\r\nSupermarket 2 — 8 laps\r\nSantorini (M) — 4 laps\r\nHelios — 3 laps\r\nDonut Plains 3 — 5 laps\r\nSupermarket 1 — 4 laps\r\nToy World Aquatica: Redux — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nQuake! (R) — 2 laps\r\nVenice — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

