---
title: 'Drift Races'
titleoverride: false
date: '07-01-2019 20:00'
event:
    host: Saffron
    ip: 'announced on Discord'
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://files.re-volt.io/packs/drift_cars.7z'
    tracklist: "AMCO Driftume\r\nSideways Sanctuary D\r\nFoxglen\r\nWarehouse Showdown\r\nStagnaro\r\nTVGP Bitume\r\nAMCO Bitume\r\nDrivers School\r\nNamco Racer\r\nNamco Trains\r\nAMCO Bitume R\r\nTouge Drift"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only **D1GP** or **D2GP** cars are allowed.

**Requirements**
* [Drift Cars](https://files.re-volt.io/packs/drift_cars.7z)
* [Drift Tracks](https://files.re-volt.io/packs/drift_tracks.7z)

First half will have 5 laps. Second half will have 4 laps.