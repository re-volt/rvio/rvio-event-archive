---
title: 'Super Pro Races'
titleoverride: false
date: '24-04-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "White Rose Chapel\r\nSnowy River\r\nRV Temple\r\nQuake! (R)\r\nVenice\r\nMoon Dawn\r\nToy World Aquatica: Redux (R)\r\nBiohazard Factory\r\nSpa-Volt 1\r\nMuseum 1 (R)\r\nToytanic 1\r\nMuseum 2\r\nIllusion\r\nPenny Racers - Harbour\r\nSantorini (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

