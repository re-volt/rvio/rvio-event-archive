---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '25-08-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "RV Temple — 2 laps\r\nToys in the Hood 1 — 3 laps\r\nQuake! — 2 laps\r\nRooftops 1 — 2 laps\r\nKadish Sprint — 2 laps\r\nRoute-77 — 3 laps\r\nToy World Mayhem — 3 laps\r\nSnowy River (R) — 3 laps\r\nCliffside — 5 laps\r\nGhost Town 2 — 4 laps\r\nJailhouse Rock — 2 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nGhost Town 1 (R) — 6 laps\r\nBotanical Garden — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

