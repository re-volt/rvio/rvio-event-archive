---
title: 'Online Platinum Cup'
titleoverride: true
date: '15-05-2020 18:00'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Online Platinum Cup'
    classlink: 'https://re-volt.io/events/2020-05-15-1800'
    tracklist: "Supermarket 1\r\nGhost Town 2\r\nToy World 1 RM\r\nMuseum 1 M\r\nToytanic 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Online Platinum Cup'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 4 cup standings and individual player progress, be sure to [check this](https://www.dropbox.com/s/qfzn2vwewz114d7/season%204%20cup%20session%20info.txt?dl=0).

### Requirements
* [Re-Volt Online car pack version 20.1](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (advised)
* Seperate game installation required if you got I/O content on the game you want to use!

### Car Selection
* Amw
* Gear Gt
* Night Drifter
* Panga
* Prophet
* Sir Gleam
* Vitesse