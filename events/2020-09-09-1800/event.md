---
title: 'Pro Cars Showcase'
titleoverride: true
date: '09-09-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Select Pro Cars'
    classlink: 'https://re-volt.io/events/2020-09-09-1800'
    tracklist: "Car-Track combination:\r\nPanga — Smashride Circuit — 2 laps\r\nSNW 35 — Ghost Town 2 — 4 laps\r\nEXE TC — Toy World Mayhem — 3 laps\r\nQuinx — Toy World 2 — 4 laps\r\nGust — Venice — 3 laps\r\nElectric Sheep — Toytanic 1 — 3 laps\r\nMini Champ — White Rose Chapel — 2 laps\r\nRVRC 20 — Toy world Aquatica: Redux — 3 laps\r\nEaglet — Metro-Volt — 3 laps\r\nVelter Ultron — Toy World 1 (R) — 4 laps\r\nKeyakizaka — Supermarket 1 — 3 laps\r\nS13 Alltune — Petrovolt — 2 laps\r\nAfter Image — Botanical Garden — 5 laps\r\nPrime Target — Toy World 2 — 4 laps\r\nAyrton SP — Holiday Camp: California Edition — 2 laps\r\nSir Gleam — Toy World Aquatica: Redux — 3 laps\r\nTT Raider — Toys in the Hood 2 — 4 laps\r\nBlack Widow — Jailhouse Rock — 3 laps\r\nSR—8000 — White Rose Chapel — 3 laps\r\nG3X — RV Temple — 2 laps\r\nVitesse — Moon Dawn — 2 laps\r\nArtair — Sakura — 2 laps\r\nShinobi — Hull Breach 3000 — 3 laps\r\nBanking — Toys in the Hood 2 — 3 laps\r\nIndy B — Swan Street — 3 laps\r\nPatriot — Smashride Circuit — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

_Ashen Forest's notes_:

Alright everyone, I am not the only one tired of the lack of variety in pro, but I'll be the guide to pretty much all the cars in the class. (With the exeption of Cougar and Humma, I know that those cars did get explored)

Note: the cars will be given to players via JOKER. If you want to meme, you can join the lobby with Toyeca, and that way you also confirm to me you read this.