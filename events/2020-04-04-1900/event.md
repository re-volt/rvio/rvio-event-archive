---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '04-04-2020 19:00'
event:
    host: 'Delecto & Narusori'
    ip: 'dele.rv.gl OR sas.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Toy World 1 (M) – 6 laps\r\nMoon Dawn  – 2 laps\r\nLunar – 3 laps\r\nGrisville – 3 laps\r\nVenice – 4 laps\r\nRooftops (R) – 4 laps\r\nSpa-Volt 1 (R) – 3 laps\r\nGhost Town 2 – 5 laps\r\nBiohazard Factory (R) – 2 laps\r\nToys in the Hood 1 – 4 laps\r\nJailhouse Rock – 3 laps\r\nThe Bunker – 4 laps\r\nDonut Plains 3 – 5 laps\r\nToy World Aquatica: Redux – 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

