---
title: 'Battle Tag'
titleoverride: false
date: '10-03-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: battle-tag
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World Battle\r\nMuseum Battle\r\nSupermarket Battle\r\nBlock Fort\r\nParty in the Toy World"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

