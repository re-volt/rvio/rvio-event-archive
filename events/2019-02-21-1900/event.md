---
title: 'Pro Races'
titleoverride: false
date: '21-02-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 2\r\nBotanical Garden\r\nToys in the Hood 1\r\nSuperMarket 2 (R)\r\nRanch\r\nQuake!\r\nVenice (R)\r\nHoliday Camp: California Edition\r\nMK64 Wario Stadium\r\nRadioactive Garden\r\nThe Bunker\r\nSantorini (R)\r\nAMCO TT\r\nCactus-Volt\r\nYABBA DABBA DOO!\r\nFool's Mate 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

