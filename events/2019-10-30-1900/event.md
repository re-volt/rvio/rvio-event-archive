---
title: 'Random Car Races'
titleoverride: true
date: '30-10-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Random Cars'
    classlink: 'https://re-volt.io/events/2019-10-28-1900'
    tracklist: "Jailhouse Rock (R)\r\nQuake! (M)\r\nToys in the Hood 2\r\nMolten Caverns\r\nToytanic 1 (R)\r\nRanch\r\nGhost Town 1\r\nIllusion\r\nVenice\r\nKadish Sprint\r\nAMCO TT (R)\r\nToy World Mayhem\r\nRooftops\r\nThe Bunker\r\nPenny Racers - Caves\r\nSwan Street"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Everyone will use the same car every race. The car will change randomly every race from the [main I/O car pool](https://re-volt.io/online/car-classes). Pick-ups will be enabled.