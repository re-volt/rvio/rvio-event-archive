---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '29-02-2020 19:00'
event:
    host: 'Delecto & URV'
    ip: 'dele.rv.gl OR u.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Molten Caverns (M) — 2 laps\r\nJailhouse Rock — 3 laps\r\nBotanical Garden (R) — 6 laps\r\nGhost Town 2 — 4 laps\r\nRoute-77 — 3 laps\r\nDonut Plains 3 — 4 laps\r\nSakura — 2 laps\r\nGhost Town 1 — 6 laps\r\nLunar — 3 laps\r\nPetroVolt (R) — 2 laps\r\nRooftops — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nCliffside — 6 laps\r\nPenny Racers - Harbour (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

