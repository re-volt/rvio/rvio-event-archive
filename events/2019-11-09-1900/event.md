---
title: 'Pro Races (Dual Lobby)'
titleoverride: true
date: '09-11-2019 19:00'
event:
    host: 'URV & Saffron'
    ip: 'u.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Illusion\r\nKadish Sprint\r\nGhost Town 2\r\nPenny Racers - Caves (R)\r\nMuseum 2 (R)\r\nSpa-Volt 2\r\nFiddlers on the Roof\r\nThe Felling Yard\r\nPetroVolt (R)\r\nSakura\r\nHull Breach 3000\r\nMoon Dawn (M)\r\nCliffside\r\nRooftops\r\nToys in the Hood 1\r\nRe-Ville"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event will be split into two different lobbies. The player limit will be set to 12 and will only be raised as needed if both lobbies reach the limit.

**Lobby #1 IP:** `u.rv.gl`<br>
**Lobby #2 IP:** `saff.rv.gl`