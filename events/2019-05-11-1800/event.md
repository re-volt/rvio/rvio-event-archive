---
title: 'Semi-Pro Races'
titleoverride: false
date: '11-05-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Cliffside\r\nSuperMarket 2\r\nGhost Town 1 (R)\r\nRanch\r\nVenice (R)\r\nToy World Mayhem\r\nRV Temple\r\nThe Felling Yard\r\nPenny Racers - Caves (M)\r\nSnowland 1 (R)\r\nGrisville\r\nYABBA DABBA DOO!\r\nMetro-Volt\r\nToy World 2\r\nRooftops"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

