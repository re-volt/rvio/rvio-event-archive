---
title: 'Pro Races'
titleoverride: false
date: '18-03-2019 21:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1 (R)\r\nSkating Toys\r\nSupermarket 2 \r\nPenny Racers - Caves (R)\r\nHoliday Camp: California Edition\r\nYABBA DABBA DOO!\r\nToytanic 1 \r\nFool's Mate 2\r\nJailhouse Rock\r\nSpa-Volt 2 (R)\r\nBiohazard Factory\r\nIndustry\r\nToy World 1\r\nQuake!\r\nSwan Street\r\nRV Temple"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

