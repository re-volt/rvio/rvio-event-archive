---
title: Slugs
titleoverride: false
date: '18-12-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 1\r\nGhost Town 1\r\nHelios \r\nAMCO TT\r\nSkating Toys (R)\r\nFool's Mate 2\r\nPalm Marsh\r\nVenice (R)\r\nRadioactive Garden\r\nQuake!\r\nGhost Town 2 (R)\r\nYABBA DABBA DOO!\r\nSakura\r\nBotanical Garden"
    vod: 'https://www.youtube.com/watch?v=JsXcCN4brS8'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-18_21-02-13.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

