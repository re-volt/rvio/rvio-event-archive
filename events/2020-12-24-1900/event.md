---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '24-12-2020 19:00'
event:
    host: 'Ashen Forest & Instant'
    ip: 'ash.rv.gl OR ins.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Rooftops — 3 laps\r\nToy World 1 (R) — 5 laps\r\nSchools Out! 1 — 4 laps\r\nMuseum 2 — 4 laps\r\nSantorini (R) — 4 laps\r\nMysterious Toy-Volt Factory 2 — 2 laps\r\nToys in the Hood 1 — 3 laps\r\nSchools Out! 2 — 2 laps\r\nThe Bunker — 4 laps\r\nToy World Mayhem (R) — 4 laps\r\nIndustry — 3 laps\r\nMoon Dawn — 2 laps\r\nToy World Aquatica: Redux — 3 laps\r\nHMS Invincible: Redux — 3 lap"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

