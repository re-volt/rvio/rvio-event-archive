---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '02-04-2020 19:00'
event:
    host: 'Kirioso & Saffron'
    ip: 'kiri.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    tracklist: "Toy World 2  – 5 laps\r\nRooftop Chase: Redux – 3 laps\r\nToytanic 1 – 3 laps\r\nHoliday Camp: California Edition – 2 laps\r\nBotanical Garden (R) – 6 laps\r\nBlood on the Rooftops – 3 laps\r\nSchool's Out – 2 laps\r\nSantorini – 4 laps\r\nToy World Mayhem (R) (M) – 4 laps\r\nSakura – 3 laps\r\nRanch – 2 laps \r\nMuseum 2 – 4 laps\r\nPenny Racers - Harbour (R) – 3 laps\r\nOverground – 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

