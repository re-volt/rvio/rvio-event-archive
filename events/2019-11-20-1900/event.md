---
title: 'Random Cars <small>(Dual Lobby)</small>'
titleoverride: true
date: '20-11-2019 19:00'
event:
    host: 'URV & Kirioso'
    ip: 'u.rv.gl OR kiri.rv.gl'
    category: special
    mode: race
    class: other
    otherclass: 'Random Cars'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Donut Plains 3\r\nOverground\r\nSkating Toys\r\nMuseum 1\r\nToy World Aquatica: Redux\r\nSakura (R)\r\nMuseum 2 (M)\r\nQuake!\r\nSpa-Volt 1\r\nToys in the Hood 2\r\nRV Temple\r\nSwan Street\r\nRooftop Chase: Redux (R)\r\nPenny Racers - Harbour\r\nBotanical Garden (R)\r\nHelios"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Everyone will use the same car every race. The car will change randomly every race from the [main I/O car pool](https://re-volt.io/online/car-classes) (including [Super Pros](https://re-volt.io/online/other-content). Pick-ups will be enabled.

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip)
* [Super Pros](https://distribute.re-volt.io/packs/io_superpros.zip)