---
title: Semi-Pros
titleoverride: false
date: '2018-09-04 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pros
    tracklist: "ToySoldierz\r\nGhost Town 2\r\nToy World Mayhem (R)\r\nRooftop Chase\r\nFool's Mate 2\r\nPalm Marsh\r\nSnowy River (R)\r\nJailhouse Rock\r\nSakura\r\nHoliday Camp: California Edition\r\nSuperMarket 1\r\nToytanic 2\r\nMuseum 1 (R)\r\nOverground\r\nSuperMarket 2"
    vod: 'https://www.youtube.com/watch?v=akgtuCexs5k'
    results: 'http://online.re-volt.io/sessions/results.php?file=casual/session_2018-09-04_21-03-06.csv'
    title: test
taxonomy:
    category:
        - events
visible: true
sidebarlayout: sidebar-left
sidebarpath: /sidebar/events
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

