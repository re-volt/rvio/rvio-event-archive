---
title: 'Halloween Carnival: Day 2'
titleoverride: false
date: '27-10-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Halloween Carnival: Day 2'
    classlink: 'https://re-volt.io/blog/halloween-carnival-2019'
    tracklist: "Halloween 2\r\nBone Island\r\nThe Catacombs\r\nThe Felling Yard EXTREME\r\nMolten Caverns\r\nSkeleton Rave\r\nMKDS Luigi's Mansion\r\nThe Monastery\r\nDead Souls\r\nSchizophrenia\r\nHotel Retro\r\nMysterious Toy-Volt Factory 2\r\nTenebrosity\r\nSpooky-Volt"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

