---
title: 'Gungame Races'
titleoverride: false
date: '29-06-2020 18:00'
event:
    host: Saffron
    ip: saff.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Gungame Cars'
    classlink: 'https://re-volt.io/online/cars'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Route-77\r\nJailhouse Rock\r\nSupermarket 2\r\nRV Temple\r\nSpa-Volt 2\r\nIndustry\r\nSpa-Volt 1\r\nMuseum 1\r\nOverground\r\nThe Felling Yard\r\nSnowy River\r\nPetroVolt\r\nAMCO TT\r\nMoon Dawn\r\nLunar\r\nGhost Town 1\r\nSakura\r\nToys in the Hood 2\r\nHull Breach 3000\r\nToytanic 2\r\nSantorini\r\nRooftop Chase: Redux\r\nMedieval: Redux\r\nThe Bunker\r\nGhost Town 2\r\nGrisville\r\nSwan Street\r\nBotanical Garden\r\nSnowland 1\r\nBiohazard Factory\r\nToy World 2\r\nHoliday Camp: California Edition\r\nToy World Mayhem\r\nImages of Giza: Redux\r\nToy World 1\r\nWhite Rose Chapel\r\nToy World Aquatica: Redux\r\nSupermarket 1\r\nSmashride Circuit\r\nQuake!\r\nRooftops\r\nBlood on the Rooftops\r\nToys in the Hood 1\r\nRooftops 1\r\nRadioactive Garden\r\nWildland\r\nMolten Caverns\r\nMuseum 2\r\nVenice\r\nFool's Mate 2\r\nMetro-Volt\r\nToySoldierz\r\nMuseum 3\r\nKadish Sprint\r\nMeltdown"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirements:
* [I/O Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [I/O Car Collection](https://distribute.re-volt.io/packs/io_cars.zip)

Everyone starts off by choosing the first car in the list, **Cambold R.** Whoever gets a podium must switch to the next car in the list below. The session ends whenever someone finishes on the podium with the last car on the list, **Phat Slug.** If multiple people finish on the podium with **Phat Slug** in a race, the player with the higher finishing position is the winner.

* Cambold R
* La Rossa
* Quinx
* Shinobi
* Yuurei V8
* RC Vector
* R6 Turbo
* Donnie TC
* Vixen
* Reddlum
* Vaanbus
* Phat Slug

Races will be a minute long, give-or-take. Tracklist will be played twice (if needed).