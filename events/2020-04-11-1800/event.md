---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '11-04-2020 18:00'
event:
    host: 'OhNej & Kirioso'
    ip: 'ohnej.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Toy World 1 (M) — 6 laps\r\nMoon Dawn — 2 laps\r\nLunar — 3 laps\r\nGrisville — 3 laps\r\nVenice — 3 laps\r\nRooftops (R) — 3 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nGhost Town 2 — 5 laps\r\nBiohazard Factory (R) — 2 laps\r\nToys in the Hood 1 — 3 laps\r\nJailhouse Rock — 3 laps\r\nThe Bunker — 4 laps\r\nDonut Plains 3 — 5 laps\r\nToy World Aquatica: Redux — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

