---
title: 'Good Legos'
titleoverride: true
date: '27-05-2020 14:30'
event:
    host: '607'
    ip: 607.rv.gl
    category: unofficial
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    tracklist: "Seaside Park\r\nBedlam\r\nMonde Glace 1\r\nDeepForest\r\nInside a Cyborg\r\nCastle Keepers\r\nSeaport Haven\r\nPhotonX\r\nRocket Town\r\nAir Garden\r\nToxic Refinery\r\nAdrenaline\r\nButterfly Cove\r\nWater Tack\r\nCrystal Mines"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Good Legos'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

### Requirements
* [607's Lego Track Pack](http://folk.simpsite.nl/lego-track-pack)