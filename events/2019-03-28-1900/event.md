---
title: 'Pro Races'
titleoverride: false
date: '28-03-2019 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Cliffside\r\nToytanic 2 \r\nSnowland 1\r\nHoliday Camp: California Edition (R)\r\nToy World 2\r\nMolten Caverns (R)\r\nSakura\r\nKadish Sprint\r\nRooftops (R)\r\nToys in the Hood 1\r\nSantorini\r\nLunar\r\nRV Temple\r\nRooftop Chase: Redux\r\nQuake!\r\nToy World Aquatica: Redux"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

