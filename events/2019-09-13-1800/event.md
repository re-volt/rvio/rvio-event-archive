---
title: 'Circuit Races (Elimination)'
titleoverride: false
date: '13-09-2019 18:00'
event:
    host: Saffron
    ip: 'TBA on Discord'
    category: competitive
    mode: race
    class: other
    otherclass: 'Real Cars'
    classlink: 'https://files.re-volt.io/packs/rlcars.zip'
    tracklist: "Touge Mountain\r\nSpecial Stage Route 5\r\nAMCO Bitume\r\nDaybreak Raceway\r\nBerm Lake Raceway\r\nAMCO Driftume\r\nLS Tama Valley Route B\r\nEmerald Pastures Raceway\r\nLS Biano 8\r\nSideways Sanctuary B\r\nTVGP Bitume\r\nGrand Prix de Morvan\r\nRV-Simo On-Road Track\r\nFoxglen\r\nStagnaro"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

