---
title: 'Semi-Pro Races'
titleoverride: false
date: '17-09-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Jailhouse Rock\r\nMuseum 2\r\nMoon Dawn\r\nBlood on the Rooftops\r\nSnowy River (R)\r\nHull Breach 3000\r\nToySoldierz\r\nToytanic 2 (R)\r\nFiddlers on the Roof\r\nMuseum 1\r\nVenice (R)\r\nSantorini\r\nQuake!\r\nPetroVolt (M)\r\nToys in the Hood 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

