---
title: 'Silver Cup'
titleoverride: false
date: '2018-09-08 19:30'
event:
    host: Whitedoom
    category: unofficial
    mode: race
    class: other
    otherclass: 'Silver Cup'
    classlink: 'https://www.dropbox.com/s/ab9t5cc7wkwm8je/Online%20cup%20data.txt?dl=0'
    tracklist: "Toy World 1\r\nToys in the Hood 1 (M)\r\nGhost Town 1\r\nToy World 2"
    results: 'https://i.imgur.com/49yK7LW.png'
sidebarlayout: global
sidebarpath: /sidebar/events
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

The IP will be shared on [Discord](https://re-volt.io/discord).<br>
The races will be held in **Arcade** mode.

**Requirements:**
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip)

**Car Selection:**
* [APC L-13](http://revoltzone.net/cars/17996/APC%20L-13)
* [High-Tech](http://revoltzone.net/cars/31040/High-Tech)
* [Groovster](http://revolt.wikia.com/wiki/Groovster)
* [Matra XL](http://revolt.wikia.com/wiki/Matra_XL)
* [RC Action](http://revoltzone.net/cars/10871/RC%20Action)
* [Senketsu GT](http://revoltzone.net/cars/15257/Senketsu%20(Pack))
* [Stonemason](http://revoltzone.net/cars/23332/Stonemason)

[More information here](https://www.dropbox.com/s/ab9t5cc7wkwm8je/Online%20cup%20data.txt?dl=0).