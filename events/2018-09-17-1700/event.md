---
title: 'Can-Am Mini Grand Prix 2'
titleoverride: false
date: '2018-09-17 17:00'
event:
    host: FZG
    category: unofficial
    mode: race
    class: other
    otherclass: 'Meseeks GT'
    classlink: 'https://re-volt.io/events/2018-09-17-1700'
    tracklist: "Venice (R)\r\nVenice Sonata (R)"
    vod: 'https://www.youtube.com/watch?v=57773tUz4Mk'
    results: 'http://online.re-volt.io/sessions/results.php?file=session_2018-09-18_00-03-31.csv'
media_order: poster8b.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](poster8b.png)

A competitive session without pickups on Arcade.<br>
8 liveries, 12 laps, 2 tracks, Meseeks GT **ONLY**.

It is recommended for each racer to pick a different livery based on the lobby room list in chronological order:
* whitebluered
* green
* blorange
* aqua
* burgundy
* white blue
* black gold
* orange

**Requirements:**
* [Meseeks GT](https://www.dropbox.com/s/p3w95e6lunov1o5/meseeksgt.zip?dl=0)
* [Venice (I/O pack)](https://distribute.re-volt.io/packs/io_tracks.zip)
* [Venice Sonata](http://revoltzone.net/tracks/4136/Venice%20Sonata)