---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '29-08-2020 18:00'
event:
    host: 'Kirioso & Etneus'
    ip: 'kiri.rv.gl OR etn.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Santorini — 4 laps\r\nSnowland 1 (R) — 4 laps\r\nRooftops (R) — 3 laps\r\nHull Breach 3000 — 4 laps\r\nSwan Street — 3 laps\r\nPetroVolt — 2 laps\r\nMuseum 2 — 4 laps\r\nMolten Caverns — 2 laps\r\nIndustry — 3 laps\r\nLunar — 3 laps\r\nVenice (R) — 3 laps\r\nSmashride Circuit — 3 laps\r\nSupermarket 1 — 4 laps\r\nToytanic 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

