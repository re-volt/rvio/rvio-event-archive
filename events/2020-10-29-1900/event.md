---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '29-10-2020 19:00'
event:
    host: 'OhNej & Ashen Forest'
    ip: 'ohnej.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Ghost Town 1 — 6 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nRooftops (R) — 3 laps\r\nMuseum 2 — 4 laps\r\nToy World 1 — 6 laps\r\nHull Breach 3000 — 3 laps\r\nFools Mate 2 — 4 laps\r\nSkating Toys: Redux (R) — 2 laps\r\nRoute-77 — 3 laps\r\nJailhouse Rock — 3 laps\r\nImages of Giza: Redux — 5 laps\r\nMuseum 3 — 3 laps\r\nLibrary — 3 laps\r\nSpa-Volt 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

