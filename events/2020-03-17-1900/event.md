---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '17-03-2020 19:00'
event:
    host: 'Saffron & Dvark'
    ip: 'saff.rv.gl OR dv.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Rooftop Chase: Redux — 3 laps\r\nToytanic 2 — 3 laps\r\nMuseum 2 — 3 laps\r\nIndustry — 2 laps\r\nJailhouse Rock — 2 laps\r\nSchool's Out — 2 laps\r\nMuseum 1 (R) — 2 laps\r\nRoute-77 — 3 laps\r\nSpa-Volt 2 (R) — 3 laps\r\nRadioactive Garden — 5 laps\r\nPenny Racers - Harbour (R) — 3 laps\r\nBotanical Garden — 5 laps\r\nHull Breach 3000 (M) — 3 laps\r\nFool's Mate 2 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

