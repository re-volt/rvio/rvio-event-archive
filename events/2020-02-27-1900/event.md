---
title: 'Rookie Races'
titleoverride: true
date: '27-02-2020 19:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Museum 2 — 3 laps\r\nToys in the Hood 2 — 3 laps\r\nToytanic 1 — 3 laps\r\nSchool's Out (M) — 2 laps\r\nSkating Toys — 2 laps\r\nSpa-Volt 1 — 3 laps\r\nHull Breach 3000 — 3 laps\r\nVenice (R) — 3 laps\r\nGrisville — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nMetro-Volt — 3 laps\r\nToys in the Hood 1 (R) — 3 laps\r\nBiohazard Factory (R) — 2 laps\r\nToySoldierz — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

