---
title: 'Amateur Races'
titleoverride: false
date: '16-03-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Helios\r\nSwan Street (R)\r\nRooftop Chase\r\nSuperMarket 1\r\nVenice\r\nToy World Mayhem\r\nMuseum 1 (R)\r\nSpa-Volt 1 (R)\r\nMK64 Wario Stadium\r\nDonut Plains 3\r\nGhost Town 2\r\nToySoldierz\r\nPenny Racers - Harbour\r\nToy World 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

