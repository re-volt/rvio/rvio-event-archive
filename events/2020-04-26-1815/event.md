---
title: 'Online Bronze Cup'
titleoverride: true
date: '26-04-2020 18:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Online Bronze Cup'
    classlink: 'https://re-volt.io/events/2020-04-26-1815'
    tracklist: "Toys in the Hood 1\r\nSupermarket 2\r\nMuseum 2\r\nBotanical Garden"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Online Bronze Cup'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 4 cup standings and individual player progress, be sure to [check this](https://www.dropbox.com/s/qfzn2vwewz114d7/season%204%20cup%20session%20info.txt?dl=0).

### Requirements
* [RVON community car pack](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (advised)


### Car Selection
* Abracadabra
* Bigvolt
* Dust Mite
* Lr 64
* Road Pirate
* Rouge
* SchoolVolt