---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '28-07-2020 18:00'
event:
    host: 'Ashen Forest & Frosttbitten'
    ip: 'ash.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Jailhouse Rock — 3 laps\r\n    Cliffside — 6 laps\r\n    Ghost Town 2 — 5 laps\r\n    Rooftops — 4 laps\r\n    Rooftops 1 — 3 laps\r\n    Sakura — 3 laps\r\n    Industry — 3 laps\r\n    Toy World Mayhem (R) — 4 laps\r\n    Kadish Sprint — 2 laps\r\n    Toy World 2 — 5 laps\r\n    Spa-Volt 1 — 3 laps\r\n    Smashride Circuit (R) — 3 laps\r\n    Supermarket 2 (R) — 9 laps\r\n    Fools Mate 2 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

