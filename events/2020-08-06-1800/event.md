---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '06-08-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Spa-Volt 1 (R) — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nBiohazard Factory — 2 laps\r\nSakura — 2 laps\r\nSantorini — 4 laps\r\nKadish Sprint — 2 laps\r\nMedieval: Redux (R) — 4 laps\r\nToy World 2 — 4 laps\r\nMuseum 2 — 3 laps\r\nSpa-Volt 2 — 3 laps\r\nGrisville — 3 laps\r\nBotanical Garden (R) — 5 laps\r\nHoliday Camp: California Edition — 2 laps\r\nSnowy River — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

