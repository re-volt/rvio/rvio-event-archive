---
title: 'Pro Slugs'
titleoverride: false
date: '11-10-2018 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: prugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "PetroVolt\r\nMetro-Volt\r\nAMCO TT (R)\r\nMuseum 1\r\nBiohazard Factory\r\nSkating Toys\r\nToys in the Hood 1 (R)\r\nToySoldierz\r\nHoliday Camp: California Edition (R)\r\nRe-Ville\r\nToytanic 2\r\nSakura\r\nToy World 1\r\nSuperMarket 2\r\nBlood on the Rooftops"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

