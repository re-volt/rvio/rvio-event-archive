---
title: 'Super Pro Races'
titleoverride: true
date: '08-10-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World Aquatica: Redux — 4 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nSupermarket 1 — 4 laps\r\nRadioactive Garden (R) — 5 laps\r\nToytanic 1 (R) — 3 laps\r\nMetro-Volt — 3 laps\r\nToy World 1 — 6 laps\r\nRooftops 1 — 3 laps\r\nThe Bunker — 4 laps\r\nGhost Town 1 — 7 laps\r\nVenice — 4 laps\r\nBiohazard Factory — 3 laps\r\nAMCO TT — 3 laps\r\nToy World Mayhem — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

