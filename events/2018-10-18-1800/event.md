---
title: Rookies
titleoverride: false
date: '18-10-2018 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: rookies
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toys in the Hood 2 (R)\r\nMK64 Wario Stadium\r\nMetro-Volt\r\nOverground\r\nSnowy River\r\nBotanical Garden\r\nLunar\r\nPetroVolt (R)\r\nQuake!\r\nGhost Town 2\r\nAMCO TT\r\nSantorini\r\nSuperMarket 1\r\nBiohazard Factory (R)"
    vod: 'https://www.youtube.com/watch?v=CDQkej0bOpc'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-18_20-02-57.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

