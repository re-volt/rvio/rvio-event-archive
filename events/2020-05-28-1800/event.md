---
title: 'Semi-Pro Races'
titleoverride: true
date: '28-05-2020 18:00'
event:
    host: 'URV & Etneus'
    ip: 'u.rv.gl OR etn.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "White Rose Chapel — 3 laps\r\nSantorini (R) — 4 laps\r\nSpa-Volt 2 — 3 laps\r\nToy World 2 — 5 laps\r\nBotanical Garden — 6 laps\r\nToy World Aquatica: Redux — 3 laps\r\nGhost Town 2 (R) — 4 laps\r\nToy World Mayhem — 4 laps\r\nSupermarket 1 — 4 laps\r\nMoon Dawn — 2 laps\r\nIndustry — 3 laps\r\nThe Bunker — 4 laps\r\nHull Breach 3000 — 4 laps\r\nSakura (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

