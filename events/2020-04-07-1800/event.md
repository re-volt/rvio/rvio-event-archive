---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '07-04-2020 18:00'
event:
    host: 'Delecto & Narusori'
    ip: 'dele.rv.gl OR sas.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Spa-Volt 2 — 3 laps\r\nRadioactive Garden — 4 laps\r\nToySoldierz — 3  laps\r\nSupermarket 2 (R) — 7 laps\r\nFiddlers on the Roof: Redux — 2 laps\r\nHull Breach 3000 — 3 laps\r\nSupermarket 1 — 4 laps\r\nToys in the Hood 2 — 2 laps\r\nMuseum 1 — 2 laps\r\nThe Felling Yard — 2 laps\r\nPetroVolt (R) — 2 laps\r\nRoute-77 (M) — 3 laps\r\nAMCO TT (R) — 3 laps\r\nQuake! — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

