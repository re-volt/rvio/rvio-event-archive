---
title: Semi-Pros
titleoverride: false
date: '2018-09-05 18:00'
event:
    host: Whitedoom
    category: unofficial
    mode: race
sidebarlayout: global
sidebarpath: /sidebar/events
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

The IP will be shared on [Discord](https://re-volt.io/discord).

**Requirements:**
* Car Collection
* Bonus Cars
* Track Collection
* Bonus Tracks
* Month Tracks

[More information here](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).