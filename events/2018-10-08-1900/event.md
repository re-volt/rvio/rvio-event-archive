---
title: '3 Lap Sprint'
titleoverride: false
date: '08-10-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: '3 Lap Sprint'
    classlink: 'https://docs.google.com/spreadsheets/d/1Xp6jlQkjyQ7GHTGBvKkgiLI24pP3JAKa4pLYwTONToc/edit#gid=549172680'
    tracklist: "AMCO TT\r\nHelios\r\nJailhouse Rock\r\nMetro-Volt\r\nRooftops\r\nSakura\r\nToy World Mayhem\r\nVenice"
    results: 'https://online.re-volt.io/sessions/results.php?file=competitive/session_2018-10-08_22-03-59.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only stock cars are allowed, excluding Purp XL and SNW 35. The I/O [Track Pack](https://distribute.re-volt.io/packs/io_tracks.zip) is required.