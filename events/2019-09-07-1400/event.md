---
title: eggdogs
titleoverride: false
date: '07-09-2019 14:00'
event:
    host: '607'
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: eggdogs
    classlink: 'http://revoltzone.net/cars/43062/eggdog'
    tracklist: 'Random Tracks from 607''s Regular Track Pack'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Simulation, 3 laps.

### Requirements:
* [eggdog](http://revoltzone.net/cars/43062/eggdog)
* [607's Regular Track Pack](http://www.folk.simpsite.nl/regular-track-pack)