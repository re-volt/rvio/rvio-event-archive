---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '19-12-2020 19:00'
event:
    host: 'Ashen Forest & Mighty'
    ip: 'ash.rv.gl OR mighty.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toys in the Hood 1 (R) — 4 laps\r\nIndustry — 3 laps\r\nBiohazard Factory (R) — 2 laps\r\nHoliday Camp: California Edition — 2 laps\r\nBotanical Garden EX — 3 laps\r\nRooftops 1 — 3 laps\r\nSchools Out! 2 — 2 laps\r\nToySoldierz — 4 laps\r\nJailhouse Rock — 3 laps\r\nToy World Aquatica: Redux (R) — 4 laps\r\nToy World 1 — 6 laps\r\nToy World 2 — 5 laps\r\nMolten Caverns — 2 laps\r\nToytanic 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

