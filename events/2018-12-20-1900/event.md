---
title: Rookies
titleoverride: false
date: '20-12-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: rookies
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 1\r\nJailhouse Rock\r\nToys in the Hood 2\r\nPenny Racers - Harbour\r\nIllusion\r\nMuseum 2 (R)\r\nToy World Mayhem (R)\r\nIndustry\r\nThe Bunker\r\nPetroVolt (R)\r\nToySoldierz\r\nMetro-Volt\r\nSnowy River\r\nSuperMarket 2"
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-20_20-04-07.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

