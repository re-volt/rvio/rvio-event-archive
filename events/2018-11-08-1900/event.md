---
title: Slugs
titleoverride: false
date: '08-11-2018 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Botanical Garden (R)\r\nSpa-Volt 1 (R)\r\nSuperMarket 1\r\nMetro-Volt\r\nLunar\r\nToy World Mayhem\r\nIndustry\r\nThe Bunker\r\nMuseum 2\r\nSantorini\r\nPenny Racers - Caves (R)\r\nGhost Town 2\r\nThe Gorge\r\nToySoldierz"
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-08_20-02-25.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

