---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '17-11-2020 19:00'
event:
    host: 'Kirioso & Frostbitten'
    ip: 'kiri.rv.gl OR frost.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Supermarket 1 — 4 laps\r\nJailhouse Rock (R) — 2 laps\r\nImages of Giza: Redux (R) — 4 laps\r\nHoliday Camp: California Edition — 2 laps\r\nVenice — 3 laps\r\nGhost Town 1 — 5 laps\r\nCliffside — 5 laps\r\nRV Temple — 2 laps\r\nOverground — 2 laps\r\nPetroVolt — 2 laps\r\nRooftops (R) — 3 laps\r\nMeltdown — 4 laps\r\nFools Mate 2 — 3 laps\r\nMuseum 1 — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

