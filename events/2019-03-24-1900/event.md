---
title: 'Battle Tag'
titleoverride: false
date: '24-03-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: battle-tag
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World Battle\r\nParty in the Toy World\r\nMuseum Battle\r\nGarden Battle\r\nSupermarket Battle\r\nNeighborhood Battle\r\nBlock Fort"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

