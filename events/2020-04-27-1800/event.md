---
title: 'Underdog Drifters'
titleoverride: false
date: '27-04-2020 18:00'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://re-volt.io/events/2020-04-27-1800'
    packages:
        - io_tracks_bonus
    tracklist: "1. AMCO Driftume\r\n2. LS Tama Valley Route C\r\n3. Grand Prix de Morvan\r\n4. TVGP Bitume\r\n5. Foxglen\r\n6. RV-Simo On-Road Track\r\n7. AMCO Bitume\r\n8. Sideways Sanctuary C"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

i will be hosting an Underdog Drifters session on Monday, 27 April at 18 UTC.
we will be using drift cars from RVZ, that is not in current I/O packs.
we will be racing on "easy to drift" I/O Circuit Tracks, for 3 laps in Arcade physics mode with no pickups.

***
car list, order from easiest at top, to hardest at bottom
- Easy:    Drift Harvester
- Easy:    Night Rider Drift      (from 'oldcars' folder, taken from newest Night Rider from RVZ)
- Easy:    Drift_Volken E36
- Med:    Monster Drift
- Med:    Drift D15
- Med:    Cy Drift
- Hard:    Most Wanted Drift
- Hard:    Netheria Drifter

***

Content Download: [https://www.dropbox.com/s/8inc12zh0kamvdp/underdogdrift.zip?dl=1](https://www.dropbox.com/s/8inc12zh0kamvdp/underdogdrift.zip?dl=1)

I/O Circuit Tracks  : [https://distribute.re-volt.io/packs/io_tracks_circuit.zip](https://distribute.re-volt.io/packs/io_tracks_circuit.zip)