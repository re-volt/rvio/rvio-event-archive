---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '02-07-2020 18:00'
event:
    host: 'Kirioso & OhNej'
    ip: 'kiri.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    The Bunker — 4 laps\r\n    Quake! (R) — 2 laps\r\n    Medieval: Redux — 5 laps\r\n    AMCO TT (R) — 3 laps\r\n    Museum 2 — 4 laps\r\n    Supermarket 1 (R) — 4 laps\r\n    PetroVolt — 2 laps\r\n    Biohazard Factory — 2 laps\r\n    Hull Breach 3000 — 4 laps\r\n    Toy World 2 — 5 laps\r\n    Botanical Garden — 6 laps\r\n    RV Temple — 2 laps\r\n    Venice — 3 laps\r\n    Blood on the Rooftops — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

