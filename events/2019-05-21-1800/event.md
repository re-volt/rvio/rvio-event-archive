---
title: 'Semi-Pro Races'
titleoverride: false
date: '21-05-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Industry\r\nKadish Sprint\r\nToy World 1\r\nToy World Aquatica: Redux\r\nMuseum 2\r\nSantorini\r\nHelios\r\nMetro-Volt\r\nBlood on the Rooftops\r\nBotanical Garden (R)\r\nVenice (R)\r\nMolten Caverns (R)\r\nThe Felling Yard\r\nQuake\r\nSwan Street\r\nToys in the Hood 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

