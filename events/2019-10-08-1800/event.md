---
title: 'Advanced Races'
titleoverride: false
date: '08-10-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Spa-Volt 1\r\nToy World Aquatica: Redux\r\nFiddlers on the Roof\r\nGhost Town 1\r\nFool's Mate 2\r\nSpa-Volt 2 (M)\r\nMetro-Volt\r\nRooftops (R)\r\nQuake! (R)\r\nIllusion (R)\r\nToytanic 1\r\nToySoldierz\r\nPenny Racers - Caves\r\nGhost Town 2\r\nYABBA DABBA DOO!"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

