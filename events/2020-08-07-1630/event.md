---
title: 'Semi-Pro Races'
titleoverride: true
date: '07-08-2020 16:30'
event:
    host: Frosttbitten
    ip: frost.rv.gl
    category: competitive
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "PetroVolt — 2 laps\r\nToy World 2 — 4 laps\r\nSpa—Volt 2 (R) — 3 laps\r\nSnowland 1 — 3 laps\r\nRooftops (R) — 3 laps\r\nIndustry — 2 laps\r\nQuake! — 2 laps\r\nMoon Dawn — 2 laps\r\nMuseum 1 — 3 laps\r\nHelios — 3 laps\r\nImages of Giza: Redux (R) — 4 laps\r\nThe Bunker — 3 laps\r\nToytanic 1 — 3 laps\r\nRooftop Chase: Redux — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

