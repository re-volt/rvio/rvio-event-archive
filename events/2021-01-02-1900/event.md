---
title: 'Pro Races'
titleoverride: true
date: '02-01-2021 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Toys in the Hood 1 (R) — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nWildland — 3 laps\r\nGhost Town 2 — 5 laps\r\nMeltdown — 4 laps\r\nCliffside — 6 laps\r\nFools Mate 2 — 4 laps\r\nGrisville — 3 laps\r\nSakura — 3 laps\r\nGhost Town 1 — 6 laps\r\nMuseum 1 — 3 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nLunar — 3 laps\r\nSmashride Circuit (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

