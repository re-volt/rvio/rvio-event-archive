---
title: 'Super Pro Races'
titleoverride: false
date: '19-04-2020 17:00'
event:
    host: Delecto
    ip: dele.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Rooftop Chase: Redux — 3 Laps\r\nSupermarket 1 (R) — 4 laps\r\nRooftops — 4 laps\r\nHolidays Camp: California Edition — 2 laps\r\nSchool's Out — 2 laps\r\nToys in the Hood 2 — 4 laps\r\nGrisville — 3 laps\r\nQuake! (R) — 2 laps\r\nHelios — 3 laps\r\nPetroVolt (R) — 2 laps\r\nSwan Street — 3 laps\r\nVenice — 4 laps\r\nToytanic 1 — 3 laps\r\nSakura — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

