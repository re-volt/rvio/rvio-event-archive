---
title: 'Pro Races'
titleoverride: false
date: '23-07-2019 18:00'
event:
    host: Narusori
    ip: flo.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "1. Ranch (R)\r\n2. Toy World Aquatica: Redux \r\n3. Toytanic 2 (M)\r\n4. The Felling Yard\r\n5. Metro-Volt\r\n6. Ghost Town 1\r\n7. Grisville\r\n8. PetroVolt\r\n9. Molten Caverns (R)\r\n10. Fiddlers on the Roof\r\n11. Toys in the Hood 2\r\n12. Industry\r\n13. Spa-Volt 1\r\n14. SuperMarket 1 (R)\r\n15. Biohazard Factory\r\n16. White Rose Chapel"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

