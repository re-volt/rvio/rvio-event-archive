---
title: 'Rookie Races'
titleoverride: true
date: '26-06-2020 16:30'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toy World 1 (R) — 4 laps\r\nMetro-Volt — 3 laps\r\nToy World Aquatica: Redux (R) — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nSwan Street — 3 laps\r\nRooftop Chace: Redux — 3 laps\r\nVenice (R) — 3 laps\r\nHelios — 3 laps\r\nSmashride Circuit — 3 laps\r\nMeltdown — 4 laps\r\nHoliday Camp: California Edition — 2 laps\r\nGrisville — 3 laps\r\nGhost Town 1 — 5 laps\r\nSupermarket 1 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

