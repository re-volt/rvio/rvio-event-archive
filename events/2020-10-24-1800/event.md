---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '24-10-2020 18:00'
event:
    host: 'Kirioso & Ashen Forest'
    ip: 'kiri.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Botanical Garden EX — 3 laps\r\nToy World 1 (R) — 6 laps\r\nHoliday Camp: California Edition — 2 laps\r\nSpa-Volt 2 (R) — 4 laps\r\nToytanic 2 — 3 laps\r\nSantorini — 4 laps\r\nSakura — 3 laps\r\nWildland (R) — 3 laps\r\nGrisville — 3 laps\r\nRooftops — 4 laps\r\nLunar — 3 laps\r\nMuseum 3 — 4 laps\r\nThe Bunker — 4 laps\r\nSupermarket 2 — 9 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

