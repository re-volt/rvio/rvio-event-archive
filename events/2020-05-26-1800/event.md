---
title: 'Rookie Races'
titleoverride: true
date: '26-05-2020 18:00'
event:
    host: Delecto
    ip: dele.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toys in the Hood 1 — 3 laps\r\nFools Mate 2 — 3 laps\r\nMeltdown — 4 laps\r\nVenice — 3 laps\r\nJailhouse Rock — 2 laps\r\nHoliday Camp: California Edition — 2 laps\r\nMuseum 2 (R) — 3 laps\r\nQuake! (R) — 2 laps\r\nKadish Sprint — 2 laps\r\nToy World 1 — 5 laps\r\nSupermarket 2 — 7 laps\r\nOverground — 2 laps\r\nSwan Street (R) — 3 laps\r\nBlood on the Rooftops — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

