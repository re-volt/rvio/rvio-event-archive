---
title: 'Drift Tourney (D1GP)'
titleoverride: true
date: '24-05-2020 16:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'D1GP Cars'
    classlink: 'https://re-volt.io/online/cars/drift'
    tracklist: "Touge Mountain\r\nAMCO Bitume (R) — 2 laps\r\nKeitune Speedway (R) — 4 laps\r\nAutumn Ring — 3 laps\r\nBerm Lake Circuit — 3 laps\r\nTVGP Bitume — 5 laps\r\nEggland — 6 laps\r\nFoxglen — 5 laps\r\nStagnaro — 5 laps\r\nRV-Simo On-Road Track — 4 laps\r\nWarehouse Showdown — 6 laps\r\nGrand Prix de Morvan — 7 laps\r\nTouge Drift (R) — 2 laps\r\nTouge Mountain (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Drift Tourney (D1GP)'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

<img src="https://re-volt.io/user/pages/events/2020-05-23-1600/logo.png">

Welcome to our first drift tourney! This is a small event that combines the results of a D2GP and a D1GP racing session over a single weekend. The difference between two categories are the pace, with D2GP being the slower one.

### Information:
* There are **no sign-ups required**, and anyone's free to join or leave at any time!
* For the final results, **some players may be teamed up together**. A player who only participated in the D2GP session can be paired up with a player who only participated in the D1GP session. To balance it out, the highest scoring player will be paired with the lowest scoring player, the 2nd highest with the 2nd lowest and so on.
* Players who joined the first session **will have their place reserved** for the D1GP session, as it is preferable to have players participate in both matches. You need to be on our [Discord server](https://discord.gg/NMT4Xdb) for this, so the IP may be given to you conveniently (alternatively, contact `URV#9830`).
* **Pick-ups will be disabled**, and we're sticking with Arcade mode. This is less about the usual Re-Volt chaos and more about connecting series of drifts, which would be otherwise obstructed by weapons or Simulation mode.
* It's going to get messy if we end up getting a full lobby, so try your best to **avoid crashing into others**. Make way for players who are lapping you if you are less experienced. In the end, even if this is a tourney, it's important to enjoy the drifting!

### Requirements
* [Drift Car Pack](https://files.re-volt.io/packs/drift_cars.7z)
* [Circuit Track Pack](https://distribute.re-volt.io/packs/io_tracks_circuit.zip)