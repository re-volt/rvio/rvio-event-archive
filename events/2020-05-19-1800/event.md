---
title: 'Amateur Races'
titleoverride: true
date: '19-05-2020 18:00'
event:
    host: Delecto
    ip: dele.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Grisville — 3 laps\r\nSupermarket 1 — 4 laps\r\nWhite Rose Chapel — 2 laps\r\nQuake! — 2 laps\r\nToytanic 2 — 3 laps\r\nSmashride Circuit (R) — 3 laps\r\nRooftops (R) — 3 laps\r\nThe Bunker — 4 laps\r\nGhost Town 2 — 4 laps\r\nKadish Sprint — 2 laps\r\nJailhouse Rock (R) — 2 laps\r\nRV Temple — 2 laps\r\nHull Breach 3000 — 3 laps\r\nToy World Mayhem — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

