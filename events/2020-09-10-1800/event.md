---
title: 'Amateur Races'
titleoverride: true
date: '10-09-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Ghost Town 1 — 6 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nMetro-Volt — 3 laps\r\nSpa-Volt 2 — 3 laps\r\nBiohazard Factory (R) — 2 laps\r\nLunar — 3 laps\r\nToys in the Hood 2 — 3 laps\r\nMolten Caverns — 2 laps\r\nMuseum 2 (R) — 3 laps\r\nWildland — 3 laps\r\nToy World 2 — 4 laps\r\nMedieval: Redux — 4 laps\r\nToySoldierz — 3 laps\r\nThe Bunker — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

