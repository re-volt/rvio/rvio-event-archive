---
title: 'Drift Races'
titleoverride: false
date: '05-06-2019 18:00'
event:
    host: Saffron/Floxit
    ip: flo.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://files.re-volt.io/packs/drift_cars.7z'
    tracklist: "LS Kart Space I\r\nNamco Racer\r\nSideways Sanctuary B\r\nTouge Drift\r\nAMCO Bitume\r\nSideways Sanctuary C\r\nDrivers School\r\nMelville\r\nStagnaro\r\nSideways Sanctuary A\r\nRV-Simo On-Road Track\r\nTVGP Bitume"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

