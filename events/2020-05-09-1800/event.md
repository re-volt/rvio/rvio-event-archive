---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '09-05-2020 18:00'
event:
    host: 'Delecto & Narusori'
    ip: 'dele.rv.gl OR naru.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Kadish Sprint — 2 laps\r\nToys in the Hood 2 — 4 laps\r\nSantorini — 4 laps\r\nRadioactive Garden (R) — 5 laps\r\nFools Mate 2 — 4 laps\r\nToy World Mayhem — 4 laps\r\nMuseum 1 — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nMolten Caverns — 2 laps\r\nImages of Giza: Redux — 5 laps\r\nSupermarket 1 — 4 laps\r\nToytanic 1 (R) — 3 laps\r\nSpa-Volt 1 — 3 laps\r\nPetroVolt (R) — 2 laps "
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

