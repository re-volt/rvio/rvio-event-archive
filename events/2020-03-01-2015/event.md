---
title: 'Normal Semi-Pro'
titleoverride: true
date: '01-03-2020 20:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Normal Semi-Pro'
    classlink: 'https://re-volt.io/events/2020-03-01-2015'
    tracklist: "Toys in the hood Ex M (4 laps)\r\nMetro-Volt (4 laps)\r\nSakura RM (4 laps)\r\nRed Rock Valley R (4 laps)\r\nRooftops R (4 laps)\r\nToy World Mayhem M (4 laps) \r\nThe Gorge M (4 laps)\r\nLunar (4 laps)\r\nToysoldierz M (4 laps)\r\nToy World 2 RM (4 laps)\r\nThe Felling Yard RM (4 laps)\r\nSwan Street RM (4 laps)\r\nToys in the Hood 1 (4 laps)\r\nSnowy River RM (4 laps)\r\nRv Temple M (3 laps)\r\nRooftop Chase: Redux (4 laps)\r\nGreen Night R (4 laps)\r\nGrisville R (4 laps)\r\nMolten Caverns R (3 laps)\r\nToys in the Hood Ex R (4 laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Normal Semi-Pro'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous avarage prestige results be sure to [check this](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Extra Old I/O Tracks](https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Extra Old I/O Cars](https://www.dropbox.com/sh/qqkplevr9df6l3a/AACsyvafB1QkhnU_GvtULqFJa?dl=1) (advised)

### Car Selection
* Any car that has been rated for semi-pro or lower on [this car list](https://www.dropbox.com/s/y9mondib39fzziy/Car%20rating%20list%20for%20online.txt?dl=0)
