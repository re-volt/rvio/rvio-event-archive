---
title: 'Shopping Cartnival'
titleoverride: false
date: '28-06-2019 18:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Trolley Exclusive'
    tracklist: "Penny Racers - Harbour\r\nThe Bunker\r\nBotanical Garden\r\nDonut Plains 3\r\nMoon Dawn\r\nToy World 1\r\nCliffside\r\nAMCO TT (R)\r\nSuperMarket 2\r\nLunar\r\nThe Felling Yard\r\nGhost Town 1\r\nToy World Mayhem\r\nToys in the Hood 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

