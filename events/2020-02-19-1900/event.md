---
title: 'Normal Advanced Races'
titleoverride: true
date: '19-02-2020 19:00'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Normal Advanced Cars'
    classlink: 'https://re-volt.io/events/2020-02-19-1900'
    tracklist: "Toys in the Hood Ex (4 laps)\r\nSkating Toys RM (3 laps)\r\nRooftops M (4 laps)\r\nToytanic 2 R (4 laps)\r\nGreen Night RM (4 laps)\r\nToy World 1 (5 laps)\r\nRooftops2 (4 laps)\r\nGround N Smash 2 (4 laps)\r\nSkating Toys M (3 laps)\r\nPenny Racers Caves RM (4 laps)\r\nRooftop Chase Redux (4 laps)\r\nFiddles on the Roof Redux R (4 laps)\r\nPenny Racers Harbour R (4 laps)\r\nRoute-77 RM (4 laps)\r\nRooftops R (4 laps)\r\nRooftop Chase Redux M (4 laps)\r\nMuseum 2 R (4 laps)\r\nDaydreaming M (4 laps)\r\nBroken Sunlight M (3 laps)\r\nMid-Sea Island M (4 laps)\r\nIllusion (4 laps)\r\nIndustry M (4 laps)\r\nChristmas Special Stage (4 laps)\r\nGrisville M (4 laps)\r\nCactus Volt (3 laps)\r\nPetroVolt R (3 laps)\r\nSchools Out RM (2 laps)\r\nSchools Out (2 laps)\r\nBotanical garden RM (5 laps)\r\nIllusion RM (4 laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Normal Advanced Car Rated Session'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous avarage prestige results be sure to [check this](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).


### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Extra Old I/O Tracks](https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Extra Old I/O Cars](https://www.dropbox.com/sh/qqkplevr9df6l3a/AACsyvafB1QkhnU_GvtULqFJa?dl=1) (advised)

### Car Selection
* Any car that has been rated for advanced or lower on [this car list](https://www.dropbox.com/s/y9mondib39fzziy/Car%20rating%20list%20for%20online.txt?dl=0)