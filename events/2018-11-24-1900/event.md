---
title: Slugs
titleoverride: false
date: '24-11-2018 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Museum 2\r\nGhost Town 2 (R)\r\nMetro-Volt\r\nToy World Mayhem (R)\r\nLunar\r\nRooftop Chase\r\nThe Bunker\r\nRooftops\r\nSuperMarket 2\r\nToySoldierz\r\nSnowy River\r\nPenny Racers - Caves\r\nIllusion (R)\r\nBotanical Garden"
    vod: 'https://www.youtube.com/watch?v=c_v3YWQaCvk'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-24_20-02-41.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

