---
title: 'Pemto 100'
titleoverride: false
date: '10-10-2018 18:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Pemto 100'
    classlink: 'http://revoltzone.net/cars/28297/Pemto'
    tracklist: "Ghost Town 2\r\nSpa-Volt 1\r\nSuperMarket 1\r\nSantorini\r\nMuseum 1\r\nOverground\r\nToys in the Hood 2\r\nPetroVolt\r\nToy World Mayhem\r\nVenice\r\nRanch\r\nRooftops\r\nBotanical Garden\r\nSakura\r\nHelios"
    vod: 'https://www.youtube.com/watch?v=HJu5EzoTJJE'
    results: 'https://online.re-volt.io/sessions/results.php?file=competitive/session_2018-10-10_20-05-28.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

You may only select [Pemto](http://revoltzone.net/cars/28297/Pemto).