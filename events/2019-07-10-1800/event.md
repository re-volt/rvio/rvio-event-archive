---
title: 'NY 54 Exclusive'
titleoverride: false
date: '10-07-2019 18:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'NY 54 Exclusive'
    classlink: 'https://revolt.fandom.com/wiki/NY_54'
    tracklist: "Toytanic 1 (R)\r\nRooftop Chase: Redux (R)\r\nMuseum 2\r\nSwan Street\r\nRanch\r\nPetroVolt (R)\r\nMetro-Volt\r\nToySoldierz\r\nWhite Rose Chapel\r\nFool's Mate 2\r\nToy World 1\r\nYABBA DABBA DOO! М\r\nGhost Town 1\r\nSakura"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

