---
title: Rookies
titleoverride: false
date: '04-12-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: rookies
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 1 (R)\r\nAMCO TT\r\nRooftops\r\nVenice\r\nSakura\r\nDonut Plains 3\r\nPenny Racers - Caves (R)\r\nCliffside\r\nHelios\r\nToy World Aquatica: Redux\r\nMuseum 2\r\nSpa-Volt 1 (R)\r\nToySoldierz\r\nToy World 1\r\nBlood on the Rooftops"
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-04_21-04-17.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

