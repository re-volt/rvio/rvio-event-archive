---
title: 'Gold Cup'
titleoverride: false
date: '28-09-2018 18:00'
event:
    host: Whitedoom
    category: unofficial
    mode: race
    class: other
    otherclass: 'Gold Cup'
    classlink: 'https://www.dropbox.com/s/ab9t5cc7wkwm8je/Online%20cup%20data.txt?dl=0'
    tracklist: "Rooftop Chase (M)\r\nDaydreaming (M)\r\nToytanic 1\r\nFiddlers on the Roof"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only 9 spots are available. To reserve a spot, contact Whitedoom on [Discord](https://re-volt.io/discord). The IP will be shared there in case any free spots are available when the event starts.

The races will be held in **Simulation** mode.

**Requirements:**
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip)
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip)

**Car Selection:**
* Crankenwagon
* Pest Control
* Hydrox
* Nice Black
* Nimiarc
* Riptor
* Varflame

[More information here](https://www.dropbox.com/s/ab9t5cc7wkwm8je/Online%20cup%20data.txt?dl=0).