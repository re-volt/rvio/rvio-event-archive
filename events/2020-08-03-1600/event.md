---
title: 'Probe-ably UFO'
titleoverride: true
date: '03-08-2020 16:00'
event:
    host: Frosttbitten
    ip: frost.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Probe UFO'
    classlink: 'https://revolt.fandom.com/wiki/Probe_UFO'
    packages:
        - io_tracks
    tracklist: "Spa—volt 1 (R) — 3 laps\r\nSchool's Out — 2 laps\r\nRe—ville — 3 laps\r\nMetro—volt — 3 laps\r\nBlood on the Rooftops — 3 laps\r\nRooftops 1 (R) — 3 laps\r\nToy World 1 — 6 laps\r\nMoon Dawn — 2 laps\r\nRooftops (R) — 3 laps\r\nMuseum 1 — 3 laps\r\nBotanical Garden — 6 laps\r\nRooftop Chase: Redux — 3 laps\r\nWildland — 3 laps\r\nQuake! — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Probe-ably UFO'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

 [Was that UFO?](https://www.youtube.com/watch?v=EbwhPFDrHUQ)