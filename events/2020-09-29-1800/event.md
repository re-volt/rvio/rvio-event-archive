---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '29-09-2020 18:00'
event:
    host: 'Frosttbitten & Ashen Forest'
    ip: 'frost.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Toys in the Hood 2 — 4 laps\r\nBlood on the Rooftops — 3 laps\r\nRadioactive Garden — 5 laps\r\nStadVolt (R) — 4 laps\r\nSkating Toys: Redux —2 laps\r\nSupermarket 2 (R) — 8 laps\r\nMedieval: Redux — 4 laps\r\nBotanical Garden EX — 2 laps\r\nMuseum 3 — 3 laps\r\nHoliday Camp — 2 laps\r\nSpa-Volt 2 — 3 laps\r\nVenice (R) — 3 laps\r\nMuseum 2 — 4 laps\r\nToy World Mayhem — 4 laps\r\nToytanic 2 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

