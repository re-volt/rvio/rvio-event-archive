---
title: 'Amateur Races'
titleoverride: true
date: '02-07-2020 19:30'
event:
    host: Geromu
    ip: 213.167.8.241
    category: unofficial
    mode: race
    class: other
    otherclass: 'Amateur Cars'
    classlink: 'https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0'
    tracklist: "Swan Street\r\nGhost Town 1\r\nCactus Volt\r\nQuake!\r\nMuseum 2\r\nBotanical Garden\r\nToys in the Hood 1\r\nRoute-77\r\nPlaya+\r\nWipeout\r\nMikes Medieval Mayhem\r\nToytanic 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirement:
* [RVON community track pack 20.2](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1) (needed)
* [RVON community car pack 20.2](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (needed)
* [RVON car list](https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0)