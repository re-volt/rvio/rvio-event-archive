---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '24-12-2019 19:00'
event:
    host: 'Skarma OR Saffron'
    ip: 'sk.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "PetroVolt\r\nMuseum 1\r\nToy World 1 (R) (M)\r\nRooftops\r\nToytanic 2\r\nRooftop Chase: Redux\r\nSnowy River\r\nQuake!\r\nPenny Racers - Caves\r\nGrisville\r\nMolten Caverns\r\nToySoldierz\r\nHelios\r\nRadioactive Garden (R)\r\nSpa-Volt 2 (R)\r\nSnowland 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

