---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '03-10-2020 18:00'
event:
    host: 'Kirioso & Ashen Forest'
    ip: 'kiri.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "PetroVolt (R) — 2 laps\r\nSupermarket 1 (R) — 4 laps\r\nHelios — 3 laps\r\nIndustry — 3 laps\r\nToy World Aquatica: Redux — 4 laps\r\nFool's Mate 2 — 4 laps\r\nToySoldierz — 4 laps\r\nToy World 2 — 5 laps\r\nGhost Town 2 — 5 laps\r\nSantorini — 4 laps\r\nRooftops 1 — 3 laps\r\nSakura — 3 laps\r\nMuseum 1 — 3 laps\r\nHull Breach 3000 — 4 laps\r\nRooftop Chase: Redux (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

