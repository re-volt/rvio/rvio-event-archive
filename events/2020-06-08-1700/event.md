---
title: 'Long Journey 4: Day 1'
titleoverride: true
date: '08-06-2020 17:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_cars_bonus
        - io_tracks
        - io_skins
        - io_tracks_bonus
    tracklist: "Waterfall\r\nMuseum 2\r\nZach's Garden\r\nMetro-Volt\r\nAvalon\r\nThe Felling Yard\r\nShoppe\r\nRadioactive Garden\r\nGrisville\r\nWipeout\r\nImages of Giza: Redux\r\nToys in the Hood 2\r\nL.A. Smash\r\nToy World Aquatica: Redux\r\nSmashride Circuit\r\nRoad in the Sky\r\nFool's Mate 2\r\nSpa-Volt 1\r\nEver After\r\nHoliday Camp: CE\r\nChess Night\r\nYABBA DABBA DOO!"
    vod: 'https://www.youtube.com/watch?v=vEb1NnjO0uc'
    results: 'https://online.re-volt.io/sessions/results.php?file=main/session_2020-06-08_19-00-19.csv'
media_order: LongJourney4.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Long Journey 4: Day 1'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](https://re-volt.io/user/pages/events/2020-06-08-1700/LongJourney4.png)