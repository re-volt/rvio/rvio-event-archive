---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '08-08-2020 18:00'
event:
    host: 'Narusori & Kirioso'
    ip: 'ash.rv.gl OR kiri.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Metro-Volt — 3 laps\r\nFools Mate 2 — 4 laps\r\nGhost Town 1 — 6 laps\r\nThe Felling Yard — 3 laps\r\nToytanic 2 — 3 laps\r\nLunar — 3 laps\r\nRooftops 1 — 3 laps\r\nWildland — 3 laps\r\nHull Breach 3000 — 4 laps\r\nSupermarket 1 (R) — 4 laps\r\nAMCO TT (R) — 3 laps\r\nGhost Town 2 — 5 laps\r\nBlood on the Rooftops — 3 laps\r\nPetroVolt (R) — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

