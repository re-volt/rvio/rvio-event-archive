---
title: '1v1 Tournament Qualifiers'
titleoverride: true
date: '01-12-2019 19:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: '1v1 Qualifiers'
    classlink: 'https://re-volt.io/events/2019-12-01-1915'
    tracklist: "Images of Giza\r\nWipeout 2097\r\nRadioactive Garden"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Open lobby for anyone interested in participating in the 1v1 qualifiers at the very last moment before the deadline. For more information, [check the 1v1 Discord server](https://discord.gg/MFHFx8d).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)

### Car Selection
* AMW
* Cougar
* Humma
* Panga
* Toyeca