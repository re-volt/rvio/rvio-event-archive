---
title: '<small>Super Pro Races (Triple Lobby)</small>'
titleoverride: true
date: '24-11-2020 19:00'
event:
    host: 'Bismarck & Mighty & Floxit'
    ip: 'bis.rv.gl OR mighty.rv.gl OR flo.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "The Bunker — 4 laps\r\nSpa-Volt 2 — 4 laps\r\nSpa-Volt 1 — 3 laps\r\nIndustry — 3 laps\r\nMedieval: Redux — 5 laps\r\nBotanical Garden — 6 laps\r\nGhost Town 2 (R) — 4 laps\r\nBlood on the Rooftops — 3 laps\r\nMuseum 3 — 4 laps\r\nHull Breach 3000 — 4 laps\r\nGhost Town 1 — 7 laps\r\nRooftops — 4 laps\r\nSchools Out! 2 (R) — 2 laps\r\nSnowy River (R) — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

