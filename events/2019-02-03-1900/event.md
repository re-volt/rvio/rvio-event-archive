---
title: 'Pro Races'
titleoverride: false
date: '02-03-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Holiday Camp: California Edition\r\nPenny Racers - Harbour\r\nSkating Toys\r\nSuperMarket 1\r\nMetro-Volt\r\nSantorini\r\nRooftops\r\nPalm Marsh\r\nGhost Town 2 (R)\r\nToys in the Hood 1\r\nThe Bunker\r\nIllusion\r\nPenny Racers - Caves (R)\r\nJailhouse Rock\r\nSakura (R)\r\nFool's Mate 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

