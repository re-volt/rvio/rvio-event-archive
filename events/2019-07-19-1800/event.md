---
title: 'Long Journey: 3rd Day'
titleoverride: false
date: '19-07-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Long Journey: 3rd Day'
    classlink: 'https://re-volt.io/events/2019-07-19-1800'
    tracklist: "PetroVolt\r\nAMCO TT\r\nMolten Caverns\r\nSantorini\r\nOverground\r\nHelios (M)\r\nHoliday Camp: California Edition\r\nKadish Sprint\r\nYABBA DABBA DOO!\r\nBlood on the Rooftops\r\nSwan Street\r\nToySoldierz\r\nJailhouse Rock (R)\r\nBiohazard Factory\r\nMoon Dawn (RM)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**A special 4-day event hosted by OhNej in celebration of Re-Volt's 20th Anniversary.**<br>
We will be racing with [Pro Cars](https://re-volt.io/online/car-classes) on Arcade mode, with pick-ups enabled and 3 laps on each track.

![](https://re-volt.io/user/pages/events/2019-07-15-1800/GoingHome.png)