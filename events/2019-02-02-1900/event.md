---
title: 'Advanced Races'
titleoverride: false
date: '02-02-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "AMCO TT\r\nToys in the Hood 1 (R)\r\nQuake!\r\nMK64 Wario Stadium\r\nLunar\r\nGhost Town 1\r\nToytanic 1\r\nToy World Aquatica: Redux (R)\r\nBlood on the Rooftops\r\nOverground\r\nSwan Street\r\nSupermarket 2\r\nJailhouse Rock\r\nIndustry\r\nVenice (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

