---
title: 'Semi-Pro Races'
titleoverride: false
date: '09-04-2019 18:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 1 (R)\r\nWhite Rose Chapel\r\nSpa-Volt 1 (R)\r\nToys in the Hood 2\r\nYABBA DABBA DOO!\r\nHelios\r\nRe-Ville\r\nToytanic 2\r\nMoon Dawn\r\nOverground\r\nSakura\r\nIllusion (R)\r\nMuseum 2\r\nFool's Mate 2\r\nPetroVolt"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

