---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '18-01-2020 19:00'
event:
    host: 'Delecto & Saffron'
    ip: 'dele.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Museum 1 — 2 laps\r\nSpa-Volt 2 — 3 laps\r\nHull Breach 3000 (M) — 3 laps\r\nFool's Mate 2 — 4 laps\r\nRoute-77 — 3 laps\r\nRooftops (R) — 3 laps\r\nSnowy River — 3 laps\r\nGrisville — 3 laps\r\nToySoldierz — 3 laps\r\nSakura — 2 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nGhost Town 2 — 4 laps\r\nPenny Racers - Caves (R) — 4 laps\r\nBotanical Garden — 5 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

