---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '18-07-2020 18:00'
event:
    host: 'Kirioso & Ashen Forest'
    ip: 'kiri.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Toys in the Hood 2 — 4 laps\r\n    Meltdown — 4 laps\r\n    Snowy River (R) — 4 laps\r\n    Jailhouse Rock — 3 laps\r\n    Supermarket 1 — 4 laps\r\n    Toys in the Hood 1 (R) — 3 laps\r\n    Spa-Volt 1 — 3 laps\r\n    Holiday Camp: California Edition (R) — 2 laps\r\n    Overground — 3 laps\r\n    White Rose Chapel — 3 laps\r\n    Medieval: Redux — 5 laps\r\n    PetroVolt — 2 laps\r\n    Fools Mate 2 — 4 laps\r\n    Toytanic 2 — 3 laps "
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

