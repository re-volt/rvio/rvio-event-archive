---
title: Drifting
titleoverride: false
date: '12-10-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://files.re-volt.io/packs/drift_cars.7z'
    tracklist: "AMCO Bitume\r\nAMCO Driftume\r\nTVGP Bitume\r\nRV-Simo On-Road Track\r\nWarehouse Showdown\r\nLS Kart Space I\r\nNamco Racer\r\nSideways Sanctuary A\r\nSideways Sanctuary B\r\nDrivers School\r\nSideways Sanctuary C\r\nSideways Sanctuary D\r\nDaybreak Raceway\r\nEggland\r\nFoxglen\r\nNamco Trains\r\nMelville\r\nStagnaro\r\nTouge Drift"
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2018-10-12_21-02-59.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Requirements:**
* [Drift Cars](https://files.re-volt.io/packs/drift_cars.7z)
* [Drift Tracks](https://files.re-volt.io/packs/drift_tracks.7z)