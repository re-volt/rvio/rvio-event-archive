---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '21-07-2020 18:00'
event:
    host: 'Kirioso & Ashen Forest'
    ip: 'kiri.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Medieval: Redux — 4 laps\r\nSakura — 2 laps\r\nThe Felling Yard — 2 laps\r\nAMCO TT (R) — 3 laps\r\nToy World 2 — 4 laps\r\nMoon Dawn — 2 laps\r\nSpa-Volt 2 — 3 laps\r\nQuake! — 2 laps\r\nToytanic 2 — 3 laps\r\nMuseum 2 — 3 laps\r\nVenice (R) — 3 laps\r\nRooftops (R) — 3 laps\r\nSantorini — 4 laps\r\nRooftops 1 — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

