---
title: 'Panga Exclusive'
titleoverride: false
date: '08-03-2019 19:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: race
    class: other
    otherclass: Panga
    classlink: 'https://revolt.fandom.com/wiki/Panga'
    tracklist: "PetroVolt\r\nRadioactive Garden\r\nToys in the Hood 1\r\nRanch\r\nMK64 Wario Stadium\r\nThe Gorge\r\nPalm Marsh\r\nVenice (R)\r\nMeltdown\r\nMuseum 2\r\nThe Felling Yard\r\nMoltern Caverns\r\nToytanic 1\r\nToy World 1 (R)\r\nOverground\r\nLunar"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

