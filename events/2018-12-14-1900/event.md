---
title: 'New Rookies'
titleoverride: false
date: '14-12-2018 19:00'
event:
    host: Shara
    ip: 'announced on Discord'
    category: unofficial
    mode: race
    class: other
    otherclass: 'New Rookies'
    tracklist: "Ghost Town 1 R\r\nSuperMarket 2 R\r\nIllusion\r\nPenny Racers - Caves\r\nRadioactive Garden\r\nPalm Marsh\r\nRe-Ville\r\nThe Bunker\r\nCactus-Volt\r\nThe Gorge\r\nSpa-Volt 1\r\nToy World 2 R\r\nMuseum 2\r\nToys In the Hood 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Join [Discord](http://re-volt.io/discord/io) for the IP.

**Car Selection:**
* Bumblebee
* Manko XS
* Scatterbolt
* Volken N/A
* Maxx Stretch
* Roadie Roy
* Falling Star
* The Mangle
* Special Delivery
* SF 54 (Phimeek)
* ZE 84
* Lotus Rups

**[Download the car pack here!](https://www.dropbox.com/s/ajtadak80bey5oq/newrookiescars.zip?dl=1)**