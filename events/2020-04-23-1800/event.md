---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '23-04-2020 18:00'
event:
    host: 'Delecto & Narusori'
    ip: 'dele.rv.gl OR naru.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "RV Temple (M) — 2 laps\r\nImages of Giza: Redux (R) — 5 laps\r\nSnowy River — 4 laps\r\nToy World Aquatica: Redux — 4 laps\r\nWhite Rose Chapel — 3 laps\r\nRoute-77 — 4 laps\r\nQuake! — 3 laps\r\nToys in the Hood 2 — 4 laps\r\nSakura — 3 laps\r\nJailhouse Rock (R) — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nToys in the Hood 1 — 4 laps\r\nMuseum 2 — 4 laps\r\nGhost Town 1 (R) — 7 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

