---
title: 'Rookie Races'
titleoverride: false
date: '21-09-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Illusion\r\nToy World Mayhem\r\nHoliday Camp: California Edition (R)\r\nOverground\r\nGhost Town 2\r\nRadioactive Garden (R)\r\nBotanical Garden\r\nYABBA DABBA DOO!\r\nKadish Sprint (M)\r\nMetro-Volt\r\nBiohazard Factory\r\nSpa-Volt 1\r\nRooftops (R)\r\nToy World 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

