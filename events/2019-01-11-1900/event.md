---
title: 'ToyVolt Exclusive'
titleoverride: false
date: '11-01-2019 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'ToyVolt Exclusive'
    classlink: 'https://re-volt.io/events/2019-01-11-1900'
    tracklist: "Toy World 1 (R)\r\nToy World Aquatica: Redux\r\nToy World EX\r\nToy World 2 (M)\r\nFreestoyle 2 (M)\r\nToy World Mayhem (RM)\r\nToy World 1\r\nToy World Aquatica: Redux (R)\r\nToy World 2\r\nFreestoyle 2\r\nToy World Mayhem\r\nToy World Aquatica: Redux (RM)\r\nToy World EX (RM)\r\nToy World 1 (RM)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only **[Rookie](https://re-volt.io/online/car-classes)** cars are allowed.