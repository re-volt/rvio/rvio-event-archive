---
title: 'Rookie Races'
titleoverride: true
date: '11-06-2020 18:00'
event:
    host: Dvark
    ip: dv.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Cliffside — 5 laps\r\nHoliday Camp: California Edition — 2 laps\r\nSpa-Volt 1 — 3 laps\r\nRooftops (R) — 3 laps\r\nMuseum 2 — 3 laps\r\nJailhouse Rock (R) — 2 laps\r\nFools Mate 2 — 3 laps\r\nGhost Town 2 — 4 laps\r\nSmashride Circuit — 2 laps\r\nToytanic 1 — 3 laps\r\nRoute-77 — 3 laps\r\nThe Bunker — 3 laps\r\nMolten Caverns (R) — 2 laps\r\nToy World Mayhem — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

