---
title: 'Clockwork Races'
titleoverride: true
date: '20-07-2020 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Classic Clockworks'
    classlink: 'https://re-volt.io/online/cars/clockwork'
    tracklist: "Toy World 1 — 5 laps\r\nRoute-77 — 2 laps\r\nHull Breach 3000 — 3 laps\r\nToy World Aquatica: Redux — 3 laps\r\nBotanical Garden (R) — 4 laps\r\nMetro-Volt — 3 laps\r\nSkating Toys — 1 lap\r\nToy World 2 — 4 laps\r\nWildland — 2 laps\r\nSmashride Circuit — 3 laps\r\nToy World Mayhem — 3 laps\r\nToySoldierz — 3 laps\r\nSuperMarket 2 — 6 laps\r\nSchool's Out! — 1 lap"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

[Classic Clockworks](https://distribute.re-volt.io/packs/io_clockworks.zip) are required.