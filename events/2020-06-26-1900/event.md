---
title: 'Rookie Races'
titleoverride: true
date: '26-06-2020 19:00'
event:
    host: Geromu
    ip: 213.167.5.10
    category: unofficial
    mode: race
    class: other
    otherclass: 'Rookie Cars'
    classlink: 'https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0'
    tracklist: "Drivers School\r\nMKDS Airship Fortress\r\nToy World Mayhem\r\nToytanic 1\r\nVenice\r\nFools Mate 2\r\nThe Felling Yard\r\nToy World 2\r\nMeltdown\r\nComputer Virus 2\r\nChilled to the Bone\r\nGo Play Outside!\r\nToys in the Hood 1\r\nIndustry"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirement:
* [RVON community track pack 20.2](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1) (needed)
* [RVON community car pack 20.2](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (needed)
* [RVON car list](https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0)