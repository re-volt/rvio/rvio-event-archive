---
title: 'Semi-Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '05-12-2020 19:00'
event:
    host: 'Kirioso & Bismarck'
    ip: 'kiri.rv.gl OR bis.rv.gl'
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Jailhouse Rock — 3 laps\r\nGhost Town 2 — 4 laps\r\nQuake! — 2 laps\r\nSpa-Volt 1 — 3 laps\r\nHull Breach 3000 — 4 laps\r\nMuseum 2 — 4 laps\r\nBotanical Garden — 6 laps\r\nRooftops 1 — 3 laps\r\nToySoldierz — 4 laps\r\nVenice (R) — 3 laps\r\nRoute-77 — 4 laps\r\nSnowy River (R) — 4 laps\r\nToys in the Hood 2 (R) — 4 laps\r\nThe Felling Yard — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

