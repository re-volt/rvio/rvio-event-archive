---
title: 'Pro Races'
titleoverride: true
date: '25-07-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Ghost Town 1 (R) — 6 laps\r\n    Moon Dawn — 2 laps\r\n    Museum 2 — 4 laps\r\n    Museum 1 — 3 laps\r\n    Hull Breach 3000 — 4 laps\r\n    Rooftop Chase: Redux (R) — 3 laps\r\n    Spa-Volt 2 (R) — 3 laps\r\n    Meltdown — 4 laps\r\n    ToySoldierz — 4 laps\r\n    Images of Giza: Redux — 5 laps\r\n    Venice — 3 laps\r\n    Supermarket 2 — 8 laps\r\n    Rooftops 1 — 3 laps\r\n    Sakura — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

