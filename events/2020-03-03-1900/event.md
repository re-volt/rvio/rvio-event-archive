---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '03-03-2020 19:00'
event:
    host: 'Saffron & OhNej'
    ip: 'saff.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Toy World Mayhem — 4 laps\r\nGhost Town 1 — 6 laps\r\nOverground — 3 laps\r\nThe Felling Yard — 3 laps\r\nPetroVolt — 2 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nSupermarket 2 (R) — 8 laps\r\nCliffside — 6 laps\r\nMuseum 1 — 3 laps\r\nToys in the Hood 1 — 3 laps\r\nAMCO TT (R) — 3 laps\r\nQuake! (M) — 2 laps\r\nToySoldierz — 4 laps\r\nMolten Caverns — 2 laps"
    vod: 'https://www.youtube.com/watch?v=6XyPnW-RT48'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

