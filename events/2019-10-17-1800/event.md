---
title: 'Amateur Races'
titleoverride: false
date: '17-10-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World Aquatica: Redux (R)\r\nCliffside (M)\r\nHoliday Camp: California Edition\r\nYABBA DABBA DOO!\r\nRV Temple\r\nToy World 2\r\nToys in the Hood 1 (R)\r\nMetro-Volt\r\nMoon Dawn\r\nQuake!\r\nBotanical Garden\r\nSpa-Volt 1 (R)\r\nSnowy River\r\nGhost Town 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

