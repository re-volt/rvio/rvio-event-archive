---
title: 'Semi-Pro Knockout Session'
titleoverride: true
date: '17-01-2020 19:00'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: Semi-Pro
    classlink: 'https://re-volt.io/events/2020-01-17-1900'
    tracklist: Random
    stream: 'https://www.twitch.tv/videos/537481406'
    vod: 'https://www.youtube.com/watch?v=npkF9Grvic4'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Knockout Session'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information, be sure to check the [Knockout Session Info](https://www.dropbox.com/s/evna7pslcw0rgvz/re-volt%20knockout%20session%20info.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Extra Old I/O Tracks](https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)

### Car Selection

* Any [stock](https://revolt.fandom.com/wiki/Category:Standard_Cars) or [DC cars](https://revolt.fandom.com/wiki/Category:Console_Cars) rated __in-game__ as **Semi-Pro** or below
* Any cars in the I/O [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) that are rated __in-game__ as **Semi-Pro** or below
* Any cars in the I/O [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) that are rated __in-game__ as **Semi-Pro** or below
* Any cars in the I/O [main car selection](https://re-volt.io/online/cars) that are classified __on the website__ as **Semi-Pro** or below
* Any cars in the I/O [bonus car selection](https://re-volt.io/online/cars/bonus) that are classified __on the website__ as **Semi-Pro** or below