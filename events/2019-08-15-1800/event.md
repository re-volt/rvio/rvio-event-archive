---
title: 'Semi-Pro Races'
titleoverride: false
date: '15-08-2019 18:00'
event:
    host: TBA
    ip: TBA
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Quake!\r\nRooftop Chase: Redux R\r\nGrisville\r\nBotanical Garden \r\nSpa-Volt 1 M\r\nRV Temple\r\nToySoldierz\r\nFool's Mate 2\r\nToys in the Hood 1\r\nPetroVolt R\r\nMuseum 2\r\nHull Breach 3000\r\nMolten Caverns\r\nLunar\r\nGhost Town 2 R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

