---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '12-03-2020 19:00'
event:
    host: 'Narusori & OhNej'
    ip: 'sas.rv.gl OR ohnej.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Swan Street — 3 laps\r\nSupermarket 1 — 4 laps\r\nMuseum 1 — 2 laps\r\nToy World Mayhem (R) — 3 laps\r\nDonut Plains 3 (R) — 4 laps\r\nCliffside — 5 laps\r\nToy World Aquatica: Redux — 3 laps\r\nBotanical Garden (M) — 5 laps\r\nHull Breach 3000 — 3 laps\r\nSupermarket 2 (R) — 7 laps\r\nRoute-77 — 3 laps\r\nSpa-Volt 1 — 3 laps\r\nPenny Racers - Harbour — 3 laps\r\nOverground — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

