---
title: 'Christmas Carnival (Day 2)'
titleoverride: true
date: '27-12-2020 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Christmas Carnival'
    classlink: 'https://re-volt.io/blog/christmas-carnival-2020'
    tracklist: "Christmas Snow Globe — 5 laps\r\nPR: Caves — 4 laps\r\nFrozen Caverns — 2 laps\r\nR2049 Tundra — 4 laps\r\nArctic Air — 2 laps\r\nAurora Borealis — 5 laps\r\nWicked Winter — 5 laps\r\nWinter Fest — 3 laps\r\nRally ZX SS 2 — 3 laps\r\nHimalaya Highway — 3 laps\r\nUranus — 2 laps\r\nWinter Madness — 10 laps\r\nTetris Festival — 4 laps\r\nChilled to the Bone — 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Christmas Carnival (Day 2)'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](https://re-volt.io/user/pages/blog/christmas-carnival-2020/carnival.jpg)
If you have not submitted a car, you are only allowed to race **XMAS 2020** unless told otherwise.

### Requirements
* [Christmas Carnival 2020 Pack](https://files.re-volt.io/packs/christmas/2020/carnival_full.7z)