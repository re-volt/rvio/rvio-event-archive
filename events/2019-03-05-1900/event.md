---
title: 'Rookie Races'
titleoverride: false
date: '05-03-2019 19:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Quake! (R)\r\nSupermarket 1\r\nAMCO TT (R)\r\nToy World Aquatica: Redux\r\nSantorini\r\nVenice\r\nRooftops\r\nSpa-Volt 1\r\nCliffside\r\nMetro-Volt\r\nSupermarket 2\r\nFool's Mate 2\r\nRooftop Chase\r\nGhost Town 2 (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

