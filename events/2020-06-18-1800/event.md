---
title: 'Semi-Pro Races'
titleoverride: true
date: '18-06-2020 18:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/cars/semi-pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "PetroVolt — 2 laps\r\nGrisville — 3 laps\r\nCliffside — 6 laps\r\nFiddlers on the Roof: Redux — 3 laps\r\nVenice (R) — 3 laps\r\nSupermarket 1 — 4 laps\r\nSpa-Volt 1 — 3 laps\r\nSupermarket 2 — 8 laps\r\nHull Breach 3000 — 4 laps\r\nGhost Town 1 (R) — 6 laps\r\nOverground — 2 laps\r\nMolten Caverns (R) — 2 laps\r\nRV Temple — 2 laps\r\nToytanic 1 — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Semi-Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

