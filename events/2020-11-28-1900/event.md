---
title: 'Pro Races'
titleoverride: true
date: '28-11-2020 19:00'
event:
    host: 'Ashen Forest'
    ip: ash.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "Quake! — 2 laps\r\nMuseum 2 — 4 laps\r\nLunar — 3 laps\r\nStadVolt — 4 laps\r\nMetro-Volt — 3 laps\r\nSnowland 1 — 4 laps\r\nMolten Caverns — 2 laps\r\nVenice (R) — 3 laps\r\nBotanical Garden EX — 3 laps\r\nToy World 2 — 5 laps\r\nSantorini (R) — 4 laps\r\nToytanic 2 — 3 laps\r\nMysterious Toy-Volt Factory — 5 laps\r\nSupermarket 1 (R) — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

