---
title: 'Dust Mite Exclusive'
titleoverride: false
date: '08-10-2019 19:00'
event:
    host: L!LMexican
    ip: mex.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Dust Mite'
    classlink: 'https://revolt.fandom.com/wiki/Dust_Mite'
    tracklist: "Ghost Town 2\r\nPenny Racers - Caves\r\nToys in the Hood 2\r\nSpa-Volt 1\r\nQuake!\r\nBiohazard Factory\r\nRooftop Chase: Redux (R)\r\nMoon Dawn\r\nSuperMarket 1\r\nJailhouse Rock (R)\r\nToy World Mayhem\r\nPenny Racers - Harbour\r\nGhost Town 1 (M)(R)\r\nDonut Plains 3"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

