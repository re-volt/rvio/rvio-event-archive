---
title: 'Online Silver Cup'
titleoverride: true
date: '28-02-2020 20:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Online Silver Cup'
    classlink: 'https://re-volt.io/events/2020-02-28-2015'
    tracklist: "Mkds Airship Fortress\r\nSpa-Volt 2 R\r\nHelios\r\nSwan Street R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 4 cup standings and individual player progress, be sure to [check this](https://www.dropbox.com/s/qfzn2vwewz114d7/season%204%20cup%20session%20info.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Extra Old I/O Tracks](https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Extra Old I/O Cars](https://www.dropbox.com/sh/qqkplevr9df6l3a/AACsyvafB1QkhnU_GvtULqFJa?dl=1) (advised)

### Car Selection
* Apc-L13
* Badd Rc
* Matra Xl
* Monster Beetle
* Panga Tc
* Reddlum
* Titan