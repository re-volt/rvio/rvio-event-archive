---
title: 'Super Pro Races'
titleoverride: false
date: '12-07-2019 18:00'
event:
    host: Geromu
    ip: ger.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/other-content'
    tracklist: "Fool's Mate 2\r\nSupermarket 1\r\nAMCO TT\r\nHoliday Camp: California Edition\r\nBlood on the Rooftops\r\nToys in the Hood 2 (R)\r\nYABBA DABBA DOO!\r\nKadish Sprint\r\nPetroVolt (R)\r\nToytanic 2\r\nRanch\r\nPenny Racers - Caves\r\nGrisville\r\nDonut Plains 3 (M)\r\nMuseum 1\r\nJailhouse Rock (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

