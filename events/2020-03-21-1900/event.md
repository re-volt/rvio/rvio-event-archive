---
title: 'Rookie Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '21-03-2020 19:00'
event:
    host: 'Delecto & Saffron'
    ip: 'dele.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/cars/rookie'
    tracklist: "Toys in the Hood 1 — 3 laps\r\nPetroVolt — 2 laps\r\nBiohazard Factory — 2 laps\r\nToy World Mayhem (R) — 3 laps\r\nSnowy River (R) — 3 laps\r\nOverground — 2 laps\r\nSnowland 1 (M) — 3 laps\r\nToy World 1 (R) — 4 laps\r\nWhite Rose Chapel — 2 laps\r\nGhost Town 2 — 4 laps\r\nToySoldierz — 3 laps\r\nSpa-Volt 1 — 3 laps\r\nLunar — 3 laps\r\nToy World 2 — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Rookie Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

