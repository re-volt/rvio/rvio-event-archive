---
title: 'Pole Poz versus Pest Control'
titleoverride: false
date: '13-03-2019 15:00'
event:
    host: L!LMexican
    ip: mex.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Pole Poz or Pest Control'
    tracklist: "Botanical Garden (R)\r\nGhost Town 2\r\nToy World 1 (M)\r\nToys in the Hood 2\r\nSupermarket 2 (M)\r\nSnowland 1\r\nSpa-volt (R)\r\nSpecial Stage Route 5\r\nSewers of the Praree\r\nRV Temple\r\nVenice (R)\r\nToy World Mayhem (R) (M)"
media_order: poster_polevspest.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](poster_polevspest.png)

Each team will consist of 8 players or less, depending on the attendence. If an uneven number of players are present, one of them will have to spectate to keep things balanced. Spectators are welcome to watch.