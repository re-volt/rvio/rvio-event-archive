---
title: 'Online Bronze Cup'
titleoverride: true
date: '05-01-2020 19:15'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'Online Bronze Cup'
    classlink: 'https://re-volt.io/events/2020-01-05-1915'
    tracklist: "Temple of the Burning Darkness\r\nHallows Eve\r\nRoad in the Sky\r\nGo Play Outside!"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

There will be a maximum 9 spots available. You can DM `whitedoom#9530` in [Discord](https://discord.gg/NMT4Xdb) to reserve an early access spot! For more information about Online Cup racing sessions, the current overall Season 3 cup standings and individual player progress, be sure to [check this out](https://www.dropbox.com/s/x8ffi2o516r8xju/season%203%20cup%20session%20info.txt?dl=0).

### Requirements
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Old I/O Tracks](<https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1>) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)

### Car Selection
* Alien
* Col. Moss
* Kanberra Kruiser
* Micro
* Phat Slug
* Volken Turbo
* Wind Up