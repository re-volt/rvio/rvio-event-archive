---
title: 'Custom Pro Races'
titleoverride: false
date: '23-10-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: other
    otherclass: 'Custom Pro Cars'
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "AMCO TT\r\nMetro-Volt (M)\r\nRooftop Chase: Redux\r\nMoon Dawn\r\nPenny Racers - Harbour\r\nSpa-Volt 2 (R)\r\nBiohazard Factory (R)\r\nToy World 2\r\nFool's Mate 2\r\nSupermarket 1 (R)\r\nRooftops\r\nHull Breach 3000\r\nBlood on the Rooftops\r\nGhost Town 2\r\nVenice\r\nToySoldierz"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Only **custom** Pro cars are allowed.