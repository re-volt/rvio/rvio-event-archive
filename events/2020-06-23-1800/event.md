---
title: 'Advanced Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '23-06-2020 18:00'
event:
    host: 'Dvark & Ashen Forest'
    ip: 'dv.rv.gl OR ash.rv.gl'
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/cars/advanced'
    packages:
        - io_cars
        - io_tracks
    tracklist: "    Venice (R) — 3 laps\r\n    Biohazard Factory — 2 laps\r\n    Hull Breach 3000 — 3 laps\r\n    Cliffside — 6 laps\r\n    RV Temple — 2 laps\r\n    Supermarket 1 (R) — 4 laps\r\n    Sakura — 2 laps\r\n    Museum 2 — 4 laps\r\n    Moon Dawn (R) — 2 laps\r\n    Museum 1 — 3 laps\r\n    Toy World 2 — 4 laps\r\n    Helios — 3 laps\r\n    Medieval: Redux — 4 laps\r\n    Meltdown — 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

