---
title: 'Aspenside Endurance'
titleoverride: true
date: '28-12-2020 16:30'
event:
    host: hajducsekb
    ip: hb.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Stock Pro Cars'
    classlink: 'https://re-volt.io/events/2020-12-28-1630'
    tracklist: 'Aspenside — 110 laps'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Make sure to redownload [Aspenside](http://revoltzone.net/tracks/57202/Aspenside)** if you downloaded it before December 27! It does not match the version included in the Christmas Championship or Christmas Carnival packs.

### Car Selection
* AMW
* Cougar
* Humma
* Panga
* Toyeca