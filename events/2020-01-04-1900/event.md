---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '04-01-2020 19:00'
event:
    host: 'URV & Delecto'
    ip: 'u.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    tracklist: "Cliffside — 4 laps\r\nThe Bunker — 3 laps\r\nMuseum 1 — 3 laps\r\nSakura — 3 laps\r\nWhite Rose Chapel (M) — 3 laps\r\nBotanical Garden (R) — 4 laps\r\nFool's Mate 2 — 4 laps\r\nMoon Dawn — 2 laps\r\nIndustry — 3 laps\r\nBiohazard Factory (R) — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nSpa-Volt 1 — 3 laps\r\nOverground — 3 laps\r\nToys in the Hood 2 — 3 laps\r\nToy World 2 — 4 laps\r\nQuake! (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

