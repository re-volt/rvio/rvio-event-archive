---
title: 'Advanced Races'
titleoverride: false
date: '15-01-2019 19:00'
event:
    host: Floxit
    ip: floxit.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 1\r\nOverground\r\nRanch\r\nToy World Aquatica: Redux (R)\r\nMuseum 1 (R) \r\nSwan Street\r\nQuake! (R)\r\nGhost Town 2\r\nMetro-Volt\r\nHelios\r\nRe-Ville\r\nToytanic 1\r\nIllusion\r\nSnowy River\r\nFool's Mate 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

