---
title: Semi-Pros
titleoverride: false
date: '11-12-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pros
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Rooftops (R)\r\nVenice (R)\r\nPetroVolt\r\nHelios \r\nRe-Ville\r\nSuperMarket 1\r\nSnowy River\r\nToy World Aquatica: Redux\r\nSkating Toys\r\nCliffside\r\nToy World Mayhem\r\nToys in the Hood 2\r\nBlood on the Rooftops\r\nGrisville (R)\r\nToy World 1"
    vod: 'https://www.youtube.com/watch?v=LTeqJjmsF4Y'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-12-11_21-02-17.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

