---
title: 'Amateur Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '07-01-2020 19:00'
event:
    host: 'URV & Saffron'
    ip: 'u.rv.gl OR saff.rv.gl'
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/cars/amateur'
    tracklist: "Toys in the Hood 1 — 3 laps\r\nHoliday Camp: California Edition — 2 laps\r\nToySoldierz (R) — 3 laps\r\nQuake! (R) — 2 laps\r\nKadish Sprint (M) — 2 laps\r\nBotanical Garden — 5 laps\r\nRoute-77 — 3 laps\r\nCliffside — 5 laps\r\nJailhouse Rock — 2 laps\r\nPenny Racers - Caves — 4 laps\r\nSwan Street — 3 laps\r\nToytanic 1 (R) — 3 laps\r\nLunar — 3 laps\r\nSuperMarket 2 — 7 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

