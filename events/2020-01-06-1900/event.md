---
title: 'All-Car Races'
titleoverride: true
date: '06-01-2020 19:00'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: 'All-Car Races'
    classlink: 'https://re-volt.io/events/2020-01-06-1900'
    tracklist: "Supermarket 2 RM\r\nTemple of the Brunign Darkness M\r\nToys in the Hood Ex R\r\nEndgame M\r\nRoute 77 RM\r\nRooftop Chase Redux R\r\nVenice RM\r\nPalm Marsh\r\nGreen Night R\r\nPalm Marsh RM\r\nToytanic 2 RM\r\nIllusion RM\r\nMysterious Toy-Volt Factory M\r\nToy World 2 R\r\nPetroVolt\r\nToy World Ex RM\r\nHoldiday Camp California Edition RM\r\nJailhouse Rock RM\r\nGreen Night M\r\nGrisville RM\r\nCactus Volt\r\nToytanic 1 R\r\nSwan Street R\r\nSakura\r\nGhost Town 1 R\r\nAmco TT R\r\nTerminus RM\r\nToys in the Hood 2 M\r\nL.A. Smash\r\nEver After M"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

For more information about normal racing sessions and previous avarage prestige results be sure to [check this](https://www.dropbox.com/s/wq65c7cjn37dr7f/re-volt%20normal%20session%20data.txt?dl=0).

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Extra Old I/O Tracks](https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Super car pack](https://www.dropbox.com/sh/uv3dvm1081w04uj/AADekFTAfRxCGicVmYsAPUEOa?dl=1) (advised)

### Car Selection
All stock, DC and I/O cars (including super pro!) and cars from the super car pack!