---
title: 'Amateur Races'
titleoverride: false
date: '16-02-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Quake!\r\nCliffside\r\nHelios\r\nYABBA DABBA DOO!\r\nMuseum 2\r\nMeltdown\r\nToySoldierz\r\nBotanical Garden (R)\r\nSpa-Volt 1 (R)\r\nThe Felling Yard\r\nSakura\r\nSupermarket 1\r\nPalm Marsh (R)\r\nToy World 2"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

