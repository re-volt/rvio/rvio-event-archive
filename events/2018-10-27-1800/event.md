---
title: 'Halloween Carnival 2018'
titleoverride: false
date: '27-10-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Halloween Carnival'
    classlink: 'https://re-volt.io/blog/halloween-carnival-2018'
    tracklist: "Halloween 2\r\nThe Catacombs\r\nToytanic 2 (R)\r\nFools Mate 2\r\nImages of Giza\r\nThe Keep\r\nSpa-Volt 1\r\nGround N Smash 2\r\nMKDS - Luigi's Mansion\r\nForest Run\r\nSakura\r\nMysterious Toy-Volt Factory\r\nRooftops\r\nHallows Eve\r\nLantern Hollow"
    vod: 'https://www.youtube.com/watch?v=Ju4nHBuaTsc'
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2018-10-27_21-07-55.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

This event requires the [Halloween Carnival](https://files.re-volt.io/packs/halloween/2018/full.7z) custom content pack.