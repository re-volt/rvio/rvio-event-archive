---
title: 'Christmas Carnival (Day 2)'
titleoverride: false
date: '23-12-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Christmas Carnival (Day 2)'
    classlink: 'https://re-volt.io/blog/christmas-carnival-2018'
    tracklist: "Christmas Snow Globe\r\nToy World - Winter\r\nCurves and Caves\r\nChristmas Crib 2011\r\nHome for Christmas\r\nPenny Racers - Caves (R)\r\n8 On Ice\r\n0 Degrees (R)\r\nGlacier Cliffs 4\r\nR2049 Tundra (R)\r\nRally in Mountain 2\r\nAurora Borealis\r\nCandy Cane Land\r\nSnowy River\r\nArtic Air\r\nSnowland 1"
    vod: 'https://www.youtube.com/watch?v=qrJYySSv7oo'
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2018-12-23_21-02-00.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

