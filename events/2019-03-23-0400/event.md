---
title: 'Pro Races'
titleoverride: false
date: '23-03-2019 04:00'
event:
    host: Santi
    ip: santi.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "RV Temple\r\nRooftops\r\nRanch\r\nToytanic 1\r\nSkating Toys (R)\r\nToys in the hood 2\r\nWhite Rose Chapel\r\nMuseum 2 (R)\r\nFiddlers on the Roof\r\nFool's Mate 2\r\nPalm Marsh\r\nIndustry\r\nHoliday Camp: California Edition (R)\r\nSpa-Volt 2\r\nLunar\r\nSnowland 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

