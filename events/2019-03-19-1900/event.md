---
title: 'Pro Races'
titleoverride: false
date: '19-03-2019 19:00'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "AMCO TT\r\nSupermarket 1\r\nRe-Ville\r\nSantorini (R)\r\nRanch\r\nSnowy River\r\nMuseum 2\r\nFiddlers on the Roof\r\nMetro-Volt\r\nCliffside\r\nToys in the Hood 1\r\nPetroVolt\r\nKadish Sprint\r\nGhost Town 2(R)\r\nMoon Dawn"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

