---
title: 'Modern Clockworks'
titleoverride: false
date: '23-11-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Modern Clockworks'
    classlink: 'https://distribute.re-volt.io/packs/io_clockworks_modern.zip'
    tracklist: "Toy World 1\r\nToy World 2\r\nSuperMarket 2\r\nJailhouse Rock\r\nBotanical Garden\r\nSuperMarket 1\r\nToySoldierz\r\nSakura\r\nVenice\r\nMuseum 2\r\nMetro-Volt\r\nQuake!\r\nToy World Aquatica: Redux\r\nToy World Mayhem"
    vod: 'https://www.youtube.com/watch?v=mgE1tryrHEc'
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2018-11-23_21-04-48.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

