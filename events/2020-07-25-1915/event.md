---
title: 'Battle Tag'
titleoverride: true
date: '25-07-2020 19:15'
event:
    host: Santi
    ip: santi.rv.gl
    category: special
    mode: battle-tag
    class: other
    otherclass: 'RC Revenge Cars'
    classlink: 'https://re-volt.io/events/2020-07-25-1915'
    packages:
        - io_lmstag
    tracklist: "Neighbourhood Battle\r\nSupermarket Battle\r\nGarden Battle\r\nMuseum Battle\r\nCementery Battle\r\nToy World Battle \r\nUnreal Tournament"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Battle Tag'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirements:
* [LMS & Tag Collection](https://distribute.re-volt.io/packs/io_lmstag.zip)
* [RC Revenge Cars](https://drive.google.com/file/d/1PS91m4bZQM8Lq09wvf1ew61axK-bN72Y/view?usp=sharing)

The following cars will be allowed:
- Acclaim F1
- Badd RC
- Big Momma
- Big Rock
- Canary XL
- Concept 3000 (Both versions PS1 and PS2)
- Dark Avenger (Both versions PS1 and PS2)
- Griffin (Both versions PS1 and PS2)
- Jungle Ranger
- Lunar Loader
- Nitrox XLi
- Phat Trucker
- RC Action
- RCA UFO (Not Flying version)
- Sarge
- Skull Duggery
- The Corporal
- Yella

Note: If more than 6 players show up, the Battle Tag time will be set to 1 minute.