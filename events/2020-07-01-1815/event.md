---
title: 'Advanced Races'
titleoverride: true
date: '01-07-2020 18:15'
event:
    host: Geromu
    ip: 213.167.8.241
    category: unofficial
    mode: race
    class: other
    otherclass: 'Advanced Cars'
    classlink: 'https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0'
    tracklist: "The Bunker\r\nFools Mate 2\r\nCake\r\nBiohazard Factory\r\nPenny Racers - Caves\r\nImages of Giza\r\nToy World 1\r\nLunar\r\nToys in the Hood 2\r\nCactus Volt\r\nJailhouse Rock\r\nRooftops1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Advanced Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Content requirement:
* [RVON community track pack 20.2](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1) (needed)
* [RVON community car pack 20.2](https://www.dropbox.com/sh/7btoncnkdll3cnr/AAB0b6Q46sBggRt1eO52Bx4Ba?dl=1) (needed)
* [RVON car list](https://www.dropbox.com/s/an3chwat86z04aw/rvon%20car%20list%20%2820.2%29.txt?dl=0)