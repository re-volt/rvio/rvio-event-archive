---
title: 'Pro Races'
titleoverride: true
date: '22-05-2020 16:00'
event:
    host: BMMXIV
    ip: bmm.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "PetroVolt — 2 laps\r\nMetro-Volt — 3 laps\r\nToy World 2 — 5 laps\r\nToytanic 2 — 3 laps\r\nIndustry — 3 laps\r\nSmashride Circuit — 3 laps\r\nToy World 1 — 6 laps\r\nRadioactive Garden — 5 laps\r\nFiddlers on the Roof: Redux (R) — 3 laps\r\nMuseum 2 (R) — 4 laps\r\nHoliday Camp: California Edition (R) — 2 laps\r\nSantorini — 4 laps\r\nKadish Sprint — 2 laps\r\nSakura — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

