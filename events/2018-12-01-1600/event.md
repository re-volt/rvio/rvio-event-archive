---
title: 'AMW vs. Cougar'
titleoverride: false
date: '01-12-2018 16:00'
event:
    host: L!LMexican
    ip: mex.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'AMW vs. Cougar'
    classlink: 'https://re-volt.io/admin/pages/events/2018-12-01-1600'
    tracklist: 'See above'
    vod: 'https://www.youtube.com/watch?v=zvu6N-KO7ic'
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2018-12-01_17-26-03.csv'
media_order: poster_amwvscougar.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Welcome, ladies and gentlemen. I am here today to present to you this exciting sponsored event
by your well-known Re-Volt sponsors in a team-based showdown between the Underdogs of the
Professional class. AMW and Cougar, both are good at overtaking their competitors with top speed,
but will that be enough in this showdown? SHOW THESE DRIVERS WHAT YOU CAN DO!

![](poster_amwvscougar.png)

===

This event, as mentioned before will be team-based. The teams will be tried to split in half, but that's decided by the attendees. 
The team of each car will select a leader to lead the drivers to victory, preferably select a color, flags, and other additional accessories to 
create paintjobs around such. The drivers will then take turns setting best times around the selected courses, the two teams can talk about
whether they can qualify together, or as separate teams.

To be able to qualify, drivers will need to beat these following times on each course:

|  Track  |  Time  |
|  :-----          |  :-----          |
|  Alexandria |  00:54:000 |
|  Toy World Mayhem |  00:37:500 |
|  Museum 1 R  | 00:54:000 |
|  Lunar |  00:47:000 |
|  Toys In The Hood 2 |  00:44:000 |
|  Museum 2 M |  00:40:500 |
|  Brazil |  01:08:500 |
|  Human Center |  00:48:500 |
|  RV Temple |  01:27:500 |
|  Jailhouse Rock |  00:56:000 |
|  Botanical Garden | 00:27:000 |
|  Toy World 2 R |  00:33:000 |
|  Petro-Volt |  01:13:000 |
|  Moon Dawn RM |  01:54:000 |
|  Fools Mate 2 |  00:37:700 |
|  ReVol-ectrix |  00:36:000 |
|  Mikes Medieval Mayhem |  00:51:500 |
|  The Mines Of Alderon |  01:42:000 |
|  Supermarket 2 M |  00:21:500 |
|  Biohazard Factory R |  01:09:500 |
|  Toytanic 2 RM |  00:52:000 |

[**Download the track pack here**](https://1drv.ms/u/s!Ak9ucU6TQNYvljSUgdVOR8jp6UWe)

To clarify, a leader will be agreed on for **Team AMW**, and **Team Cougar**. Everybody that wants to join the event can DM me personally on [Discord](https://re-volt.io/discord/io) so it’s easier to keep track. The team leaders need to choose a color to have paintjobs around (e.g. blue for AMW). This means that every paintjob that involves a good amount of blue can be used to race in the team section, which adds personality to the mix. You can create skins, just make sure to send them to me or your team leader. This will be due at the same time the times will be due.

The event's time trial posts will be due on Friday at 14 UTC, which is the last day you can gain entry. You will only need to beat 5 of the 21 selected courses, so choose wisely. You do not need to choose one or the other during time trials but must set the time trials with either car. The teams will then be split for the event.

This will be a no-pickups competition as well and we will race 4 laps per track. May the better drivers win!
I recommend that whatever car you use as a time trial competitor, you also use it as your go-to team car.

—L!LMexican