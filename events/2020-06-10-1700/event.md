---
title: 'Long Journey 4: Day 2'
titleoverride: true
date: '10-06-2020 17:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/cars/pro'
    packages:
        - io_cars
        - io_cars_bonus
        - io_skins
        - io_tracks
        - io_tracks_bonus
    tracklist: "Harbor Lights\r\nRV Temple\r\nPenny Racers - Harbour\r\nLunar\r\nCake\r\nThe Gorge\r\nSantorini\r\nBelgium\r\nSuperMarket 2\r\nToytanic 1\r\nMK64 - Wario Stadium\r\nSwan Street\r\nSnowland 1\r\nPalm Marsh\r\nPetroVolt\r\nEndgame\r\nRooftop Chase: Redux\r\nSunset Hills\r\nHoliday Camp\r\nThe Bunker\r\nGhost Town 2\r\nMoon Dawn"
    vod: 'https://www.youtube.com/watch?v=-1Pe5k9MRV8'
    results: 'https://online.re-volt.io/sessions/results.php?file=main/session_2020-06-10_19-00-04.csv'
media_order: LongJourney4.png
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Long Journey 4: Day 2'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

![](http://re-volt.io/user/pages/events/2020-06-10-1700/LongJourney4.png)