---
title: Slugs
titleoverride: false
date: '09-10-2018 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Swan Street (R)\r\nRooftops\r\nFool's Mate 2\r\nLunar\r\nPenny Racers - Caves\r\nQuake! (R)\r\nToy World Aquatica: Redux\r\nCliffside\r\nJailhouse Rock\r\nSnowy River\r\nSuperMarket 1 (R)\r\nToy World Mayhem\r\nGhost Town 2\r\nGhost Town 1"
    vod: 'https://www.youtube.com/watch?v=IhqyP-3H2yw'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-10-09_21-03-07.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

