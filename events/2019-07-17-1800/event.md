---
title: 'Long Journey: 2nd Day'
titleoverride: false
date: '17-07-2019 18:00'
event:
    host: OhNej
    ip: ohnej.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Long Journey: 2nd Day'
    classlink: 'https://re-volt.io/events/2019-07-17-1800'
    tracklist: "Toy World Aquatica: Redux\r\nSakura\r\nLunar\r\nThe Felling Yard\r\nSkating Toys (RM)\r\nRe-Ville\r\nRadioactive Garden (R)\r\nQuake!\r\nWhite Rose Chapel\r\nPenny Racers - Harbour\r\nSnowy River\r\nVenice\r\nFool's Mate 2\r\nIndustry\r\nRanch (M)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**A special 4-day event hosted by OhNej in celebration of Re-Volt's 20th Anniversary.**<br>
We will be racing with [Pro Cars](https://re-volt.io/online/car-classes) on Arcade mode, with pick-ups enabled and 3 laps on each track.

![](https://re-volt.io/user/pages/events/2019-07-15-1800/GoingHome.png)