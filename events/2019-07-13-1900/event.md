---
title: 'Gold Cup'
titleoverride: false
date: '13-07-2019 19:15'
event:
    host: Geromu
    ip: ger.rv.gl
    category: tourney
    mode: race
    class: other
    otherclass: 'Gold Cup'
    classlink: 'https://re-volt.io/blog/online-championship-mode-tournament'
    tracklist: "Toys in the Hood 2 (5 laps)\r\nToy World 1 M (8 laps)\r\nToytanic 1 (5 laps)\r\nMuseum 1 (5 laps) "
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

**Signing up is required for participation in this event.** [More information here.](https://re-volt.io/blog/online-championship-mode-tournament)