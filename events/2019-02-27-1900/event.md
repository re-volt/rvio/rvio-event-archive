---
title: 'Phat Slug Exclusive'
titleoverride: false
date: '27-02-2019 19:00'
event:
    host: Kiwi
    ip: kiwi.rv.gl
    category: competitive
    mode: race
    class: other
    otherclass: 'Phat Slug Exclusive'
    classlink: 'https://revolt.fandom.com/wiki/Phat_Slug'
    tracklist: 'Random Tracks'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Visit **[RVGL Ladder](https://ladder.rv.gl/)** for more information!

**Links**:
* [Track Selection](https://ladder.rv.gl/info/#events-general-tracks)
* [Car Selection](https://ladder.rv.gl/info/#events-general-cars)
* [Ranking](https://ladder.rv.gl/info/#events-general-ranking)