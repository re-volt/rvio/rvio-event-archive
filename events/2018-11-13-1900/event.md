---
title: Slugs
titleoverride: false
date: '13-11-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: slugs
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Ghost Town 1 (R)\r\nQuake! (R)\r\nSantorini\r\nMuseum 2\r\nPetroVolt\r\nMetro-Volt\r\nThe Bunker\r\nMK64 Wario Stadium\r\nRe-Ville\r\nVenice\r\nGhost Town 2\r\nSkating Toys\r\nRooftops\r\nRooftop Chase (R)"
    vod: 'https://www.youtube.com/watch?v=wmWtK6GiRPc'
    results: 'https://online.re-volt.io/sessions/results.php?file=casual/session_2018-11-13_21-03-27.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

