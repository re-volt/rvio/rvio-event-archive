---
title: 'Pro Races'
titleoverride: false
date: '12-01-2019 19:00'
event:
    host: Wichilie
    ip: w.rv.gl
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World 2\r\nToytanic 2 (R)\r\nVenice\r\nHoliday Camp: California Edition\r\nRanch (R)\r\nBlood on the Rooftops\r\nRooftop Chase\r\nPalm Marsh (R)\r\nGround N Smash 2\r\nGhost Town 2\r\nKadish Sprint\r\nThe Felling Yard\r\nCliffside\r\nLunar\r\nRadioactive Garden\r\nBotanical Garden"
    stream: 'http://twitch.tv/rvio'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

