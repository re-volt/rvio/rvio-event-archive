---
title: 'Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '19-11-2019 19:00'
event:
    host: 'URV & Narusori'
    ip: 'u.rv.gl OR sas.rv.gl'
    category: casual
    mode: race
    class: pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toy World Aquatica: Redux\r\nRadioactive Garden\r\nBiohazard Factory\r\nToytanic 1\r\nSupermarket 1\r\nGhost Town 2\r\nMetro-Volt\r\nIndustry\r\nGrisville\r\nGhost Town 1 (R)\r\nLunar\r\nBlood on the Rooftops\r\nHoliday Camp: California Edition\r\nSakura (R)\r\nMoon Dawn\r\nIllusion (R) (M)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

