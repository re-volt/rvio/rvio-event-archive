---
title: 'Rookie Races'
titleoverride: false
date: '28-05-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: rookie
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "SuperMarket 1\r\nToytanic 2 RM\r\nOverground\r\nToy World Mayhem\r\nHelios\r\nGhost Town 2\r\nRooftops R\r\nBiohazard Factory\r\nPetroVolt R\r\nCliffside\r\nIllusion\r\nSantorini\r\nHoliday Camp: California Edition\r\nFiddlers on the Roof"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

