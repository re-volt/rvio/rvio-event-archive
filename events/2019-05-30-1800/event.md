---
title: 'Amateur Races'
titleoverride: false
date: '30-05-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: amateur
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Jailhouse Rock\r\nBotanical Garden\r\nToys in the Hood 2 RM\r\nGhost Town 2 R\r\nSnowy River\r\nSpa-Volt 1 R\r\nSuperMarket 2\r\nVenice\r\nFool's Mate 2\r\nYABBA DABBA DOO!\r\nRanch\r\nSkating Toys\r\nSantorini\r\nSnowland 1"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

