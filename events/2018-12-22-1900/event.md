---
title: 'Christmas Carnival (Day 1)'
titleoverride: false
date: '22-12-2018 19:00'
event:
    host: URV
    ip: u.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Christmas Carnival (Day 1)'
    classlink: 'https://re-volt.io/blog/christmas-carnival-2018'
    tracklist: "Penny Racers - Caves\r\nThe Glaciers\r\nCandyland\r\nR2049 Tundra\r\nChristmas Special Stage\r\nWinter fest\r\nChristmas Lights\r\nFrostyZ Rally\r\nRally ZX SS 2\r\nIcy Canyon\r\nSnow Pass\r\nWinter Park\r\nWicked Winter\r\n0 Degrees\r\nZero Degrees\r\nChilled To The Bone"
    vod: 'https://www.youtube.com/watch?v=5XLlk9rc0wA'
    results: 'https://online.re-volt.io/sessions/results.php?file=special/session_2018-12-22_21-01-28.csv'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

