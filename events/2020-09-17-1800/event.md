---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '17-09-2020 18:00'
event:
    host: 'Kirioso & Etneus'
    ip: 'kiri.rv.gl OR etn.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    packages:
        - io_cars
        - io_tracks
    tracklist: "ToySoldierz — 4 laps\r\nSantorini — 4 laps\r\nHull Breach 3000 — 4 laps\r\nSpa-Volt 1 (R) — 3 laps\r\nSupermarket 1 — 4 laps\r\nKadish Sprint — 2 laps\r\nVenice — 4 laps\r\nLunar — 3 laps\r\nRooftops 1 (R) — 3 laps\r\nMolten Caverns — 2 laps\r\nSnowy River — 4 laps\r\nBotanical Garden — 6 laps\r\nGhost Town 2 — 5 laps\r\nToytanic 1 (R) — 3 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Super Pro Races'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

