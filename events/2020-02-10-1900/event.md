---
title: 'Amateur Knockout Session'
titleoverride: true
date: '10-02-2020 19:00'
event:
    host: Whitedoom
    ip: wd.rv.gl
    category: other
    mode: race
    class: other
    otherclass: 'Amateur Cars'
    classlink: 'https://re-volt.io/events/2020-02-10-1900'
    tracklist: Random
    stream: 'https://www.twitch.tv/ohnej6'
    vod: 'https://www.youtube.com/watch?v=Rg62pzRHLCI'
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Amateur Knockout Session'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

### Requirements
* [Track Collection](https://distribute.re-volt.io/packs/io_tracks.zip) (needed)
* [Bonus Tracks](https://distribute.re-volt.io/packs/io_tracks_bonus.zip) (needed)
* [Extra Old Io Tracks](https://www.dropbox.com/sh/qbv6h75oox8p4ck/AAAZT1z5BOxIN7Rs585Q5cHsa?dl=1) (needed)
* [Car Collection](https://distribute.re-volt.io/packs/io_cars.zip) (advised)
* [Bonus Cars](https://distribute.re-volt.io/packs/io_cars_bonus.zip) (advised)
* [Extra Old Io Cars](https://www.dropbox.com/sh/qqkplevr9df6l3a/AACsyvafB1QkhnU_GvtULqFJa?dl=1) (advised)

### Car Selection
* Any [stock](https://revolt.fandom.com/wiki/Category:Standard_Cars) or [DC cars](https://revolt.fandom.com/wiki/Category:Console_Cars) rated in-game as Amateur or below
* Any [custom content included in the I/O packs](https://re-volt.io/downloads/packs) that are rated in-game as Amateur or below
* Any [cars included in the I/O selection](https://re-volt.io/online/cars) that are classified on the website as Amateur or below
* Any [cars included in the old I/O car pack](https://www.dropbox.com/s/qfy5bzs1uibqair/Old%20io%20car%20list.txt?dl=0) that are rated in-game as Amateur or below
* Nano Rascall and Vixen are excluded from this session!