---
title: 'Advanced Races'
titleoverride: false
date: '18:00 16-04-2019'
event:
    host: Floxit
    ip: flo.rv.gl
    category: casual
    mode: race
    class: advanced
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Cliffside\r\nSpa-Volt 1 (R)\r\nSantorini\r\nToytanic 2 \r\nFool's Mate 2\r\nHelios\r\nBiohazard Factory\r\nGhost Town 1 (R)\r\nSnowland 1\r\nVenice\r\nAMCO TT\r\nRooftops\r\nMetro-Volt\r\nToy World 1 \r\nToySoldierz (R)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

