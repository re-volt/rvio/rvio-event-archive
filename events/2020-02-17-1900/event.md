---
title: 'Random Cars'
titleoverride: true
date: '17-02-2020 19:00'
event:
    host: Saffron
    ip: saff.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Random Cars'
    classlink: 'https://re-volt.io/online/cars'
    tracklist: "School's Out! - 2 laps\r\nFiddlers on the Roof: Redux - 3 laps\r\nMoon Dawn - 2 laps\r\nRooftop Chase: Redux R - 3 laps\r\nKadish Sprint - 2 laps\r\nBotanical Garden - 6 laps\r\nSakura - 3 laps\r\nToy World 2 M - 5 laps\r\nToys in the Hood 2 - 4 laps\r\nSantorini - 4 laps\r\nSupermarket 1 R - 4 laps\r\nVenice - 3 laps\r\nJailhouse Rock - 3 laps\r\nSchool's Out! R - 2 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

