---
title: 'Drift Races'
titleoverride: false
date: '24-01-2020 19:00'
event:
    host: Saffron
    ip: saff.rv.gl
    category: special
    mode: race
    class: other
    otherclass: 'Drift Cars'
    classlink: 'https://re-volt.io/online/cars/more'
    tracklist: "Touge Mountain - Stage 2\r\nEggland\r\nSideways Sanctuary A\r\nAMCO Bitume\r\nSideways Sanctuary B\r\nAMCO Driftume\r\nSideways Sanctuary C\r\nMelville\r\nSideways Sanctuary D\r\nTVGP Bitume\r\nRV-Simo On-Road Track\r\nFoxglen\r\nBerm Lake Raceway\r\nTouge Mountain R"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

### Requirements
* [Drift Cars](https://files.re-volt.io/packs/drift_cars.7z)
* [Circuit Tracks](https://files.re-volt.io/packs/circuit_tracks.zip)