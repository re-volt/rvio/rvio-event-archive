---
title: 'Staged Knockout'
titleoverride: true
date: '03-07-2020 20:15'
event:
    host: Geromu
    ip: 213.167.8.241
    category: casual
    mode: race
    class: other
    tracklist: "Chilled to the Bone\r\nRanch\r\nSkating Toys\r\nSuperMarket 1\r\nMuseum 2\r\nBelgium\r\nL.A. Smash\r\nBroken Sunlight\r\nDonut Plains 3\r\nMKDD - Mushroom City\r\nToy World 2\r\nCake\r\nPalm Marsh\r\nComputer Virus 2\r\nHallows Eve\r\nHelios\r\nHoliday Camp: California Edition\r\nWipeOut\r\nIllusion\r\nSantorini\r\nGhost Town 1\r\nToy World EX\r\nToy World 1\r\nBiohazard Factory"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Staged Knockout'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Staged Knockout - All players start with a stock Pro of their choice. The winner swaps to a Semi-Pro, then to Advanced, then to Amateur, then to Rookie. The first player to win with a rookie - wins the event.