---
title: 'Semi-Pro Races'
titleoverride: false
date: '09-07-2019 18:00'
event:
    host: URV
    ip: u.rv.gl
    category: casual
    mode: race
    class: semi-pro
    classlink: 'https://re-volt.io/online/car-classes'
    tracklist: "Toytanic 2\r\nIllusion (R)\r\nRanch (RM)\r\nAMCO TT\r\nJailhouse Rock\r\nToy World Aquatica: Redux\r\nGhost Town 1 (R)\r\nLunar\r\nMuseum 1\r\nSuperMarket 1\r\nVenice\r\nToySoldierz\r\nYABBA DABBA DOO! \r\nThe Bunker\r\nWhite Rose Chapel\r\nSnowland 1 (Bonus Race, Unranked)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

