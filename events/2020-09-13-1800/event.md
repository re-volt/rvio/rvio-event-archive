---
title: 'Eatium Exclusive'
titleoverride: true
date: '13-09-2020 18:00'
event:
    host: Hajducsekb
    ip: hb.rv.gl
    category: unofficial
    mode: race
    class: other
    otherclass: Eatium
    classlink: 'http://revoltzone.net/cars/28649/Eatium'
    tracklist: "Museum 3 (8 Laps)\r\nDrivers School (8 Laps)\r\nToys in the Hood 1 (8 Laps)\r\nIllusion (7 Laps)\r\nDonut Plains 3 (12 Laps)\r\nHoliday Camp (5 Laps)\r\nSynthwave (10 Laps)\r\nToy World Mayhem (10 Laps)\r\nVenice (8 Laps)\r\nToy World 1 (15 Laps)"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
googletitle: 'Eatium Exclusive'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

## Rules:
- Car: Eatium
- Pick-Ups: Off!
- Laps: Variable
- Collision: Arcade

## Downloads:
- [Re-Volt Online Trackpack](https://www.dropbox.com/sh/1qpgkrayc1oatgv/AADGbw3VIQWwpxICzN5Ko5dha?dl=1)
- [Eatium](http://revoltzone.net/cars/28649/Eatium)