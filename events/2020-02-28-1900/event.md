---
title: 'Super Pro Races <small>(Dual Lobby)</small>'
titleoverride: true
date: '28-02-2020 19:00'
event:
    host: 'Saffron & Delecto'
    ip: 'saff.rv.gl OR dele.rv.gl'
    category: casual
    mode: race
    class: other
    otherclass: 'Super Pro Cars'
    classlink: 'https://re-volt.io/online/cars/superpro'
    tracklist: "Toys in the Hood 1 - 4 laps\r\nVenice - 4 laps\r\nBotanical Garden RM - 6 laps\r\nSchool's Out! R - 2 laps\r\nGhost Town 1 - 7 laps\r\nMetro-Volt - 3 laps\r\nBlood on the Rooftops - 3 laps\r\nMolten Caverns - 2 laps\r\nDonut Plains 3 - 5 laps\r\nGhost Town 2 - 5 laps\r\nRV Temple - 2 laps \r\nRooftop Chase: Redux R - 3 laps\r\nOverground - 3 laps\r\nToy World Mayhem - 4 laps"
sidebarlayout: global
sidebarpath: /sidebar/events
showcase: '0'
toc:
    enabled: '0'
    horizontal: '0'
twitterenable: true
twittercardoptions: summary
articleenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
facebookenable: true
---

Cambold R, Commandine and Saeger is not allowed in this lobby.